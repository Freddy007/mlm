-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 05, 2017 at 10:26 PM
-- Server version: 10.1.24-MariaDB-cll-lve
-- PHP Version: 5.6.30

/* SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO"; */
time_zone := "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wwwfpvzk_nairapalclub`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE SEQUENCE admin_seq;

CREATE TABLE IF NOT EXISTS admin (
  id int NOT NULL DEFAULT NEXTVAL ('admin_seq'),
  sitelogo varchar(255) DEFAULT NULL,
  sitelogolight varchar(255) DEFAULT NULL,
  sitesmallicon varchar(255) DEFAULT NULL,
  defaultmaleavatar varchar(255) DEFAULT NULL,
  envato_purchasecode varchar(255) NOT NULL,
  sitename varchar(255) NOT NULL,
  siteurl varchar(255) NOT NULL,
  sitemail varchar(255) NOT NULL,
  appcurrency varchar(255) NOT NULL,
  appcurrencysymbol varchar(255) NOT NULL,
  appcurrencyexchangerate int NOT NULL,
  title varchar(255) DEFAULT NULL,
  message longtext,
  match_mode varchar(255) DEFAULT NULL,
  app_mode varchar(255) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE admin_seq RESTART WITH 2;

--
-- Dumping data for table `admin`
--

INSERT INTO admin (id, sitelogo, sitelogolight, sitesmallicon, defaultmaleavatar, envato_purchasecode, sitename, siteurl, sitemail, appcurrency, appcurrencysymbol, appcurrencyexchangerate, title, message, match_mode, app_mode) VALUES
(1, '/uploads/logo/Nairapal/762f078afe65b878d03d2afc1fa24f5a.png', '/uploads/logo/Nairapal/4e6735e2168efb4dc2a600b0846f6588.png', '/uploads/logo/Nairapal/bcd91492e682d153c8675c673868fcef.png', '', 'manualpeerupinstall', 'Nairapal', 'http://www.nairapal.club', 'support@nairapal.club', 'GPS', '₱', 65, 'ADDITIONAL PACK HAS BEEN ADDED', '<p>rnrn</p><div><p>Thanks for the use of the day</p></div><p></p>', 'automatic', 'unverified');

-- --------------------------------------------------------

--
-- Table structure for table `feeder`
--

CREATE SEQUENCE feeder_seq;

CREATE TABLE IF NOT EXISTS feeder (
  id int NOT NULL DEFAULT NEXTVAL ('feeder_seq'),
  feeder int DEFAULT NULL,
  receiver int DEFAULT NULL,
  pack int DEFAULT NULL,
  feeder_pack_subscription int DEFAULT NULL,
  receiver_pack_subscription int DEFAULT NULL,
  feeder_time_start timestamp(0) NOT NULL,
  feeder_time_end timestamp(0) NOT NULL,
  feeder_time_extended varchar(10) DEFAULT NULL,
  payment_method varchar(255) DEFAULT NULL,
  payment_bank_name varchar(255) DEFAULT NULL,
  payment_account_number varchar(255) DEFAULT NULL,
  payment_account_name varchar(255) DEFAULT NULL,
  payment_depositor_name varchar(255) DEFAULT NULL,
  payment_location varchar(255) DEFAULT NULL,
  payment_uploaded_file varchar(255) DEFAULT NULL,
  receiver_time_start timestamp(0) DEFAULT NULL,
  receiver_time_end timestamp(0) DEFAULT NULL,
  status varchar(20) NOT NULL,
  scam_paid varchar(255) DEFAULT NULL,
  jury int DEFAULT NULL,
  created timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE feeder_seq RESTART WITH 101;

CREATE INDEX IDX_DB1A5EE6DB1A5EE6 ON feeder (feeder);
CREATE INDEX IDX_DB1A5EE63DB88C96 ON feeder (receiver);
CREATE INDEX IDX_DB1A5EE697DE5E23 ON feeder (pack);
CREATE INDEX IDX_DB1A5EE6D009EE10 ON feeder (feeder_pack_subscription);
CREATE INDEX IDX_DB1A5EE6DFE70FBE ON feeder (receiver_pack_subscription);

-- --------------------------------------------------------

--
-- Table structure for table `jury`
--

CREATE SEQUENCE jury_seq;

CREATE TABLE IF NOT EXISTS jury (
  id int NOT NULL DEFAULT NEXTVAL ('jury_seq'),
  feeder_id int NOT NULL,
  receiver_id int NOT NULL,
  feeder_entry_id int NOT NULL,
  pack_subscription_id int NOT NULL,
  pack_id int NOT NULL,
  verdict longtext NOT NULL,
  status varchar(40) NOT NULL,
  created_date timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE jury_seq RESTART WITH 3;

-- --------------------------------------------------------

--
-- Table structure for table `jury_message`
--

CREATE SEQUENCE jury_message_seq;

CREATE TABLE IF NOT EXISTS jury_message (
  id int NOT NULL DEFAULT NEXTVAL ('jury_message_seq'),
  jury_id int NOT NULL,
  sender_id int NOT NULL,
  sender_type varchar(40) NOT NULL,
  message longtext NOT NULL,
  attachment varchar(255) DEFAULT NULL,
  created_date timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)    ;

ALTER SEQUENCE jury_message_seq RESTART WITH 1;

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

CREATE SEQUENCE mail_seq;

CREATE TABLE IF NOT EXISTS mail (
  id int NOT NULL DEFAULT NEXTVAL ('mail_seq'),
  subject varchar(255) NOT NULL,
  mail longtext NOT NULL,
  created timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE mail_seq RESTART WITH 7;

-- --------------------------------------------------------

--
-- Table structure for table `mail_recipient`
--

CREATE SEQUENCE mail_recipient_seq;

CREATE TABLE IF NOT EXISTS mail_recipient (
  id int NOT NULL DEFAULT NEXTVAL ('mail_recipient_seq'),
  mail int DEFAULT NULL,
  user int DEFAULT NULL,
  status varchar(255) NOT NULL,
  created timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE mail_recipient_seq RESTART WITH 59;

CREATE INDEX IDX_10DEE52A5126AC48 ON mail_recipient (mail);
CREATE INDEX IDX_10DEE52A8D93D649 ON mail_recipient (user);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE SEQUENCE news_seq;

CREATE TABLE IF NOT EXISTS news (
  id int NOT NULL DEFAULT NEXTVAL ('news_seq'),
  title varchar(255) NOT NULL,
  article longtext NOT NULL,
  image varchar(255) NOT NULL,
  created timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE news_seq RESTART WITH 4;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE SEQUENCE notification_seq;

CREATE TABLE IF NOT EXISTS notification (
  id int NOT NULL DEFAULT NEXTVAL ('notification_seq'),
  type int NOT NULL,
  subject int NOT NULL,
  object int DEFAULT NULL,
  details varchar(255) NOT NULL,
  status varchar(255) NOT NULL DEFAULT 'unread',
  created timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE notification_seq RESTART WITH 536;

-- --------------------------------------------------------

--
-- Table structure for table `pack`
--

CREATE SEQUENCE pack_seq;

CREATE TABLE IF NOT EXISTS pack (
  id int NOT NULL DEFAULT NEXTVAL ('pack_seq'),
  name varchar(255) NOT NULL,
  amount int NOT NULL,
  feeders int NOT NULL,
  created timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE pack_seq RESTART WITH 62;

--
-- Dumping data for table `pack`
--

INSERT INTO pack (id, name, amount, feeders, created) VALUES
(57, 'Standard Peerup Pack', 1000, 2, '2017-12-02 09:52:23');

-- --------------------------------------------------------

--
-- Table structure for table `pack_subscription`
--

CREATE SEQUENCE pack_subscription_seq;

CREATE TABLE IF NOT EXISTS pack_subscription (
  id int NOT NULL DEFAULT NEXTVAL ('pack_subscription_seq'),
  pack int DEFAULT NULL,
  user int DEFAULT NULL,
  auto_recirculate int DEFAULT NULL,
  user_type varchar(10) NOT NULL,
  feeder_counter int NOT NULL,
  feeder_total int NOT NULL,
  book_cancel varchar(255) DEFAULT NULL,
  admin_track int DEFAULT NULL,
  admin int DEFAULT NULL,
  admin_track_status varchar(15) DEFAULT NULL,
  admin_track_limit int NOT NULL,
  status varchar(10) NOT NULL,
  type varchar(10) NOT NULL,
  created timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE pack_subscription_seq RESTART WITH 324;

CREATE INDEX IDX_5C3DF87E97DE5E23 ON pack_subscription (pack);
CREATE INDEX IDX_5C3DF87E8D93D649 ON pack_subscription (user);
CREATE INDEX IDX_5C3DF87E6BE7F41B ON pack_subscription (auto_recirculate);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE SEQUENCE review_seq;

CREATE TABLE IF NOT EXISTS review (
  id int NOT NULL DEFAULT NEXTVAL ('review_seq'),
  user_id int NOT NULL,
  testimony varchar(150) NOT NULL,
  status varchar(10) DEFAULT NULL,
  created_date timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE review_seq RESTART WITH 2;

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE SEQUENCE support_seq;

CREATE TABLE IF NOT EXISTS support (
  id int NOT NULL DEFAULT NEXTVAL ('support_seq'),
  user_id int NOT NULL,
  subject varchar(255) NOT NULL,
  status varchar(40) NOT NULL,
  created_date timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE support_seq RESTART WITH 17;

-- --------------------------------------------------------

--
-- Table structure for table `support_message`
--

CREATE SEQUENCE support_message_seq;

CREATE TABLE IF NOT EXISTS support_message (
  id int NOT NULL DEFAULT NEXTVAL ('support_message_seq'),
  support_id int NOT NULL,
  sender varchar(40) NOT NULL,
  message longtext NOT NULL,
  attachment varchar(255) DEFAULT NULL,
  created_date timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE support_message_seq RESTART WITH 19;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE SEQUENCE transaction_seq;

CREATE TABLE IF NOT EXISTS transaction (
  id int NOT NULL DEFAULT NEXTVAL ('transaction_seq'),
  pack int DEFAULT NULL,
  feeder_pack_subscription int DEFAULT NULL,
  feeder int DEFAULT NULL,
  receiver int DEFAULT NULL,
  amount int NOT NULL,
  receiver_share int NOT NULL,
  status varchar(40) NOT NULL,
  created timestamp(0) NOT NULL,
  PRIMARY KEY (id)
)     ;

ALTER SEQUENCE transaction_seq RESTART WITH 9;

CREATE INDEX IDX_723705D197DE5E23 ON transaction (pack);
CREATE INDEX IDX_723705D1D009EE10 ON transaction (feeder_pack_subscription);
CREATE INDEX IDX_723705D1DB1A5EE6 ON transaction (feeder);
CREATE INDEX IDX_723705D13DB88C96 ON transaction (receiver);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE SEQUENCE user_seq;

CREATE TABLE IF NOT EXISTS user (
  id int NOT NULL DEFAULT NEXTVAL ('user_seq'),
  username varchar(40) NOT NULL,
  title varchar(40) NOT NULL,
  name varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  bitcoin_address varchar(255) DEFAULT NULL,
  password varchar(40) NOT NULL,
  sex varchar(6) NOT NULL,
  avatar varchar(255) DEFAULT NULL,
  phone_one varchar(19) NOT NULL,
  phone_two varchar(19) DEFAULT NULL,
  resident_country varchar(80) NOT NULL,
  resident_state varchar(40) NOT NULL,
  bank_name varchar(255) DEFAULT NULL,
  bank_choice varchar(255) DEFAULT NULL,
  bank_account_name varchar(255) DEFAULT NULL,
  bank_name_two varchar(255) DEFAULT NULL,
  bank_name_one varchar(255) DEFAULT NULL,
  account_name_one varchar(255) DEFAULT NULL,
  account_name_two varchar(255) DEFAULT NULL,
  bank_account_number varchar(40) DEFAULT NULL,
  account_number_one varchar(255) DEFAULT NULL,
  account_number_two varchar(255) DEFAULT NULL,
  bank_account_type varchar(40) DEFAULT NULL,
  agency_one varchar(255) DEFAULT NULL,
  agency_two varchar(100) DEFAULT NULL,
  operation_one varchar(255) DEFAULT NULL,
  operation_two varchar(255) DEFAULT NULL,
  cpf_one varchar(255) DEFAULT NULL,
  cpf_two varchar(255) DEFAULT NULL,
  account_type_one varchar(255) DEFAULT NULL,
  account_type_two varchar(255) DEFAULT NULL,
  purse int NOT NULL,
  status varchar(40) NOT NULL,
  role varchar(40) NOT NULL,
  referrer int DEFAULT NULL,
  created timestamp(0) NOT NULL,
  activation_key varchar(40) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT UNIQ_8D93D649F85E0677 UNIQUE  (username),
  CONSTRAINT UNIQ_8D93D649E7927C74 UNIQUE  (email)
)     ;

ALTER SEQUENCE user_seq RESTART WITH 265;

CREATE INDEX avatar ON user (avatar);

--
-- Dumping data for table `user`
--

INSERT INTO user (id, username, title, name, email, bitcoin_address, password, sex, avatar, phone_one, phone_two, resident_country, resident_state, bank_name, bank_choice, bank_account_name, bank_name_two, bank_name_one, account_name_one, account_name_two, bank_account_number, account_number_one, account_number_two, bank_account_type, agency_one, agency_two, operation_one, operation_two, cpf_one, cpf_two, account_type_one, account_type_two, purse, status, role, referrer, created, activation_key) VALUES
(1, 'admin', 'Mr.', 'Administrator', 'admin@site.com', NULL, 'i3yjWBTyaQlzektetaOMCJIHbHw=', 'Male', NULL, '08039540050', '08057430898', '', 'Anabra', 'First Bank Nigeria', '2', 'Oladeji  David', 'First Bank Nigeria', 'GTBANK', 'OLADEJI David Oluwayanmi', 'Oladeji  David', '0013333', '0013867176', '0013333', 'Savings', '', '', '', '', '', '', 'Savings', 'Savings', 0, 'active', 'ROLE_SUPER_ADMIN', NULL, '2017-05-26 12:47:09', 'activated'),
(208, 'testuser', 'Mr.', 'Test user', 'testuser@gmaal.com', '1dfghjkl;klgyuhgfdfijhyugfv@gmail.com', 'i3yjWBTyaQlzektetaOMCJIHbHw=', 'male', NULL, '09876544567', NULL, 'GB', 'Kent', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'active', 'ROLE_USER', NULL, '2017-11-10 10:23:37', 'activated');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `feeder`
--
ALTER TABLE feeder
  ADD CONSTRAINT FK_DB1A5EE63DB88C96 FOREIGN KEY (receiver) REFERENCES user (id),
  ADD CONSTRAINT FK_DB1A5EE697DE5E23 FOREIGN KEY (pack) REFERENCES `pack` (id),
  ADD CONSTRAINT FK_DB1A5EE6D009EE10 FOREIGN KEY (feeder_pack_subscription) REFERENCES `pack_subscription` (id),
  ADD CONSTRAINT FK_DB1A5EE6DB1A5EE6 FOREIGN KEY (feeder) REFERENCES `user` (id),
  ADD CONSTRAINT FK_DB1A5EE6DFE70FBE FOREIGN KEY (receiver_pack_subscription) REFERENCES `pack_subscription` (id);

--
-- Constraints for table `mail_recipient`
--
ALTER TABLE mail_recipient
  ADD CONSTRAINT FK_10DEE52A5126AC48 FOREIGN KEY (mail) REFERENCES mail (id),
  ADD CONSTRAINT FK_10DEE52A8D93D649 FOREIGN KEY (user) REFERENCES `user` (id);

--
-- Constraints for table `pack_subscription`
--
ALTER TABLE pack_subscription
  ADD CONSTRAINT FK_5C3DF87E6BE7F41B FOREIGN KEY (auto_recirculate) REFERENCES pack (id),
  ADD CONSTRAINT FK_5C3DF87E8D93D649 FOREIGN KEY (user) REFERENCES `user` (id),
  ADD CONSTRAINT FK_5C3DF87E97DE5E23 FOREIGN KEY (pack) REFERENCES `pack` (id);

--
-- Constraints for table `transaction`
--
ALTER TABLE transaction
  ADD CONSTRAINT FK_723705D13DB88C96 FOREIGN KEY (receiver) REFERENCES user (id),
  ADD CONSTRAINT FK_723705D197DE5E23 FOREIGN KEY (pack) REFERENCES `pack` (id),
  ADD CONSTRAINT FK_723705D1D009EE10 FOREIGN KEY (feeder_pack_subscription) REFERENCES `pack_subscription` (id),
  ADD CONSTRAINT FK_723705D1DB1A5EE6 FOREIGN KEY (feeder) REFERENCES `user` (id);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
