-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 05, 2017 at 10:26 PM
-- Server version: 10.1.24-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wwwfpvzk_nairapalclub`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitelogo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sitelogolight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sitesmallicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defaultmaleavatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `envato_purchasecode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sitename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `siteurl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sitemail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `appcurrency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `appcurrencysymbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `appcurrencyexchangerate` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `match_mode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_mode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `sitelogo`, `sitelogolight`, `sitesmallicon`, `defaultmaleavatar`, `envato_purchasecode`, `sitename`, `siteurl`, `sitemail`, `appcurrency`, `appcurrencysymbol`, `appcurrencyexchangerate`, `title`, `message`, `match_mode`, `app_mode`) VALUES
(1, '/uploads/logo/Nairapal/762f078afe65b878d03d2afc1fa24f5a.png', '/uploads/logo/Nairapal/4e6735e2168efb4dc2a600b0846f6588.png', '/uploads/logo/Nairapal/bcd91492e682d153c8675c673868fcef.png', '', 'manualpeerupinstall', 'Nairapal', 'http://www.nairapal.club', 'support@nairapal.club', 'GPS', '₱', 65, 'ADDITIONAL PACK HAS BEEN ADDED', '<p>\r\n\r\n</p><div><p>Thanks for the use of the day</p></div><p></p>', 'automatic', 'unverified');

-- --------------------------------------------------------

--
-- Table structure for table `feeder`
--

CREATE TABLE IF NOT EXISTS `feeder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feeder` int(11) DEFAULT NULL,
  `receiver` int(11) DEFAULT NULL,
  `pack` int(11) DEFAULT NULL,
  `feeder_pack_subscription` int(11) DEFAULT NULL,
  `receiver_pack_subscription` int(11) DEFAULT NULL,
  `feeder_time_start` datetime NOT NULL,
  `feeder_time_end` datetime NOT NULL,
  `feeder_time_extended` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_depositor_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_uploaded_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver_time_start` datetime DEFAULT NULL,
  `receiver_time_end` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `scam_paid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jury` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DB1A5EE6DB1A5EE6` (`feeder`),
  KEY `IDX_DB1A5EE63DB88C96` (`receiver`),
  KEY `IDX_DB1A5EE697DE5E23` (`pack`),
  KEY `IDX_DB1A5EE6D009EE10` (`feeder_pack_subscription`),
  KEY `IDX_DB1A5EE6DFE70FBE` (`receiver_pack_subscription`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=101 ;

-- --------------------------------------------------------

--
-- Table structure for table `jury`
--

CREATE TABLE IF NOT EXISTS `jury` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feeder_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `feeder_entry_id` int(11) NOT NULL,
  `pack_subscription_id` int(11) NOT NULL,
  `pack_id` int(11) NOT NULL,
  `verdict` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `jury_message`
--

CREATE TABLE IF NOT EXISTS `jury_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jury_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `sender_type` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

CREATE TABLE IF NOT EXISTS `mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `mail_recipient`
--

CREATE TABLE IF NOT EXISTS `mail_recipient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_10DEE52A5126AC48` (`mail`),
  KEY `IDX_10DEE52A8D93D649` (`user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `object` int(11) DEFAULT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unread',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=536 ;

-- --------------------------------------------------------

--
-- Table structure for table `pack`
--

CREATE TABLE IF NOT EXISTS `pack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `feeders` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=62 ;

--
-- Dumping data for table `pack`
--

INSERT INTO `pack` (`id`, `name`, `amount`, `feeders`, `created`) VALUES
(57, 'Standard Peerup Pack', 1000, 2, '2017-12-02 09:52:23');

-- --------------------------------------------------------

--
-- Table structure for table `pack_subscription`
--

CREATE TABLE IF NOT EXISTS `pack_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pack` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `auto_recirculate` int(11) DEFAULT NULL,
  `user_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `feeder_counter` int(11) NOT NULL,
  `feeder_total` int(11) NOT NULL,
  `book_cancel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_track` int(11) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  `admin_track_status` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_track_limit` int(11) NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5C3DF87E97DE5E23` (`pack`),
  KEY `IDX_5C3DF87E8D93D649` (`user`),
  KEY `IDX_5C3DF87E6BE7F41B` (`auto_recirculate`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=324 ;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `testimony` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE IF NOT EXISTS `support` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- Table structure for table `support_message`
--

CREATE TABLE IF NOT EXISTS `support_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `support_id` int(11) NOT NULL,
  `sender` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pack` int(11) DEFAULT NULL,
  `feeder_pack_subscription` int(11) DEFAULT NULL,
  `feeder` int(11) DEFAULT NULL,
  `receiver` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `receiver_share` int(11) NOT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_723705D197DE5E23` (`pack`),
  KEY `IDX_723705D1D009EE10` (`feeder_pack_subscription`),
  KEY `IDX_723705D1DB1A5EE6` (`feeder`),
  KEY `IDX_723705D13DB88C96` (`receiver`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bitcoin_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `sex` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_one` varchar(19) COLLATE utf8_unicode_ci NOT NULL,
  `phone_two` varchar(19) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resident_country` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `resident_state` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_choice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name_two` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name_one` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_name_one` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_name_two` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_number` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number_one` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number_two` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_type` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agency_one` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agency_two` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operation_one` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operation_two` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpf_one` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cpf_two` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_type_one` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_type_two` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purse` int(11) NOT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `referrer` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `activation_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  KEY `avatar` (`avatar`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=265 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `title`, `name`, `email`, `bitcoin_address`, `password`, `sex`, `avatar`, `phone_one`, `phone_two`, `resident_country`, `resident_state`, `bank_name`, `bank_choice`, `bank_account_name`, `bank_name_two`, `bank_name_one`, `account_name_one`, `account_name_two`, `bank_account_number`, `account_number_one`, `account_number_two`, `bank_account_type`, `agency_one`, `agency_two`, `operation_one`, `operation_two`, `cpf_one`, `cpf_two`, `account_type_one`, `account_type_two`, `purse`, `status`, `role`, `referrer`, `created`, `activation_key`) VALUES
(1, 'admin', 'Mr.', 'Administrator', 'admin@site.com', NULL, 'i3yjWBTyaQlzektetaOMCJIHbHw=', 'Male', NULL, '08039540050', '08057430898', '', 'Anabra', 'First Bank Nigeria', '2', 'Oladeji  David', 'First Bank Nigeria', 'GTBANK', 'OLADEJI David Oluwayanmi', 'Oladeji  David', '0013333', '0013867176', '0013333', 'Savings', '', '', '', '', '', '', 'Savings', 'Savings', 0, 'active', 'ROLE_SUPER_ADMIN', NULL, '2017-05-26 12:47:09', 'activated'),
(208, 'testuser', 'Mr.', 'Test user', 'testuser@gmaal.com', '1dfghjkl;klgyuhgfdfijhyugfv@gmail.com', 'i3yjWBTyaQlzektetaOMCJIHbHw=', 'male', NULL, '09876544567', NULL, 'GB', 'Kent', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'active', 'ROLE_USER', NULL, '2017-11-10 10:23:37', 'activated');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `feeder`
--
ALTER TABLE `feeder`
  ADD CONSTRAINT `FK_DB1A5EE63DB88C96` FOREIGN KEY (`receiver`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_DB1A5EE697DE5E23` FOREIGN KEY (`pack`) REFERENCES `pack` (`id`),
  ADD CONSTRAINT `FK_DB1A5EE6D009EE10` FOREIGN KEY (`feeder_pack_subscription`) REFERENCES `pack_subscription` (`id`),
  ADD CONSTRAINT `FK_DB1A5EE6DB1A5EE6` FOREIGN KEY (`feeder`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_DB1A5EE6DFE70FBE` FOREIGN KEY (`receiver_pack_subscription`) REFERENCES `pack_subscription` (`id`);

--
-- Constraints for table `mail_recipient`
--
ALTER TABLE `mail_recipient`
  ADD CONSTRAINT `FK_10DEE52A5126AC48` FOREIGN KEY (`mail`) REFERENCES `mail` (`id`),
  ADD CONSTRAINT `FK_10DEE52A8D93D649` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

--
-- Constraints for table `pack_subscription`
--
ALTER TABLE `pack_subscription`
  ADD CONSTRAINT `FK_5C3DF87E6BE7F41B` FOREIGN KEY (`auto_recirculate`) REFERENCES `pack` (`id`),
  ADD CONSTRAINT `FK_5C3DF87E8D93D649` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_5C3DF87E97DE5E23` FOREIGN KEY (`pack`) REFERENCES `pack` (`id`);

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `FK_723705D13DB88C96` FOREIGN KEY (`receiver`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_723705D197DE5E23` FOREIGN KEY (`pack`) REFERENCES `pack` (`id`),
  ADD CONSTRAINT `FK_723705D1D009EE10` FOREIGN KEY (`feeder_pack_subscription`) REFERENCES `pack_subscription` (`id`),
  ADD CONSTRAINT `FK_723705D1DB1A5EE6` FOREIGN KEY (`feeder`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
