<?php

/* public/public.nav.html.twig */
class __TwigTemplate_e35fabd1f3a2fcf7d590edc64edd864bb92f3863c2c09e702be0b32dbf93e08d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_067f8a99af4e9e2f0cfc3278954f0f60c176c7392ea047abc4a4d0952a650331 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_067f8a99af4e9e2f0cfc3278954f0f60c176c7392ea047abc4a4d0952a650331->enter($__internal_067f8a99af4e9e2f0cfc3278954f0f60c176c7392ea047abc4a4d0952a650331_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/public.nav.html.twig"));

        // line 1
        echo "<!-- Navigation -->
<nav id=\"mainNav\" class=\"navbar navbar-default navbar-fixed-top navbar-custom\">
    <div class=\"container\">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header page-scroll\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span> Menu <i class=\"fa fa-bars\"></i>
            </button>
            ";
        // line 10
        echo "                ";
        // line 11
        echo "            ";
        // line 12
        echo "        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"hidden\">
                    <a href=\"#page-top\"></a>
                </li>


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>";
        
        $__internal_067f8a99af4e9e2f0cfc3278954f0f60c176c7392ea047abc4a4d0952a650331->leave($__internal_067f8a99af4e9e2f0cfc3278954f0f60c176c7392ea047abc4a4d0952a650331_prof);

    }

    public function getTemplateName()
    {
        return "public/public.nav.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  36 => 12,  34 => 11,  32 => 10,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Navigation -->
<nav id=\"mainNav\" class=\"navbar navbar-default navbar-fixed-top navbar-custom\">
    <div class=\"container\">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header page-scroll\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span> Menu <i class=\"fa fa-bars\"></i>
            </button>
            {#<a class=\"navbar-brand\" href=\"{% if app.request.get('_route') != 'home' %}{{ path('home') }}{% else %}#page-top{% endif %}\" style=\"padding:0\">#}
                {#<img src=\"{{ adm.sitelogolight }}\">#}
            {#</a>#}
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"hidden\">
                    <a href=\"#page-top\"></a>
                </li>


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>", "public/public.nav.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public\\public.nav.html.twig");
    }
}
