<?php

/* public/public.nav.html.twig */
class __TwigTemplate_a2a39ce4d4070e9eb9c4d4e0254b4f9f030eb82eae651ce77af58f1d656f13a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8697e03290789a88e9c80c0d20b167ecd34ebc5db780edd259b9538a079597f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8697e03290789a88e9c80c0d20b167ecd34ebc5db780edd259b9538a079597f->enter($__internal_d8697e03290789a88e9c80c0d20b167ecd34ebc5db780edd259b9538a079597f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/public.nav.html.twig"));

        // line 1
        echo "<!-- Navigation -->
<nav id=\"mainNav\" class=\"navbar navbar-default navbar-fixed-top navbar-custom\">
    <div class=\"container\">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header page-scroll\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span> Menu <i class=\"fa fa-bars\"></i>
            </button>
            ";
        // line 10
        echo "                ";
        // line 11
        echo "            ";
        // line 12
        echo "        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"hidden\">
                    <a href=\"#page-top\"></a>
                </li>

                <li class=\"page-scroll\">
                    <a href=\"http://www.paidlife.club\">About</a>
                </li>

                <li class=\"page-scroll\">
                    <a href=\"http://www.paidlife.club\">How It Works</a>
                </li>


                <li class=\"page-scroll\">
                    <a href=\"http://www.paidlife.club\">FAQ</a>
                </li>


                ";
        // line 36
        echo "                    ";
        // line 37
        echo "                ";
        // line 38
        echo "                ";
        // line 39
        echo "                    ";
        // line 40
        echo "                ";
        // line 41
        echo "                ";
        // line 42
        echo "                    ";
        // line 43
        echo "                ";
        // line 44
        echo "                ";
        // line 45
        echo "                    ";
        // line 46
        echo "                ";
        // line 47
        echo "                ";
        // line 48
        echo "
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>";
        
        $__internal_d8697e03290789a88e9c80c0d20b167ecd34ebc5db780edd259b9538a079597f->leave($__internal_d8697e03290789a88e9c80c0d20b167ecd34ebc5db780edd259b9538a079597f_prof);

    }

    public function getTemplateName()
    {
        return "public/public.nav.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 48,  83 => 47,  81 => 46,  79 => 45,  77 => 44,  75 => 43,  73 => 42,  71 => 41,  69 => 40,  67 => 39,  65 => 38,  63 => 37,  61 => 36,  36 => 12,  34 => 11,  32 => 10,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Navigation -->
<nav id=\"mainNav\" class=\"navbar navbar-default navbar-fixed-top navbar-custom\">
    <div class=\"container\">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header page-scroll\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span> Menu <i class=\"fa fa-bars\"></i>
            </button>
            {#<a class=\"navbar-brand\" href=\"{% if app.request.get('_route') != 'home' %}{{ path('home') }}{% else %}#page-top{% endif %}\" style=\"padding:0\">#}
                {#<img src=\"{{ adm.sitelogolight }}\">#}
            {#</a>#}
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"hidden\">
                    <a href=\"#page-top\"></a>
                </li>

                <li class=\"page-scroll\">
                    <a href=\"http://www.paidlife.club\">About</a>
                </li>

                <li class=\"page-scroll\">
                    <a href=\"http://www.paidlife.club\">How It Works</a>
                </li>


                <li class=\"page-scroll\">
                    <a href=\"http://www.paidlife.club\">FAQ</a>
                </li>


                {#<li class=\"page-scroll\">#}
                    {#<a href=\"{% if app.request.get('_route') != 'home' %}{{ path('home')~'#testimonies' }}{% else %}#testimonies{% endif %}\">Testimonies</a>#}
                {#</li>#}
                {#<li class=\"page-scroll\">#}
                    {#<a href=\"{% if app.request.get('_route') != 'home' %}{{ path('home')~'#contact' }}{% else %}#contact{% endif %}\">Contact</a>#}
                {#</li>#}
                {#<li class=\"page-scroll\">#}
                    {#<a href=\"{{ path('login') }}\">Login</a>#}
                {#</li>#}
                {#<li class=\"page-scroll\">#}
                    {#<a href=\"{{ path('register_pack') }}\">Register</a>#}
                {#</li>#}
                {##}

            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>", "public/public.nav.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public\\public.nav.html.twig");
    }
}
