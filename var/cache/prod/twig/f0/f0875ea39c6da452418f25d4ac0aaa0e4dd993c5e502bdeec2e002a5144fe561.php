<?php

/* god-mode/list-admin.html.twig */
class __TwigTemplate_252c4d8e371421485d4bb5e28195dd477396bbb90cdb176f6e0672e3117e989e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/list-admin.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba1ac1f9aebe470f22f6d10b951ffc1fccafadb52e43e41735e8494fcf4be764 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba1ac1f9aebe470f22f6d10b951ffc1fccafadb52e43e41735e8494fcf4be764->enter($__internal_ba1ac1f9aebe470f22f6d10b951ffc1fccafadb52e43e41735e8494fcf4be764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/list-admin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ba1ac1f9aebe470f22f6d10b951ffc1fccafadb52e43e41735e8494fcf4be764->leave($__internal_ba1ac1f9aebe470f22f6d10b951ffc1fccafadb52e43e41735e8494fcf4be764_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_563594fef793f742cef91cb106ccf8d290d6fdfaba80720c2fd6682a0aabfd97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_563594fef793f742cef91cb106ccf8d290d6fdfaba80720c2fd6682a0aabfd97->enter($__internal_563594fef793f742cef91cb106ccf8d290d6fdfaba80720c2fd6682a0aabfd97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getTotalItemCount", array()), "html", null, true);
        echo " Admin";
        if (($this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        
        $__internal_563594fef793f742cef91cb106ccf8d290d6fdfaba80720c2fd6682a0aabfd97->leave($__internal_563594fef793f742cef91cb106ccf8d290d6fdfaba80720c2fd6682a0aabfd97_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3883866e3f20cdafd56847f7949546d2cb0d02baced353c8c8b88e78b9b0eb7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3883866e3f20cdafd56847f7949546d2cb0d02baced353c8c8b88e78b9b0eb7c->enter($__internal_3883866e3f20cdafd56847f7949546d2cb0d02baced353c8c8b88e78b9b0eb7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getTotalItemCount", array()), "html", null, true);
        echo " Admin";
        if (($this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        // line 23
        echo "        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-users\"></i> Admin Users</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    ";
        // line 38
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_SUPER_ADMIN")) {
            // line 39
            echo "                        <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_super_admin");
            echo "\" class=\"list-group-item active\">List Super Admin</a>
                    ";
        }
        // line 41
        echo "                    <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item active\">List Admin</a>
                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item\">List Users</a>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item\">List Payments</a>
                    <a href=\"";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"";
        // line 53
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 54
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <div class=\"row\">
                    <div class=\"col-sm-2\">
                    </div>
                    <div class=\"col-sm-8 text-center lead\">
                        ";
        // line 64
        if (($this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getTotalItemCount", array()) != 0)) {
            // line 65
            echo "                        Showing ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getTotalItemCount", array()), "html", null, true);
            echo " Admin";
            if (($this->getAttribute((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            // line 66
            echo "                        ";
        } else {
            // line 67
            echo "                            0 Admins
                        ";
        }
        // line 69
        echo "                    </div>
                    <div class=\"col-sm-2\">
                    </div>
                </div>
                <table class=\"table table-hover table-responsive table-striped\">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")));
        foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
            // line 84
            echo "                            <tr>
                                <td>";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "getName", array(), "method"), "html", null, true);
            echo "</td>
                                <td>";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "getUsername", array(), "method"), "html", null, true);
            echo "</td>
                                <td>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "getRole", array(), "method"), "html", null, true);
            echo "</td>
                                <td>
                                    <a href=\"";
            // line 89
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_admin", array("admin" => $this->getAttribute($context["admin"], "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-xs btn-warning\">Edit</a>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "                    </tbody>
                </table>

                <div class=\"navigation text-center\">
                    ";
        // line 97
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")));
        echo "
                </div>
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_3883866e3f20cdafd56847f7949546d2cb0d02baced353c8c8b88e78b9b0eb7c->leave($__internal_3883866e3f20cdafd56847f7949546d2cb0d02baced353c8c8b88e78b9b0eb7c_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/list-admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  276 => 97,  270 => 93,  260 => 89,  255 => 87,  251 => 86,  247 => 85,  244 => 84,  240 => 83,  224 => 69,  220 => 67,  217 => 66,  206 => 65,  204 => 64,  191 => 54,  187 => 53,  183 => 52,  179 => 51,  175 => 50,  171 => 49,  167 => 48,  163 => 47,  159 => 46,  155 => 45,  151 => 44,  147 => 43,  143 => 42,  138 => 41,  132 => 39,  130 => 38,  126 => 37,  111 => 25,  107 => 23,  101 => 22,  95 => 18,  86 => 15,  83 => 14,  79 => 13,  76 => 12,  67 => 9,  64 => 8,  60 => 7,  57 => 6,  51 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}{{ admins.getTotalItemCount }} Admin{% if admins.getTotalItemCount != 1 %}s{% endif %}{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            {{ admins.getTotalItemCount }} Admin{% if admins.getTotalItemCount != 1 %}s{% endif %}
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-users\"></i> Admin Users</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    {% if is_granted('ROLE_SUPER_ADMIN') %}
                        <a href=\"{{ path('list_super_admin') }}\" class=\"list-group-item active\">List Super Admin</a>
                    {% endif %}
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item active\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item\">List Users</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <div class=\"row\">
                    <div class=\"col-sm-2\">
                    </div>
                    <div class=\"col-sm-8 text-center lead\">
                        {% if admins.getTotalItemCount != 0 %}
                        Showing {{ admins.getPaginationData.firstItemNumber }} to {{ admins.getPaginationData.lastItemNumber }} out of {{ admins.getTotalItemCount }} Admin{% if admins.getTotalItemCount != 1 %}s{% endif %}
                        {% else %}
                            0 Admins
                        {% endif %}
                    </div>
                    <div class=\"col-sm-2\">
                    </div>
                </div>
                <table class=\"table table-hover table-responsive table-striped\">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Role</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% for admin in admins %}
                            <tr>
                                <td>{{ admin.getName() }}</td>
                                <td>{{ admin.getUsername() }}</td>
                                <td>{{ admin.getRole() }}</td>
                                <td>
                                    <a href=\"{{ path('edit_admin', {'admin' : admin.getId()}) }}\" class=\"btn btn-xs btn-warning\">Edit</a>
                                </td>
                            </tr>
                        {% endfor %}
                    </tbody>
                </table>

                <div class=\"navigation text-center\">
                    {{ knp_pagination_render(admins) }}
                </div>
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}", "god-mode/list-admin.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\list-admin.html.twig");
    }
}
