<?php

/* god-mode/list-packsubscriptions.html.twig */
class __TwigTemplate_8d55cf8f9256f4041327aeef077366c4004b77a46d7983cb44c97e35727c938b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/list-packsubscriptions.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e7eb0cf3aec9e26e6104a1ee9313a6c542b6dde7fbeea08836aab8c9eb0cad56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7eb0cf3aec9e26e6104a1ee9313a6c542b6dde7fbeea08836aab8c9eb0cad56->enter($__internal_e7eb0cf3aec9e26e6104a1ee9313a6c542b6dde7fbeea08836aab8c9eb0cad56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/list-packsubscriptions.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e7eb0cf3aec9e26e6104a1ee9313a6c542b6dde7fbeea08836aab8c9eb0cad56->leave($__internal_e7eb0cf3aec9e26e6104a1ee9313a6c542b6dde7fbeea08836aab8c9eb0cad56_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_76e5d506e8d91fa7887a8cb9d65ab5127c14cfe41643095b307f3509f752b251 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76e5d506e8d91fa7887a8cb9d65ab5127c14cfe41643095b307f3509f752b251->enter($__internal_76e5d506e8d91fa7887a8cb9d65ab5127c14cfe41643095b307f3509f752b251_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getTotalItemCount", array()), "html", null, true);
        echo " Pack";
        if (($this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        
        $__internal_76e5d506e8d91fa7887a8cb9d65ab5127c14cfe41643095b307f3509f752b251->leave($__internal_76e5d506e8d91fa7887a8cb9d65ab5127c14cfe41643095b307f3509f752b251_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c6dbd5fbe783abd7046eb7530cfb78752c91266ea1f4cc9228f2d5a3ba9b5172 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6dbd5fbe783abd7046eb7530cfb78752c91266ea1f4cc9228f2d5a3ba9b5172->enter($__internal_c6dbd5fbe783abd7046eb7530cfb78752c91266ea1f4cc9228f2d5a3ba9b5172_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getTotalItemCount", array()), "html", null, true);
        echo " Pack Subscription";
        if (($this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        // line 23
        echo "        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-pie-chart\"></i> Pack Subscriptions</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item\">List Admin</a>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item\">List Users</a>
                    <a href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item\">List Payments</a>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <div class=\"row\">
                    <div class=\"col-sm-2\">
                    </div>
                    <div class=\"col-sm-8 text-center lead\">
                        ";
        // line 61
        if (($this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getTotalItemCount", array()) != 0)) {
            // line 62
            echo "                        Showing ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getTotalItemCount", array()), "html", null, true);
            echo " Pack";
            if (($this->getAttribute((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            // line 63
            echo "                        ";
        } else {
            // line 64
            echo "                            0 packsubs
                        ";
        }
        // line 66
        echo "                    </div>
                    <div class=\"col-sm-2\">
                    </div>
                </div>
                <table class=\"table table-hover table-responsive table-striped\">
                    <thead>
                        <tr>

                            <th>Pack User</th>
                            <th>Name</th>
                            <th>Pack Status</th>
                            <th>AutoCycle(Y/N)</th>
                            <th>User Type</th>
                            <th>Feeders Total</th>
                            <th>Feeders Left</th>
                            <th>Options</th>s
                        </tr>
                    </thead>
                    <tbody>
                        ";
        // line 85
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 86
            echo "                            ";
            if (($this->getAttribute($context["pack"], "getUser", array()) != null)) {
                // line 87
                echo "                            <tr>
                                <td> ";
                // line 88
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pack"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo " </td>
                                <td>";
                // line 89
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pack"], "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</td>
                                <td>";
                // line 90
                echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getStatus", array(), "method"), "html", null, true);
                echo "</td>
                                <td></td>
                                <td>";
                // line 92
                echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getUserType", array(), "method"), "html", null, true);
                echo "</td>
                                <td>";
                // line 93
                echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getFeederTotal", array(), "method"), "html", null, true);
                echo "</td>
                                <td>";
                // line 94
                echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getFeederCounter", array(), "method"), "html", null, true);
                echo "</td>
                                <td>
                                    ";
                // line 96
                if (($this->getAttribute($context["pack"], "getStatus", array(), "method") == "active")) {
                    // line 97
                    echo "                                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("do_block_packsubscription", array("pack" => $this->getAttribute($context["pack"], "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-flat btn-primary\">Put on Wait</a>
                                    ";
                } else {
                    // line 99
                    echo "                                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("do_activate_packsubscription", array("pack" => $this->getAttribute($context["pack"], "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-flat btn-success\">Activate Subscription</a>
                                    ";
                }
                // line 101
                echo "                                </td>

                            </tr>
                            ";
            }
            // line 105
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "                    </tbody>
                </table>

                <div class=\"navigation text-center\">
                    ";
        // line 110
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["packsubs"]) ? $context["packsubs"] : $this->getContext($context, "packsubs")));
        echo "
                </div>
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_c6dbd5fbe783abd7046eb7530cfb78752c91266ea1f4cc9228f2d5a3ba9b5172->leave($__internal_c6dbd5fbe783abd7046eb7530cfb78752c91266ea1f4cc9228f2d5a3ba9b5172_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/list-packsubscriptions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  304 => 110,  298 => 106,  292 => 105,  286 => 101,  280 => 99,  274 => 97,  272 => 96,  267 => 94,  263 => 93,  259 => 92,  254 => 90,  250 => 89,  246 => 88,  243 => 87,  240 => 86,  236 => 85,  215 => 66,  211 => 64,  208 => 63,  197 => 62,  195 => 61,  182 => 51,  178 => 50,  174 => 49,  170 => 48,  166 => 47,  162 => 46,  158 => 45,  154 => 44,  150 => 43,  146 => 42,  142 => 41,  138 => 40,  134 => 39,  130 => 38,  126 => 37,  111 => 25,  107 => 23,  101 => 22,  95 => 18,  86 => 15,  83 => 14,  79 => 13,  76 => 12,  67 => 9,  64 => 8,  60 => 7,  57 => 6,  51 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}{{ packsubs.getTotalItemCount }} Pack{% if packsubs.getTotalItemCount != 1 %}s{% endif %}{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            {{ packsubs.getTotalItemCount }} Pack Subscription{% if packsubs.getTotalItemCount != 1 %}s{% endif %}
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-pie-chart\"></i> Pack Subscriptions</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item\">List Users</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <div class=\"row\">
                    <div class=\"col-sm-2\">
                    </div>
                    <div class=\"col-sm-8 text-center lead\">
                        {% if packsubs.getTotalItemCount != 0 %}
                        Showing {{ packsubs.getPaginationData.firstItemNumber }} to {{ packsubs.getPaginationData.lastItemNumber }} out of {{ packsubs.getTotalItemCount }} Pack{% if packsubs.getTotalItemCount != 1 %}s{% endif %}
                        {% else %}
                            0 packsubs
                        {% endif %}
                    </div>
                    <div class=\"col-sm-2\">
                    </div>
                </div>
                <table class=\"table table-hover table-responsive table-striped\">
                    <thead>
                        <tr>

                            <th>Pack User</th>
                            <th>Name</th>
                            <th>Pack Status</th>
                            <th>AutoCycle(Y/N)</th>
                            <th>User Type</th>
                            <th>Feeders Total</th>
                            <th>Feeders Left</th>
                            <th>Options</th>s
                        </tr>
                    </thead>
                    <tbody>
                        {% for pack in packsubs %}
                            {% if pack.getUser != null %}
                            <tr>
                                <td> {{ pack.getUser().getName() }} </td>
                                <td>{{ pack.getPack().getName() }}</td>
                                <td>{{ pack.getStatus() }}</td>
                                <td></td>
                                <td>{{ pack.getUserType() }}</td>
                                <td>{{ pack.getFeederTotal() }}</td>
                                <td>{{ pack.getFeederCounter() }}</td>
                                <td>
                                    {% if pack.getStatus() == 'active' %}
                                        <a href=\"{{ path('do_block_packsubscription', {'pack' : pack.getId()}) }}\" class=\"btn btn-xs btn-flat btn-primary\">Put on Wait</a>
                                    {% else %}
                                        <a href=\"{{ path('do_activate_packsubscription', {'pack' : pack.getId()}) }}\" class=\"btn btn-xs btn-flat btn-success\">Activate Subscription</a>
                                    {% endif %}
                                </td>

                            </tr>
                            {% endif %}
                        {% endfor %}
                    </tbody>
                </table>

                <div class=\"navigation text-center\">
                    {{ knp_pagination_render(packsubs) }}
                </div>
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}", "god-mode/list-packsubscriptions.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\list-packsubscriptions.html.twig");
    }
}
