<?php

/* public/login.html.twig */
class __TwigTemplate_d5d63c0a6e7ff6cc2f43b61d0c47021a5508792618ad898c2eebdd45f9482e81 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("public.base.html.twig", "public/login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "public.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a47bf1c60c0e38ef0a07ce586516496c46f680774bf0984747e8512e9f999d82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a47bf1c60c0e38ef0a07ce586516496c46f680774bf0984747e8512e9f999d82->enter($__internal_a47bf1c60c0e38ef0a07ce586516496c46f680774bf0984747e8512e9f999d82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a47bf1c60c0e38ef0a07ce586516496c46f680774bf0984747e8512e9f999d82->leave($__internal_a47bf1c60c0e38ef0a07ce586516496c46f680774bf0984747e8512e9f999d82_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_628a2a9f2adeed0f4636511ad84a05344fd2ee2d6452707847cd14c1128b3499 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_628a2a9f2adeed0f4636511ad84a05344fd2ee2d6452707847cd14c1128b3499->enter($__internal_628a2a9f2adeed0f4636511ad84a05344fd2ee2d6452707847cd14c1128b3499_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Sign In";
        
        $__internal_628a2a9f2adeed0f4636511ad84a05344fd2ee2d6452707847cd14c1128b3499->leave($__internal_628a2a9f2adeed0f4636511ad84a05344fd2ee2d6452707847cd14c1128b3499_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_2c93dd6eb88c555d2e341fd7bb1e33acf4b8b3ed28f6e9fddf87216d4a56c579 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c93dd6eb88c555d2e341fd7bb1e33acf4b8b3ed28f6e9fddf87216d4a56c579->enter($__internal_2c93dd6eb88c555d2e341fd7bb1e33acf4b8b3ed28f6e9fddf87216d4a56c579_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Sign In</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">
                <div class=\"login-box\" style=\"margin: 0 auto !important\">
                    <!-- /.login-logo -->
                    <div class=\"login-box-body\">
                        ";
        // line 17
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 18
            echo "                            <div class=\"alert alert-danger\">
                                ";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array())), "html", null, true);
            echo "
                            </div>
                        ";
        }
        // line 22
        echo "
                        <form action=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login_check");
        echo "\" method=\"post\">
                            <div class=\"form-group has-feedback\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"username\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\">
                                <span class=\"fa fa-user-o form-control-feedback\"></span>
                            </div>
                            <div class=\"form-group has-feedback\">
                                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\">
                                <span class=\"fa fa-key form-control-feedback\"></span>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-xs-8\">
                                    <div class=\"checkbox icheck\">
                                        <label>
                                            <input type=\"checkbox\" name=\"_remember_me\"> Remember Me
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class=\"col-xs-4\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Sign In</button>
                                </div>
                                <!-- /.col -->
                            </div>
                            <input type=\"hidden\" name=\"ver\" value=\"u\">
                        </form>


                        <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("forgot_password");
        echo "\">Forgot password</a> | <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register_pack");
        echo "\" class=\"text-center\">Register</a>
                    </div>
                    <!-- /.login-box-body -->
                </div>
                <!-- /.login-box -->
            </div>
        </div>
    </section>

";
        
        $__internal_2c93dd6eb88c555d2e341fd7bb1e33acf4b8b3ed28f6e9fddf87216d4a56c579->leave($__internal_2c93dd6eb88c555d2e341fd7bb1e33acf4b8b3ed28f6e9fddf87216d4a56c579_prof);

    }

    public function getTemplateName()
    {
        return "public/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 50,  85 => 25,  80 => 23,  77 => 22,  71 => 19,  68 => 18,  66 => 17,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'public.base.html.twig' %}

{% block title %}Sign In{% endblock %}

{% block body %}

    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Sign In</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">
                <div class=\"login-box\" style=\"margin: 0 auto !important\">
                    <!-- /.login-logo -->
                    <div class=\"login-box-body\">
                        {% if error %}
                            <div class=\"alert alert-danger\">
                                {{ error.messageKey|trans(error.messageData) }}
                            </div>
                        {% endif %}

                        <form action=\"{{ path('security_login_check') }}\" method=\"post\">
                            <div class=\"form-group has-feedback\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"username\" value=\"{{ last_username }}\">
                                <span class=\"fa fa-user-o form-control-feedback\"></span>
                            </div>
                            <div class=\"form-group has-feedback\">
                                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\">
                                <span class=\"fa fa-key form-control-feedback\"></span>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-xs-8\">
                                    <div class=\"checkbox icheck\">
                                        <label>
                                            <input type=\"checkbox\" name=\"_remember_me\"> Remember Me
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class=\"col-xs-4\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Sign In</button>
                                </div>
                                <!-- /.col -->
                            </div>
                            <input type=\"hidden\" name=\"ver\" value=\"u\">
                        </form>


                        <a href=\"{{ path('forgot_password') }}\">Forgot password</a> | <a href=\"{{ path('register_pack') }}\" class=\"text-center\">Register</a>
                    </div>
                    <!-- /.login-box-body -->
                </div>
                <!-- /.login-box -->
            </div>
        </div>
    </section>

{% endblock %}", "public/login.html.twig", "/var/www/html/mlm/app/Resources/views/public/login.html.twig");
    }
}
