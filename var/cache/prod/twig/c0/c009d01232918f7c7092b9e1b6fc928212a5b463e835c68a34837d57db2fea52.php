<?php

/* member/member.page-main-sidebar.html.twig */
class __TwigTemplate_667934d10338202fc3b3a3b79a438b7620ff62c431437d93fa050290fb6fb8e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f06ffbb2fe359fbabc673f6552553b773a1787ddb3edf60d2e4ebaaae56d9dc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f06ffbb2fe359fbabc673f6552553b773a1787ddb3edf60d2e4ebaaae56d9dc8->enter($__internal_f06ffbb2fe359fbabc673f6552553b773a1787ddb3edf60d2e4ebaaae56d9dc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/member.page-main-sidebar.html.twig"));

        // line 1
        echo "<!-- Left side column. contains the logo and sidebar -->
<aside class=\"main-sidebar\">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class=\"sidebar\">

      \t<!-- Sidebar user panel -->
      \t<div class=\"user-panel\">
        \t<div class=\"pull-left image\">
          \t\t<img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getAvatar", array(), "method")), "html", null, true);
        echo "\" class=\"img-circle\" alt=\"User Image\">
        \t</div>
        \t<div class=\"pull-left info\">
          \t\t<p>";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "</p>
\t\t\t\t";
        // line 14
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getRole", array(), "method") == "ROLE_SUPER_ADMIN")) {
            // line 15
            echo "\t\t\t\t\t<a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminprofile");
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getUsername", array(), "method"), "html", null, true);
            echo "</a>
\t\t\t\t";
        } else {
            // line 17
            echo "\t\t\t\t\t<a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profile");
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getUsername", array(), "method"), "html", null, true);
            echo "</a>
\t\t\t\t";
        }
        // line 19
        echo "        \t</div>
      \t</div> <!-- End Sidebar user panel -->

      \t<!-- sidebar menu: : style can be found in sidebar.less -->
      \t<ul class=\"sidebar-menu\">
        \t<li class=\"header\">MAIN NAVIGATION</li>
\t\t\t";
        // line 25
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_SUPER_ADMIN")) {
            // line 26
            echo "\t\t\t\t<li ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_dashboard")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 27
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-dashboard\"></i> <span>Dashboard</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li ";
            // line 31
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 32
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_settings");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-cog\"></i> <span>Settings</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<hr>
\t\t\t\t<li ";
            // line 37
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 38
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-archive\"></i> <span>View Packs</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li ";
            // line 42
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 43
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-newspaper-o\"></i> <span>View Announcements</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li ";
            // line 47
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 48
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-users\"></i> <span>List All Users</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li ";
            // line 52
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 53
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-users\"></i> <span>List Admins</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<hr>
\t\t\t\t<li ";
            // line 58
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 59
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-archive\"></i> <span>Create Packs</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li ";
            // line 63
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 64
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-user-plus\"></i> <span>Create Admin</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li ";
            // line 68
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 69
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-bullhorn\"></i> <span>New Announcement</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li ";
            // line 73
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "god_mode_settings")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 74
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-mail-forward\"></i> <span>Message All Users</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<hr>

\t\t\t";
        }
        // line 81
        echo "\t\t\t<li>

\t\t\t</li>
        \t";
        // line 84
        if (((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getRole", array(), "method") == "ROLE_SUPER_ADMIN") && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getDbGranted", array(), "method") != null)) || (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getRole", array(), "method") == "ROLE_ADMIN") || ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getRole", array(), "method") == "ROLE_USER")))) {
            // line 85
            echo "\t        <li ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "dashboard")) {
                echo "class=\"active\"";
            }
            echo ">
\t          \t<a href=\"";
            // line 86
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
            echo "\">
\t            \t<i class=\"fa fa-dashboard\"></i> <span>Dashboard</span>
\t          \t</a>
\t        </li>
\t        <li ";
            // line 90
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "profile")) {
                echo "class=\"active\"";
            }
            echo ">
\t          \t<a href=\"";
            // line 91
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profile");
            echo "\">
\t            \t<i class=\"fa fa-user\"></i> <span>Profile</span>
\t          \t</a>
\t        </li>
\t        <li ";
            // line 95
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "referrals")) {
                echo "class=\"active\"";
            }
            echo ">
\t          \t<a href=\"";
            // line 96
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("referrals");
            echo "\">
\t            \t<i class=\"fa fa-users\"></i> <span>My Referrals</span>
\t            \t<span class=\"pull-right-container\">
\t              \t\t<small class=\"label pull-right bg-yellow\">";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getReferralsCount", array(), "method"), "html", null, true);
            echo "</small>
\t            \t</span>
\t          \t</a>
\t        </li>
\t        <li ";
            // line 103
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "packs")) {
                echo "class=\"active\"";
            }
            echo ">
\t          \t<a href=\"";
            // line 104
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("packs");
            echo "\">
\t            \t<i class=\"fa fa-pie-chart\"></i> <span>My Packs</span>
\t            \t<span class=\"pull-right-container\">
\t              \t\t<small class=\"label pull-right bg-red\">";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPacks", array(), "method"), "html", null, true);
            echo "</small>
\t            \t</span>
\t          \t</a>
\t        </li>
\t        <li ";
            // line 111
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "purse")) {
                echo "class=\"active\"";
            }
            echo ">
\t          \t<a href=\"";
            // line 112
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("purse");
            echo "\">
\t            \t<i class=\"fa fa-money\"></i> <span>My Wallet</span>
\t            \t<span class=\"pull-right-container\">
\t              \t\t<small class=\"label pull-right bg-green\">";
            // line 115
            echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getFormattedPurse", array(), "method"), "html", null, true);
            echo "</small>
\t            \t</span>
\t          \t</a>
\t        </li>
        \t";
        }
        // line 120
        echo "\t        ";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 121
            echo "\t\t\t\t<li ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "demi_god_support")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t\t\t\t<a href=\"";
            // line 122
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("demi_god_support");
            echo "\">
\t\t\t\t\t\t<i class=\"fa fa-ticket\"></i> <span>User Tickets</span>
\t\t\t\t\t\t<span class=\"pull-right-container\">
\t\t              \t\t<small class=\"label pull-right bg-orange\">";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getSupportCount", array(), "method"), "html", null, true);
            echo "</small>
\t\t            \t</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t        ";
        } else {
            // line 130
            echo "\t\t        <li ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "support")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t          \t<a href=\"";
            // line 131
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("support");
            echo "\">
\t\t            \t<i class=\"fa fa-ticket\"></i> <span>My Tickets</span>
\t\t          \t</a>
\t\t        </li>
\t    \t";
        }
        // line 136
        echo "\t    \t";
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 137
            echo "\t        \t<li ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "demi_god_jury")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t          \t<a href=\"";
            // line 138
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("demi_god_jury");
            echo "\">
\t\t            \t<i class=\"fa fa-university\"></i> <span>User Court Cases</span>
\t\t            \t<span class=\"pull-right-container\">
\t\t              \t\t<small class=\"label pull-right bg-red\">";
            // line 141
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getJuryCount", array(), "method"), "html", null, true);
            echo "</small>
\t\t            \t</span>
\t\t          \t</a>
\t\t        </li>
\t\t    ";
        } else {
            // line 146
            echo "\t\t        <li ";
            if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "jury")) {
                echo "class=\"active\"";
            }
            echo ">
\t\t          \t<a href=\"";
            // line 147
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jury");
            echo "\">
\t\t            \t<i class=\"fa fa-university\"></i> <span>My Court Cases</span>
\t\t          \t</a>
\t\t        </li>
\t    \t";
        }
        // line 152
        echo "\t        <li ";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "reviews")) {
            echo "class=\"active\"";
        }
        echo ">
\t          \t<a href=\"";
        // line 153
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reviews");
        echo "\">
\t            \t<i class=\"fa fa-comments-o\"></i> <span>My Testimonies</span>
\t          \t</a>
\t        </li>

        </ul>
\t\t<!-- End sidebar menu: : style can be found in sidebar.less -->

    </section> <!-- End sidebar -->

</aside> <!-- End Left side column -->";
        
        $__internal_f06ffbb2fe359fbabc673f6552553b773a1787ddb3edf60d2e4ebaaae56d9dc8->leave($__internal_f06ffbb2fe359fbabc673f6552553b773a1787ddb3edf60d2e4ebaaae56d9dc8_prof);

    }

    public function getTemplateName()
    {
        return "member/member.page-main-sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 153,  378 => 152,  370 => 147,  363 => 146,  355 => 141,  349 => 138,  342 => 137,  339 => 136,  331 => 131,  324 => 130,  316 => 125,  310 => 122,  303 => 121,  300 => 120,  291 => 115,  285 => 112,  279 => 111,  272 => 107,  266 => 104,  260 => 103,  253 => 99,  247 => 96,  241 => 95,  234 => 91,  228 => 90,  221 => 86,  214 => 85,  212 => 84,  207 => 81,  197 => 74,  191 => 73,  184 => 69,  178 => 68,  171 => 64,  165 => 63,  158 => 59,  152 => 58,  144 => 53,  138 => 52,  131 => 48,  125 => 47,  118 => 43,  112 => 42,  105 => 38,  99 => 37,  91 => 32,  85 => 31,  78 => 27,  71 => 26,  69 => 25,  61 => 19,  53 => 17,  45 => 15,  43 => 14,  39 => 13,  33 => 10,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Left side column. contains the logo and sidebar -->
<aside class=\"main-sidebar\">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class=\"sidebar\">

      \t<!-- Sidebar user panel -->
      \t<div class=\"user-panel\">
        \t<div class=\"pull-left image\">
          \t\t<img src=\"{{ asset(app.user.getAvatar()) }}\" class=\"img-circle\" alt=\"User Image\">
        \t</div>
        \t<div class=\"pull-left info\">
          \t\t<p>{{ app.user.getName() }}</p>
\t\t\t\t{% if (app.user.getRole() == 'ROLE_SUPER_ADMIN') %}
\t\t\t\t\t<a href=\"{{ path('adminprofile') }}\" >{{ app.user.getUsername() }}</a>
\t\t\t\t{% else %}
\t\t\t\t\t<a href=\"{{ path('profile') }}\" >{{ app.user.getUsername() }}</a>
\t\t\t\t{% endif %}
        \t</div>
      \t</div> <!-- End Sidebar user panel -->

      \t<!-- sidebar menu: : style can be found in sidebar.less -->
      \t<ul class=\"sidebar-menu\">
        \t<li class=\"header\">MAIN NAVIGATION</li>
\t\t\t{% if is_granted('ROLE_SUPER_ADMIN') %}
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_dashboard' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('god_mode_dashboard') }}\">
\t\t\t\t\t\t<i class=\"fa fa-dashboard\"></i> <span>Dashboard</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('god_mode_settings') }}\">
\t\t\t\t\t\t<i class=\"fa fa-cog\"></i> <span>Settings</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<hr>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('list_pack') }}\">
\t\t\t\t\t\t<i class=\"fa fa-archive\"></i> <span>View Packs</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('list_news') }}\">
\t\t\t\t\t\t<i class=\"fa fa-newspaper-o\"></i> <span>View Announcements</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('list_users') }}\">
\t\t\t\t\t\t<i class=\"fa fa-users\"></i> <span>List All Users</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('list_admin') }}\">
\t\t\t\t\t\t<i class=\"fa fa-users\"></i> <span>List Admins</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<hr>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('create_pack') }}\">
\t\t\t\t\t\t<i class=\"fa fa-archive\"></i> <span>Create Packs</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('create_admin') }}\">
\t\t\t\t\t\t<i class=\"fa fa-user-plus\"></i> <span>Create Admin</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('create_news') }}\">
\t\t\t\t\t\t<i class=\"fa fa-bullhorn\"></i> <span>New Announcement</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<li {% if app.request.get('_route') == 'god_mode_settings' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('god_mode_compose_mail') }}\">
\t\t\t\t\t\t<i class=\"fa fa-mail-forward\"></i> <span>Message All Users</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t\t\t\t<hr>

\t\t\t{% endif %}
\t\t\t<li>

\t\t\t</li>
        \t{% if (app.user.getRole() == 'ROLE_SUPER_ADMIN' and app.user.getDbGranted() != null) or (app.user.getRole() == 'ROLE_ADMIN' or app.user.getRole() == 'ROLE_USER') %}
\t        <li {% if app.request.get('_route') == 'dashboard' %}class=\"active\"{% endif %}>
\t          \t<a href=\"{{ path('dashboard') }}\">
\t            \t<i class=\"fa fa-dashboard\"></i> <span>Dashboard</span>
\t          \t</a>
\t        </li>
\t        <li {% if app.request.get('_route') == 'profile' %}class=\"active\"{% endif %}>
\t          \t<a href=\"{{ path('profile') }}\">
\t            \t<i class=\"fa fa-user\"></i> <span>Profile</span>
\t          \t</a>
\t        </li>
\t        <li {% if app.request.get('_route') == 'referrals' %}class=\"active\"{% endif %}>
\t          \t<a href=\"{{ path('referrals') }}\">
\t            \t<i class=\"fa fa-users\"></i> <span>My Referrals</span>
\t            \t<span class=\"pull-right-container\">
\t              \t\t<small class=\"label pull-right bg-yellow\">{{ app.user.getReferralsCount() }}</small>
\t            \t</span>
\t          \t</a>
\t        </li>
\t        <li {% if app.request.get('_route') == 'packs' %}class=\"active\"{% endif %}>
\t          \t<a href=\"{{ path('packs') }}\">
\t            \t<i class=\"fa fa-pie-chart\"></i> <span>My Packs</span>
\t            \t<span class=\"pull-right-container\">
\t              \t\t<small class=\"label pull-right bg-red\">{{ app.user.getPacks() }}</small>
\t            \t</span>
\t          \t</a>
\t        </li>
\t        <li {% if app.request.get('_route') == 'purse' %}class=\"active\"{% endif %}>
\t          \t<a href=\"{{ path('purse') }}\">
\t            \t<i class=\"fa fa-money\"></i> <span>My Wallet</span>
\t            \t<span class=\"pull-right-container\">
\t              \t\t<small class=\"label pull-right bg-green\">{{ app_currency_symbol }}{{ app.user.getFormattedPurse() }}</small>
\t            \t</span>
\t          \t</a>
\t        </li>
        \t{% endif %}
\t        {% if is_granted('ROLE_ADMIN') %}
\t\t\t\t<li {% if app.request.get('_route') == 'demi_god_support' %}class=\"active\"{% endif %}>
\t\t\t\t\t<a href=\"{{ path('demi_god_support') }}\">
\t\t\t\t\t\t<i class=\"fa fa-ticket\"></i> <span>User Tickets</span>
\t\t\t\t\t\t<span class=\"pull-right-container\">
\t\t              \t\t<small class=\"label pull-right bg-orange\">{{ app.user.getSupportCount() }}</small>
\t\t            \t</span>
\t\t\t\t\t</a>
\t\t\t\t</li>
\t        {% else %}
\t\t        <li {% if app.request.get('_route') == 'support' %}class=\"active\"{% endif %}>
\t\t          \t<a href=\"{{ path('support') }}\">
\t\t            \t<i class=\"fa fa-ticket\"></i> <span>My Tickets</span>
\t\t          \t</a>
\t\t        </li>
\t    \t{% endif %}
\t    \t{% if is_granted('ROLE_ADMIN') %}
\t        \t<li {% if app.request.get('_route') == 'demi_god_jury' %}class=\"active\"{% endif %}>
\t\t          \t<a href=\"{{ path('demi_god_jury') }}\">
\t\t            \t<i class=\"fa fa-university\"></i> <span>User Court Cases</span>
\t\t            \t<span class=\"pull-right-container\">
\t\t              \t\t<small class=\"label pull-right bg-red\">{{ app.user.getJuryCount() }}</small>
\t\t            \t</span>
\t\t          \t</a>
\t\t        </li>
\t\t    {% else %}
\t\t        <li {% if app.request.get('_route') == 'jury' %}class=\"active\"{% endif %}>
\t\t          \t<a href=\"{{ path('jury') }}\">
\t\t            \t<i class=\"fa fa-university\"></i> <span>My Court Cases</span>
\t\t          \t</a>
\t\t        </li>
\t    \t{% endif %}
\t        <li {% if app.request.get('_route') == 'reviews' %}class=\"active\"{% endif %}>
\t          \t<a href=\"{{ path('reviews') }}\">
\t            \t<i class=\"fa fa-comments-o\"></i> <span>My Testimonies</span>
\t          \t</a>
\t        </li>

        </ul>
\t\t<!-- End sidebar menu: : style can be found in sidebar.less -->

    </section> <!-- End sidebar -->

</aside> <!-- End Left side column -->", "member/member.page-main-sidebar.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member\\member.page-main-sidebar.html.twig");
    }
}
