<?php

/* member/supports.html.twig */
class __TwigTemplate_8458ad36db3e76b563bfc55b6b786f36091cd26fefae9076e6bf46d5f83e9629 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/supports.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a1e36460eb2102fa9a3d937fcfff2d81f2538563d4c5eafca72136f9321eb39d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1e36460eb2102fa9a3d937fcfff2d81f2538563d4c5eafca72136f9321eb39d->enter($__internal_a1e36460eb2102fa9a3d937fcfff2d81f2538563d4c5eafca72136f9321eb39d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/supports.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a1e36460eb2102fa9a3d937fcfff2d81f2538563d4c5eafca72136f9321eb39d->leave($__internal_a1e36460eb2102fa9a3d937fcfff2d81f2538563d4c5eafca72136f9321eb39d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8992befa0f7ce14400444820c923a43f3925d3af25a47ebab4cc902ad2da0271 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8992befa0f7ce14400444820c923a43f3925d3af25a47ebab4cc902ad2da0271->enter($__internal_8992befa0f7ce14400444820c923a43f3925d3af25a47ebab4cc902ad2da0271_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Tickets";
        
        $__internal_8992befa0f7ce14400444820c923a43f3925d3af25a47ebab4cc902ad2da0271->leave($__internal_8992befa0f7ce14400444820c923a43f3925d3af25a47ebab4cc902ad2da0271_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_d5e90b640047bc46b34955d18fc572326e5f5e8dd6ffa0e9e18d57bd1642ae57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5e90b640047bc46b34955d18fc572326e5f5e8dd6ffa0e9e18d57bd1642ae57->enter($__internal_d5e90b640047bc46b34955d18fc572326e5f5e8dd6ffa0e9e18d57bd1642ae57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tTickets
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-ticket\"></i> Tickets</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">

          \t\t<!-- small box -->
          \t\t<div class=\"small-box bg-red\">
            \t\t<div class=\"inner\">
              \t\t\t<h3>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supports"]) ? $context["supports"] : $this->getContext($context, "supports")), "getTotalItemCount", array()), "html", null, true);
        echo "</h3>
              \t\t\t<p>Tickets</p>
            \t\t</div>
            \t\t<div class=\"icon\">
              \t\t\t<i class=\"fa fa-ticket\"></i>
            \t\t</div>
            \t\t<a class=\"small-box-footer\">Tickets Count <i class=\"fa fa-arrow-circle-right\"></i></a>
          \t\t</div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
          \t\t\t<a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_support");
        echo "\" class=\"list-group-item\">Create New Ticket</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("support");
        echo "\" class=\"list-group-item list-group-item-success\">View Tickets</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
    \t\t\t";
        // line 44
        if (($this->getAttribute((isset($context["supports"]) ? $context["supports"] : $this->getContext($context, "supports")), "getTotalItemCount", array()) == 0)) {
            // line 45
            echo "    \t\t\t\t<div class=\"alert alert-warning\">
    \t\t\t\t\t<p class=\"lead text-center\">You have not created any ticket yet</p>
    \t\t\t\t\t<hr>
    \t\t\t\t\t<a href=\"";
            // line 48
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_support");
            echo "\" class=\"btn btn-info btn-block\" style=\"text-decoration: none\">Create Ticket</a>
    \t\t\t\t</div>
    \t\t\t";
        } else {
            // line 51
            echo "\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing ";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["supports"]) ? $context["supports"] : $this->getContext($context, "supports")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["supports"]) ? $context["supports"] : $this->getContext($context, "supports")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["supports"]) ? $context["supports"] : $this->getContext($context, "supports")), "getTotalItemCount", array()), "html", null, true);
            echo " Ticket";
            if (($this->getAttribute((isset($context["supports"]) ? $context["supports"] : $this->getContext($context, "supports")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            echo "</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>#</th>
\t    \t\t\t\t\t\t\t\t<th>Subject</th>
\t    \t\t\t\t\t\t\t\t<th>Status</th>
\t    \t\t\t\t\t\t\t\t<th>Date</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t";
            // line 68
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["supports"]) ? $context["supports"] : $this->getContext($context, "supports")));
            foreach ($context['_seq'] as $context["_key"] => $context["support"]) {
                // line 69
                echo "\t    \t\t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($context["support"], "getId", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td><a href=\"";
                // line 71
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("view_support", array("support" => $this->getAttribute($context["support"], "getId", array(), "method"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["support"], "getSubject", array(), "method"), "html", null, true);
                echo "</a></td>
\t    \t\t\t\t\t\t\t\t\t<td>
\t    \t\t\t\t\t\t\t\t\t\t";
                // line 73
                if (($this->getAttribute($context["support"], "getStatus", array(), "method") == "user_reply")) {
                    // line 74
                    echo "\t    \t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-warning\">User Reply</button>
\t    \t\t\t\t\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 75
$context["support"], "getStatus", array(), "method") == "admin_reply")) {
                    // line 76
                    echo "\t    \t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-success\">Admin Reply</button>
\t    \t\t\t\t\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 77
$context["support"], "getStatus", array(), "method") == "blank")) {
                    // line 78
                    echo "\t    \t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-info\">Blank</button>
\t    \t\t\t\t\t\t\t\t\t\t";
                } elseif (($this->getAttribute(                // line 79
$context["support"], "getStatus", array(), "method") == "closed")) {
                    // line 80
                    echo "\t    \t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-danger\">Closed</button>
\t    \t\t\t\t\t\t\t\t\t\t";
                }
                // line 82
                echo "\t    \t\t\t\t\t\t\t\t\t</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 83
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["support"], "getCreatedDate", array(), "method"), "d F, Y"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['support'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 86
            echo "\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    ";
            // line 92
            echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["supports"]) ? $context["supports"] : $this->getContext($context, "supports")));
            echo "
\t                </div>
    \t\t\t";
        }
        // line 95
        echo "    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section> <!-- End Main Content -->

";
        
        $__internal_d5e90b640047bc46b34955d18fc572326e5f5e8dd6ffa0e9e18d57bd1642ae57->leave($__internal_d5e90b640047bc46b34955d18fc572326e5f5e8dd6ffa0e9e18d57bd1642ae57_prof);

    }

    public function getTemplateName()
    {
        return "member/supports.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 95,  207 => 92,  199 => 86,  190 => 83,  187 => 82,  183 => 80,  181 => 79,  178 => 78,  176 => 77,  173 => 76,  171 => 75,  168 => 74,  166 => 73,  159 => 71,  155 => 70,  152 => 69,  148 => 68,  122 => 53,  118 => 51,  112 => 48,  107 => 45,  105 => 44,  96 => 38,  92 => 37,  78 => 26,  61 => 12,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Tickets{% endblock %}

{% block body %}
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tTickets
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-ticket\"></i> Tickets</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">

          \t\t<!-- small box -->
          \t\t<div class=\"small-box bg-red\">
            \t\t<div class=\"inner\">
              \t\t\t<h3>{{ supports.getTotalItemCount }}</h3>
              \t\t\t<p>Tickets</p>
            \t\t</div>
            \t\t<div class=\"icon\">
              \t\t\t<i class=\"fa fa-ticket\"></i>
            \t\t</div>
            \t\t<a class=\"small-box-footer\">Tickets Count <i class=\"fa fa-arrow-circle-right\"></i></a>
          \t\t</div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
          \t\t\t<a href=\"{{ path('create_support') }}\" class=\"list-group-item\">Create New Ticket</a>
                    <a href=\"{{ path('support') }}\" class=\"list-group-item list-group-item-success\">View Tickets</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
    \t\t\t{% if supports.getTotalItemCount == 0 %}
    \t\t\t\t<div class=\"alert alert-warning\">
    \t\t\t\t\t<p class=\"lead text-center\">You have not created any ticket yet</p>
    \t\t\t\t\t<hr>
    \t\t\t\t\t<a href=\"{{ path('create_support') }}\" class=\"btn btn-info btn-block\" style=\"text-decoration: none\">Create Ticket</a>
    \t\t\t\t</div>
    \t\t\t{% else %}
\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing {{ supports.getPaginationData.firstItemNumber }} to {{ supports.getPaginationData.lastItemNumber }} out of {{ supports.getTotalItemCount }} Ticket{% if supports.getTotalItemCount != 1 %}s{% endif %}</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>#</th>
\t    \t\t\t\t\t\t\t\t<th>Subject</th>
\t    \t\t\t\t\t\t\t\t<th>Status</th>
\t    \t\t\t\t\t\t\t\t<th>Date</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t{% for support in supports %}
\t    \t\t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t\t<td>{{ support.getId() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td><a href=\"{{ path('view_support', {'support' : support.getId()}) }}\">{{ support.getSubject() }}</a></td>
\t    \t\t\t\t\t\t\t\t\t<td>
\t    \t\t\t\t\t\t\t\t\t\t{% if support.getStatus() == 'user_reply' %}
\t    \t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-warning\">User Reply</button>
\t    \t\t\t\t\t\t\t\t\t\t{% elseif support.getStatus() == 'admin_reply' %}
\t    \t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-success\">Admin Reply</button>
\t    \t\t\t\t\t\t\t\t\t\t{% elseif support.getStatus() == 'blank' %}
\t    \t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-info\">Blank</button>
\t    \t\t\t\t\t\t\t\t\t\t{% elseif support.getStatus() == 'closed' %}
\t    \t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-xs btn-danger\">Closed</button>
\t    \t\t\t\t\t\t\t\t\t\t{% endif %}
\t    \t\t\t\t\t\t\t\t\t</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ support.getCreatedDate()|date('d F, Y') }}</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t{% endfor %}
\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    {{ knp_pagination_render(supports) }}
\t                </div>
    \t\t\t{% endif %}
    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section> <!-- End Main Content -->

{% endblock %}", "member/supports.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\member\\supports.html.twig");
    }
}
