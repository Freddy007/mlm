<?php

/* god-mode/adminprofile.html.twig */
class __TwigTemplate_b948fb101b712b355456d4efa229c8965e663f78ad49ec4622f1d9effe733fd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/adminprofile.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0eefd480585e462b09090eea14c01edf4e76248150d55d44713f445cb5452512 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0eefd480585e462b09090eea14c01edf4e76248150d55d44713f445cb5452512->enter($__internal_0eefd480585e462b09090eea14c01edf4e76248150d55d44713f445cb5452512_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/adminprofile.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0eefd480585e462b09090eea14c01edf4e76248150d55d44713f445cb5452512->leave($__internal_0eefd480585e462b09090eea14c01edf4e76248150d55d44713f445cb5452512_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_26a682237fedbc86c365d140d4e91fc1e27b84808e706f26d75339fb940e5809 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26a682237fedbc86c365d140d4e91fc1e27b84808e706f26d75339fb940e5809->enter($__internal_26a682237fedbc86c365d140d4e91fc1e27b84808e706f26d75339fb940e5809_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        
        $__internal_26a682237fedbc86c365d140d4e91fc1e27b84808e706f26d75339fb940e5809->leave($__internal_26a682237fedbc86c365d140d4e91fc1e27b84808e706f26d75339fb940e5809_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_179d7641c65b07fd38da1a42f4504c1faf31ee37f8393fc62d04b22a63da7e03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_179d7641c65b07fd38da1a42f4504c1faf31ee37f8393fc62d04b22a63da7e03->enter($__internal_179d7641c65b07fd38da1a42f4504c1faf31ee37f8393fc62d04b22a63da7e03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "'s Profile</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li class=\"active\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">

            <!-- Left Side -->
            <div class=\"col-md-4\">
                <!-- Links -->
                <div class=\"list-group\">
                    <a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_settings");
        echo "\" class=\"list-group-item list-group-item\">Settings</a>
                    <a href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminprofile");
        echo "\" class=\"list-group-item list-group-item-success\">Profile</a>
                    <a href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminbank_details");
        echo "\" class=\"list-group-item\">Bank Details</a>
                    <a href=\"";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminchange_password");
        echo "\" class=\"list-group-item\">Change Password</a>
                </div> <!-- End Links -->

                <!-- Profile Image -->
                <div class=\"box box-warning\">

                    <div class=\"box-body box-profile\">

                        <img class=\"profile-user-img img-responsive img-circle\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getAvatar", array(), "method")), "html", null, true);
        echo "\" alt=\"User profile picture\">

                        <h3 class=\"profile-username text-center\">";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "</h3>

                        <p class=\"text-muted text-center\">";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getUsername", array(), "method"), "html", null, true);
        echo "</p>

                        <ul class=\"list-group list-group-unbordered\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneOne", array(), "method"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 52
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneTwo", array(), "method") != null)) {
            // line 53
            echo "                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneTwo", array(), "method"), "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        // line 57
        echo "                            <li class=\"list-group-item\">
                                <i class=\"fa fa-envelope-o\"></i> <a class=\"pull-right\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getEmail", array(), "method"), "html", null, true);
        echo "</a>
                            </li>

                        </ul>
                    </div>
                </div> <!-- End Profile Image -->


            </div>

            <!-- Right Side -->
            <div class=\"col-md-8\">

                <div class=\"box box-info\">

                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Edit Personal Details</h3>
                    </div>

                    <div class=\"box-body\">

                        ";
        // line 79
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminprofile"), "attr" => array("class" => "form-horizontal")));
        echo "

                        ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "personalSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 82
            echo "                            <div class=\"alert alert-success alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <p>";
            // line 84
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "
                        <div class=\"form-group\">
                            <label for=\"inputUsername\" class=\"col-sm-2 control-label\">Username</label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\">";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getUsername", array(), "method"), "html", null, true);
        echo "</p>
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"inputEmail\" class=\"col-sm-2 control-label\">Email</label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\">";
        // line 100
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getEmail", array(), "method"), "html", null, true);
        echo "</p>
                            </div>
                        </div>

                        <span class=\"text-danger\">";
        // line 104
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "name", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            ";
        // line 106
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "name", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Full Name"));
        echo "

                            <div class=\"col-sm-10\">
                                ";
        // line 109
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "name", array()), 'widget');
        echo "
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"inputAvatar\" class=\"col-sm-2 control-label\">Avatar</label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\"><img class=\"profile-user-img img-responsive\" src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getAvatar", array(), "method")), "html", null, true);
        echo "\" style=\"margin: 0; border: none;\"></p>
                            </div>
                        </div>

                        <span class=\"text-danger\">";
        // line 121
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "avatarImage", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            ";
        // line 123
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "avatarImage", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Change Avatar"));
        echo "

                            <div class=\"col-sm-10\">
                                ";
        // line 126
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "avatarImage", array()), 'widget');
        echo "
                            </div>
                        </div>


                        <span class=\"text-danger\">";
        // line 131
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "phone_one", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            ";
        // line 133
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "phone_one", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label")) + (twig_test_empty($_label_ = $this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "phone_one", array())) ? array() : array("label" => $_label_)));
        echo "

                            <div class=\"col-sm-10\">
                                ";
        // line 136
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "phone_one", array()), 'widget');
        echo "
                            </div>
                        </div>

                        <span class=\"text-danger\">";
        // line 140
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "phone_two", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            ";
        // line 142
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "phone_two", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Phone No 2"));
        echo "

                            <div class=\"col-sm-10\">
                                ";
        // line 145
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), "phone_two", array()), 'widget');
        echo "
                            </div>
                        </div>

                        ";
        // line 150
        echo "                        ";
        // line 151
        echo "                            ";
        // line 152
        echo "
                            ";
        // line 154
        echo "                                ";
        // line 155
        echo "                            ";
        // line 156
        echo "                        ";
        // line 157
        echo "
                        ";
        // line 159
        echo "                        ";
        // line 160
        echo "                            ";
        // line 161
        echo "
                            ";
        // line 163
        echo "                                ";
        // line 164
        echo "                            ";
        // line 165
        echo "                        ";
        // line 166
        echo "
                        ";
        // line 168
        echo "                        ";
        // line 169
        echo "                            ";
        // line 170
        echo "
                            ";
        // line 172
        echo "                                ";
        // line 173
        echo "                            ";
        // line 174
        echo "                        ";
        // line 175
        echo "
                        <div class=\"row\">
                            <div class=\"col-sm-12\">
                                <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Save Personal Details</button>
                            </div>
                        </div>

                        ";
        // line 182
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["personalForm"]) ? $context["personalForm"] : $this->getContext($context, "personalForm")), 'form_end');
        echo "
                    </div>

                </div>

            </div>

        </div>

    </section>

";
        
        $__internal_179d7641c65b07fd38da1a42f4504c1faf31ee37f8393fc62d04b22a63da7e03->leave($__internal_179d7641c65b07fd38da1a42f4504c1faf31ee37f8393fc62d04b22a63da7e03_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/adminprofile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  370 => 182,  361 => 175,  359 => 174,  357 => 173,  355 => 172,  352 => 170,  350 => 169,  348 => 168,  345 => 166,  343 => 165,  341 => 164,  339 => 163,  336 => 161,  334 => 160,  332 => 159,  329 => 157,  327 => 156,  325 => 155,  323 => 154,  320 => 152,  318 => 151,  316 => 150,  309 => 145,  303 => 142,  298 => 140,  291 => 136,  285 => 133,  280 => 131,  272 => 126,  266 => 123,  261 => 121,  254 => 117,  243 => 109,  237 => 106,  232 => 104,  225 => 100,  214 => 92,  207 => 87,  198 => 84,  194 => 82,  190 => 81,  185 => 79,  161 => 58,  158 => 57,  152 => 54,  149 => 53,  147 => 52,  142 => 50,  135 => 46,  130 => 44,  125 => 42,  114 => 34,  110 => 33,  106 => 32,  102 => 31,  86 => 18,  82 => 17,  77 => 15,  72 => 12,  63 => 9,  60 => 8,  56 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}{{ app.user.getName() }}{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('notice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>{{ app.user.getName() }}'s Profile</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li class=\"active\">{{ app.user.getName() }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">

            <!-- Left Side -->
            <div class=\"col-md-4\">
                <!-- Links -->
                <div class=\"list-group\">
                    <a href=\"{{ path('god_mode_settings') }}\" class=\"list-group-item list-group-item\">Settings</a>
                    <a href=\"{{ path('adminprofile') }}\" class=\"list-group-item list-group-item-success\">Profile</a>
                    <a href=\"{{ path('adminbank_details') }}\" class=\"list-group-item\">Bank Details</a>
                    <a href=\"{{ path('adminchange_password') }}\" class=\"list-group-item\">Change Password</a>
                </div> <!-- End Links -->

                <!-- Profile Image -->
                <div class=\"box box-warning\">

                    <div class=\"box-body box-profile\">

                        <img class=\"profile-user-img img-responsive img-circle\" src=\"{{ asset(app.user.getAvatar()) }}\" alt=\"User profile picture\">

                        <h3 class=\"profile-username text-center\">{{ app.user.getName() }}</h3>

                        <p class=\"text-muted text-center\">{{ app.user.getUsername() }}</p>

                        <ul class=\"list-group list-group-unbordered\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">{{ app.user.getPhoneOne() }}</a>
                            </li>
                            {% if app.user.getPhoneTwo() != null %}
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">{{ app.user.getPhoneTwo() }}</a>
                                </li>
                            {% endif %}
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-envelope-o\"></i> <a class=\"pull-right\">{{ app.user.getEmail() }}</a>
                            </li>

                        </ul>
                    </div>
                </div> <!-- End Profile Image -->


            </div>

            <!-- Right Side -->
            <div class=\"col-md-8\">

                <div class=\"box box-info\">

                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Edit Personal Details</h3>
                    </div>

                    <div class=\"box-body\">

                        {{ form_start(personalForm, {'action': path('adminprofile'), 'attr': {'class': 'form-horizontal'}}) }}

                        {% for flash_message in app.session.flashBag.get('personalSuccess') %}
                            <div class=\"alert alert-success alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <p>{{ flash_message }}</p>
                            </div>
                        {% endfor %}

                        <div class=\"form-group\">
                            <label for=\"inputUsername\" class=\"col-sm-2 control-label\">Username</label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\">{{ app.user.getUsername() }}</p>
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"inputEmail\" class=\"col-sm-2 control-label\">Email</label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\">{{ app.user.getEmail() }}</p>
                            </div>
                        </div>

                        <span class=\"text-danger\">{{ form_errors(personalForm.name) }}</span>
                        <div class=\"form-group\">
                            {{ form_label(personalForm.name, 'Full Name', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                            <div class=\"col-sm-10\">
                                {{ form_widget(personalForm.name) }}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"inputAvatar\" class=\"col-sm-2 control-label\">Avatar</label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\"><img class=\"profile-user-img img-responsive\" src=\"{{ asset(app.user.getAvatar()) }}\" style=\"margin: 0; border: none;\"></p>
                            </div>
                        </div>

                        <span class=\"text-danger\">{{ form_errors(personalForm.avatarImage) }}</span>
                        <div class=\"form-group\">
                            {{ form_label(personalForm.avatarImage, 'Change Avatar', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                            <div class=\"col-sm-10\">
                                {{ form_widget(personalForm.avatarImage) }}
                            </div>
                        </div>


                        <span class=\"text-danger\">{{ form_errors(personalForm.phone_one) }}</span>
                        <div class=\"form-group\">
                            {{ form_label(personalForm.phone_one, personalForm.phone_one, { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                            <div class=\"col-sm-10\">
                                {{ form_widget(personalForm.phone_one) }}
                            </div>
                        </div>

                        <span class=\"text-danger\">{{ form_errors(personalForm.phone_two) }}</span>
                        <div class=\"form-group\">
                            {{ form_label(personalForm.phone_two, 'Phone No 2', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                            <div class=\"col-sm-10\">
                                {{ form_widget(personalForm.phone_two) }}
                            </div>
                        </div>

                        {#<span class=\"text-danger\">{{ form_errors(personalForm.title) }}</span>#}
                        {#<div class=\"form-group\">#}
                            {#{{ form_label(personalForm.title, 'Title', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}#}

                            {#<div class=\"col-sm-10\">#}
                                {#{{ form_widget(personalForm.title) }}#}
                            {#</div>#}
                        {#</div>#}

                        {#<span class=\"text-danger\">{{ form_errors(personalForm.bitcoin_address) }}</span>#}
                        {#<div class=\"form-group\">#}
                            {#{{ form_label(personalForm.bitcoin_address, 'Bitcoin Address', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}#}

                            {#<div class=\"col-sm-10\">#}
                                {#{{ form_widget(personalForm.bitcoin_address) }}#}
                            {#</div>#}
                        {#</div>#}

                        {#<span class=\"text-danger\">{{ form_errors(personalForm.resident_state) }}</span>#}
                        {#<div class=\"form-group\">#}
                            {#{{ form_label(personalForm.resident_state, 'Resident State', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}#}

                            {#<div class=\"col-sm-10\">#}
                                {#{{ form_widget(personalForm.resident_state) }}#}
                            {#</div>#}
                        {#</div>#}

                        <div class=\"row\">
                            <div class=\"col-sm-12\">
                                <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Save Personal Details</button>
                            </div>
                        </div>

                        {{ form_end(personalForm) }}
                    </div>

                </div>

            </div>

        </div>

    </section>

{% endblock %}", "god-mode/adminprofile.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\adminprofile.html.twig");
    }
}
