<?php

/* god-mode/edit-user.html.twig */
class __TwigTemplate_cf92098e7b9b77bb66cab3e1f2b37ec7770862e46a78a8fd35c727189dbbd160 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/edit-user.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac8bcd06aad30ad17086db53584af8e17dac5d5989db5e9bc8d0ec021d541ab7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac8bcd06aad30ad17086db53584af8e17dac5d5989db5e9bc8d0ec021d541ab7->enter($__internal_ac8bcd06aad30ad17086db53584af8e17dac5d5989db5e9bc8d0ec021d541ab7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/edit-user.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ac8bcd06aad30ad17086db53584af8e17dac5d5989db5e9bc8d0ec021d541ab7->leave($__internal_ac8bcd06aad30ad17086db53584af8e17dac5d5989db5e9bc8d0ec021d541ab7_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_a3d1c903ebea1b2b81f0f0940c84049569ae5158ca1edc6b87fbf939c261c23a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3d1c903ebea1b2b81f0f0940c84049569ae5158ca1edc6b87fbf939c261c23a->enter($__internal_a3d1c903ebea1b2b81f0f0940c84049569ae5158ca1edc6b87fbf939c261c23a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Edit ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "getName", array(), "method"), "html", null, true);
        
        $__internal_a3d1c903ebea1b2b81f0f0940c84049569ae5158ca1edc6b87fbf939c261c23a->leave($__internal_a3d1c903ebea1b2b81f0f0940c84049569ae5158ca1edc6b87fbf939c261c23a_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_650872d0728b2d324aa5a9cfa713139cc44298e5650f39258b86954817b5901c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_650872d0728b2d324aa5a9cfa713139cc44298e5650f39258b86954817b5901c->enter($__internal_650872d0728b2d324aa5a9cfa713139cc44298e5650f39258b86954817b5901c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Edit ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "getName", array(), "method"), "html", null, true);
        echo "</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li><a href=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\"><i class=\"fa fa-users\"></i> Admin Users</a></li>
            <li class=\"active\"><i class=\"fa fa-user\"></i> ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "getName", array(), "method"), "html", null, true);
        echo "</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item\">List Admin</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item active\">List Users</a>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item\">Wallet Update</a>
                    <a href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item\">List Payments</a>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <h3>Edit ";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "getName", array(), "method"), "html", null, true);
        echo "</h3>
                ";
        // line 58
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_user", array("user" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "getId", array(), "method"))), "attr" => array("class" => "form-horizontal")));
        echo "

                    <span class=\"text-danger\">";
        // line 60
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'errors');
        echo "</span>
                    <div class=\"form-group\">
                        ";
        // line 62
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Username"));
        echo "

                        <div class=\"col-sm-9\">
                            ";
        // line 65
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget');
        echo "
                        </div>
                    </div>

                    <span class=\"text-danger\">";
        // line 69
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'errors');
        echo "</span>
                    <div class=\"form-group\">
                        ";
        // line 71
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Name"));
        echo "

                        <div class=\"col-sm-9\">
                            ";
        // line 74
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget');
        echo "
                        </div>
                    </div>

                    <span class=\"text-danger\">";
        // line 78
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "</span>
                    <div class=\"form-group\">
                        ";
        // line 80
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Email"));
        echo "

                        <div class=\"col-sm-9\">
                            ";
        // line 83
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "
                        </div>
                    </div>


                    <span class=\"text-danger\">";
        // line 88
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phone_one", array()), 'errors');
        echo "</span>
                    <div class=\"form-group\">
                        ";
        // line 90
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phone_one", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Phone 1"));
        echo "

                        <div class=\"col-sm-9\">
                            ";
        // line 93
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phone_one", array()), 'widget');
        echo "
                        </div>
                    </div>
                    <span class=\"text-danger\">";
        // line 96
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "purse", array()), 'errors');
        echo "</span>
                    <div class=\"form-group\">
                        ";
        // line 98
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "purse", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Wallet"));
        echo "

                        <div class=\"col-sm-9\">
                            ";
        // line 101
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "purse", array()), 'widget');
        echo "
                        </div>
                    </div>

                    ";
        // line 106
        echo "                    ";
        // line 107
        echo "                        ";
        // line 108
        echo "
                        ";
        // line 110
        echo "                            ";
        // line 111
        echo "                        ";
        // line 112
        echo "                    ";
        // line 113
        echo "


                    <div class=\"row\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Update</button>
                        </div>
                    </div>
                ";
        // line 121
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_650872d0728b2d324aa5a9cfa713139cc44298e5650f39258b86954817b5901c->leave($__internal_650872d0728b2d324aa5a9cfa713139cc44298e5650f39258b86954817b5901c_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/edit-user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  315 => 121,  305 => 113,  303 => 112,  301 => 111,  299 => 110,  296 => 108,  294 => 107,  292 => 106,  285 => 101,  279 => 98,  274 => 96,  268 => 93,  262 => 90,  257 => 88,  249 => 83,  243 => 80,  238 => 78,  231 => 74,  225 => 71,  220 => 69,  213 => 65,  207 => 62,  202 => 60,  197 => 58,  193 => 57,  184 => 51,  180 => 50,  176 => 49,  172 => 48,  168 => 47,  164 => 46,  160 => 45,  156 => 44,  152 => 43,  148 => 42,  144 => 41,  140 => 40,  136 => 39,  132 => 38,  128 => 37,  124 => 36,  110 => 25,  106 => 24,  102 => 23,  97 => 21,  92 => 18,  83 => 15,  80 => 14,  76 => 13,  73 => 12,  64 => 9,  61 => 8,  57 => 7,  54 => 6,  48 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Edit {{ user.getName() }}{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Edit {{ user.getName() }}</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li><a href=\"{{ path('list_admin') }}\"><i class=\"fa fa-users\"></i> Admin Users</a></li>
            <li class=\"active\"><i class=\"fa fa-user\"></i> {{ user.getName() }}</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item active\">List Users</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item\">Wallet Update</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <h3>Edit {{ user.getName() }}</h3>
                {{ form_start(form, {'action': path('edit_user', {'user' : user.getId()}), 'attr': {'class': 'form-horizontal'}}) }}

                    <span class=\"text-danger\">{{ form_errors(form.username) }}</span>
                    <div class=\"form-group\">
                        {{ form_label(form.username, 'Username', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                        <div class=\"col-sm-9\">
                            {{ form_widget(form.username) }}
                        </div>
                    </div>

                    <span class=\"text-danger\">{{ form_errors(form.name) }}</span>
                    <div class=\"form-group\">
                        {{ form_label(form.name, 'Name', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                        <div class=\"col-sm-9\">
                            {{ form_widget(form.name) }}
                        </div>
                    </div>

                    <span class=\"text-danger\">{{ form_errors(form.email) }}</span>
                    <div class=\"form-group\">
                        {{ form_label(form.email, 'Email', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                        <div class=\"col-sm-9\">
                            {{ form_widget(form.email) }}
                        </div>
                    </div>


                    <span class=\"text-danger\">{{ form_errors(form.phone_one) }}</span>
                    <div class=\"form-group\">
                        {{ form_label(form.phone_one, 'Phone 1', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                        <div class=\"col-sm-9\">
                            {{ form_widget(form.phone_one) }}
                        </div>
                    </div>
                    <span class=\"text-danger\">{{ form_errors(form.purse) }}</span>
                    <div class=\"form-group\">
                        {{ form_label(form.purse, 'Wallet', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                        <div class=\"col-sm-9\">
                            {{ form_widget(form.purse) }}
                        </div>
                    </div>

                    {#<span class=\"text-danger\">{{ form_errors(form.sex) }}</span>#}
                    {#<div class=\"form-group\">#}
                        {#{{ form_label(form.sex, 'Gender', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}#}

                        {#<div class=\"col-sm-9\">#}
                            {#{{ form_widget(form.sex) }}#}
                        {#</div>#}
                    {#</div>#}



                    <div class=\"row\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Update</button>
                        </div>
                    </div>
                {{ form_end(form) }}
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}", "god-mode/edit-user.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\edit-user.html.twig");
    }
}
