<?php

/* god-mode/dashboard.html.twig */
class __TwigTemplate_790c58f6c44516911f0900c21e510f2874ec2c31ccb7ac1c4c2c324f04a62ba4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/dashboard.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ff2ba143ad0cf41fe3d592daefb46015c2cd18754e4dc95a1cf008687c66b81 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ff2ba143ad0cf41fe3d592daefb46015c2cd18754e4dc95a1cf008687c66b81->enter($__internal_7ff2ba143ad0cf41fe3d592daefb46015c2cd18754e4dc95a1cf008687c66b81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/dashboard.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7ff2ba143ad0cf41fe3d592daefb46015c2cd18754e4dc95a1cf008687c66b81->leave($__internal_7ff2ba143ad0cf41fe3d592daefb46015c2cd18754e4dc95a1cf008687c66b81_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_3315c85f19fcbf39fb198b9c3c44eecea67b14e18e63bf1c03dc9bcf2f6ee5f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3315c85f19fcbf39fb198b9c3c44eecea67b14e18e63bf1c03dc9bcf2f6ee5f3->enter($__internal_3315c85f19fcbf39fb198b9c3c44eecea67b14e18e63bf1c03dc9bcf2f6ee5f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "God Mode Dashboard";
        
        $__internal_3315c85f19fcbf39fb198b9c3c44eecea67b14e18e63bf1c03dc9bcf2f6ee5f3->leave($__internal_3315c85f19fcbf39fb198b9c3c44eecea67b14e18e63bf1c03dc9bcf2f6ee5f3_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_bd6947bec367dd5557a7f4ad1374da374c75a5e3eca60dabd7bea7655a2c03d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd6947bec367dd5557a7f4ad1374da374c75a5e3eca60dabd7bea7655a2c03d1->enter($__internal_bd6947bec367dd5557a7f4ad1374da374c75a5e3eca60dabd7bea7655a2c03d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 7
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 8
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "

    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "failureNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "deleteSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 19
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 20
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "

    ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "deleteFailure"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 26
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 27
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            God Mode Dashboard
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"active\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
        <!-- Small boxes (Stat box) -->
        <div class=\"row\">
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-aqua\">
                    <div class=\"inner\">
                        <p>View Admins</p>
                    </div>
                    <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-green\">
                    <div class=\"inner\">
                        <p>View Packages</p>
                    </div>
                    <a href=\"";
        // line 59
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-red\">
                    <div class=\"inner\">
                        <p>View News</p>
                    </div>
                    <a href=\"";
        // line 68
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-yellow\">
                    <div class=\"inner\">
                        <p>View Notice Board</p>
                    </div>
                    <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
        </div> <!-- End Small boxes (Stat box) -->

        <!-- Info Boxes -->
        <div class=\"row\">
            <div class=\"col-sm-3\">
                <div class=\"info-box\">
                    <span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-users\"></i></span>
                     <!-- Info-box-content -->
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Total Users</span>
                        <span class=\"info-box-number\">";
        // line 90
        echo twig_escape_filter($this->env, (isset($context["usersCount"]) ? $context["usersCount"] : $this->getContext($context, "usersCount")), "html", null, true);
        echo "</span>
                    </div> <!-- End info-box-content -->

                </div>
            </div>
            <div class=\"col-sm-3\">
                <div class=\"info-box\">
                    <span class=\"info-box-icon bg-green\"><i class=\"fa fa-users\"></i></span>
                    <!-- Info-box-content -->
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Active Users</span>
                        <span class=\"info-box-number\">";
        // line 101
        echo twig_escape_filter($this->env, (isset($context["activeUsersCount"]) ? $context["activeUsersCount"] : $this->getContext($context, "activeUsersCount")), "html", null, true);
        echo "</span>
                    </div> <!-- End info-box-content -->
                </div>
            </div>
            <div class=\"col-sm-3\">
                <div class=\"info-box\">
                    <span class=\"info-box-icon bg-red\"><i class=\"fa fa-users\"></i></span>
                     <!-- Info-box-content -->
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Blocked Users</span>
                        <span class=\"info-box-number\">";
        // line 111
        echo twig_escape_filter($this->env, (isset($context["blockedUsersCount"]) ? $context["blockedUsersCount"] : $this->getContext($context, "blockedUsersCount")), "html", null, true);
        echo "</span>
                    </div> <!-- End info-box-content -->
                </div>
            </div>
            <div class=\"col-sm-3\">
                <div class=\"info-box\">
                    <span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-archive\"></i></span>
                    <!-- Info-box-content -->
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Total Packs</span>
                        <span class=\"info-box-number\">";
        // line 121
        echo twig_escape_filter($this->env, (isset($context["packsCount"]) ? $context["packsCount"] : $this->getContext($context, "packsCount")), "html", null, true);
        echo "</span>
                    </div> <!-- End info-box-content -->
                </div>
            </div>
        </div> <!-- End Info Boxes -->

        <!-- Extras -->
        <div class=\"row\">
            <div class=\"col-sm-6\">
                <a href=\"";
        // line 130
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"btn btn-lg btn-block btn-warning btn-flat\"><i class=\"fa fa-ban\"></i> Unblock User</a>
            </div>
            <div class=\"col-sm-6\">
                <a href=\"";
        // line 133
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"btn btn-lg btn-block btn-info btn-flat\"><i class=\"fa fa-unlock\"></i> Reset Password</a>
            </div>
        </div> <!-- End Extras -->

        <hr>

        <!-- Mailbox -->
        <div class=\"row\">
            <div class=\"col-sm-4\">
                <a href=\"";
        // line 142
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"btn btn-lg btn-block btn-success btn-flat\"><i class=\"fa fa-envelope\"></i> Mail</a>
            </div>
            <div class=\"col-sm-4\">
                <a href=\"";
        // line 145
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"btn btn-lg btn-block btn-danger btn-flat\"><i class=\"fa fa-comments-o\"></i> Approve Testimonies</a>
            </div>
            <div class=\"col-sm-4\">
                <a href=\"";
        // line 148
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"btn btn-lg btn-block btn-success btn-flat\"><i class=\"fa fa-users\"></i> List Users</a>
            </div>
        </div>

        <hr>

        <div class=\"row\">
            <div class=\"col-sm-6\">
                <a href=\"";
        // line 156
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("change_password");
        echo "\" class=\"btn btn-lg btn-block btn-info btn-flat\"><i class=\"fa fa-unlock\"></i> Change Admin Password</a>
            </div>
            <div class=\"col-sm-6\">
                <a href=\"";
        // line 159
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("run_cron");
        echo "\" class=\"btn btn-lg btn-block btn-danger btn-flat\"><i class=\"fa fa-warning\"></i> Block Unpaid Users</a>
            </div>
        </div>

        <hr>

        ";
        // line 166
        echo "            ";
        // line 167
        echo "                ";
        // line 168
        echo "            ";
        // line 169
        echo "            ";
        // line 170
        echo "                ";
        // line 171
        echo "            ";
        // line 172
        echo "        ";
        // line 173
        echo "
        <hr>

        <div class=\"row\">
            <div class=\"col-sm-6\">
                <a href=\"";
        // line 178
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("move_jury_cases");
        echo "\" class=\"btn btn-lg btn-block btn-warning btn-flat\"><i class=\"fa fa-empire\"></i> Move Cases to Jury</a>
            </div>
            <div class=\"col-sm-6\">
                <a href=\"";
        // line 181
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("block_scam_users");
        echo "\" class=\"btn btn-lg btn-block btn-warning btn-flat\"><i class=\"fa fa-ban\"></i> Block Jury Users</a>
            </div>
        </div>

        <hr>

        <hr>

        <!-- Mailbox -->
        <div class=\"row\">

            ";
        // line 192
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packs"]) ? $context["packs"] : $this->getContext($context, "packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 193
            echo "                <div class=\"col-sm-6\">
                    <a href=\"";
            // line 194
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_merge_users", array("pack" => $this->getAttribute($context["pack"], "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-lg btn-block btn-success btn-flat\"><i class=\"fa fa-envelope\"></i> Merge Users in ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getName", array(), "method"), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getFormattedAmount", array(), "method"), "html", null, true);
            echo ")</a>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 197
        echo "        </div>
    </section>


    ";
        // line 202
        echo "        ";
        // line 203
        echo "        ";
        // line 204
        echo "            ";
        // line 205
        echo "            ";
        // line 206
        echo "            ";
        // line 207
        echo "            ";
        // line 208
        echo "        ";
        // line 209
        echo "        ";
        // line 210
        echo "        ";
        // line 211
        echo "        ";
        // line 212
        echo "            ";
        // line 213
        echo "                ";
        // line 214
        echo "                ";
        // line 215
        echo "                ";
        // line 216
        echo "
                ";
        // line 218
        echo "            ";
        // line 219
        echo "        ";
        // line 220
        echo "        ";
        // line 221
        echo "    ";
        // line 222
        echo "

";
        
        $__internal_bd6947bec367dd5557a7f4ad1374da374c75a5e3eca60dabd7bea7655a2c03d1->leave($__internal_bd6947bec367dd5557a7f4ad1374da374c75a5e3eca60dabd7bea7655a2c03d1_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 222,  414 => 221,  412 => 220,  410 => 219,  408 => 218,  405 => 216,  403 => 215,  401 => 214,  399 => 213,  397 => 212,  395 => 211,  393 => 210,  391 => 209,  389 => 208,  387 => 207,  385 => 206,  383 => 205,  381 => 204,  379 => 203,  377 => 202,  371 => 197,  357 => 194,  354 => 193,  350 => 192,  336 => 181,  330 => 178,  323 => 173,  321 => 172,  319 => 171,  317 => 170,  315 => 169,  313 => 168,  311 => 167,  309 => 166,  300 => 159,  294 => 156,  283 => 148,  277 => 145,  271 => 142,  259 => 133,  253 => 130,  241 => 121,  228 => 111,  215 => 101,  201 => 90,  185 => 77,  173 => 68,  161 => 59,  149 => 50,  127 => 30,  118 => 27,  115 => 26,  111 => 25,  107 => 23,  98 => 20,  95 => 19,  90 => 18,  81 => 15,  78 => 14,  74 => 13,  70 => 11,  61 => 8,  58 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}God Mode Dashboard{% endblock %}

{% block body %}
    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}


    {% for flash_message in app.session.flashBag.get('failureNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}
    {% for flash_message in app.session.flashBag.get('deleteSuccess') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}


    {% for flash_message in app.session.flashBag.get('deleteFailure') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            God Mode Dashboard
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"active\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
        <!-- Small boxes (Stat box) -->
        <div class=\"row\">
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-aqua\">
                    <div class=\"inner\">
                        <p>View Admins</p>
                    </div>
                    <a href=\"{{ path('list_admin') }}\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-green\">
                    <div class=\"inner\">
                        <p>View Packages</p>
                    </div>
                    <a href=\"{{ path('list_pack') }}\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-red\">
                    <div class=\"inner\">
                        <p>View News</p>
                    </div>
                    <a href=\"{{ path('list_news') }}\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-yellow\">
                    <div class=\"inner\">
                        <p>View Notice Board</p>
                    </div>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
        </div> <!-- End Small boxes (Stat box) -->

        <!-- Info Boxes -->
        <div class=\"row\">
            <div class=\"col-sm-3\">
                <div class=\"info-box\">
                    <span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-users\"></i></span>
                     <!-- Info-box-content -->
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Total Users</span>
                        <span class=\"info-box-number\">{{ usersCount }}</span>
                    </div> <!-- End info-box-content -->

                </div>
            </div>
            <div class=\"col-sm-3\">
                <div class=\"info-box\">
                    <span class=\"info-box-icon bg-green\"><i class=\"fa fa-users\"></i></span>
                    <!-- Info-box-content -->
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Active Users</span>
                        <span class=\"info-box-number\">{{ activeUsersCount }}</span>
                    </div> <!-- End info-box-content -->
                </div>
            </div>
            <div class=\"col-sm-3\">
                <div class=\"info-box\">
                    <span class=\"info-box-icon bg-red\"><i class=\"fa fa-users\"></i></span>
                     <!-- Info-box-content -->
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Blocked Users</span>
                        <span class=\"info-box-number\">{{ blockedUsersCount }}</span>
                    </div> <!-- End info-box-content -->
                </div>
            </div>
            <div class=\"col-sm-3\">
                <div class=\"info-box\">
                    <span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-archive\"></i></span>
                    <!-- Info-box-content -->
                    <div class=\"info-box-content\">
                        <span class=\"info-box-text\">Total Packs</span>
                        <span class=\"info-box-number\">{{ packsCount }}</span>
                    </div> <!-- End info-box-content -->
                </div>
            </div>
        </div> <!-- End Info Boxes -->

        <!-- Extras -->
        <div class=\"row\">
            <div class=\"col-sm-6\">
                <a href=\"{{ path('unblock_user') }}\" class=\"btn btn-lg btn-block btn-warning btn-flat\"><i class=\"fa fa-ban\"></i> Unblock User</a>
            </div>
            <div class=\"col-sm-6\">
                <a href=\"{{ path('reset_password') }}\" class=\"btn btn-lg btn-block btn-info btn-flat\"><i class=\"fa fa-unlock\"></i> Reset Password</a>
            </div>
        </div> <!-- End Extras -->

        <hr>

        <!-- Mailbox -->
        <div class=\"row\">
            <div class=\"col-sm-4\">
                <a href=\"{{ path('god_mode_mails') }}\" class=\"btn btn-lg btn-block btn-success btn-flat\"><i class=\"fa fa-envelope\"></i> Mail</a>
            </div>
            <div class=\"col-sm-4\">
                <a href=\"{{ path('list_approve_reviews') }}\" class=\"btn btn-lg btn-block btn-danger btn-flat\"><i class=\"fa fa-comments-o\"></i> Approve Testimonies</a>
            </div>
            <div class=\"col-sm-4\">
                <a href=\"{{ path('list_users') }}\" class=\"btn btn-lg btn-block btn-success btn-flat\"><i class=\"fa fa-users\"></i> List Users</a>
            </div>
        </div>

        <hr>

        <div class=\"row\">
            <div class=\"col-sm-6\">
                <a href=\"{{ path('change_password') }}\" class=\"btn btn-lg btn-block btn-info btn-flat\"><i class=\"fa fa-unlock\"></i> Change Admin Password</a>
            </div>
            <div class=\"col-sm-6\">
                <a href=\"{{ path('run_cron') }}\" class=\"btn btn-lg btn-block btn-danger btn-flat\"><i class=\"fa fa-warning\"></i> Block Unpaid Users</a>
            </div>
        </div>

        <hr>

        {#<div class=\"row\">#}
            {#<div class=\"col-sm-6\">#}
                {#<a target=\"_blank\" href=\"http://docs.nairapal.club\" class=\"btn btn-lg btn-block btn-success btn-flat\"><i class=\"fa fa-book\"></i> View Docs</a>#}
            {#</div>#}
            {#<div class=\"col-sm-6\">#}
                {#<a target=\"_blank\" href=\"http://moneymachine.nairapal.club\" class=\"btn btn-lg btn-block btn-success btn-flat\"><i class=\"fa fa-apple\"></i> Bonus Script</a>#}
            {#</div>#}
        {#</div>#}

        <hr>

        <div class=\"row\">
            <div class=\"col-sm-6\">
                <a href=\"{{ path('move_jury_cases') }}\" class=\"btn btn-lg btn-block btn-warning btn-flat\"><i class=\"fa fa-empire\"></i> Move Cases to Jury</a>
            </div>
            <div class=\"col-sm-6\">
                <a href=\"{{ path('block_scam_users') }}\" class=\"btn btn-lg btn-block btn-warning btn-flat\"><i class=\"fa fa-ban\"></i> Block Jury Users</a>
            </div>
        </div>

        <hr>

        <hr>

        <!-- Mailbox -->
        <div class=\"row\">

            {% for pack in packs %}
                <div class=\"col-sm-6\">
                    <a href=\"{{ path('god_mode_merge_users', {'pack' : pack.getId()}) }}\" class=\"btn btn-lg btn-block btn-success btn-flat\"><i class=\"fa fa-envelope\"></i> Merge Users in {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }})</a>
                </div>
            {% endfor %}
        </div>
    </section>


    {#<table class=\"table table-hover table-responsive table-striped\">#}
        {#<thead>#}
        {#<tr>#}
            {#<th>Feeder</th>#}
            {#<th>Receiver</th>#}
            {#<th>Feeder Time Start</th>#}
            {#<th>Recommit</th>#}
        {#</tr>#}
        {#</thead>#}
        {#<tbody>#}
        {#{% for user in notPaidUsers %}#}
            {#<tr>#}
                {#<td>{{ user.getFeeder() }}</td>#}
                {#<td>{{ user.getReceiver() }}</td>#}
                {#<td>#}

                {#</td>#}
            {#</tr>#}
        {#{% endfor %}#}
        {#</tbody>#}
    {#</table>#}


{% endblock %}", "god-mode/dashboard.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\dashboard.html.twig");
    }
}
