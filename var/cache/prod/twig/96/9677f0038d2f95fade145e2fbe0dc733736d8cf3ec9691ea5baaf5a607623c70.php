<?php

/* public/login.html.twig */
class __TwigTemplate_3eb5d61a8c6e6b1c6254f6cd5aa93cd5073ba8506e08cdc4625a26759f62abab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("public.base.html.twig", "public/login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "public.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13aaf5e96a960e30f18cacb1e5540a991bbae9a2e4863025096ef8d1805598b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13aaf5e96a960e30f18cacb1e5540a991bbae9a2e4863025096ef8d1805598b6->enter($__internal_13aaf5e96a960e30f18cacb1e5540a991bbae9a2e4863025096ef8d1805598b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_13aaf5e96a960e30f18cacb1e5540a991bbae9a2e4863025096ef8d1805598b6->leave($__internal_13aaf5e96a960e30f18cacb1e5540a991bbae9a2e4863025096ef8d1805598b6_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_efcfec96868cda01d6eeda339f291068a3915aa763ad16d763a6ee35d62310ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efcfec96868cda01d6eeda339f291068a3915aa763ad16d763a6ee35d62310ac->enter($__internal_efcfec96868cda01d6eeda339f291068a3915aa763ad16d763a6ee35d62310ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Sign In";
        
        $__internal_efcfec96868cda01d6eeda339f291068a3915aa763ad16d763a6ee35d62310ac->leave($__internal_efcfec96868cda01d6eeda339f291068a3915aa763ad16d763a6ee35d62310ac_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_e003d115819065e43f2e42089b59f2e90f757cfa859b0d4ef9c79ce24c465b50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e003d115819065e43f2e42089b59f2e90f757cfa859b0d4ef9c79ce24c465b50->enter($__internal_e003d115819065e43f2e42089b59f2e90f757cfa859b0d4ef9c79ce24c465b50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Sign In</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">
                <div class=\"login-box\" style=\"margin: 0 auto !important\">
                    <!-- /.login-logo -->
                    <div class=\"login-box-body\">
                        ";
        // line 17
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 18
            echo "                            <div class=\"alert alert-danger\">
                                ";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array())), "html", null, true);
            echo "
                            </div>
                        ";
        }
        // line 22
        echo "
                        <form action=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login_check");
        echo "\" method=\"post\">
                            <div class=\"form-group has-feedback\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"username\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\">
                                <span class=\"fa fa-user-o form-control-feedback\"></span>
                            </div>
                            <div class=\"form-group has-feedback\">
                                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\">
                                <span class=\"fa fa-key form-control-feedback\"></span>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-xs-8\">
                                    <div class=\"checkbox icheck\">
                                        <label>
                                            <input type=\"checkbox\" name=\"_remember_me\"> Remember Me
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class=\"col-xs-4\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Sign In</button>
                                </div>
                                <!-- /.col -->
                            </div>
                            <input type=\"hidden\" name=\"ver\" value=\"u\">
                        </form>


                        <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("forgot_password");
        echo "\">Forgot password</a> | <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register_pack");
        echo "\" class=\"text-center\">Register</a>
                    </div>
                    <!-- /.login-box-body -->
                </div>
                <!-- /.login-box -->
            </div>
        </div>
    </section>

";
        
        $__internal_e003d115819065e43f2e42089b59f2e90f757cfa859b0d4ef9c79ce24c465b50->leave($__internal_e003d115819065e43f2e42089b59f2e90f757cfa859b0d4ef9c79ce24c465b50_prof);

    }

    public function getTemplateName()
    {
        return "public/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 50,  85 => 25,  80 => 23,  77 => 22,  71 => 19,  68 => 18,  66 => 17,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'public.base.html.twig' %}

{% block title %}Sign In{% endblock %}

{% block body %}

    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Sign In</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">
                <div class=\"login-box\" style=\"margin: 0 auto !important\">
                    <!-- /.login-logo -->
                    <div class=\"login-box-body\">
                        {% if error %}
                            <div class=\"alert alert-danger\">
                                {{ error.messageKey|trans(error.messageData) }}
                            </div>
                        {% endif %}

                        <form action=\"{{ path('security_login_check') }}\" method=\"post\">
                            <div class=\"form-group has-feedback\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"username\" value=\"{{ last_username }}\">
                                <span class=\"fa fa-user-o form-control-feedback\"></span>
                            </div>
                            <div class=\"form-group has-feedback\">
                                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\">
                                <span class=\"fa fa-key form-control-feedback\"></span>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-xs-8\">
                                    <div class=\"checkbox icheck\">
                                        <label>
                                            <input type=\"checkbox\" name=\"_remember_me\"> Remember Me
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class=\"col-xs-4\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Sign In</button>
                                </div>
                                <!-- /.col -->
                            </div>
                            <input type=\"hidden\" name=\"ver\" value=\"u\">
                        </form>


                        <a href=\"{{ path('forgot_password') }}\">Forgot password</a> | <a href=\"{{ path('register_pack') }}\" class=\"text-center\">Register</a>
                    </div>
                    <!-- /.login-box-body -->
                </div>
                <!-- /.login-box -->
            </div>
        </div>
    </section>

{% endblock %}", "public/login.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public\\login.html.twig");
    }
}
