<?php

/* public/public.footer.html.twig */
class __TwigTemplate_a6275a87f536b02f019db61549743385730676a04fe677c7db02a852f69f9265 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df1ebece9c6746b361b72a20fbdb32269c7f17b884aef8b142908f07074ad8ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df1ebece9c6746b361b72a20fbdb32269c7f17b884aef8b142908f07074ad8ef->enter($__internal_df1ebece9c6746b361b72a20fbdb32269c7f17b884aef8b142908f07074ad8ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/public.footer.html.twig"));

        // line 1
        echo "<!-- Footer -->
<footer class=\"text-center\" style=\"position: relative; bottom: -150px;\">
    <div class=\"footer-below\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    Copyright &copy; ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["app_name"]) ? $context["app_name"] : $this->getContext($context, "app_name")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo "
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class=\"scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md\">
    <a class=\"btn btn-primary\" href=\"#page-top\">
        <i class=\"fa fa-chevron-up\"></i>
    </a>
</div>";
        
        $__internal_df1ebece9c6746b361b72a20fbdb32269c7f17b884aef8b142908f07074ad8ef->leave($__internal_df1ebece9c6746b361b72a20fbdb32269c7f17b884aef8b142908f07074ad8ef_prof);

    }

    public function getTemplateName()
    {
        return "public/public.footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 7,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Footer -->
<footer class=\"text-center\" style=\"position: relative; bottom: -150px;\">
    <div class=\"footer-below\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    Copyright &copy; {{ app_name }} {{ 'now'|date('Y') }}
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class=\"scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md\">
    <a class=\"btn btn-primary\" href=\"#page-top\">
        <i class=\"fa fa-chevron-up\"></i>
    </a>
</div>", "public/public.footer.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public\\public.footer.html.twig");
    }
}
