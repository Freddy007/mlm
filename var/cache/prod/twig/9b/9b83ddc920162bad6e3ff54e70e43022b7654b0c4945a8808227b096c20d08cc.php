<?php

/* god-mode/list-payments.html.twig */
class __TwigTemplate_f153b727c8f62da138a0cf488c6469f439331f63ec758cbcddaae8856521db00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/list-payments.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7ce901c050ed6b710c8112305867c6beb542c499c08359ecf1588a9a04a9468 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7ce901c050ed6b710c8112305867c6beb542c499c08359ecf1588a9a04a9468->enter($__internal_f7ce901c050ed6b710c8112305867c6beb542c499c08359ecf1588a9a04a9468_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/list-payments.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f7ce901c050ed6b710c8112305867c6beb542c499c08359ecf1588a9a04a9468->leave($__internal_f7ce901c050ed6b710c8112305867c6beb542c499c08359ecf1588a9a04a9468_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_42b50e3ca9814e55566ca598b31612ff67a81d630209033f87ddae057fd7f504 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42b50e3ca9814e55566ca598b31612ff67a81d630209033f87ddae057fd7f504->enter($__internal_42b50e3ca9814e55566ca598b31612ff67a81d630209033f87ddae057fd7f504_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getTotalItemCount", array()), "html", null, true);
        echo " Pack";
        if (($this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        
        $__internal_42b50e3ca9814e55566ca598b31612ff67a81d630209033f87ddae057fd7f504->leave($__internal_42b50e3ca9814e55566ca598b31612ff67a81d630209033f87ddae057fd7f504_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3beceb5faabce8801a2cfaa117531fd70fbfcba945885e13c883d92a4c9f1ab2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3beceb5faabce8801a2cfaa117531fd70fbfcba945885e13c883d92a4c9f1ab2->enter($__internal_3beceb5faabce8801a2cfaa117531fd70fbfcba945885e13c883d92a4c9f1ab2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getTotalItemCount", array()), "html", null, true);
        echo " Pack Subscription";
        if (($this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        // line 23
        echo "        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-pie-chart\"></i> Pack Subscriptions</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item\">List Admin</a>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item\">List Users</a>
                    <a href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item active\">List Payments</a>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <div class=\"row\">
                    <div class=\"col-sm-2\">
                    </div>
                    <div class=\"col-sm-8 text-center lead\">
                        ";
        // line 61
        if (($this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getTotalItemCount", array()) != 0)) {
            // line 62
            echo "                            Showing ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getTotalItemCount", array()), "html", null, true);
            echo " Pack";
            if (($this->getAttribute((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            // line 63
            echo "                        ";
        } else {
            // line 64
            echo "                            0 adminpayments
                        ";
        }
        // line 66
        echo "                    </div>
                    <div class=\"col-sm-2\">
                    </div>
                </div>
                <table class=\"table table-hover table-responsive table-striped\">
                    <thead>
                    <tr>

                        <th>S/N</th>
                        <th>Donator Name</th>
                        <th>Receiver Name</th>
                        <th width=\"5%\">Amount</th>
                        <th width=\"10%\">Payment Method</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th width=\"5%\">Screenshot</th>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 85
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["payment"]) {
            // line 88
            echo "                        <tr>
                            <td>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo " </td>
                            <td>";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["payment"], "getFeeder", array(), "method"), "getName", array(), "method"), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["payment"], "getFeeder", array(), "method"), "getPhoneOne", array(), "method"), "html", null, true);
            echo ")</td>
                            <td>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["payment"], "getReceiver", array()), "getName", array(), "method"), "html", null, true);
            echo "  (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["payment"], "getReceiver", array(), "method"), "getPhoneOne", array(), "method"), "html", null, true);
            echo ")</td>
                            <td width=\"5%\">";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["payment"], "getPack", array(), "method"), "getAmount", array(), "method"), "html", null, true);
            echo "</td>
                            <td>
                                ";
            // line 94
            echo twig_escape_filter($this->env, $this->getAttribute($context["payment"], "getPaymentMethod", array(), "method"), "html", null, true);
            echo "
                            </td>
                            <td>
                                ";
            // line 97
            if (($this->getAttribute($context["payment"], "getStatus", array(), "method") == "payment_made")) {
                // line 98
                echo "                                    <btn class=\"btn btn-xs btn-flat btn-info\">UnConfirmed</btn>
                                ";
            } elseif (($this->getAttribute(            // line 99
$context["payment"], "getStatus", array(), "method") == "scam_paid")) {
                // line 100
                echo "                                    <btn class=\"btn btn-xs btn-flat btn-danger\">Scam Payment</btn>
                                ";
            } elseif (($this->getAttribute(            // line 101
$context["payment"], "getStatus", array(), "method") == "scam")) {
                // line 102
                echo "                                    <btn class=\"btn btn-xs btn-flat btn-danger\">Scam</btn>
                                ";
            } elseif (($this->getAttribute(            // line 103
$context["payment"], "getStatus", array(), "method") == "fully_paid")) {
                // line 104
                echo "                                    <btn class=\"btn btn-xs btn-flat btn-success\">Confirmed</btn>
                                ";
            } elseif (($this->getAttribute(            // line 105
$context["payment"], "getStatus", array(), "method") == "not_paid")) {
                // line 106
                echo "                                    <btn class=\"btn btn-xs btn-flat btn-warning\">Not Paid</btn>
                                ";
            } elseif (($this->getAttribute(            // line 107
$context["payment"], "getStatus", array(), "method") == "cancelled")) {
                // line 108
                echo "                                    <btn class=\"btn btn-xs btn-flat btn-danger\">Cancelled</btn>
                                ";
            } elseif (($this->getAttribute(            // line 109
$context["payment"], "getStatus", array(), "method") == "jury")) {
                // line 110
                echo "                                    <btn class=\"btn btn-xs btn-flat btn-warning\">In Jury</btn>
                                ";
            }
            // line 112
            echo "                            </td>
                            <td>
                                <a href=\"";
            // line 114
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("confirm_payment", array("feeder" => $this->getAttribute($context["payment"], "getId", array(), "method"))), "html", null, true);
            echo "\"><btn  class=\"btn btn-xs btn-flat btn-warning\">Confirm Donation</btn></a><br/><br/>
                                <a href=\"";
            // line 115
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scam_payment", array("feeder" => $this->getAttribute($context["payment"], "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-xs btn-flat btn-danger\">Mark as Scam</a>

                            </td>
                            <td width=\"5%\">
                                ";
            // line 119
            if ((($this->getAttribute($context["payment"], "getPaymentUploadedFile", array(), "method") != null) || ($this->getAttribute($context["payment"], "getPaymentUploadedFile", array(), "method") != ""))) {
                // line 120
                echo "                                    <p class=\"form-control-static\"><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["payment"], "getPaymentUploadedFile", array(), "method")), "html", null, true);
                echo "\" target=\"_blank\"> <img class=\"img-responsive\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($context["payment"], "getPaymentUploadedFile", array(), "method")), "html", null, true);
                echo "\" style=\" width:50%; margin: 0; border: none;\"></a></p>
                                ";
            } else {
                // line 122
                echo "                                    <p class=\"form-control-static\">No Upload yet</p>
                                ";
            }
            // line 124
            echo "                            </td>

                        </tr>

                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "                    </tbody>
                </table>

                <div class=\"navigation text-center\">
                    ";
        // line 133
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["adminpayments"]) ? $context["adminpayments"] : $this->getContext($context, "adminpayments")));
        echo "
                </div>
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_3beceb5faabce8801a2cfaa117531fd70fbfcba945885e13c883d92a4c9f1ab2->leave($__internal_3beceb5faabce8801a2cfaa117531fd70fbfcba945885e13c883d92a4c9f1ab2_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/list-payments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  372 => 133,  366 => 129,  348 => 124,  344 => 122,  336 => 120,  334 => 119,  327 => 115,  323 => 114,  319 => 112,  315 => 110,  313 => 109,  310 => 108,  308 => 107,  305 => 106,  303 => 105,  300 => 104,  298 => 103,  295 => 102,  293 => 101,  290 => 100,  288 => 99,  285 => 98,  283 => 97,  277 => 94,  272 => 92,  266 => 91,  260 => 90,  256 => 89,  253 => 88,  236 => 85,  215 => 66,  211 => 64,  208 => 63,  197 => 62,  195 => 61,  182 => 51,  178 => 50,  174 => 49,  170 => 48,  166 => 47,  162 => 46,  158 => 45,  154 => 44,  150 => 43,  146 => 42,  142 => 41,  138 => 40,  134 => 39,  130 => 38,  126 => 37,  111 => 25,  107 => 23,  101 => 22,  95 => 18,  86 => 15,  83 => 14,  79 => 13,  76 => 12,  67 => 9,  64 => 8,  60 => 7,  57 => 6,  51 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}{{ adminpayments.getTotalItemCount }} Pack{% if adminpayments.getTotalItemCount != 1 %}s{% endif %}{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            {{ adminpayments.getTotalItemCount }} Pack Subscription{% if adminpayments.getTotalItemCount != 1 %}s{% endif %}
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-pie-chart\"></i> Pack Subscriptions</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item\">List Users</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item active\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <div class=\"row\">
                    <div class=\"col-sm-2\">
                    </div>
                    <div class=\"col-sm-8 text-center lead\">
                        {% if adminpayments.getTotalItemCount != 0 %}
                            Showing {{ adminpayments.getPaginationData.firstItemNumber }} to {{ adminpayments.getPaginationData.lastItemNumber }} out of {{ adminpayments.getTotalItemCount }} Pack{% if adminpayments.getTotalItemCount != 1 %}s{% endif %}
                        {% else %}
                            0 adminpayments
                        {% endif %}
                    </div>
                    <div class=\"col-sm-2\">
                    </div>
                </div>
                <table class=\"table table-hover table-responsive table-striped\">
                    <thead>
                    <tr>

                        <th>S/N</th>
                        <th>Donator Name</th>
                        <th>Receiver Name</th>
                        <th width=\"5%\">Amount</th>
                        <th width=\"10%\">Payment Method</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th width=\"5%\">Screenshot</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for payment in adminpayments

                    %}
                        <tr>
                            <td>{{ loop.index }} </td>
                            <td>{{ payment.getFeeder().getName() }} ({{ payment.getFeeder().getPhoneOne() }})</td>
                            <td>{{ payment.getReceiver.getName() }}  ({{ payment.getReceiver().getPhoneOne() }})</td>
                            <td width=\"5%\">{{ payment.getPack().getAmount() }}</td>
                            <td>
                                {{ payment.getPaymentMethod() }}
                            </td>
                            <td>
                                {% if payment.getStatus() == 'payment_made' %}
                                    <btn class=\"btn btn-xs btn-flat btn-info\">UnConfirmed</btn>
                                {% elseif payment.getStatus() == 'scam_paid' %}
                                    <btn class=\"btn btn-xs btn-flat btn-danger\">Scam Payment</btn>
                                {% elseif payment.getStatus() == 'scam' %}
                                    <btn class=\"btn btn-xs btn-flat btn-danger\">Scam</btn>
                                {% elseif payment.getStatus() == 'fully_paid' %}
                                    <btn class=\"btn btn-xs btn-flat btn-success\">Confirmed</btn>
                                {% elseif payment.getStatus() == 'not_paid' %}
                                    <btn class=\"btn btn-xs btn-flat btn-warning\">Not Paid</btn>
                                {% elseif payment.getStatus() == 'cancelled' %}
                                    <btn class=\"btn btn-xs btn-flat btn-danger\">Cancelled</btn>
                                {% elseif payment.getStatus() == 'jury' %}
                                    <btn class=\"btn btn-xs btn-flat btn-warning\">In Jury</btn>
                                {% endif %}
                            </td>
                            <td>
                                <a href=\"{{ path('confirm_payment', {'feeder' : payment.getId()}) }}\"><btn  class=\"btn btn-xs btn-flat btn-warning\">Confirm Donation</btn></a><br/><br/>
                                <a href=\"{{ path('scam_payment', {'feeder' : payment.getId()}) }}\" class=\"btn btn-xs btn-flat btn-danger\">Mark as Scam</a>

                            </td>
                            <td width=\"5%\">
                                {% if (payment.getPaymentUploadedFile() != null) or (payment.getPaymentUploadedFile() !='') %}
                                    <p class=\"form-control-static\"><a href=\"{{ asset(payment.getPaymentUploadedFile()) }}\" target=\"_blank\"> <img class=\"img-responsive\" src=\"{{ asset(payment.getPaymentUploadedFile()) }}\" style=\" width:50%; margin: 0; border: none;\"></a></p>
                                {% else %}
                                    <p class=\"form-control-static\">No Upload yet</p>
                                {% endif %}
                            </td>

                        </tr>

                    {% endfor %}
                    </tbody>
                </table>

                <div class=\"navigation text-center\">
                    {{ knp_pagination_render(adminpayments) }}
                </div>
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}", "god-mode/list-payments.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\list-payments.html.twig");
    }
}
