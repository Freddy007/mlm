<?php

/* god-mode/list-approve-reviews.html.twig */
class __TwigTemplate_97b7cb24d542d75e6884294854859b1ea306aacd4b82a618ae8b2da918140d37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/list-approve-reviews.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5cec58f3fad36d1199e625b1776b5996613e6b19acbf0830c9d847fb50710add = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5cec58f3fad36d1199e625b1776b5996613e6b19acbf0830c9d847fb50710add->enter($__internal_5cec58f3fad36d1199e625b1776b5996613e6b19acbf0830c9d847fb50710add_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/list-approve-reviews.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5cec58f3fad36d1199e625b1776b5996613e6b19acbf0830c9d847fb50710add->leave($__internal_5cec58f3fad36d1199e625b1776b5996613e6b19acbf0830c9d847fb50710add_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_2079864694dcec9b754dfa634c178d20c5ccaa97a41723717b9a01b149aadda9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2079864694dcec9b754dfa634c178d20c5ccaa97a41723717b9a01b149aadda9->enter($__internal_2079864694dcec9b754dfa634c178d20c5ccaa97a41723717b9a01b149aadda9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()), "html", null, true);
        echo " Testimon";
        if (($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()) != 1)) {
            echo "ies";
        } else {
            echo "y";
        }
        
        $__internal_2079864694dcec9b754dfa634c178d20c5ccaa97a41723717b9a01b149aadda9->leave($__internal_2079864694dcec9b754dfa634c178d20c5ccaa97a41723717b9a01b149aadda9_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_7e35e209f6e7a68130c020dd8dcb5e4a086f2f0e8c031fbd34219de6601fded2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e35e209f6e7a68130c020dd8dcb5e4a086f2f0e8c031fbd34219de6601fded2->enter($__internal_7e35e209f6e7a68130c020dd8dcb5e4a086f2f0e8c031fbd34219de6601fded2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "reviewSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "reviewError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()), "html", null, true);
        echo " Testimon";
        if (($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()) != 1)) {
            echo "ies";
        } else {
            echo "y";
        }
        // line 23
        echo "        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-comments-o\"></i> Testimonies</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item\">List Admin</a>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item\">List Users</a>
                    <a href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item\">List Payments</a>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item active\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                ";
        // line 57
        if (($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()) > 0)) {
            // line 58
            echo "                    <div class=\"row\">
                        <div class=\"col-sm-2\">
                        </div>
                        <div class=\"col-sm-8 text-center lead\">
                            Showing ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()), "html", null, true);
            echo " Testimon";
            if (($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()) != 1)) {
                echo "ies";
            } else {
                echo "y";
            }
            // line 63
            echo "                        </div>
                        <div class=\"col-sm-2\">
                        </div>
                    </div>
                    <table class=\"table table-hover table-responsive table-striped\">
                        <thead>
                            <tr>
                                <th>Testimony</th>
                                <th>User</th>
                                <th>Status</th>
                                <th>Options</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
            // line 78
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")));
            foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                // line 79
                echo "                                <tr>
                                    <td>";
                // line 80
                echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "getTestimony", array(), "method"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 81
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["review"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</td>
                                    <td>
                                        ";
                // line 83
                if (($this->getAttribute($context["review"], "getStatus", array(), "method") == null)) {
                    // line 84
                    echo "                                            <button class=\"btn btn-xs btn-default\">Not Approved</button>
                                        ";
                } elseif (($this->getAttribute(                // line 85
$context["review"], "getStatus", array(), "method") == "approved")) {
                    // line 86
                    echo "                                            <button class=\"btn btn-xs btn-success\">Approved</button>
                                        ";
                } else {
                    // line 88
                    echo "                                            <button class=\"btn btn-xs btn-danger\">Disapproved</button>
                                        ";
                }
                // line 90
                echo "                                    </td>
                                    <td>
                                        ";
                // line 92
                if (($this->getAttribute($context["review"], "getStatus", array(), "method") == null)) {
                    // line 93
                    echo "                                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("do_approve_review", array("review" => $this->getAttribute($context["review"], "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-success\">Approve</a>
                                        ";
                } elseif (($this->getAttribute(                // line 94
$context["review"], "getStatus", array(), "method") == "approved")) {
                    // line 95
                    echo "                                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("do_disapprove_review", array("review" => $this->getAttribute($context["review"], "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-danger\">Disapprove</a>
                                        ";
                } else {
                    // line 97
                    echo "                                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("do_approve_review", array("review" => $this->getAttribute($context["review"], "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-xs btn-success\">Approve</a>
                                        ";
                }
                // line 99
                echo "                                    </td>
                                    <td>";
                // line 100
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["review"], "getCreatedDate", array(), "method"), "d M. Y H:i"), "html", null, true);
                echo "</td>
                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 103
            echo "                        </tbody>
                    </table>

                    <div class=\"navigation text-center\">
                        ";
            // line 107
            echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")));
            echo "
                    </div>
                ";
        } else {
            // line 110
            echo "                    <div class=\"alert alert-info\">
                        <p class=\"text-center\">No member has posted a testimony yet. Kindly wait while they benefit from this system then post testimonies.</p>
                    </div>
                ";
        }
        // line 114
        echo "            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_7e35e209f6e7a68130c020dd8dcb5e4a086f2f0e8c031fbd34219de6601fded2->leave($__internal_7e35e209f6e7a68130c020dd8dcb5e4a086f2f0e8c031fbd34219de6601fded2_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/list-approve-reviews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  318 => 114,  312 => 110,  306 => 107,  300 => 103,  291 => 100,  288 => 99,  282 => 97,  276 => 95,  274 => 94,  269 => 93,  267 => 92,  263 => 90,  259 => 88,  255 => 86,  253 => 85,  250 => 84,  248 => 83,  243 => 81,  239 => 80,  236 => 79,  232 => 78,  215 => 63,  203 => 62,  197 => 58,  195 => 57,  186 => 51,  182 => 50,  178 => 49,  174 => 48,  170 => 47,  166 => 46,  162 => 45,  158 => 44,  154 => 43,  150 => 42,  146 => 41,  142 => 40,  138 => 39,  134 => 38,  130 => 37,  115 => 25,  111 => 23,  103 => 22,  97 => 18,  88 => 15,  85 => 14,  81 => 13,  78 => 12,  69 => 9,  66 => 8,  62 => 7,  59 => 6,  53 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}{{ reviews.getTotalItemCount }} Testimon{% if reviews.getTotalItemCount != 1 %}ies{% else %}y{% endif %}{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('reviewSuccess') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('reviewError') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            {{ reviews.getTotalItemCount }} Testimon{% if reviews.getTotalItemCount != 1 %}ies{% else %}y{% endif %}
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-comments-o\"></i> Testimonies</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item\">List Users</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item active\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                {% if reviews.getTotalItemCount > 0 %}
                    <div class=\"row\">
                        <div class=\"col-sm-2\">
                        </div>
                        <div class=\"col-sm-8 text-center lead\">
                            Showing {{ reviews.getPaginationData.firstItemNumber }} to {{ reviews.getPaginationData.lastItemNumber }} out of {{ reviews.getTotalItemCount }} Testimon{% if reviews.getTotalItemCount != 1 %}ies{% else %}y{% endif %}
                        </div>
                        <div class=\"col-sm-2\">
                        </div>
                    </div>
                    <table class=\"table table-hover table-responsive table-striped\">
                        <thead>
                            <tr>
                                <th>Testimony</th>
                                <th>User</th>
                                <th>Status</th>
                                <th>Options</th>
                                <th>Created</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for review in reviews %}
                                <tr>
                                    <td>{{ review.getTestimony() }}</td>
                                    <td>{{ review.getUser().getName() }}</td>
                                    <td>
                                        {% if review.getStatus() == NULL %}
                                            <button class=\"btn btn-xs btn-default\">Not Approved</button>
                                        {% elseif review.getStatus() == 'approved' %}
                                            <button class=\"btn btn-xs btn-success\">Approved</button>
                                        {% else %}
                                            <button class=\"btn btn-xs btn-danger\">Disapproved</button>
                                        {% endif %}
                                    </td>
                                    <td>
                                        {% if review.getStatus() == NULL %}
                                            <a href=\"{{ path('do_approve_review', {'review' : review.getId()}) }}\" class=\"btn btn-xs btn-success\">Approve</a>
                                        {% elseif review.getStatus() == 'approved' %}
                                            <a href=\"{{ path('do_disapprove_review', {'review' : review.getId()}) }}\" class=\"btn btn-xs btn-danger\">Disapprove</a>
                                        {% else %}
                                            <a href=\"{{ path('do_approve_review', {'review' : review.getId()}) }}\" class=\"btn btn-xs btn-success\">Approve</a>
                                        {% endif %}
                                    </td>
                                    <td>{{ review.getCreatedDate()|date('d M. Y H:i') }}</td>
                                </tr>
                            {% endfor %}
                        </tbody>
                    </table>

                    <div class=\"navigation text-center\">
                        {{ knp_pagination_render(reviews) }}
                    </div>
                {% else %}
                    <div class=\"alert alert-info\">
                        <p class=\"text-center\">No member has posted a testimony yet. Kindly wait while they benefit from this system then post testimonies.</p>
                    </div>
                {% endif %}
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}", "god-mode/list-approve-reviews.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\list-approve-reviews.html.twig");
    }
}
