<?php

/* member/member.page-main-footer.html.twig */
class __TwigTemplate_5ecc3d97676c8bd2a086c6b82442dbb759437643f998db04bf9eea37dbfffb48 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70f505819cb3ac9fe41807581f8d21a0fa24bc02376d50ea9390565e8ceb19a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70f505819cb3ac9fe41807581f8d21a0fa24bc02376d50ea9390565e8ceb19a8->enter($__internal_70f505819cb3ac9fe41807581f8d21a0fa24bc02376d50ea9390565e8ceb19a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/member.page-main-footer.html.twig"));

        // line 1
        echo "<!-- Main footer -->
<footer class=\"main-footer\">
    <div class=\"pull-right hidden-xs\">
        Trusted and Genuine Returns
    </div>
    <strong>Copyright &copy; ";
        // line 6
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["app_name"]) ? $context["app_name"] : $this->getContext($context, "app_name")), "html", null, true);
        echo ".</strong> All rights reserved.
</footer> <!-- End main footer -->";
        
        $__internal_70f505819cb3ac9fe41807581f8d21a0fa24bc02376d50ea9390565e8ceb19a8->leave($__internal_70f505819cb3ac9fe41807581f8d21a0fa24bc02376d50ea9390565e8ceb19a8_prof);

    }

    public function getTemplateName()
    {
        return "member/member.page-main-footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Main footer -->
<footer class=\"main-footer\">
    <div class=\"pull-right hidden-xs\">
        Trusted and Genuine Returns
    </div>
    <strong>Copyright &copy; {{ \"now\"|date(\"Y\") }} {{ app_name }}.</strong> All rights reserved.
</footer> <!-- End main footer -->", "member/member.page-main-footer.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member\\member.page-main-footer.html.twig");
    }
}
