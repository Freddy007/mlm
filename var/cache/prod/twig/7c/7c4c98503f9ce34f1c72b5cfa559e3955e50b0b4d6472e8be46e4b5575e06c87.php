<?php

/* member/dashboard.html.twig */
class __TwigTemplate_993d5347d81cd0d46c1c74b241243f8239cfd73751cf0ecf04b5a3ceaccf9a9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/dashboard.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd85089768e13d1d6f6fed39504835ead566f353e026fa4e10491ead377d6d3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd85089768e13d1d6f6fed39504835ead566f353e026fa4e10491ead377d6d3b->enter($__internal_dd85089768e13d1d6f6fed39504835ead566f353e026fa4e10491ead377d6d3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/dashboard.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dd85089768e13d1d6f6fed39504835ead566f353e026fa4e10491ead377d6d3b->leave($__internal_dd85089768e13d1d6f6fed39504835ead566f353e026fa4e10491ead377d6d3b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_aa544fdf88606d13e61d890be99a5b33a083bd78c351f8af70b281c2818fb542 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aa544fdf88606d13e61d890be99a5b33a083bd78c351f8af70b281c2818fb542->enter($__internal_aa544fdf88606d13e61d890be99a5b33a083bd78c351f8af70b281c2818fb542_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Dashboard";
        
        $__internal_aa544fdf88606d13e61d890be99a5b33a083bd78c351f8af70b281c2818fb542->leave($__internal_aa544fdf88606d13e61d890be99a5b33a083bd78c351f8af70b281c2818fb542_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_9b8b8aa211ff0b09e7c14191953d7647507d24c9f0cb83062be3de139df3c34f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b8b8aa211ff0b09e7c14191953d7647507d24c9f0cb83062be3de139df3c34f->enter($__internal_9b8b8aa211ff0b09e7c14191953d7647507d24c9f0cb83062be3de139df3c34f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "dashboardSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "dashboardError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"active\"><i class=\"fa fa-dashboard\"></i> Dashboard</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
        ";
        // line 32
        if (((isset($context["noticeBoard"]) ? $context["noticeBoard"] : $this->getContext($context, "noticeBoard")) != null)) {
            // line 33
            echo "            <!-- Alert Bar -->
            <div class=\"alert alert-info\">
                <h3 class=\"text-danger\" style=\"margin-top:0\">";
            // line 35
            echo twig_escape_filter($this->env, (isset($context["app_name"]) ? $context["app_name"] : $this->getContext($context, "app_name")), "html", null, true);
            echo " Notice Board</h3>
                <h4>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["noticeBoard"]) ? $context["noticeBoard"] : $this->getContext($context, "noticeBoard")), "getTitle", array(), "method"), "html", null, true);
            echo "</h4>
                <p>";
            // line 37
            echo $this->getAttribute((isset($context["noticeBoard"]) ? $context["noticeBoard"] : $this->getContext($context, "noticeBoard")), "getMessage", array(), "method");
            echo "</p>
            </div> <!-- End Alert Bar -->
        ";
        }
        // line 40
        echo "
        <!-- Small boxes (Stat box) -->
        <div class=\"row\">
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-aqua\">
                    <div class=\"inner\">
                        <h3>";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getReferralsCount", array(), "method"), "html", null, true);
        echo "</h3>
                        <p>Referral";
        // line 48
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getReferralsCount", array(), "method") != 1)) {
            echo "s";
        }
        echo "</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-ios-people\"></i>
                    </div>
                    <a href=\"";
        // line 53
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("referrals");
        echo "\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-green\">
                    <div class=\"inner\">
                        <h3>";
        // line 60
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getFormattedPurse", array(), "method"), "html", null, true);
        echo "</h3>
                        <p>Wallet</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-cash\"></i>
                    </div>
                    <a href=\"";
        // line 66
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("purse");
        echo "\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-yellow\">
                    <div class=\"inner\">
                        <h3>";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPacks", array(), "method"), "html", null, true);
        echo "</h3>
                        <p>Active Packs</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-ios-pie\"></i>
                    </div>
                    <a href=\"";
        // line 79
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("packs");
        echo "\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-red\">
                    <div class=\"inner\">
                        <h3>";
        // line 86
        echo twig_escape_filter($this->env, (isset($context["feedersCount"]) ? $context["feedersCount"] : $this->getContext($context, "feedersCount")), "html", null, true);
        echo "</h3>
                        <p>Active Feeders</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-arrow-graph-down-right\"></i>
                    </div>
                    <a href=\"";
        // line 92
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("feeders");
        echo "\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div>
            </div>
        </div> <!-- End Small boxes (Stat box) -->

        <hr>
        <div class=\"row\">
            <div class=\"col-sm-12\">
                <h2 class=\"text-center\">My Packages</h2>
            </div>

            <div class=\"col-sm-12\">
                ";
        // line 104
        if ((twig_length_filter($this->env, (isset($context["subscriptions"]) ? $context["subscriptions"] : $this->getContext($context, "subscriptions"))) == 0)) {
            // line 105
            echo "                    <!-- Plans Panel -->
                    <div class=\"row\">
                        <div class=\"col-sm-12\">
                            <div class=\"box box-solid\">
                                <div class=\"box-header with-border text-center\">
                                    <i class=\"fa fa-pie-chart\"></i>

                                    <h3 class=\"box-title\">No Active Subscriptions</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class=\"box-body\">
                                    <div class=\"alert alert-warning text-center\">
                                        <h4><i class=\"icon fa fa-warning\"></i> You have not subscribed for any package</h4>
                                        It seems you have not subscribed to any package yet! Click the button below to get started<br><br>
                                        <a href=\"";
            // line 119
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("packs");
            echo "\" class=\"btn btn-info btn-lg\" style=\"text-decoration: none\">Subscribe to a Package</a>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div> <!-- End Plans Panel -->
                ";
        } else {
            // line 127
            echo "                    <div class=\"row\">
                        ";
            // line 128
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["subscriptions"]) ? $context["subscriptions"] : $this->getContext($context, "subscriptions")));
            foreach ($context['_seq'] as $context["_key"] => $context["subscription"]) {
                // line 129
                echo "
                            ";
                // line 130
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["subscription"]);
                foreach ($context['_seq'] as $context["_key"] => $context["packSubscription"]) {
                    // line 131
                    echo "                                ";
                    // line 132
                    echo "                                <div class=\"col-sm-12\">
                                    <div class=\"plan-header\">
                                        <h1>";
                    // line 134
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
                    echo " Package (";
                    echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getPack", array(), "method"), "getFormattedAmount", array(), "method"), "html", null, true);
                    echo ") ";
                    if (($this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getType", array(), "method") == "purse")) {
                        echo "[Purse]";
                    }
                    echo "</h1>
                                    </div>
                                </div>
                                ";
                    // line 138
                    echo "
                                ";
                    // line 140
                    echo "                                ";
                    if ((($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getRole", array(), "method") == "ROLE_USER") && ($this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getType", array(), "method") != "purse"))) {
                        // line 141
                        echo "                                    ";
                        // line 142
                        echo "                                    <div class=\"col-sm-12\">
                                        <div class=\"box box-solid\">
                                            <!-- /.box-header -->
                                            <div class=\"box-body text-center\">
                                                ";
                        // line 147
                        echo "                                                    ";
                        // line 148
                        echo "                                                    ";
                        // line 149
                        echo "                                                    ";
                        // line 150
                        echo "                                                    ";
                        // line 151
                        echo "                                                ";
                        // line 152
                        echo "                                                    ";
                        // line 153
                        echo "                                                    ";
                        // line 154
                        echo "                                                    ";
                        // line 155
                        echo "                                                ";
                        // line 156
                        echo "                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </div>
                                    ";
                        // line 161
                        echo "                                ";
                    }
                    // line 162
                    echo "                                ";
                    // line 163
                    echo "
                                ";
                    // line 165
                    echo "                                ";
                    if (($this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getStatus", array(), "method") == "waiting")) {
                        // line 166
                        echo "                                    <div class=\"row\">
                                        <div class=\"col-sm-8 col-sm-offset-2\">
                                            <div class=\"alert alert-info\">
                                                <p>We have taken your request for processing. Kindly be patient as the system finds the next available member for you to make payment to.</p>
                                            </div>
                                        </div>
                                    </div>
                                ";
                    } elseif (($this->getAttribute($this->getAttribute(                    // line 173
$context["packSubscription"], "subscription", array()), "getStatus", array(), "method") == "inactive")) {
                        // line 174
                        echo "                                    ";
                        // line 175
                        echo "                                    <!-- Member Payment Panel -->
                                    ";
                        // line 176
                        if (($this->getAttribute($context["packSubscription"], "feederEntry", array()) != null)) {
                            // line 177
                            echo "                                        <div class=\"row\">
                                            <div class=\"col-sm-8 col-sm-offset-2\">
                                                ";
                            // line 180
                            echo "                                                ";
                            if (($this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getStatus", array(), "method") == "not_paid")) {
                                // line 181
                                echo "                                                    <div class=\"alert alert-danger text-center\">
                                                        <p>You are to make payment to the receiver below</p>
                                                        <p>Time left to complete payment</p>
                                                        <p id=\"countdownGiverClock";
                                // line 184
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\">
                                                            <strong>Hrs: </strong>
                                                            <span class=\"hourGiverCountDownU";
                                // line 186
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                            <strong>| Mins: </strong>
                                                            <span class=\"minuteGiverCountDownU";
                                // line 188
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                            <strong>| Secs: </strong>
                                                            <span class=\"secondGiverCountDownU";
                                // line 190
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                        </p>
                                                        <script type=\"text/javascript\">
                                                            var deadlineGiverU";
                                // line 193
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo " = \"";
                                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                                echo "\";
                                                            initializeClock('countdownGiverClock";
                                // line 194
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'hourGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'minuteGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'secondGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', deadlineGiverU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo ");
                                                        </script>
                                                    </div>
                                                ";
                            } elseif ((($this->getAttribute($this->getAttribute(                            // line 197
$context["packSubscription"], "feederEntry", array()), "getStatus", array(), "method") == "payment_made") || (($this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getScamPaid", array(), "method") == "scam_paid") && ($this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getStatus", array(), "method") == "scam")))) {
                                // line 198
                                echo "                                                    <div class=\"alert alert-info text-center\">
                                                        <p>You have made payment to the person below</p>
                                                        <p>Time left for receiver to confirm payment</p>
                                                        <p id=\"countdownGiverClock";
                                // line 201
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\">
                                                            <strong>Hrs: </strong>
                                                            <span class=\"hourGiverCountDownU";
                                // line 203
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                            <strong>| Mins: </strong>
                                                            <span class=\"minuteGiverCountDownU";
                                // line 205
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                            <strong>| Secs: </strong>
                                                            <span class=\"secondGiverCountDownU";
                                // line 207
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                        </p>
                                                        <script type=\"text/javascript\">
                                                            var deadlineGiverU";
                                // line 210
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo " = \"";
                                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getReceiverTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                                echo "\";
                                                            initializeClock('countdownGiverClock";
                                // line 211
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'hourGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'minuteGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'secondGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', deadlineGiverU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo ");
                                                        </script>
                                                    </div>
                                                ";
                            } elseif (($this->getAttribute($this->getAttribute(                            // line 214
$context["packSubscription"], "feederEntry", array()), "getStatus", array(), "method") == "jury")) {
                                // line 215
                                echo "                                                    <div class=\"alert alert-warning text-center\">
                                                        <p>There seems to be a dispute on this payment. Hence, it has been moved to the Jury's</p>
                                                    </div>
                                                ";
                            } elseif (($this->getAttribute($this->getAttribute(                            // line 218
$context["packSubscription"], "feederEntry", array()), "getStatus", array(), "method") == "scam")) {
                                // line 219
                                echo "                                                    <div class=\"alert alert-danger text-center\">
                                                        <p>The receiver below has marked your payment as a scam</p>
                                                        <p>Time left for you to properly complete payment before being blocked</p>
                                                        <p class=\"text-center\" id=\"countdownGiverClock";
                                // line 222
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\">
                                                            <strong>Hrs: </strong>
                                                            <span class=\"hourGiverCountDownU";
                                // line 224
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                            <strong>| Mins: </strong>
                                                            <span class=\"minuteGiverCountDownU";
                                // line 226
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                            <strong>| Secs: </strong>
                                                            <span class=\"secondGiverCountDownU";
                                // line 228
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "\"></span>
                                                        </p>
                                                        <script type=\"text/javascript\">
                                                            var deadlineGiverU";
                                // line 231
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo " = \"";
                                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                                echo "\";
                                                            initializeClock('countdownGiverClock";
                                // line 232
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'hourGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'minuteGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', 'secondGiverCountDownU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo "', deadlineGiverU";
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"), "html", null, true);
                                echo ");
                                                        </script>
                                                    </div>
                                                ";
                            }
                            // line 236
                            echo "                                                ";
                            if (($this->getAttribute($context["packSubscription"], "receiver", array()) != null)) {
                                // line 237
                                echo "                                                    <div class=\"box box-success\">
                                                        <div class=\"box-body box-profile\">
                                                            <img class=\"profile-user-img img-responsive img-circle\" src=\"";
                                // line 239
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getAvatar", array(), "method")), "html", null, true);
                                echo "\" alt=\"User profile picture\">

                                                            <h3 class=\"profile-username text-center\">";
                                // line 241
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getName", array(), "method"), "html", null, true);
                                echo "</h3>

                                                            <p class=\"text-muted text-center\">";
                                // line 243
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getUsername", array(), "method"), "html", null, true);
                                echo "</p>

                                                            <ul class=\"list-group list-group-unbordered\">
                                                                <li class=\"list-group-item\">
                                                                    <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">";
                                // line 247
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getPhoneOne", array(), "method"), "html", null, true);
                                echo "</a>
                                                                </li>
                                                                ";
                                // line 249
                                if (($this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getPhoneTwo", array(), "method") != null)) {
                                    // line 250
                                    echo "                                                                    <li class=\"list-group-item\">
                                                                        <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">";
                                    // line 251
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getPhoneTwo", array(), "method"), "html", null, true);
                                    echo "</a>
                                                                    </li>
                                                                ";
                                }
                                // line 254
                                echo "                                                                <li class=\"list-group-item\">
                                                                    <i class=\"fa fa-envelope-o\"></i> <a class=\"pull-right\">";
                                // line 255
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getEmail", array(), "method"), "html", null, true);
                                echo "</a>
                                                                </li>
                                                                ";
                                // line 257
                                if (($this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getBankName", array(), "method") != null)) {
                                    // line 258
                                    echo "                                                                    <li class=\"list-group-item\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-sm-6\">
                                                                                <i class=\"fa fa-bank\"></i> <strong>Bank</strong>
                                                                                <br>
                                                                                <span>";
                                    // line 263
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getBankName", array(), "method"), "html", null, true);
                                    echo "</span>
                                                                            </div>
                                                                            <div class=\"col-sm-6\">
                                                                                <i class=\"fa fa-archive\"></i> <strong>Account Number</strong>
                                                                                <br>
                                                                                <span>";
                                    // line 268
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getBankAccountNumber", array(), "method"), "html", null, true);
                                    echo "</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class=\"list-group-item\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-sm-6\">
                                                                                <i class=\"fa fa-th\"></i> <strong>Account Name</strong>
                                                                                <br>
                                                                                <span>";
                                    // line 277
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getBankAccountName", array(), "method"), "html", null, true);
                                    echo "</span>
                                                                            </div>
                                                                            <div class=\"col-sm-6\">
                                                                                <i class=\"fa fa-th\"></i> <strong>Account Type</strong>
                                                                                <br>
                                                                                <span>";
                                    // line 282
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "receiver", array()), "getReceiver", array(), "method"), "getBankAccountType", array(), "method"), "html", null, true);
                                    echo "</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                ";
                                }
                                // line 287
                                echo "                                                            </ul>
                                                        </div>
                                                    </div>
                                                ";
                            }
                            // line 291
                            echo "                                                ";
                            if (($this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getStatus", array(), "method") == "jury")) {
                                // line 292
                                echo "                                                    <a href=\"";
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_case", array("case" => $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getJury", array(), "method"))), "html", null, true);
                                echo "\" class=\"btn btn-block btn-warning btn-lg\">Go to the Jury's Stand</a>
                                                ";
                            } else {
                                // line 294
                                echo "                                                    <a href=\"";
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("make_payment", array("feeder" => $this->getAttribute($this->getAttribute($context["packSubscription"], "feederEntry", array()), "getId", array(), "method"))), "html", null, true);
                                echo "\" class=\"btn btn-block btn-warning btn-lg\">Payment Page</a>
                                                ";
                            }
                            // line 296
                            echo "                                            </div>
                                        </div>
                                    ";
                        }
                        // line 299
                        echo "                                ";
                    } elseif (($this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getStatus", array(), "method") == "active")) {
                        // line 300
                        echo "                                    ";
                        if (((twig_length_filter($this->env, (isset($context["futureSubscriptions"]) ? $context["futureSubscriptions"] : $this->getContext($context, "futureSubscriptions"))) > 1) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getRole", array(), "method") == "ROLE_USER"))) {
                            // line 301
                            echo "                                        <div class=\"col-sm-12\">
                                            ";
                            // line 303
                            echo "                                                ";
                            // line 304
                            echo "                                                ";
                            // line 305
                            echo "                                                ";
                            // line 306
                            echo "                                            ";
                            // line 307
                            echo "                                        </div>
                                    ";
                        }
                        // line 309
                        echo "                                    ";
                        // line 310
                        echo "                                    ";
                        if (($this->getAttribute($context["packSubscription"], "feeders", array()) == null)) {
                            // line 311
                            echo "                                        ";
                            // line 312
                            echo "                                        <div class=\"col-sm-12\">
                                            <div class=\"box box-solid\">
                                                <div class=\"box-header with-border text-center\">
                                                    <i class=\"fa fa-users\"></i>

                                                    <h3 class=\"box-title\">No Feeders Yet (";
                            // line 317
                            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
                            echo " Package)</h3>
                                                </div>
                                                <!-- /.box-header -->
                                                <div class=\"box-body\">
                                                    <div class=\"alert alert-warning text-center\">

                                                        ";
                            // line 323
                            if (($this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getRecommitRequired", array(), "method") == true)) {
                                // line 324
                                echo "                                                            ";
                                // line 325
                                echo "
                                                            <h4><i class=\"icon fa fa-warning\"></i> Pay Recommitment Amount</h4>
                                                            <p>You will be matched to pay recommitment of your PH to a Receiver before you GH.</p>

                                                            ";
                            } else {
                                // line 330
                                echo "
                                                                <h4><i class=\"icon fa fa-warning\"></i> No feeder assigned to pay you yet</h4>
                                                                <p>Your pack subscription has been activated. Kindly be patient as the system matches feeders to pay you. The System will match a total of ";
                                // line 332
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getFeederTotal", array(), "method"), "html", null, true);
                                echo " feeders to pay you.</p>

                                                        ";
                            }
                            // line 335
                            echo "
                                                        ";
                            // line 337
                            echo "                                                        ";
                            // line 338
                            echo "                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                        </div> <!-- End Downlines Panel -->
                                    ";
                        } else {
                            // line 344
                            echo "                                        ";
                            // line 345
                            echo "                                        <div class=\"text-center\">
                                            <div class=\"col-sm-8 col-sm-offset-2\">
                                                <div class=\"alert alert-info\">
                                                    <p>The System will match a total of ";
                            // line 348
                            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getFeederTotal", array(), "method"), "html", null, true);
                            echo " feeders to pay you. ";
                            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["packSubscription"], "subscription", array()), "getFeederCounter", array(), "method"), "html", null, true);
                            echo " remaining feeders for the system to match to you</p>
                                                </div>
                                            </div>
                                            ";
                            // line 351
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["packSubscription"], "feeders", array()));
                            foreach ($context['_seq'] as $context["_key"] => $context["feeder"]) {
                                // line 352
                                echo "                                                <div class=\"col-sm-6\">
                                                    <div class=\"box box-widget widget-user\">
                                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                                        <div class=\"widget-user-header bg-aqua-active\">
                                                            <h3 class=\"widget-user-username\">";
                                // line 356
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getName", array(), "method"), "html", null, true);
                                echo "</h3>
                                                            <h5 class=\"widget-user-desc\">";
                                // line 357
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getUsername", array(), "method"), "html", null, true);
                                echo "</h5>
                                                        </div>
                                                        <div class=\"widget-user-image\">
                                                            <img class=\"img-circle\" src=\"";
                                // line 360
                                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getAvatar", array(), "method")), "html", null, true);
                                echo "\" alt=\"User Avatar\">
                                                        </div>
                                                        <div class=\"box-footer\">
                                                            <div class=\"row\">
                                                                <div class=\"col-sm-6 border-right\">
                                                                    <div class=\"description-block\">
                                                                        <h5 class=\"description-header\"><i class=\"fa fa-phone\"></i> ";
                                // line 366
                                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getPhoneOne", array(), "method"), "html", null, true);
                                echo "</h5>
                                                                    </div>
                                                                    <!-- /.description-block -->
                                                                </div>
                                                                ";
                                // line 370
                                if (($this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getPhoneTwo", array(), "method") != null)) {
                                    // line 371
                                    echo "                                                                    <div class=\"col-sm-6 border-right\">
                                                                        <div class=\"description-block\">
                                                                            <h5 class=\"description-header\"><i class=\"fa fa-phone\"></i> ";
                                    // line 373
                                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getPhoneTwo", array(), "method"), "html", null, true);
                                    echo "</h5>
                                                                        </div>
                                                                        <!-- /.description-block -->
                                                                    </div>
                                                                ";
                                }
                                // line 378
                                echo "                                                            </div>
                                                            <div class=\"row\">
                                                                <div class=\"col-sm-12\">
                                                                    ";
                                // line 382
                                echo "                                                                    ";
                                if (($this->getAttribute($context["feeder"], "getStatus", array(), "method") == "not_paid")) {
                                    // line 383
                                    echo "                                                                        <p>This feeder is to make payment to you</p>
                                                                        <p>Time left to complete payment</p>
                                                                        <p id=\"countdownClock";
                                    // line 385
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\">
                                                                            <strong>Hrs: </strong>
                                                                            <span class=\"hourCountDown";
                                    // line 387
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                            <strong>| Mins: </strong>
                                                                            <span class=\"minuteCountDown";
                                    // line 389
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                            <strong>| Secs: </strong>
                                                                            <span class=\"secondCountDown";
                                    // line 391
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                        </p>
                                                                        <script type=\"text/javascript\">
                                                                            var deadlineGiver";
                                    // line 394
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo " = \"";
                                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["feeder"], "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                                    echo "\";
                                                                            initializeClock('countdownClock";
                                    // line 395
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'hourCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'minuteCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'secondCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', deadlineGiver";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo ");
                                                                        </script>
                                                                        ";
                                } elseif ((($this->getAttribute(                                // line 397
$context["feeder"], "getStatus", array(), "method") == "payment_made") || (($this->getAttribute($context["feeder"], "getScamPaid", array(), "method") == "scam_paid") && ($this->getAttribute($context["feeder"], "getStatus", array(), "method") == "scam")))) {
                                    // line 398
                                    echo "                                                                        <p>This feeder has made payment to you</p>
                                                                        <p>Time left for you to confirm payment</p>
                                                                        <p id=\"countdownClock";
                                    // line 400
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\">
                                                                            <strong>Hrs: </strong>
                                                                            <span class=\"hourCountDown";
                                    // line 402
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                            <strong>| Mins: </strong>
                                                                            <span class=\"minuteCountDown";
                                    // line 404
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                            <strong>| Secs: </strong>
                                                                            <span class=\"secondCountDown";
                                    // line 406
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                        </p>
                                                                        <script type=\"text/javascript\">
                                                                            var deadlineGiver";
                                    // line 409
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo " = \"";
                                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["feeder"], "getReceiverTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                                    echo "\";
                                                                            initializeClock('countdownClock";
                                    // line 410
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'hourCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'minuteCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'secondCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', deadlineGiver";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo ");
                                                                        </script>
                                                                        ";
                                } elseif (($this->getAttribute(                                // line 412
$context["feeder"], "getStatus", array(), "method") == "jury")) {
                                    // line 413
                                    echo "                                                                        <p>There seems to be a dispute on this payment. Hence, it has been moved to the Jury's</p>
                                                                        ";
                                } elseif (($this->getAttribute(                                // line 414
$context["feeder"], "getStatus", array(), "method") == "scam")) {
                                    // line 415
                                    echo "                                                                        <p>You have marked this payment as a scam</p>
                                                                        <p>Time left for feeder to properly complete payment before being blocked</p>
                                                                        <p class=\"text-center\" id=\"countdownClock";
                                    // line 417
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\">
                                                                            <strong>Hours: </strong>
                                                                            <span class=\"hourCountDown";
                                    // line 419
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                            <strong>| Minutes: </strong>
                                                                            <span class=\"minuteCountDown";
                                    // line 421
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                            <strong>| Seconds: </strong>
                                                                            <span class=\"secondCountDown";
                                    // line 423
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "\"></span>
                                                                        </p>
                                                                        <script type=\"text/javascript\">
                                                                            var deadlineGiver";
                                    // line 426
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo " = \"";
                                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["feeder"], "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                                    echo "\";
                                                                            initializeClock('countdownClock";
                                    // line 427
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'hourCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'minuteCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', 'secondCountDown";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo "', deadlineGiver";
                                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                                    echo ");
                                                                        </script>
                                                                        ";
                                } elseif (($this->getAttribute(                                // line 429
$context["feeder"], "getStatus", array(), "method") == "fully_paid")) {
                                    // line 430
                                    echo "                                                                        <p>You have fully confirmed this payment</p>
                                                                    ";
                                }
                                // line 432
                                echo "                                                                </div>
                                                            </div>
                                                        </div>
                                                        ";
                                // line 435
                                if (($this->getAttribute($context["feeder"], "getStatus", array(), "method") == "jury")) {
                                    // line 436
                                    echo "                                                            <a href=\"";
                                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_case", array("case" => $this->getAttribute($context["feeder"], "getJury", array(), "method"))), "html", null, true);
                                    echo "\" class=\"btn btn-lg btn-block btn-warning\">Go to the Court</a>
                                                        ";
                                } elseif (($this->getAttribute(                                // line 437
$context["feeder"], "getStatus", array(), "method") == "fully_paid")) {
                                    // line 438
                                    echo "                                                            <a href=\"";
                                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("check_payment", array("feeder" => $this->getAttribute($context["feeder"], "getId", array(), "method"))), "html", null, true);
                                    echo "\" class=\"btn btn-lg btn-block btn-success\">Payment Confirmed</a>
                                                        ";
                                } else {
                                    // line 440
                                    echo "                                                            <a href=\"";
                                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("check_payment", array("feeder" => $this->getAttribute($context["feeder"], "getId", array(), "method"))), "html", null, true);
                                    echo "\" class=\"btn btn-lg btn-block btn-warning\">Check Payment</a>
                                                        ";
                                }
                                // line 442
                                echo "                                                    </div>
                                                </div>
                                            ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['feeder'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 445
                            echo "                                        </div>
                                    ";
                        }
                        // line 447
                        echo "                                ";
                    }
                    // line 448
                    echo "                                ";
                    // line 449
                    echo "
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['packSubscription'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 451
                echo "
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subscription'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 453
            echo "                    </div>
                ";
        }
        // line 455
        echo "            </div>
        </div>

        <hr>
        <!-- Profile Preview & Timeline -->
        <div class=\"row\">

            <!-- Profile Preview -->
            <div class=\"col-sm-12 col-md-6\">
                <div class=\"box box-success\">
                    <div class=\"box-body box-profile\">
                        <img class=\"profile-user-img img-responsive img-circle\" src=\"";
        // line 466
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getAvatar", array(), "method")), "html", null, true);
        echo "\" alt=\"User profile picture\">

                        <h3 class=\"profile-username text-center\">";
        // line 468
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "</h3>

                        <p class=\"text-muted text-center\">";
        // line 470
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getUsername", array(), "method"), "html", null, true);
        echo "</p>

                        <ul class=\"list-group list-group-unbordered\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">";
        // line 474
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneOne", array(), "method"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 476
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneTwo", array(), "method") != null)) {
            // line 477
            echo "                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">";
            // line 478
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneTwo", array(), "method"), "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        // line 481
        echo "                            <li class=\"list-group-item\">
                                <i class=\"fa fa-envelope-o\"></i> <a class=\"pull-right\">";
        // line 482
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getEmail", array(), "method"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 484
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankName", array(), "method") != null)) {
            // line 485
            echo "                                <li class=\"list-group-item\">
                                    <div class=\"row\">
                                        <div class=\"col-sm-4\">
                                            <i class=\"fa fa-bank\"></i> <strong>Bank</strong>
                                            <br>
                                            <span>";
            // line 490
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankName", array(), "method"), "html", null, true);
            echo "</span>
                                        </div>
                                        <div class=\"col-sm-4\">
                                            <i class=\"fa fa-archive\"></i> <strong>Account Name</strong>
                                            <br>
                                            <span>";
            // line 495
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankAccountName", array(), "method"), "html", null, true);
            echo "</span>
                                        </div>
                                        <div class=\"col-sm-4\">
                                            <i class=\"fa fa-archive\"></i> <strong>Account Number</strong>
                                            <br>
                                            <span>";
            // line 500
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankAccountNumber", array(), "method"), "html", null, true);
            echo "</span>
                                        </div>
                                        <div class=\"col-sm-4\">
                                            <i class=\"fa fa-th\"></i> <strong>Account Type</strong>
                                            <br>
                                            <span>";
            // line 505
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankAccountType", array(), "method"), "html", null, true);
            echo "</span>
                                        </div>
                                    </div>
                                </li>
                            ";
        }
        // line 510
        echo "                        </ul>

                        <a href=\"";
        // line 512
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profile");
        echo "\" class=\"btn btn-success btn-block\"><b>Go to Profile</b></a>
                    </div>
                </div>
            </div> <!-- End Profile Preview -->

            <!-- Timeline -->
            <div class=\"col-sm-12 col-md-6\">
                <ul class=\"timeline\">

                    ";
        // line 521
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["timeline"]) ? $context["timeline"] : $this->getContext($context, "timeline")));
        foreach ($context['_seq'] as $context["date"] => $context["entry"]) {
            // line 522
            echo "                        <!-- timeline time label -->
                        <li class=\"time-label\">
                            <span class=\"bg-green\">
                                ";
            // line 525
            echo twig_escape_filter($this->env, $context["date"], "html", null, true);
            echo "
                            </span>
                        </li>
                        <!-- /.timeline-label -->
                        ";
            // line 529
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["entry"]);
            foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
                // line 530
                echo "                            <!-- timeline item -->
                            <li>
                                <i class=\"fa ";
                // line 532
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "getIcon", array(), "method"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "getBackground", array(), "method"), "html", null, true);
                echo "\"></i>

                                <div class=\"timeline-item\">
                                    <span class=\"time\"><i class=\"fa fa-clock-o\"></i> ";
                // line 535
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["event"], "getCreated", array(), "method"), "H:i"), "html", null, true);
                echo "</span>

                                    <h3 class=\"timeline-header\">";
                // line 537
                echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "getDetails", array(), "method"), "html", null, true);
                echo "</h3>

                                    ";
                // line 539
                if (($this->getAttribute($context["event"], "getType", array(), "method") == 6)) {
                    // line 540
                    echo "                                        <div class=\"timeline-body\">
                                            <h4>";
                    // line 541
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["event"], "getMisc", array(), "method"), "getMail", array(), "method"), "getSubject", array(), "method"), "html", null, true);
                    echo "</h4>
                                            <p>";
                    // line 542
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["event"], "getMisc", array(), "method"), "getMail", array(), "method"), "getMail", array(), "method"), "html", null, true);
                    echo "</p>
                                        </div>
                                        <div class=\"timeline-footer\">
                                            <p><a href=\"";
                    // line 545
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("member_view_mail", array("mailRecipient" => $this->getAttribute($this->getAttribute($context["event"], "getMisc", array(), "method"), "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-info btn-flat\">View</a> <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("member_delete_mail", array("mailRecipient" => $this->getAttribute($this->getAttribute($context["event"], "getMisc", array(), "method"), "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-danger btn-flat\">Delete</a></p>
                                        </div>
                                    ";
                }
                // line 548
                echo "                                </div>
                            </li>
                            <!-- END timeline item -->
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 552
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['date'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 553
        echo "
                    <li>
                        <i class=\"fa fa-clock-o bg-gray\"></i>
                    </li>
                </ul>
            </div> <!-- End Timeline -->

        </div> <!-- End Profile Preview & Timeline -->
    </section>
";
        
        $__internal_9b8b8aa211ff0b09e7c14191953d7647507d24c9f0cb83062be3de139df3c34f->leave($__internal_9b8b8aa211ff0b09e7c14191953d7647507d24c9f0cb83062be3de139df3c34f_prof);

    }

    public function getTemplateName()
    {
        return "member/dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1178 => 553,  1172 => 552,  1163 => 548,  1155 => 545,  1149 => 542,  1145 => 541,  1142 => 540,  1140 => 539,  1135 => 537,  1130 => 535,  1122 => 532,  1118 => 530,  1114 => 529,  1107 => 525,  1102 => 522,  1098 => 521,  1086 => 512,  1082 => 510,  1074 => 505,  1066 => 500,  1058 => 495,  1050 => 490,  1043 => 485,  1041 => 484,  1036 => 482,  1033 => 481,  1027 => 478,  1024 => 477,  1022 => 476,  1017 => 474,  1010 => 470,  1005 => 468,  1000 => 466,  987 => 455,  983 => 453,  976 => 451,  969 => 449,  967 => 448,  964 => 447,  960 => 445,  952 => 442,  946 => 440,  940 => 438,  938 => 437,  933 => 436,  931 => 435,  926 => 432,  922 => 430,  920 => 429,  907 => 427,  901 => 426,  895 => 423,  890 => 421,  885 => 419,  880 => 417,  876 => 415,  874 => 414,  871 => 413,  869 => 412,  856 => 410,  850 => 409,  844 => 406,  839 => 404,  834 => 402,  829 => 400,  825 => 398,  823 => 397,  810 => 395,  804 => 394,  798 => 391,  793 => 389,  788 => 387,  783 => 385,  779 => 383,  776 => 382,  771 => 378,  763 => 373,  759 => 371,  757 => 370,  750 => 366,  741 => 360,  735 => 357,  731 => 356,  725 => 352,  721 => 351,  713 => 348,  708 => 345,  706 => 344,  698 => 338,  696 => 337,  693 => 335,  687 => 332,  683 => 330,  676 => 325,  674 => 324,  672 => 323,  663 => 317,  656 => 312,  654 => 311,  651 => 310,  649 => 309,  645 => 307,  643 => 306,  641 => 305,  639 => 304,  637 => 303,  634 => 301,  631 => 300,  628 => 299,  623 => 296,  617 => 294,  611 => 292,  608 => 291,  602 => 287,  594 => 282,  586 => 277,  574 => 268,  566 => 263,  559 => 258,  557 => 257,  552 => 255,  549 => 254,  543 => 251,  540 => 250,  538 => 249,  533 => 247,  526 => 243,  521 => 241,  516 => 239,  512 => 237,  509 => 236,  494 => 232,  488 => 231,  482 => 228,  477 => 226,  472 => 224,  467 => 222,  462 => 219,  460 => 218,  455 => 215,  453 => 214,  439 => 211,  433 => 210,  427 => 207,  422 => 205,  417 => 203,  412 => 201,  407 => 198,  405 => 197,  391 => 194,  385 => 193,  379 => 190,  374 => 188,  369 => 186,  364 => 184,  359 => 181,  356 => 180,  352 => 177,  350 => 176,  347 => 175,  345 => 174,  343 => 173,  334 => 166,  331 => 165,  328 => 163,  326 => 162,  323 => 161,  317 => 156,  315 => 155,  313 => 154,  311 => 153,  309 => 152,  307 => 151,  305 => 150,  303 => 149,  301 => 148,  299 => 147,  293 => 142,  291 => 141,  288 => 140,  285 => 138,  272 => 134,  268 => 132,  266 => 131,  262 => 130,  259 => 129,  255 => 128,  252 => 127,  241 => 119,  225 => 105,  223 => 104,  208 => 92,  199 => 86,  189 => 79,  180 => 73,  170 => 66,  160 => 60,  150 => 53,  140 => 48,  136 => 47,  127 => 40,  121 => 37,  117 => 36,  113 => 35,  109 => 33,  107 => 32,  91 => 18,  82 => 15,  79 => 14,  75 => 13,  72 => 12,  63 => 9,  60 => 8,  56 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Dashboard{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('dashboardSuccess') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('dashboardError') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"active\"><i class=\"fa fa-dashboard\"></i> Dashboard</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
        {% if noticeBoard != NULL %}
            <!-- Alert Bar -->
            <div class=\"alert alert-info\">
                <h3 class=\"text-danger\" style=\"margin-top:0\">{{ app_name }} Notice Board</h3>
                <h4>{{ noticeBoard.getTitle() }}</h4>
                <p>{{ noticeBoard.getMessage()|raw }}</p>
            </div> <!-- End Alert Bar -->
        {% endif %}

        <!-- Small boxes (Stat box) -->
        <div class=\"row\">
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-aqua\">
                    <div class=\"inner\">
                        <h3>{{ app.user.getReferralsCount() }}</h3>
                        <p>Referral{% if app.user.getReferralsCount() != 1 %}s{% endif %}</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-ios-people\"></i>
                    </div>
                    <a href=\"{{ path('referrals') }}\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-green\">
                    <div class=\"inner\">
                        <h3>{{ app_currency_symbol }}{{ app.user.getFormattedPurse() }}</h3>
                        <p>Wallet</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-cash\"></i>
                    </div>
                    <a href=\"{{ path('purse') }}\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-yellow\">
                    <div class=\"inner\">
                        <h3>{{ app.user.getPacks() }}</h3>
                        <p>Active Packs</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-ios-pie\"></i>
                    </div>
                    <a href=\"{{ path('packs') }}\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->
            </div>
            <div class=\"col-lg-3 col-xs-6\">
                <!-- small box -->
                <div class=\"small-box bg-red\">
                    <div class=\"inner\">
                        <h3>{{ feedersCount }}</h3>
                        <p>Active Feeders</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"ion ion-arrow-graph-down-right\"></i>
                    </div>
                    <a href=\"{{ path('feeders') }}\" class=\"small-box-footer\">More info <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div>
            </div>
        </div> <!-- End Small boxes (Stat box) -->

        <hr>
        <div class=\"row\">
            <div class=\"col-sm-12\">
                <h2 class=\"text-center\">My Packages</h2>
            </div>

            <div class=\"col-sm-12\">
                {% if subscriptions|length == 0 %}
                    <!-- Plans Panel -->
                    <div class=\"row\">
                        <div class=\"col-sm-12\">
                            <div class=\"box box-solid\">
                                <div class=\"box-header with-border text-center\">
                                    <i class=\"fa fa-pie-chart\"></i>

                                    <h3 class=\"box-title\">No Active Subscriptions</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class=\"box-body\">
                                    <div class=\"alert alert-warning text-center\">
                                        <h4><i class=\"icon fa fa-warning\"></i> You have not subscribed for any package</h4>
                                        It seems you have not subscribed to any package yet! Click the button below to get started<br><br>
                                        <a href=\"{{ path('packs') }}\" class=\"btn btn-info btn-lg\" style=\"text-decoration: none\">Subscribe to a Package</a>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div> <!-- End Plans Panel -->
                {% else %}
                    <div class=\"row\">
                        {% for subscription in subscriptions %}

                            {% for packSubscription in subscription %}
                                {# Pack Subscription Header #}
                                <div class=\"col-sm-12\">
                                    <div class=\"plan-header\">
                                        <h1>{{ packSubscription.subscription.getPack().getName() }} Package ({{ app_currency_symbol }}{{ packSubscription.subscription.getPack().getFormattedAmount() }}) {% if packSubscription.subscription.getType() == 'purse' %}[Purse]{% endif %}</h1>
                                    </div>
                                </div>
                                {# End Pack Subscription Header #}

                                {# Check if subscription is normal type #}
                                {% if app.user.getRole() == 'ROLE_USER' and packSubscription.subscription.getType() != 'purse' %}
                                    {# check autorecirculate #}
                                    <div class=\"col-sm-12\">
                                        <div class=\"box box-solid\">
                                            <!-- /.box-header -->
                                            <div class=\"box-body text-center\">
                                                {#{% if packSubscription.subscription.getAutoRecirculate() != null %}#}
                                                    {# User has selected a pack to autorecirculate on #}
                                                    {#<p>Upon completion of this package, you will be auto recycled on the <strong>{{ packSubscription.subscription.getAutoRecirculate().getName() }} ({{ app_currency_symbol }}{{ packSubscription.subscription.getAutoRecirculate().getFormattedAmount() }})</strong> Package.</p>#}
                                                    {#<p>Click the button below to cancel auto recycle for the <strong>{{ packSubscription.subscription.getAutoRecirculate().getName() }} ({{ app_currency_symbol }}{{ packSubscription.subscription.getAutoRecirculate().getFormattedAmount() }})</strong> Package and stay your current <strong>{{ packSubscription.subscription.getPack().getName() }} ({{ app_currency_symbol }}{{ packSubscription.subscription.getPack().getFormattedAmount() }})</strong></p>#}
                                                    {#<a href=\"{{ path('cancel_autorecirculation', {'packSubscription' : packSubscription.subscription.getId()}) }}\" class=\"btn btn-primary btn-lg\">Cancel Auto Recycle</a><hr>#}
                                                {#{% else %}#}
                                                    {# User has not selected another pack to autorecirculate on #}
                                                    {#<p>You have not selected a package to autorecycle on. When you complete this package, the system will not autorecycle you.</p>#}
                                                    {#<a href=\"{{ path('book_autorecirculation', {'packSubscription' : packSubscription.subscription.getId()}) }}\" class=\"btn btn-lg btn-primary\">Book Autorecycle</a><hr>#}
                                                {#{% endif %}#}
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                    </div>
                                    {# end autorecirculation check #}
                                {% endif %}
                                {# end subscription check #}

                                {# Check if subscription has been activated #}
                                {% if packSubscription.subscription.getStatus() == 'waiting' %}
                                    <div class=\"row\">
                                        <div class=\"col-sm-8 col-sm-offset-2\">
                                            <div class=\"alert alert-info\">
                                                <p>We have taken your request for processing. Kindly be patient as the system finds the next available member for you to make payment to.</p>
                                            </div>
                                        </div>
                                    </div>
                                {% elseif packSubscription.subscription.getStatus() == 'inactive' %}
                                    {# User is to make payment. Get Receiver Details #}
                                    <!-- Member Payment Panel -->
                                    {% if packSubscription.feederEntry != null %}
                                        <div class=\"row\">
                                            <div class=\"col-sm-8 col-sm-offset-2\">
                                                {# Check feeder entry conditions #}
                                                {% if packSubscription.feederEntry.getStatus() == 'not_paid' %}
                                                    <div class=\"alert alert-danger text-center\">
                                                        <p>You are to make payment to the receiver below</p>
                                                        <p>Time left to complete payment</p>
                                                        <p id=\"countdownGiverClock{{ packSubscription.feederEntry.getId() }}\">
                                                            <strong>Hrs: </strong>
                                                            <span class=\"hourGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                            <strong>| Mins: </strong>
                                                            <span class=\"minuteGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                            <strong>| Secs: </strong>
                                                            <span class=\"secondGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                        </p>
                                                        <script type=\"text/javascript\">
                                                            var deadlineGiverU{{ packSubscription.feederEntry.getId() }} = \"{{ packSubscription.feederEntry.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                            initializeClock('countdownGiverClock{{ packSubscription.feederEntry.getId() }}', 'hourGiverCountDownU{{ packSubscription.feederEntry.getId() }}', 'minuteGiverCountDownU{{ packSubscription.feederEntry.getId() }}', 'secondGiverCountDownU{{ packSubscription.feederEntry.getId() }}', deadlineGiverU{{ packSubscription.feederEntry.getId() }});
                                                        </script>
                                                    </div>
                                                {% elseif packSubscription.feederEntry.getStatus() == 'payment_made' or (packSubscription.feederEntry.getScamPaid() == 'scam_paid' and packSubscription.feederEntry.getStatus() == 'scam') %}
                                                    <div class=\"alert alert-info text-center\">
                                                        <p>You have made payment to the person below</p>
                                                        <p>Time left for receiver to confirm payment</p>
                                                        <p id=\"countdownGiverClock{{ packSubscription.feederEntry.getId() }}\">
                                                            <strong>Hrs: </strong>
                                                            <span class=\"hourGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                            <strong>| Mins: </strong>
                                                            <span class=\"minuteGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                            <strong>| Secs: </strong>
                                                            <span class=\"secondGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                        </p>
                                                        <script type=\"text/javascript\">
                                                            var deadlineGiverU{{ packSubscription.feederEntry.getId() }} = \"{{ packSubscription.feederEntry.getReceiverTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                            initializeClock('countdownGiverClock{{ packSubscription.feederEntry.getId() }}', 'hourGiverCountDownU{{ packSubscription.feederEntry.getId() }}', 'minuteGiverCountDownU{{ packSubscription.feederEntry.getId() }}', 'secondGiverCountDownU{{ packSubscription.feederEntry.getId() }}', deadlineGiverU{{ packSubscription.feederEntry.getId() }});
                                                        </script>
                                                    </div>
                                                {% elseif packSubscription.feederEntry.getStatus() == 'jury' %}
                                                    <div class=\"alert alert-warning text-center\">
                                                        <p>There seems to be a dispute on this payment. Hence, it has been moved to the Jury's</p>
                                                    </div>
                                                {% elseif packSubscription.feederEntry.getStatus() == 'scam' %}
                                                    <div class=\"alert alert-danger text-center\">
                                                        <p>The receiver below has marked your payment as a scam</p>
                                                        <p>Time left for you to properly complete payment before being blocked</p>
                                                        <p class=\"text-center\" id=\"countdownGiverClock{{ packSubscription.feederEntry.getId() }}\">
                                                            <strong>Hrs: </strong>
                                                            <span class=\"hourGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                            <strong>| Mins: </strong>
                                                            <span class=\"minuteGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                            <strong>| Secs: </strong>
                                                            <span class=\"secondGiverCountDownU{{ packSubscription.feederEntry.getId() }}\"></span>
                                                        </p>
                                                        <script type=\"text/javascript\">
                                                            var deadlineGiverU{{ packSubscription.feederEntry.getId() }} = \"{{ packSubscription.feederEntry.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                            initializeClock('countdownGiverClock{{ packSubscription.feederEntry.getId() }}', 'hourGiverCountDownU{{ packSubscription.feederEntry.getId() }}', 'minuteGiverCountDownU{{ packSubscription.feederEntry.getId() }}', 'secondGiverCountDownU{{ packSubscription.feederEntry.getId() }}', deadlineGiverU{{ packSubscription.feederEntry.getId() }});
                                                        </script>
                                                    </div>
                                                {% endif %}
                                                {% if packSubscription.receiver != null %}
                                                    <div class=\"box box-success\">
                                                        <div class=\"box-body box-profile\">
                                                            <img class=\"profile-user-img img-responsive img-circle\" src=\"{{ asset(packSubscription.receiver.getReceiver().getAvatar()) }}\" alt=\"User profile picture\">

                                                            <h3 class=\"profile-username text-center\">{{ packSubscription.receiver.getReceiver().getName() }}</h3>

                                                            <p class=\"text-muted text-center\">{{ packSubscription.receiver.getReceiver().getUsername() }}</p>

                                                            <ul class=\"list-group list-group-unbordered\">
                                                                <li class=\"list-group-item\">
                                                                    <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">{{ packSubscription.receiver.getReceiver().getPhoneOne() }}</a>
                                                                </li>
                                                                {% if packSubscription.receiver.getReceiver().getPhoneTwo() != null %}
                                                                    <li class=\"list-group-item\">
                                                                        <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">{{ packSubscription.receiver.getReceiver().getPhoneTwo() }}</a>
                                                                    </li>
                                                                {% endif %}
                                                                <li class=\"list-group-item\">
                                                                    <i class=\"fa fa-envelope-o\"></i> <a class=\"pull-right\">{{ packSubscription.receiver.getReceiver().getEmail() }}</a>
                                                                </li>
                                                                {% if packSubscription.receiver.getReceiver().getBankName() != null %}
                                                                    <li class=\"list-group-item\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-sm-6\">
                                                                                <i class=\"fa fa-bank\"></i> <strong>Bank</strong>
                                                                                <br>
                                                                                <span>{{ packSubscription.receiver.getReceiver().getBankName() }}</span>
                                                                            </div>
                                                                            <div class=\"col-sm-6\">
                                                                                <i class=\"fa fa-archive\"></i> <strong>Account Number</strong>
                                                                                <br>
                                                                                <span>{{ packSubscription.receiver.getReceiver().getBankAccountNumber() }}</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <li class=\"list-group-item\">
                                                                        <div class=\"row\">
                                                                            <div class=\"col-sm-6\">
                                                                                <i class=\"fa fa-th\"></i> <strong>Account Name</strong>
                                                                                <br>
                                                                                <span>{{ packSubscription.receiver.getReceiver().getBankAccountName() }}</span>
                                                                            </div>
                                                                            <div class=\"col-sm-6\">
                                                                                <i class=\"fa fa-th\"></i> <strong>Account Type</strong>
                                                                                <br>
                                                                                <span>{{ packSubscription.receiver.getReceiver().getBankAccountType() }}</span>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                {% endif %}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                {% endif %}
                                                {% if packSubscription.feederEntry.getStatus() == 'jury'%}
                                                    <a href=\"{{ path('user_case', {'case' : packSubscription.feederEntry.getJury()}) }}\" class=\"btn btn-block btn-warning btn-lg\">Go to the Jury's Stand</a>
                                                {% else %}
                                                    <a href=\"{{ path('make_payment', {'feeder' : packSubscription.feederEntry.getId()}) }}\" class=\"btn btn-block btn-warning btn-lg\">Payment Page</a>
                                                {% endif %}
                                            </div>
                                        </div>
                                    {%  endif %}
                                {% elseif packSubscription.subscription.getStatus() == 'active' %}
                                    {% if futureSubscriptions|length > 1 and app.user.getRole() == 'ROLE_USER' %}
                                        <div class=\"col-sm-12\">
                                            {#<div class=\"alert alert-success text-center\">#}
                                                {#<p>It seems like you have more than one subscription running. If you wish to cancel one of your subscriptions, click the button below</p>#}
                                                {#<p><a href=\"{{ path('cancel_pack_subscription', {'packSubscription' : packSubscription.subscription.getId()}) }}\" class=\"btn btn-warning\">Book Cancel For {{ packSubscription.subscription.getPack().getName() }} Package</a></p>#}
                                                {#<p>NOTE: By clicking the button above, your subscription will not be cancelled immediately. After confirming the payment of your last feeder, instead of the system to autorecycle you, your subscription for this package will be cancelled.</p>#}
                                            {#</div>#}
                                        </div>
                                    {% endif %}
                                    {# User has made payment. Get Feeders Details #}
                                    {% if packSubscription.feeders == null %}
                                        {# No feeder assigned yet #}
                                        <div class=\"col-sm-12\">
                                            <div class=\"box box-solid\">
                                                <div class=\"box-header with-border text-center\">
                                                    <i class=\"fa fa-users\"></i>

                                                    <h3 class=\"box-title\">No Feeders Yet ({{ packSubscription.subscription.getPack().getName() }} Package)</h3>
                                                </div>
                                                <!-- /.box-header -->
                                                <div class=\"box-body\">
                                                    <div class=\"alert alert-warning text-center\">

                                                        {% if packSubscription.subscription.getRecommitRequired() == true %}
                                                            {#<p>You will be required to pay Recommitment</p>#}

                                                            <h4><i class=\"icon fa fa-warning\"></i> Pay Recommitment Amount</h4>
                                                            <p>You will be matched to pay recommitment of your PH to a Receiver before you GH.</p>

                                                            {% else %}

                                                                <h4><i class=\"icon fa fa-warning\"></i> No feeder assigned to pay you yet</h4>
                                                                <p>Your pack subscription has been activated. Kindly be patient as the system matches feeders to pay you. The System will match a total of {{ packSubscription.subscription.getFeederTotal() }} feeders to pay you.</p>

                                                        {% endif %}

                                                        {#<h4><i class=\"icon fa fa-warning\"></i> No feeder assigned to pay you yet</h4>#}
                                                        {#<p>Your pack subscription has been activated. Kindly be patient as the system matches feeders to pay you. The System will match a total of {{ packSubscription.subscription.getFeederTotal() }} feeders to pay you.</p>#}
                                                    </div>
                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                        </div> <!-- End Downlines Panel -->
                                    {% else %}
                                        {# Feeders assigned #}
                                        <div class=\"text-center\">
                                            <div class=\"col-sm-8 col-sm-offset-2\">
                                                <div class=\"alert alert-info\">
                                                    <p>The System will match a total of {{ packSubscription.subscription.getFeederTotal() }} feeders to pay you. {{ packSubscription.subscription.getFeederCounter() }} remaining feeders for the system to match to you</p>
                                                </div>
                                            </div>
                                            {% for feeder in packSubscription.feeders %}
                                                <div class=\"col-sm-6\">
                                                    <div class=\"box box-widget widget-user\">
                                                        <!-- Add the bg color to the header using any of the bg-* classes -->
                                                        <div class=\"widget-user-header bg-aqua-active\">
                                                            <h3 class=\"widget-user-username\">{{ feeder.getFeeder().getName() }}</h3>
                                                            <h5 class=\"widget-user-desc\">{{ feeder.getFeeder().getUsername() }}</h5>
                                                        </div>
                                                        <div class=\"widget-user-image\">
                                                            <img class=\"img-circle\" src=\"{{ asset(feeder.getFeeder().getAvatar()) }}\" alt=\"User Avatar\">
                                                        </div>
                                                        <div class=\"box-footer\">
                                                            <div class=\"row\">
                                                                <div class=\"col-sm-6 border-right\">
                                                                    <div class=\"description-block\">
                                                                        <h5 class=\"description-header\"><i class=\"fa fa-phone\"></i> {{ feeder.getFeeder().getPhoneOne() }}</h5>
                                                                    </div>
                                                                    <!-- /.description-block -->
                                                                </div>
                                                                {% if feeder.getFeeder().getPhoneTwo() != null %}
                                                                    <div class=\"col-sm-6 border-right\">
                                                                        <div class=\"description-block\">
                                                                            <h5 class=\"description-header\"><i class=\"fa fa-phone\"></i> {{ feeder.getFeeder().getPhoneTwo() }}</h5>
                                                                        </div>
                                                                        <!-- /.description-block -->
                                                                    </div>
                                                                {% endif %}
                                                            </div>
                                                            <div class=\"row\">
                                                                <div class=\"col-sm-12\">
                                                                    {# Check feeder entry conditions #}
                                                                    {% if feeder.getStatus() == 'not_paid' %}
                                                                        <p>This feeder is to make payment to you</p>
                                                                        <p>Time left to complete payment</p>
                                                                        <p id=\"countdownClock{{ feeder.getId() }}\">
                                                                            <strong>Hrs: </strong>
                                                                            <span class=\"hourCountDown{{ feeder.getId() }}\"></span>
                                                                            <strong>| Mins: </strong>
                                                                            <span class=\"minuteCountDown{{ feeder.getId() }}\"></span>
                                                                            <strong>| Secs: </strong>
                                                                            <span class=\"secondCountDown{{ feeder.getId() }}\"></span>
                                                                        </p>
                                                                        <script type=\"text/javascript\">
                                                                            var deadlineGiver{{ feeder.getId() }} = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                                            initializeClock('countdownClock{{ feeder.getId() }}', 'hourCountDown{{ feeder.getId() }}', 'minuteCountDown{{ feeder.getId() }}', 'secondCountDown{{ feeder.getId() }}', deadlineGiver{{ feeder.getId() }});
                                                                        </script>
                                                                        {% elseif feeder.getStatus() == 'payment_made' or (feeder.getScamPaid() == 'scam_paid' and feeder.getStatus() == 'scam') %}
                                                                        <p>This feeder has made payment to you</p>
                                                                        <p>Time left for you to confirm payment</p>
                                                                        <p id=\"countdownClock{{ feeder.getId() }}\">
                                                                            <strong>Hrs: </strong>
                                                                            <span class=\"hourCountDown{{ feeder.getId() }}\"></span>
                                                                            <strong>| Mins: </strong>
                                                                            <span class=\"minuteCountDown{{ feeder.getId() }}\"></span>
                                                                            <strong>| Secs: </strong>
                                                                            <span class=\"secondCountDown{{ feeder.getId() }}\"></span>
                                                                        </p>
                                                                        <script type=\"text/javascript\">
                                                                            var deadlineGiver{{ feeder.getId() }} = \"{{ feeder.getReceiverTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                                            initializeClock('countdownClock{{ feeder.getId() }}', 'hourCountDown{{ feeder.getId() }}', 'minuteCountDown{{ feeder.getId() }}', 'secondCountDown{{ feeder.getId() }}', deadlineGiver{{ feeder.getId() }});
                                                                        </script>
                                                                        {% elseif feeder.getStatus() == 'jury' %}
                                                                        <p>There seems to be a dispute on this payment. Hence, it has been moved to the Jury's</p>
                                                                        {% elseif feeder.getStatus() == 'scam' %}
                                                                        <p>You have marked this payment as a scam</p>
                                                                        <p>Time left for feeder to properly complete payment before being blocked</p>
                                                                        <p class=\"text-center\" id=\"countdownClock{{ feeder.getId() }}\">
                                                                            <strong>Hours: </strong>
                                                                            <span class=\"hourCountDown{{ feeder.getId() }}\"></span>
                                                                            <strong>| Minutes: </strong>
                                                                            <span class=\"minuteCountDown{{ feeder.getId() }}\"></span>
                                                                            <strong>| Seconds: </strong>
                                                                            <span class=\"secondCountDown{{ feeder.getId() }}\"></span>
                                                                        </p>
                                                                        <script type=\"text/javascript\">
                                                                            var deadlineGiver{{ feeder.getId() }} = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                                            initializeClock('countdownClock{{ feeder.getId() }}', 'hourCountDown{{ feeder.getId() }}', 'minuteCountDown{{ feeder.getId() }}', 'secondCountDown{{ feeder.getId() }}', deadlineGiver{{ feeder.getId() }});
                                                                        </script>
                                                                        {% elseif feeder.getStatus() == 'fully_paid' %}
                                                                        <p>You have fully confirmed this payment</p>
                                                                    {% endif %}
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {% if feeder.getStatus() == 'jury'%}
                                                            <a href=\"{{ path('user_case', {'case' : feeder.getJury()}) }}\" class=\"btn btn-lg btn-block btn-warning\">Go to the Court</a>
                                                        {% elseif feeder.getStatus() == 'fully_paid' %}
                                                            <a href=\"{{ path('check_payment', {'feeder' : feeder.getId()}) }}\" class=\"btn btn-lg btn-block btn-success\">Payment Confirmed</a>
                                                        {% else %}
                                                            <a href=\"{{ path('check_payment', {'feeder' : feeder.getId()}) }}\" class=\"btn btn-lg btn-block btn-warning\">Check Payment</a>
                                                        {% endif %}
                                                    </div>
                                                </div>
                                            {% endfor %}
                                        </div>
                                    {% endif %}
                                {% endif %}
                                {# End Activation Check #}

                            {% endfor %}

                        {% endfor %}
                    </div>
                {% endif %}
            </div>
        </div>

        <hr>
        <!-- Profile Preview & Timeline -->
        <div class=\"row\">

            <!-- Profile Preview -->
            <div class=\"col-sm-12 col-md-6\">
                <div class=\"box box-success\">
                    <div class=\"box-body box-profile\">
                        <img class=\"profile-user-img img-responsive img-circle\" src=\"{{ asset(app.user.getAvatar()) }}\" alt=\"User profile picture\">

                        <h3 class=\"profile-username text-center\">{{ app.user.getName() }}</h3>

                        <p class=\"text-muted text-center\">{{ app.user.getUsername() }}</p>

                        <ul class=\"list-group list-group-unbordered\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">{{ app.user.getPhoneOne() }}</a>
                            </li>
                            {% if app.user.getPhoneTwo() != null %}
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">{{ app.user.getPhoneTwo() }}</a>
                                </li>
                            {% endif %}
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-envelope-o\"></i> <a class=\"pull-right\">{{ app.user.getEmail() }}</a>
                            </li>
                            {% if app.user.getBankName() != null %}
                                <li class=\"list-group-item\">
                                    <div class=\"row\">
                                        <div class=\"col-sm-4\">
                                            <i class=\"fa fa-bank\"></i> <strong>Bank</strong>
                                            <br>
                                            <span>{{ app.user.getBankName() }}</span>
                                        </div>
                                        <div class=\"col-sm-4\">
                                            <i class=\"fa fa-archive\"></i> <strong>Account Name</strong>
                                            <br>
                                            <span>{{ app.user.getBankAccountName() }}</span>
                                        </div>
                                        <div class=\"col-sm-4\">
                                            <i class=\"fa fa-archive\"></i> <strong>Account Number</strong>
                                            <br>
                                            <span>{{ app.user.getBankAccountNumber() }}</span>
                                        </div>
                                        <div class=\"col-sm-4\">
                                            <i class=\"fa fa-th\"></i> <strong>Account Type</strong>
                                            <br>
                                            <span>{{ app.user.getBankAccountType() }}</span>
                                        </div>
                                    </div>
                                </li>
                            {% endif %}
                        </ul>

                        <a href=\"{{ path('profile') }}\" class=\"btn btn-success btn-block\"><b>Go to Profile</b></a>
                    </div>
                </div>
            </div> <!-- End Profile Preview -->

            <!-- Timeline -->
            <div class=\"col-sm-12 col-md-6\">
                <ul class=\"timeline\">

                    {% for date, entry in timeline %}
                        <!-- timeline time label -->
                        <li class=\"time-label\">
                            <span class=\"bg-green\">
                                {{ date }}
                            </span>
                        </li>
                        <!-- /.timeline-label -->
                        {% for event in entry %}
                            <!-- timeline item -->
                            <li>
                                <i class=\"fa {{ event.getIcon() }} {{ event.getBackground() }}\"></i>

                                <div class=\"timeline-item\">
                                    <span class=\"time\"><i class=\"fa fa-clock-o\"></i> {{ event.getCreated()|date(\"H:i\") }}</span>

                                    <h3 class=\"timeline-header\">{{ event.getDetails() }}</h3>

                                    {% if event.getType() == 6 %}
                                        <div class=\"timeline-body\">
                                            <h4>{{ event.getMisc().getMail().getSubject() }}</h4>
                                            <p>{{ event.getMisc().getMail().getMail() }}</p>
                                        </div>
                                        <div class=\"timeline-footer\">
                                            <p><a href=\"{{ path('member_view_mail', {'mailRecipient' : event.getMisc().getId()}) }}\" class=\"btn btn-info btn-flat\">View</a> <a href=\"{{ path('member_delete_mail', {'mailRecipient' : event.getMisc().getId()}) }}\" class=\"btn btn-danger btn-flat\">Delete</a></p>
                                        </div>
                                    {% endif %}
                                </div>
                            </li>
                            <!-- END timeline item -->
                        {% endfor %}
                    {% endfor %}

                    <li>
                        <i class=\"fa fa-clock-o bg-gray\"></i>
                    </li>
                </ul>
            </div> <!-- End Timeline -->

        </div> <!-- End Profile Preview & Timeline -->
    </section>
{% endblock %}", "member/dashboard.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member\\dashboard.html.twig");
    }
}
