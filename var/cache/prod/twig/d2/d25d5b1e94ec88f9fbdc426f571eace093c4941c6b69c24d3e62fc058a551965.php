<?php

/* member/member.page-top-header.html.twig */
class __TwigTemplate_af1d4595745a30d8baafcf8c37070b633f94b13fb63bdaeea1eb67249f8161a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ad0f6399bba6d02e47638b3b1babe6920bea6eb9cd6ef6dd5ecce2558775838 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2ad0f6399bba6d02e47638b3b1babe6920bea6eb9cd6ef6dd5ecce2558775838->enter($__internal_2ad0f6399bba6d02e47638b3b1babe6920bea6eb9cd6ef6dd5ecce2558775838_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/member.page-top-header.html.twig"));

        // line 1
        echo "<!-- Page Top Header -->
<header class=\"main-header\">

    <!-- Logo -->
    <a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\" class=\"logo\">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class=\"logo-mini\">
            ";
        // line 9
        echo "        </span>
        <span class=\"logo-mini\"><img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/nav-logo-small.png"), "html", null, true);
        echo "\"></span>
        <!-- logo for regular state and mobile devices 200x50 pixels -->
        ";
        // line 13
        echo "        ";
        // line 14
        echo "        <span class=\"logo-lg\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/logo.jpg"), "html", null, true);
        echo "\"></span>
    </a> <!-- End Logo -->

    <!-- Header Navbar -->
    <nav class=\"navbar navbar-static-top\">

        <!-- Sidebar toggle button-->
        <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
            <span class=\"sr-only\">Toggle navigation</span>
        </a> <!-- End Sidebar toggle button-->

        <!-- Navbar Custom Menu -->
        <div class=\"navbar-custom-menu\">

            <ul class=\"nav navbar-nav\">
                <!-- Notifications: style can be found in dropdown.less-->
                <li class=\"dropdown messages-menu\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                        <i class=\"fa fa-globe\"></i>
                        <span class=\"label label-danger\">";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getNotificationCount", array(), "method"), "html", null, true);
        echo "</span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li class=\"header\">You have ";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getNotificationCount", array(), "method"), "html", null, true);
        echo " new notification";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getNotificationCount", array(), "method") != 1)) {
            echo "s";
        }
        echo "</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class=\"menu\">
                                ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["timeline"]) ? $context["timeline"] : $this->getContext($context, "timeline")));
        foreach ($context['_seq'] as $context["date"] => $context["entry"]) {
            // line 41
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["entry"]);
            foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
                // line 42
                echo "                                        ";
                // line 43
                echo "                                        ";
                if (($this->getAttribute($context["event"], "getType", array(), "method") > 0)) {
                    // line 44
                    echo "                                            <li style=\"background-color: ";
                    if (($this->getAttribute($context["event"], "getStatus", array()) == "unread")) {
                        echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "getColor", array(), "method"), "html", null, true);
                    } else {
                        echo "white";
                    }
                    echo "\"><!-- start message -->

                                                ";
                    // line 47
                    echo "                                                    <a href=\"";
                    if (($this->getAttribute($context["event"], "getType", array()) > 0)) {
                        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("visit_notif", array("id" => $this->getAttribute($context["event"], "getId", array(), "method"))), "html", null, true);
                    } else {
                        echo "#";
                    }
                    echo "\" >
                                                    <h4 style=\"margin:0\">
                                                        ";
                    // line 49
                    echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "getDetails", array(), "method"), "html", null, true);
                    echo "
                                                        <small><i class=\"fa ";
                    // line 50
                    echo twig_escape_filter($this->env, $this->getAttribute($context["event"], "getIcon", array(), "method"), "html", null, true);
                    echo "\"></i> ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["event"], "getCreated", array(), "method"), "H:i"), "html", null, true);
                    echo "</small>
                                                    </h4>
                                                </a>
                                            </li>
                                        ";
                }
                // line 55
                echo "                                        <!-- end message -->
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['date'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                            </ul>
                        </li>
                        <li class=\"footer\"><a href=\"";
        // line 60
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\">See All Notifications</a></li>
                    </ul>
                </li>
                <!-- Messages: style can be found in dropdown.less-->
                <li class=\"dropdown messages-menu\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                        <i class=\"fa fa-envelope-o\"></i>
                        <span class=\"label label-danger\">";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getMailCount", array(), "method"), "html", null, true);
        echo "</span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li class=\"header\">You have ";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getMailCount", array(), "method"), "html", null, true);
        echo " new message";
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getMailCount", array(), "method") != 1)) {
            echo "s";
        }
        echo "</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class=\"menu\">
                                ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getMenuMail", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["mail"]) {
            // line 75
            echo "                                    <li><!-- start message -->
                                        <a href=\"#\" ";
            // line 76
            if (($this->getAttribute($context["mail"], "getStatus", array(), "method") == "unread")) {
                echo "style=\"background-color:#f4f4f4\"";
            }
            echo ">
                                            <h4 style=\"margin:0\">
                                                Support Team <small><i class=\"fa fa-clock-o\"></i> ";
            // line 78
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["mail"], "getMail", array(), "method"), "getCreated", array(), "method"), "d M. Y H:i"), "html", null, true);
            echo "</small>
                                            </h4>
                                            <p style=\"margin:0\">";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["mail"], "getMail", array(), "method"), "getSubject", array(), "method"), "html", null, true);
            echo "</p>
                                        </a>
                                    </li>
                                    <!-- end message -->
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "                            </ul>
                        </li>
                        <li class=\"footer\"><a href=\"";
        // line 87
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("member_mails");
        echo "\">See All Messages</a></li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class=\"dropdown user user-menu\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                        <img src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getAvatar", array(), "method")), "html", null, true);
        echo "\" class=\"user-image\" alt=\"User Image\">
                        <span class=\"hidden-xs\">";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "</span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <!-- User image -->
                        <li class=\"user-header\">
                            <img src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getAvatar", array(), "method")), "html", null, true);
        echo "\" class=\"img-circle\" alt=\"User Image\">
                            <p>
                                ";
        // line 102
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "
                                <small>";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getUsername", array(), "method"), "html", null, true);
        echo "</small>
                            </p>
                        </li> <!-- End User image -->
                        <!-- Menu Footer-->
                        <li class=\"user-footer\">
                            <div class=\"pull-left\">
                                ";
        // line 109
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getRole", array(), "method") == "ROLE_SUPER_ADMIN")) {
            // line 110
            echo "                                    <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminprofile");
            echo "\" class=\"btn btn-default btn-flat\">Profile</a>
                                ";
        } else {
            // line 112
            echo "                                    <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profile");
            echo "\" class=\"btn btn-default btn-flat\">Profile</a>
                                ";
        }
        // line 114
        echo "                            </div>
                            <div class=\"pull-right\">
                                <a href=\"";
        // line 116
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("logout");
        echo "\" class=\"btn btn-default btn-flat\">Sign out</a>
                            </div>
                        </li> <!-- End Menu Footer-->
                    </ul>
                </li> <!-- End User Account -->
            </ul>

        </div> <!-- End Navbar Custom Menu -->

    </nav> <!-- End Header Navbar -->

</header> <!-- End Page Top Header -->";
        
        $__internal_2ad0f6399bba6d02e47638b3b1babe6920bea6eb9cd6ef6dd5ecce2558775838->leave($__internal_2ad0f6399bba6d02e47638b3b1babe6920bea6eb9cd6ef6dd5ecce2558775838_prof);

    }

    public function getTemplateName()
    {
        return "member/member.page-top-header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 116,  264 => 114,  258 => 112,  252 => 110,  250 => 109,  241 => 103,  237 => 102,  232 => 100,  224 => 95,  220 => 94,  210 => 87,  206 => 85,  195 => 80,  190 => 78,  183 => 76,  180 => 75,  176 => 74,  165 => 70,  159 => 67,  149 => 60,  145 => 58,  139 => 57,  132 => 55,  122 => 50,  118 => 49,  108 => 47,  98 => 44,  95 => 43,  93 => 42,  88 => 41,  84 => 40,  73 => 36,  67 => 33,  44 => 14,  42 => 13,  37 => 10,  34 => 9,  28 => 5,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Page Top Header -->
<header class=\"main-header\">

    <!-- Logo -->
    <a href=\"{{ path('dashboard') }}\" class=\"logo\">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class=\"logo-mini\">
            {#<img src=\"{{ adm.sitesmallicon }}\">#}
        </span>
        <span class=\"logo-mini\"><img src=\"{{ asset('img/nav-logo-small.png') }}\"></span>
        <!-- logo for regular state and mobile devices 200x50 pixels -->
        {#<span class=\"logo-lg\"><img src=\"{{ adm.sitelogo }}\"></span>#}
        {#<span class=\"logo-lg\"><img src=\"{{ asset('img/nav-logo.png') }}\"></span>#}
        <span class=\"logo-lg\"><img src=\"{{ asset('img/logo.jpg') }}\"></span>
    </a> <!-- End Logo -->

    <!-- Header Navbar -->
    <nav class=\"navbar navbar-static-top\">

        <!-- Sidebar toggle button-->
        <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
            <span class=\"sr-only\">Toggle navigation</span>
        </a> <!-- End Sidebar toggle button-->

        <!-- Navbar Custom Menu -->
        <div class=\"navbar-custom-menu\">

            <ul class=\"nav navbar-nav\">
                <!-- Notifications: style can be found in dropdown.less-->
                <li class=\"dropdown messages-menu\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                        <i class=\"fa fa-globe\"></i>
                        <span class=\"label label-danger\">{{ app.user.getNotificationCount()  }}</span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li class=\"header\">You have {{ app.user.getNotificationCount()  }} new notification{% if app.user.getNotificationCount()  != 1 %}s{% endif %}</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class=\"menu\">
                                {% for date, entry in timeline %}
                                    {% for event in entry %}
                                        {#{% if event.getType() > 10000 %}#}
                                        {% if event.getType() > 0 %}
                                            <li style=\"background-color: {% if event.getStatus == 'unread' %}{{ event.getColor() }}{% else %}white{% endif %}\"><!-- start message -->

                                                {#<a href=\"{% if event.getType > 10000 %}{{ path('visit_notif', {'id': event.getId() }) }}{% else %}#{% endif %}\" >#}
                                                    <a href=\"{% if event.getType > 0 %}{{ path('visit_notif', {'id': event.getId() }) }}{% else %}#{% endif %}\" >
                                                    <h4 style=\"margin:0\">
                                                        {{ event.getDetails() }}
                                                        <small><i class=\"fa {{ event.getIcon() }}\"></i> {{ event.getCreated()|date(\"H:i\") }}</small>
                                                    </h4>
                                                </a>
                                            </li>
                                        {% endif %}
                                        <!-- end message -->
                                    {% endfor %}
                                {% endfor %}
                            </ul>
                        </li>
                        <li class=\"footer\"><a href=\"{{ path('dashboard') }}\">See All Notifications</a></li>
                    </ul>
                </li>
                <!-- Messages: style can be found in dropdown.less-->
                <li class=\"dropdown messages-menu\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                        <i class=\"fa fa-envelope-o\"></i>
                        <span class=\"label label-danger\">{{ app.user.getMailCount() }}</span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <li class=\"header\">You have {{ app.user.getMailCount() }} new message{% if app.user.getMailCount() != 1 %}s{% endif %}</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class=\"menu\">
                                {% for mail in app.user.getMenuMail() %}
                                    <li><!-- start message -->
                                        <a href=\"#\" {% if mail.getStatus() == 'unread' %}style=\"background-color:#f4f4f4\"{% endif %}>
                                            <h4 style=\"margin:0\">
                                                Support Team <small><i class=\"fa fa-clock-o\"></i> {{ mail.getMail().getCreated()|date('d M. Y H:i') }}</small>
                                            </h4>
                                            <p style=\"margin:0\">{{ mail.getMail().getSubject() }}</p>
                                        </a>
                                    </li>
                                    <!-- end message -->
                                {% endfor %}
                            </ul>
                        </li>
                        <li class=\"footer\"><a href=\"{{ path('member_mails') }}\">See All Messages</a></li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class=\"dropdown user user-menu\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                        <img src=\"{{ asset(app.user.getAvatar()) }}\" class=\"user-image\" alt=\"User Image\">
                        <span class=\"hidden-xs\">{{ app.user.getName() }}</span>
                    </a>
                    <ul class=\"dropdown-menu\">
                        <!-- User image -->
                        <li class=\"user-header\">
                            <img src=\"{{ asset(app.user.getAvatar()) }}\" class=\"img-circle\" alt=\"User Image\">
                            <p>
                                {{ app.user.getName() }}
                                <small>{{ app.user.getUsername() }}</small>
                            </p>
                        </li> <!-- End User image -->
                        <!-- Menu Footer-->
                        <li class=\"user-footer\">
                            <div class=\"pull-left\">
                                {% if (app.user.getRole() == 'ROLE_SUPER_ADMIN') %}
                                    <a href=\"{{ path('adminprofile') }}\" class=\"btn btn-default btn-flat\">Profile</a>
                                {% else %}
                                    <a href=\"{{ path('profile') }}\" class=\"btn btn-default btn-flat\">Profile</a>
                                {% endif %}
                            </div>
                            <div class=\"pull-right\">
                                <a href=\"{{ path('logout') }}\" class=\"btn btn-default btn-flat\">Sign out</a>
                            </div>
                        </li> <!-- End Menu Footer-->
                    </ul>
                </li> <!-- End User Account -->
            </ul>

        </div> <!-- End Navbar Custom Menu -->

    </nav> <!-- End Header Navbar -->

</header> <!-- End Page Top Header -->", "member/member.page-top-header.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member\\member.page-top-header.html.twig");
    }
}
