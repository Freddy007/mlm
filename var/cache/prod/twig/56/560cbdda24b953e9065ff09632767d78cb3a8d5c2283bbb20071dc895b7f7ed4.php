<?php

/* god-mode/recommitments.html.twig */
class __TwigTemplate_92f3c479e8eed65ea4a70fb84970f8ca0aa6f7ceec2ab2e933b9168bacf8f0ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/recommitments.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ea043ca9a7cbf0628534a3c1c0a956ce8c6086ed3ae6f4f20ad522b5bc7fd88f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea043ca9a7cbf0628534a3c1c0a956ce8c6086ed3ae6f4f20ad522b5bc7fd88f->enter($__internal_ea043ca9a7cbf0628534a3c1c0a956ce8c6086ed3ae6f4f20ad522b5bc7fd88f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/recommitments.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ea043ca9a7cbf0628534a3c1c0a956ce8c6086ed3ae6f4f20ad522b5bc7fd88f->leave($__internal_ea043ca9a7cbf0628534a3c1c0a956ce8c6086ed3ae6f4f20ad522b5bc7fd88f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8585a500146007152ddef55321693f3cd7138e06095a9fa18638b6978b447ffd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8585a500146007152ddef55321693f3cd7138e06095a9fa18638b6978b447ffd->enter($__internal_8585a500146007152ddef55321693f3cd7138e06095a9fa18638b6978b447ffd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["recommitUsersCount"]) ? $context["recommitUsersCount"] : $this->getContext($context, "recommitUsersCount")), "html", null, true);
        echo "  Users waiting to Recommit List

";
        
        $__internal_8585a500146007152ddef55321693f3cd7138e06095a9fa18638b6978b447ffd->leave($__internal_8585a500146007152ddef55321693f3cd7138e06095a9fa18638b6978b447ffd_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_809b84ceba5545be742e528dc31ad2bb2fd42b9339b26e5eaf774c8af65a795c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_809b84ceba5545be742e528dc31ad2bb2fd42b9339b26e5eaf774c8af65a795c->enter($__internal_809b84ceba5545be742e528dc31ad2bb2fd42b9339b26e5eaf774c8af65a795c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "
    ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 12
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 13
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "
    ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 18
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 19
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            <br>
            <a href=\"/god-mode/recommitments/123\">";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["recommitUsersCount"]) ? $context["recommitUsersCount"] : $this->getContext($context, "recommitUsersCount")), "html", null, true);
        echo "</a>  Users waiting to Recommit List
            ";
        // line 29
        echo "        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-users\"></i> Merge Users</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <div class=\"col-sm-12\">
            </div>
            <div class=\"clearfix\"></div>
        </div>

        <div class=\"row\">
            <!-- Mini Side Bar -->
            ";
        // line 48
        echo "                ";
        // line 49
        echo "                    ";
        // line 50
        echo "                    ";
        // line 51
        echo "                    ";
        // line 52
        echo "                    ";
        // line 53
        echo "                    ";
        // line 54
        echo "                    ";
        // line 55
        echo "                    ";
        // line 56
        echo "                    ";
        // line 57
        echo "                    ";
        // line 58
        echo "                    ";
        // line 59
        echo "                    ";
        // line 60
        echo "                    ";
        // line 61
        echo "                    ";
        // line 62
        echo "                    ";
        // line 63
        echo "                    ";
        // line 64
        echo "                    ";
        // line 65
        echo "                ";
        // line 66
        echo "            ";
        // line 67
        echo "
            <div class=\"dropdown\" style=\"display:inline-block\">
                <button class=\"btn btn-warning dropdown-toggle\" type=\"button\" id=\"sortMenu\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Sort By <span class=\"caret\"></span></button>
                <ul class=\"dropdown-menu\" aria-labelledby=\"sortMenu\">
                    <li>
                        <a href=\"/god-mode/recommitments/123\">Recommitments Only </a>
                    </li>

                    <li>
                        <a href=\"/god-mode/recommitments/123?receivers=all\">All Receivers </a>
                    </li>

                </ul>
            </div>


            <!-- Main Content -->
            <div class=\"col-sm-12\">
                ";
        // line 85
        if (((isset($context["mergeType"]) ? $context["mergeType"] : $this->getContext($context, "mergeType")) == "automatic")) {
            // line 86
            echo "                    <div class=\"alert alert-warning\">
                        <p>Sorry, Merging Mode Set To Automatic. If you wish to manually handle things yourself, kindly switch mode back to Manual</p>
                    </div>
                ";
        } else {
            // line 90
            echo "                    <div class=\"row\">
                        <div class=\"col-sm-2\">
                        </div>
                        <div class=\"col-sm-8 text-center lead\">
                            <a href=\"/god-mode/recommitments/123\">";
            // line 94
            echo twig_escape_filter($this->env, (isset($context["recommitUsersCount"]) ? $context["recommitUsersCount"] : $this->getContext($context, "recommitUsersCount")), "html", null, true);
            echo "</a>  Users waiting to Recommit List


                            ";
            // line 98
            echo "                        </div>
                        <div class=\"col-sm-2\">
                        </div>
                    </div>
                    ";
            // line 103
            echo "


                    <table class=\"table table-hover table-responsive table-striped\">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Feeder(s)</th>
                            <th>Recommit</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
            // line 116
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["activeRecommitUsers"]) ? $context["activeRecommitUsers"] : $this->getContext($context, "activeRecommitUsers")));
            foreach ($context['_seq'] as $context["_key"] => $context["waitingSubscription"]) {
                // line 117
                echo "                            <tr>
                                <td>
                                    ";
                // line 119
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["waitingSubscription"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "<br>

                                    <button class=\"btn btn-primary history-btn\" type=\"button\" data-toggle=\"collapse\" data-id=\"";
                // line 121
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["waitingSubscription"], "getUser", array(), "method"), "getId", array(), "method"), "html", null, true);
                echo "\"
                                            data-target=\"#";
                // line 122
                echo twig_escape_filter($this->env, $this->getAttribute($context["waitingSubscription"], "getId", array(), "method"), "html", null, true);
                echo "\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                                        View  ";
                // line 123
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["waitingSubscription"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "'s history
                                    </button>

                                    <div class=\"collapse\" id=\"";
                // line 126
                echo twig_escape_filter($this->env, $this->getAttribute($context["waitingSubscription"], "getId", array(), "method"), "html", null, true);
                echo "\">
                                        <div class=\"well lists-";
                // line 127
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["waitingSubscription"], "getUser", array(), "method"), "getId", array(), "method"), "html", null, true);
                echo "\">

                                        </div>
                                    </div>

                                </td>
                                <td>";
                // line 133
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["waitingSubscription"], "getUser", array(), "method"), "getUsername", array(), "method"), "html", null, true);
                echo "</td>
                                <td>";
                // line 134
                echo twig_escape_filter($this->env, $this->getAttribute($context["waitingSubscription"], "getFeederTotal", array(), "method"), "html", null, true);
                echo "</td>
                                <td>
                                    ";
                // line 136
                if (($this->getAttribute($context["waitingSubscription"], "getRecommitRequired", array(), "method") == true)) {
                    // line 137
                    echo "                                        <input type=\"checkbox\" class=\"recommit\" checked data-id=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["waitingSubscription"], "getId", array(), "method"), "html", null, true);
                    echo "\">

                                    ";
                } else {
                    // line 140
                    echo "
                                        <input type=\"checkbox\" class=\"recommit\" data-id=\"";
                    // line 141
                    echo twig_escape_filter($this->env, $this->getAttribute($context["waitingSubscription"], "getId", array(), "method"), "html", null, true);
                    echo "\">

                                    ";
                }
                // line 144
                echo "
                                </td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['waitingSubscription'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 148
            echo "                        </tbody>
                    </table>

                    ";
            // line 152
            echo "                        ";
            // line 153
            echo "                    ";
            // line 154
            echo "                ";
        }
        // line 155
        echo "            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_809b84ceba5545be742e528dc31ad2bb2fd42b9339b26e5eaf774c8af65a795c->leave($__internal_809b84ceba5545be742e528dc31ad2bb2fd42b9339b26e5eaf774c8af65a795c_prof);

    }

    // line 161
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7d87a3cf62828e21dbb4efbcaed378bff3bea08db45a917e8874f227111f9591 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d87a3cf62828e21dbb4efbcaed378bff3bea08db45a917e8874f227111f9591->enter($__internal_7d87a3cf62828e21dbb4efbcaed378bff3bea08db45a917e8874f227111f9591_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 162
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(document).ready(function(\$) {
            \$('.recommit').on('click',function () {

                var status = \$(this).is(':checked') ? 1: 0;

                var id = \$(this).data('id');

                var base_url = window.location.origin;

                \$.get(base_url+'/god-mode/change-recommitment/'+id+'/'+status,function () {
//                    alert(id);
                }).fail(function(){
                    alert('failed');
                }).done(function(){
                    // Display a success toast, with a title
                    toastr.success('Action successful!', 'PaidLife Says!')
                })
            })

            \$('.history-btn').on('click',function(){
                var id = \$(this).data('id');

                var base_url = window.location.origin;

                \$.get(base_url+'/god-mode/subscription-history/'+id, function (data) {
//                \$.get('http://localhost/mainscript/public_html/god-mode/subscription-history/'+id, function (data) {

                }).done(function (data) {

                    var content = '';

                    \$.each(data, function (index, value) {
                        content+=''+ data[index] +''
                    });

                    content+='';
                    console.log(data.data);
                    console.log(content);

                    \$('.lists-'+id).html(content);

                }).fail(function(){
                    alert('failed');
                })
            })
        });
    </script>
";
        
        $__internal_7d87a3cf62828e21dbb4efbcaed378bff3bea08db45a917e8874f227111f9591->leave($__internal_7d87a3cf62828e21dbb4efbcaed378bff3bea08db45a917e8874f227111f9591_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/recommitments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 162,  331 => 161,  320 => 155,  317 => 154,  315 => 153,  313 => 152,  308 => 148,  299 => 144,  293 => 141,  290 => 140,  283 => 137,  281 => 136,  276 => 134,  272 => 133,  263 => 127,  259 => 126,  253 => 123,  249 => 122,  245 => 121,  240 => 119,  236 => 117,  232 => 116,  217 => 103,  211 => 98,  205 => 94,  199 => 90,  193 => 86,  191 => 85,  171 => 67,  169 => 66,  167 => 65,  165 => 64,  163 => 63,  161 => 62,  159 => 61,  157 => 60,  155 => 59,  153 => 58,  151 => 57,  149 => 56,  147 => 55,  145 => 54,  143 => 53,  141 => 52,  139 => 51,  137 => 50,  135 => 49,  133 => 48,  114 => 31,  110 => 29,  106 => 27,  99 => 22,  90 => 19,  87 => 18,  83 => 17,  80 => 16,  71 => 13,  68 => 12,  64 => 11,  61 => 10,  55 => 9,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}

    {{ recommitUsersCount }}  Users waiting to Recommit List

{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            <br>
            <a href=\"/god-mode/recommitments/123\">{{ recommitUsersCount }}</a>  Users waiting to Recommit List
            {#For {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) Package<br>#}
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-users\"></i> Merge Users</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <div class=\"col-sm-12\">
            </div>
            <div class=\"clearfix\"></div>
        </div>

        <div class=\"row\">
            <!-- Mini Side Bar -->
            {#<div class=\"col-sm-3\">#}
                {#<div class=\"list-group\">#}
                    {#<a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>#}
                    {#<a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>#}
                    {#<a href=\"{{ path('list_users') }}\" class=\"list-group-item\">List Users</a>#}
                    {#<a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>#}
                    {#<a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>#}
                    {#<a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>#}
                    {#<a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>#}
                    {#<a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>#}
                    {#<a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>#}
                    {#<a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>#}
                    {#<a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>#}
                    {#<a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>#}
                    {#<a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>#}
                    {#<a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>#}
                    {#<a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>#}
                    {#<a href=\"{{ path('god_mode_merge_users', {'pack' : pack.getId()}) }}\" class=\"list-group-item active\">Merge Users</a>#}
                {#</div>#}
            {#</div> <!-- End Mini Side Bar -->#}

            <div class=\"dropdown\" style=\"display:inline-block\">
                <button class=\"btn btn-warning dropdown-toggle\" type=\"button\" id=\"sortMenu\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Sort By <span class=\"caret\"></span></button>
                <ul class=\"dropdown-menu\" aria-labelledby=\"sortMenu\">
                    <li>
                        <a href=\"/god-mode/recommitments/123\">Recommitments Only </a>
                    </li>

                    <li>
                        <a href=\"/god-mode/recommitments/123?receivers=all\">All Receivers </a>
                    </li>

                </ul>
            </div>


            <!-- Main Content -->
            <div class=\"col-sm-12\">
                {% if mergeType == 'automatic' %}
                    <div class=\"alert alert-warning\">
                        <p>Sorry, Merging Mode Set To Automatic. If you wish to manually handle things yourself, kindly switch mode back to Manual</p>
                    </div>
                {% else %}
                    <div class=\"row\">
                        <div class=\"col-sm-2\">
                        </div>
                        <div class=\"col-sm-8 text-center lead\">
                            <a href=\"/god-mode/recommitments/123\">{{ recommitUsersCount }}</a>  Users waiting to Recommit List


                            {#{{ waitingSubscriptions.getTotalItemCount }} User{% if waitingSubscriptions.getTotalItemCount != 1 %}s{% endif %} in Waiting List For {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) Package#}
                        </div>
                        <div class=\"col-sm-2\">
                        </div>
                    </div>
                    {#</div>#}



                    <table class=\"table table-hover table-responsive table-striped\">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Feeder(s)</th>
                            <th>Recommit</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for waitingSubscription in activeRecommitUsers %}
                            <tr>
                                <td>
                                    {{ waitingSubscription.getUser().getName() }}<br>

                                    <button class=\"btn btn-primary history-btn\" type=\"button\" data-toggle=\"collapse\" data-id=\"{{ waitingSubscription.getUser().getId() }}\"
                                            data-target=\"#{{ waitingSubscription.getId() }}\" aria-expanded=\"false\" aria-controls=\"collapseExample\">
                                        View  {{ waitingSubscription.getUser().getName() }}'s history
                                    </button>

                                    <div class=\"collapse\" id=\"{{ waitingSubscription.getId() }}\">
                                        <div class=\"well lists-{{ waitingSubscription.getUser().getId() }}\">

                                        </div>
                                    </div>

                                </td>
                                <td>{{ waitingSubscription.getUser().getUsername() }}</td>
                                <td>{{ waitingSubscription.getFeederTotal() }}</td>
                                <td>
                                    {% if waitingSubscription.getRecommitRequired() == true %}
                                        <input type=\"checkbox\" class=\"recommit\" checked data-id=\"{{ waitingSubscription.getId() }}\">

                                    {% else %}

                                        <input type=\"checkbox\" class=\"recommit\" data-id=\"{{ waitingSubscription.getId() }}\">

                                    {% endif %}

                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>

                    {#<div class=\"navigation text-center\">#}
                        {#{{ knp_pagination_render(waitingSubscriptions) }}#}
                    {#</div>#}
                {% endif %}
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}

{% block javascripts %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(document).ready(function(\$) {
            \$('.recommit').on('click',function () {

                var status = \$(this).is(':checked') ? 1: 0;

                var id = \$(this).data('id');

                var base_url = window.location.origin;

                \$.get(base_url+'/god-mode/change-recommitment/'+id+'/'+status,function () {
//                    alert(id);
                }).fail(function(){
                    alert('failed');
                }).done(function(){
                    // Display a success toast, with a title
                    toastr.success('Action successful!', 'PaidLife Says!')
                })
            })

            \$('.history-btn').on('click',function(){
                var id = \$(this).data('id');

                var base_url = window.location.origin;

                \$.get(base_url+'/god-mode/subscription-history/'+id, function (data) {
//                \$.get('http://localhost/mainscript/public_html/god-mode/subscription-history/'+id, function (data) {

                }).done(function (data) {

                    var content = '';

                    \$.each(data, function (index, value) {
                        content+=''+ data[index] +''
                    });

                    content+='';
                    console.log(data.data);
                    console.log(content);

                    \$('.lists-'+id).html(content);

                }).fail(function(){
                    alert('failed');
                })
            })
        });
    </script>
{% endblock %}", "god-mode/recommitments.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\god-mode\\recommitments.html.twig");
    }
}
