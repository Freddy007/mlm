<?php

/* public/registration.html.twig */
class __TwigTemplate_33f094c2d52fcd0e07a8ec94d049e913cfea0bff71da02d79a0bdfee37993879 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("public.base.html.twig", "public/registration.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "public.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_135a1b3ed8f2682124aecbc89a63c63efb9d5dba7ab48c8f6ff5457e4bdd12b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_135a1b3ed8f2682124aecbc89a63c63efb9d5dba7ab48c8f6ff5457e4bdd12b8->enter($__internal_135a1b3ed8f2682124aecbc89a63c63efb9d5dba7ab48c8f6ff5457e4bdd12b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/registration.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_135a1b3ed8f2682124aecbc89a63c63efb9d5dba7ab48c8f6ff5457e4bdd12b8->leave($__internal_135a1b3ed8f2682124aecbc89a63c63efb9d5dba7ab48c8f6ff5457e4bdd12b8_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_2fdc96fbeeba200a2a05be28e37209a9cb93c4739dbce6c31d5d36c3e1c61615 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fdc96fbeeba200a2a05be28e37209a9cb93c4739dbce6c31d5d36c3e1c61615->enter($__internal_2fdc96fbeeba200a2a05be28e37209a9cb93c4739dbce6c31d5d36c3e1c61615_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Sign Up";
        
        $__internal_2fdc96fbeeba200a2a05be28e37209a9cb93c4739dbce6c31d5d36c3e1c61615->leave($__internal_2fdc96fbeeba200a2a05be28e37209a9cb93c4739dbce6c31d5d36c3e1c61615_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_31c6e63f6242d596dfc3f03e16bc376277e92a634232b13dac96fd7e3857dd49 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31c6e63f6242d596dfc3f03e16bc376277e92a634232b13dac96fd7e3857dd49->enter($__internal_31c6e63f6242d596dfc3f03e16bc376277e92a634232b13dac96fd7e3857dd49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Sign Up</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">

                <div class=\"register-box\" style=\"margin: 0 auto !important\">

                    <div class=\"register-box-body\">

                        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "userReferrerError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 20
            echo "                            <div class=\"alert alert-danger\">
                                <p>";
            // line 21
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "
                        ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "userReferrer"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 26
            echo "                            <div class=\"alert alert-success\">
                                <p>";
            // line 27
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
                        ";
        // line 31
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "

                            <div class=\"alert alert-warning\">
                                <p class=\"lead text-center\">You have selected the ";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getName", array(), "method"), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ") Package</p>
                            </div>

                            <div class=\"text-danger\">";
        // line 37
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'errors');
        echo "</div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "username", array()), 'widget');
        echo "
                                <span class=\"fa fa-user form-control-feedback\"></span>
                            </div>
                            <span class=\"text-danger\">
                                ";
        // line 44
        echo "                            </span>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 47
        echo "                                <span class=\"form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">";
        // line 49
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'errors');
        echo "</div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 51
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget');
        echo "
                                <span class=\"fa fa-user-o form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">";
        // line 54
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "</div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 56
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "
                                <span class=\"fa fa-envelope-o form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">
                                ";
        // line 61
        echo "                            </div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 64
        echo "                                <span class=\"fa fa-envelope-o form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">";
        // line 66
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phone_one", array()), 'errors');
        echo "</div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 68
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phone_one", array()), 'widget');
        echo "
                                <span class=\"fa fa-phone form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">";
        // line 71
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'errors');
        echo "</div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 73
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "first", array()), 'widget');
        echo "
                                <span class=\"fa fa-key form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">";
        // line 76
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'errors');
        echo "</div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 78
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "plainPassword", array()), "second", array()), 'widget');
        echo "
                                <span class=\"fa fa-lock form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">
                                ";
        // line 83
        echo "                            </div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 86
        echo "                                <span class=\"form-control-feedback\"></span>
                            </div>
                           <div class=\"text-danger\">
                               ";
        // line 90
        echo "                           </div>
                            <div class=\"form-group has-feedback\" style=\"line-height: 28px;\" >
                                ";
        // line 93
        echo "                                <span class=\"form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">
                                ";
        // line 97
        echo "                            </div>
                            <div class=\"form-group has-feedback\">
                                ";
        // line 100
        echo "                                <span class=\"form-control-feedback\"></span>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-xs-8\">
                                    <a href=\"";
        // line 104
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" class=\"text-center\">I already have an account</a>
                                </div>
                                <!-- /.col -->
                                <div class=\"col-xs-4\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Register</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        ";
        // line 112
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

                    </div>

                </div>
            </div>
        </div>
    </section>

";
        
        $__internal_31c6e63f6242d596dfc3f03e16bc376277e92a634232b13dac96fd7e3857dd49->leave($__internal_31c6e63f6242d596dfc3f03e16bc376277e92a634232b13dac96fd7e3857dd49_prof);

    }

    public function getTemplateName()
    {
        return "public/registration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 112,  234 => 104,  228 => 100,  224 => 97,  219 => 93,  215 => 90,  210 => 86,  206 => 83,  199 => 78,  194 => 76,  188 => 73,  183 => 71,  177 => 68,  172 => 66,  168 => 64,  164 => 61,  157 => 56,  152 => 54,  146 => 51,  141 => 49,  137 => 47,  133 => 44,  126 => 39,  121 => 37,  112 => 34,  106 => 31,  103 => 30,  94 => 27,  91 => 26,  87 => 25,  84 => 24,  75 => 21,  72 => 20,  68 => 19,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'public.base.html.twig' %}

{% block title %}Sign Up{% endblock %}

{% block body %}

    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Sign Up</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">

                <div class=\"register-box\" style=\"margin: 0 auto !important\">

                    <div class=\"register-box-body\">

                        {% for flash_message in app.session.flashBag.get('userReferrerError') %}
                            <div class=\"alert alert-danger\">
                                <p>{{ flash_message }}</p>
                            </div>
                        {% endfor %}

                        {% for flash_message in app.session.flashBag.get('userReferrer') %}
                            <div class=\"alert alert-success\">
                                <p>{{ flash_message }}</p>
                            </div>
                        {% endfor %}

                        {{ form_start(form) }}

                            <div class=\"alert alert-warning\">
                                <p class=\"lead text-center\">You have selected the {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) Package</p>
                            </div>

                            <div class=\"text-danger\">{{ form_errors(form.username) }}</div>
                            <div class=\"form-group has-feedback\">
                                {{ form_widget(form.username) }}
                                <span class=\"fa fa-user form-control-feedback\"></span>
                            </div>
                            <span class=\"text-danger\">
                                {#{{ form_errors(form.title) }}#}
                            </span>
                            <div class=\"form-group has-feedback\">
                                {#{{ form_widget(form.title) }}#}
                                <span class=\"form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">{{ form_errors(form.name) }}</div>
                            <div class=\"form-group has-feedback\">
                                {{ form_widget(form.name) }}
                                <span class=\"fa fa-user-o form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">{{ form_errors(form.email) }}</div>
                            <div class=\"form-group has-feedback\">
                                {{ form_widget(form.email) }}
                                <span class=\"fa fa-envelope-o form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">
                                {#{{ form_errors(form.bitcoin_address) }}#}
                            </div>
                            <div class=\"form-group has-feedback\">
                                {#{{ form_widget(form.bitcoin_address) }}#}
                                <span class=\"fa fa-envelope-o form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">{{ form_errors(form.phone_one) }}</div>
                            <div class=\"form-group has-feedback\">
                                {{ form_widget(form.phone_one) }}
                                <span class=\"fa fa-phone form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">{{ form_errors(form.plainPassword.first) }}</div>
                            <div class=\"form-group has-feedback\">
                                {{ form_widget(form.plainPassword.first) }}
                                <span class=\"fa fa-key form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">{{ form_errors(form.plainPassword.second) }}</div>
                            <div class=\"form-group has-feedback\">
                                {{ form_widget(form.plainPassword.second) }}
                                <span class=\"fa fa-lock form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">
                                {#{{ form_errors(form.sex) }}#}
                            </div>
                            <div class=\"form-group has-feedback\">
                                {#{{ form_widget(form.sex) }}#}
                                <span class=\"form-control-feedback\"></span>
                            </div>
                           <div class=\"text-danger\">
                               {#{{ form_errors(form.resident_country) }}#}
                           </div>
                            <div class=\"form-group has-feedback\" style=\"line-height: 28px;\" >
                                {#{{ form_widget(form.resident_country) }}#}
                                <span class=\"form-control-feedback\"></span>
                            </div>
                            <div class=\"text-danger\">
                                {#{{ form_errors(form.resident_state) }}#}
                            </div>
                            <div class=\"form-group has-feedback\">
                                {#{{ form_widget(form.resident_state) }}#}
                                <span class=\"form-control-feedback\"></span>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-xs-8\">
                                    <a href=\"{{ path('login') }}\" class=\"text-center\">I already have an account</a>
                                </div>
                                <!-- /.col -->
                                <div class=\"col-xs-4\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Register</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        {{ form_end(form) }}

                    </div>

                </div>
            </div>
        </div>
    </section>

{% endblock %}", "public/registration.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public\\registration.html.twig");
    }
}
