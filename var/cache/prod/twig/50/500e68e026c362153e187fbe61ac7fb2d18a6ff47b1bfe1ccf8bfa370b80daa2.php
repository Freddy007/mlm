<?php

/* god-mode/compose-mail.html.twig */
class __TwigTemplate_997218cdb9f753447d43ea72ea6758bed322a5cb528e5877e4ca246e9a3a2ec6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/compose-mail.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71f599be147ac281dbda10c6ab234252f2cc89a8b54b736405e0b0abc6a7d386 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71f599be147ac281dbda10c6ab234252f2cc89a8b54b736405e0b0abc6a7d386->enter($__internal_71f599be147ac281dbda10c6ab234252f2cc89a8b54b736405e0b0abc6a7d386_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/compose-mail.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_71f599be147ac281dbda10c6ab234252f2cc89a8b54b736405e0b0abc6a7d386->leave($__internal_71f599be147ac281dbda10c6ab234252f2cc89a8b54b736405e0b0abc6a7d386_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_9ec75279979b3d708312e9fb617cda410178bd28e49ba41b8ab459880f6507e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ec75279979b3d708312e9fb617cda410178bd28e49ba41b8ab459880f6507e5->enter($__internal_9ec75279979b3d708312e9fb617cda410178bd28e49ba41b8ab459880f6507e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Compose Mail";
        
        $__internal_9ec75279979b3d708312e9fb617cda410178bd28e49ba41b8ab459880f6507e5->leave($__internal_9ec75279979b3d708312e9fb617cda410178bd28e49ba41b8ab459880f6507e5_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_f2e8ed131ff8b8023d18bd069f595a0d38b7f71d2c63c16ef603d3e07b874909 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2e8ed131ff8b8023d18bd069f595a0d38b7f71d2c63c16ef603d3e07b874909->enter($__internal_f2e8ed131ff8b8023d18bd069f595a0d38b7f71d2c63c16ef603d3e07b874909_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "mailSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "mailError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Compose Mail</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li><a href=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\"><i class=\"fa fa-newspaper-o\"></i> Mail</a></li>
            <li class=\"active\"><i class=\"fa fa-plus\"></i> Compose Mail</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item\">List Admin</a>
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item\">List Users</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item\">List Payments</a>
                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item active\">Compose Mail</a>
                    <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <h3>Compose Mail</h3>
      \t\t\t\t";
        // line 56
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail"), "attr" => array("class" => "form-horizontal")));
        echo "

          \t\t\t\t<span class=\"text-danger\">";
        // line 58
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subject", array()), 'errors');
        echo "</span>
      \t\t\t\t\t<div class=\"form-group\">
                \t\t\t";
        // line 60
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subject", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Subject"));
        echo "

                \t\t\t<div class=\"col-sm-10\">
                  \t\t\t\t";
        // line 63
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "subject", array()), 'widget');
        echo "
                \t\t\t</div>
          \t\t\t\t</div>

          \t\t\t\t<span class=\"text-danger\">";
        // line 67
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mail", array()), 'errors');
        echo "</span>
      \t\t\t\t\t<div class=\"form-group\">
                \t\t\t";
        // line 69
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mail", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Mail"));
        echo "

                \t\t\t<div class=\"col-sm-10\">
                  \t\t\t\t";
        // line 72
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "mail", array()), 'widget', array("attr" => array("class" => "bswysiwyg", "style" => "width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;")));
        echo "
                \t\t\t</div>
          \t\t\t\t</div>

\t\t\t            <div class=\"row\">
\t\t\t            \t<div class=\"col-sm-12\">
\t\t\t            \t\t<button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Compose Mail</button>
\t\t\t            \t</div>
\t\t\t            </div>
      \t\t\t\t";
        // line 81
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            </div> <!-- End Main Content -->
    \t</div>
    </section> <!-- End Main content -->
";
        
        $__internal_f2e8ed131ff8b8023d18bd069f595a0d38b7f71d2c63c16ef603d3e07b874909->leave($__internal_f2e8ed131ff8b8023d18bd069f595a0d38b7f71d2c63c16ef603d3e07b874909_prof);

    }

    // line 87
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_452bcec90e005e04f4c6551a754d4c705605c590337392c0a5ba77cc06f3643c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_452bcec90e005e04f4c6551a754d4c705605c590337392c0a5ba77cc06f3643c->enter($__internal_452bcec90e005e04f4c6551a754d4c705605c590337392c0a5ba77cc06f3643c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 88
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function () {
            //bootstrap WYSIHTML5 - text editor
            \$(\".bswysiwyg\").wysihtml5({
              toolbar: {
                \"image\": false, //Button to insert an image. Default true,
                \"color\": true
              }
            });
        });
    </script>
";
        
        $__internal_452bcec90e005e04f4c6551a754d4c705605c590337392c0a5ba77cc06f3643c->leave($__internal_452bcec90e005e04f4c6551a754d4c705605c590337392c0a5ba77cc06f3643c_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/compose-mail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  247 => 88,  241 => 87,  229 => 81,  217 => 72,  211 => 69,  206 => 67,  199 => 63,  193 => 60,  188 => 58,  183 => 56,  173 => 49,  169 => 48,  165 => 47,  161 => 46,  157 => 45,  153 => 44,  149 => 43,  145 => 42,  141 => 41,  137 => 40,  133 => 39,  129 => 38,  125 => 37,  121 => 36,  117 => 35,  103 => 24,  99 => 23,  92 => 18,  83 => 15,  80 => 14,  76 => 13,  73 => 12,  64 => 9,  61 => 8,  57 => 7,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Compose Mail{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('mailSuccess') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('mailError') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Compose Mail</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li><a href=\"{{ path('god_mode_mails') }}\"><i class=\"fa fa-newspaper-o\"></i> Mail</a></li>
            <li class=\"active\"><i class=\"fa fa-plus\"></i> Compose Mail</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item\">List Users</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item active\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <h3>Compose Mail</h3>
      \t\t\t\t{{ form_start(form, {'action': path('god_mode_compose_mail'), 'attr': {'class': 'form-horizontal'}}) }}

          \t\t\t\t<span class=\"text-danger\">{{ form_errors(form.subject) }}</span>
      \t\t\t\t\t<div class=\"form-group\">
                \t\t\t{{ form_label(form.subject, \"Subject\", { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                \t\t\t<div class=\"col-sm-10\">
                  \t\t\t\t{{ form_widget(form.subject) }}
                \t\t\t</div>
          \t\t\t\t</div>

          \t\t\t\t<span class=\"text-danger\">{{ form_errors(form.mail) }}</span>
      \t\t\t\t\t<div class=\"form-group\">
                \t\t\t{{ form_label(form.mail, \"Mail\", { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                \t\t\t<div class=\"col-sm-10\">
                  \t\t\t\t{{ form_widget(form.mail, { 'attr': {'class': 'bswysiwyg', 'style': 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'} }) }}
                \t\t\t</div>
          \t\t\t\t</div>

\t\t\t            <div class=\"row\">
\t\t\t            \t<div class=\"col-sm-12\">
\t\t\t            \t\t<button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Compose Mail</button>
\t\t\t            \t</div>
\t\t\t            </div>
      \t\t\t\t{{ form_end(form) }}
            </div> <!-- End Main Content -->
    \t</div>
    </section> <!-- End Main content -->
{% endblock %}

{% block javascripts %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(function () {
            //bootstrap WYSIHTML5 - text editor
            \$(\".bswysiwyg\").wysihtml5({
              toolbar: {
                \"image\": false, //Button to insert an image. Default true,
                \"color\": true
              }
            });
        });
    </script>
{% endblock %}", "god-mode/compose-mail.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\compose-mail.html.twig");
    }
}
