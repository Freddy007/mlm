<?php

/* public/public.footer.html.twig */
class __TwigTemplate_2ed225e13322e76e5ceb6c862ca2e34fd88cc9aaa6bd854b9fd6e3315568a2a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a6b189526fd80886cca799d2cdeaaf167ffb738c37383df2e483ae5861f2859a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6b189526fd80886cca799d2cdeaaf167ffb738c37383df2e483ae5861f2859a->enter($__internal_a6b189526fd80886cca799d2cdeaaf167ffb738c37383df2e483ae5861f2859a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/public.footer.html.twig"));

        // line 1
        echo "<!-- Footer -->
<footer class=\"text-center\" style=\"position: relative; bottom: -150px;\">
    <div class=\"footer-below\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    Copyright &copy; ";
        // line 7
        echo twig_escape_filter($this->env, ($context["app_name"] ?? $this->getContext($context, "app_name")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo "
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class=\"scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md\">
    <a class=\"btn btn-primary\" href=\"#page-top\">
        <i class=\"fa fa-chevron-up\"></i>
    </a>
</div>";
        
        $__internal_a6b189526fd80886cca799d2cdeaaf167ffb738c37383df2e483ae5861f2859a->leave($__internal_a6b189526fd80886cca799d2cdeaaf167ffb738c37383df2e483ae5861f2859a_prof);

    }

    public function getTemplateName()
    {
        return "public/public.footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 7,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Footer -->
<footer class=\"text-center\" style=\"position: relative; bottom: -150px;\">
    <div class=\"footer-below\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    Copyright &copy; {{ app_name }} {{ 'now'|date('Y') }}
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class=\"scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md\">
    <a class=\"btn btn-primary\" href=\"#page-top\">
        <i class=\"fa fa-chevron-up\"></i>
    </a>
</div>", "public/public.footer.html.twig", "/var/www/html/mlm/app/Resources/views/public/public.footer.html.twig");
    }
}
