<?php

/* god-mode/edit-pack.html.twig */
class __TwigTemplate_bfc0bea0a89fbede5952f3fc066e2d8e07e8f58ecba2e15b878b34160c8ce54c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/edit-pack.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42f4fe105f4b1a0236a9959ad07dd250ed0dec24a106e0569da4ac8df0017073 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42f4fe105f4b1a0236a9959ad07dd250ed0dec24a106e0569da4ac8df0017073->enter($__internal_42f4fe105f4b1a0236a9959ad07dd250ed0dec24a106e0569da4ac8df0017073_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/edit-pack.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_42f4fe105f4b1a0236a9959ad07dd250ed0dec24a106e0569da4ac8df0017073->leave($__internal_42f4fe105f4b1a0236a9959ad07dd250ed0dec24a106e0569da4ac8df0017073_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e04c7ca2079863052f2b6034e0834767603ba5233c08b1fff8ddb00dfe66aa7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e04c7ca2079863052f2b6034e0834767603ba5233c08b1fff8ddb00dfe66aa7a->enter($__internal_e04c7ca2079863052f2b6034e0834767603ba5233c08b1fff8ddb00dfe66aa7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Edit ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getName", array(), "method"), "html", null, true);
        echo " Pack (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ")";
        
        $__internal_e04c7ca2079863052f2b6034e0834767603ba5233c08b1fff8ddb00dfe66aa7a->leave($__internal_e04c7ca2079863052f2b6034e0834767603ba5233c08b1fff8ddb00dfe66aa7a_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_0f4c1fb32297e7d523009ee76e1e092941551e8ddcb47b8f8a58a9a8d2872be2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f4c1fb32297e7d523009ee76e1e092941551e8ddcb47b8f8a58a9a8d2872be2->enter($__internal_0f4c1fb32297e7d523009ee76e1e092941551e8ddcb47b8f8a58a9a8d2872be2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Edit ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getName", array(), "method"), "html", null, true);
        echo " Plan (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ") <button class=\"btn btn-lg btn-danger\" data-toggle=\"modal\" data-target=\"#viewCancelModal\">Delete</button></h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li><a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\"><i class=\"fa fa-pie-chart\"></i> Packages</a></li>
            <li class=\"active\"><i class=\"fa fa-plus\"></i> Edit ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getName", array(), "method"), "html", null, true);
        echo " Pack (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ")</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item\">List Admin</a>
                    <a href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item\">List Users</a>
                    <a href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item\">List Payments</a>
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <h3>Edit ";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getName", array(), "method"), "html", null, true);
        echo " Pack (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ")</h3>
                ";
        // line 51
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_pack", array("pack" => $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getId", array(), "method"))), "attr" => array("class" => "form-horizontal")));
        echo "

                    <span class=\"text-danger\">";
        // line 53
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'errors');
        echo "</span>
                    <div class=\"form-group\">
                        ";
        // line 55
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Name"));
        echo "

                        <div class=\"col-sm-9\">
                            ";
        // line 58
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget');
        echo "
                        </div>
                    </div>

                    <span class=\"text-danger\">";
        // line 62
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'errors');
        echo "</span>
                    <div class=\"form-group\">
                        ";
        // line 64
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Amount"));
        echo "

                        <div class=\"col-sm-9\">
                            ";
        // line 67
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "amount", array()), 'widget');
        echo "
                        </div>
                    </div>

                    <span class=\"text-danger\">";
        // line 71
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "feeders", array()), 'errors');
        echo "</span>
                    <div class=\"form-group\">
                        ";
        // line 73
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "feeders", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Feeders"));
        echo "

                        <div class=\"col-sm-9\">
                            ";
        // line 76
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "feeders", array()), 'widget');
        echo "
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Update</button>
                        </div>
                    </div>
                ";
        // line 85
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_0f4c1fb32297e7d523009ee76e1e092941551e8ddcb47b8f8a58a9a8d2872be2->leave($__internal_0f4c1fb32297e7d523009ee76e1e092941551e8ddcb47b8f8a58a9a8d2872be2_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/edit-pack.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 85,  239 => 76,  233 => 73,  228 => 71,  221 => 67,  215 => 64,  210 => 62,  203 => 58,  197 => 55,  192 => 53,  187 => 51,  180 => 50,  171 => 44,  167 => 43,  163 => 42,  159 => 41,  155 => 40,  151 => 39,  147 => 38,  143 => 37,  139 => 36,  135 => 35,  131 => 34,  127 => 33,  123 => 32,  119 => 31,  115 => 30,  98 => 19,  94 => 18,  90 => 17,  82 => 15,  77 => 12,  68 => 9,  65 => 8,  61 => 7,  58 => 6,  52 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Edit {{ pack.getName() }} Pack ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}){% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Edit {{ pack.getName() }} Plan ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) <button class=\"btn btn-lg btn-danger\" data-toggle=\"modal\" data-target=\"#viewCancelModal\">Delete</button></h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li><a href=\"{{ path('list_pack') }}\"><i class=\"fa fa-pie-chart\"></i> Packages</a></li>
            <li class=\"active\"><i class=\"fa fa-plus\"></i> Edit {{ pack.getName() }} Pack ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }})</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item\">List Users</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <h3>Edit {{ pack.getName() }} Pack ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }})</h3>
                {{ form_start(form, {'action': path('edit_pack', {'pack' : pack.getId()}), 'attr': {'class': 'form-horizontal'}}) }}

                    <span class=\"text-danger\">{{ form_errors(form.name) }}</span>
                    <div class=\"form-group\">
                        {{ form_label(form.name, 'Name', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                        <div class=\"col-sm-9\">
                            {{ form_widget(form.name) }}
                        </div>
                    </div>

                    <span class=\"text-danger\">{{ form_errors(form.amount) }}</span>
                    <div class=\"form-group\">
                        {{ form_label(form.amount, 'Amount', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                        <div class=\"col-sm-9\">
                            {{ form_widget(form.amount) }}
                        </div>
                    </div>

                    <span class=\"text-danger\">{{ form_errors(form.feeders) }}</span>
                    <div class=\"form-group\">
                        {{ form_label(form.feeders, 'Feeders', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                        <div class=\"col-sm-9\">
                            {{ form_widget(form.feeders) }}
                        </div>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-sm-12\">
                            <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Update</button>
                        </div>
                    </div>
                {{ form_end(form) }}
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}", "god-mode/edit-pack.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\edit-pack.html.twig");
    }
}
