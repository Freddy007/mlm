<?php

/* demi-god-mode/admin-list-jury.html.twig */
class __TwigTemplate_68bf22f33738256d5b77816c9dc4bca72de2646cc5f7b67b75dea8f97a9f3dd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "demi-god-mode/admin-list-jury.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5eacd2042d699a9cd02460aacf831d13ce6c8a38912a61e16668ef8394b3a63b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5eacd2042d699a9cd02460aacf831d13ce6c8a38912a61e16668ef8394b3a63b->enter($__internal_5eacd2042d699a9cd02460aacf831d13ce6c8a38912a61e16668ef8394b3a63b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "demi-god-mode/admin-list-jury.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5eacd2042d699a9cd02460aacf831d13ce6c8a38912a61e16668ef8394b3a63b->leave($__internal_5eacd2042d699a9cd02460aacf831d13ce6c8a38912a61e16668ef8394b3a63b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c64d5cacc8df4fccb6524aa7b93c43122e4150df53bc6179bf8daa6ae030832d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c64d5cacc8df4fccb6524aa7b93c43122e4150df53bc6179bf8daa6ae030832d->enter($__internal_c64d5cacc8df4fccb6524aa7b93c43122e4150df53bc6179bf8daa6ae030832d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Admin Court Cases";
        
        $__internal_c64d5cacc8df4fccb6524aa7b93c43122e4150df53bc6179bf8daa6ae030832d->leave($__internal_c64d5cacc8df4fccb6524aa7b93c43122e4150df53bc6179bf8daa6ae030832d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_3e826e8d126aad6e04f265fb5d152e63f5341e484b17ec6002db54ea8732f2da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e826e8d126aad6e04f265fb5d152e63f5341e484b17ec6002db54ea8732f2da->enter($__internal_3e826e8d126aad6e04f265fb5d152e63f5341e484b17ec6002db54ea8732f2da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tAdmin Court Cases
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-university\"></i> Court</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">
                <!-- small box -->
                <div class=\"small-box bg-purple\">
                    <div class=\"inner\">
                        <h3>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getJuryCount", array(), "method"), "html", null, true);
        echo "</h3>
                        <p>Active Court Cases</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"fa fa-university\"></i>
                    </div>
                    <a class=\"small-box-footer\">Active Court Cases <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
                    <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("demi_god_jury");
        echo "\" class=\"list-group-item list-group-item-success\">View Cases</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
    \t\t\t";
        // line 42
        if (($this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getTotalItemCount", array()) == 0)) {
            // line 43
            echo "    \t\t\t\t<div class=\"alert alert-info\">
    \t\t\t\t\t<p class=\"lead text-center\">No Court Cases Yet</p>
    \t\t\t\t\t<p>Court cases only appear here when a member has a payment dispute with another member. Payment Disputes range from \"Upline not confirming payment before the given time elapses\" to \"Downline presenting payment evidence that has been marked as dubious by the upline\". When members have disputes of such, their court cases will appear here.</p>
    \t\t\t\t</div>
    \t\t\t";
        } else {
            // line 48
            echo "\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getTotalItemCount", array()), "html", null, true);
            echo " Case";
            if (($this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            echo "</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>#</th>
\t    \t\t\t\t\t\t\t\t<th>Receiver</th>
\t    \t\t\t\t\t\t\t\t<th>Feeder</th>
\t    \t\t\t\t\t\t\t\t<th>Status</th>
\t    \t\t\t\t\t\t\t\t<th>Date</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")));
            foreach ($context['_seq'] as $context["_key"] => $context["case"]) {
                // line 67
                echo "\t    \t\t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t\t<td><a href=\"";
                // line 68
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("demi_god_case", array("jury" => $this->getAttribute($context["case"], "getId", array(), "method"))), "html", null, true);
                echo "\">Case #";
                echo twig_escape_filter($this->env, $this->getAttribute($context["case"], "getId", array(), "method"), "html", null, true);
                echo "</a></td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["case"], "getReceiver", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["case"], "getFeeder", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t            ";
                // line 72
                if (($this->getAttribute($context["case"], "getStatus", array(), "method") == "giver_reply")) {
                    // line 73
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-info\">Feeder Reply</button>
\t\t\t\t\t\t\t\t\t            ";
                } elseif (($this->getAttribute(                // line 74
$context["case"], "getStatus", array(), "method") == "receiver_reply")) {
                    // line 75
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-primary\">Receiver Reply</button>
\t\t\t\t\t\t\t\t\t            ";
                } elseif (($this->getAttribute(                // line 76
$context["case"], "getStatus", array(), "method") == "admin_reply")) {
                    // line 77
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-warning\">Admin Reply</button>
\t\t\t\t\t\t\t\t\t            ";
                } elseif (($this->getAttribute(                // line 78
$context["case"], "getStatus", array(), "method") == "open")) {
                    // line 79
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-success\">Open</button>
\t\t\t\t\t\t\t\t\t            ";
                } elseif (($this->getAttribute(                // line 80
$context["case"], "getStatus", array(), "method") == "closed")) {
                    // line 81
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-danger\">Closed</button>
\t\t\t\t\t\t\t\t\t            ";
                }
                // line 83
                echo "\t    \t\t\t\t\t\t\t\t\t</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 84
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["case"], "getCreatedDate", array(), "method"), "d F, Y"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['case'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 87
            echo "\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    ";
            // line 93
            echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")));
            echo "
\t                </div>
    \t\t\t";
        }
        // line 96
        echo "    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section> <!-- End Main Content -->

";
        
        $__internal_3e826e8d126aad6e04f265fb5d152e63f5341e484b17ec6002db54ea8732f2da->leave($__internal_3e826e8d126aad6e04f265fb5d152e63f5341e484b17ec6002db54ea8732f2da_prof);

    }

    public function getTemplateName()
    {
        return "demi-god-mode/admin-list-jury.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 96,  208 => 93,  200 => 87,  191 => 84,  188 => 83,  184 => 81,  182 => 80,  179 => 79,  177 => 78,  174 => 77,  172 => 76,  169 => 75,  167 => 74,  164 => 73,  162 => 72,  157 => 70,  153 => 69,  147 => 68,  144 => 67,  140 => 66,  113 => 50,  109 => 48,  102 => 43,  100 => 42,  91 => 36,  77 => 25,  61 => 12,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Admin Court Cases{% endblock %}

{% block body %}
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tAdmin Court Cases
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-university\"></i> Court</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">
                <!-- small box -->
                <div class=\"small-box bg-purple\">
                    <div class=\"inner\">
                        <h3>{{ app.user.getJuryCount() }}</h3>
                        <p>Active Court Cases</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"fa fa-university\"></i>
                    </div>
                    <a class=\"small-box-footer\">Active Court Cases <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
                    <a href=\"{{ path('demi_god_jury') }}\" class=\"list-group-item list-group-item-success\">View Cases</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
    \t\t\t{% if cases.getTotalItemCount == 0 %}
    \t\t\t\t<div class=\"alert alert-info\">
    \t\t\t\t\t<p class=\"lead text-center\">No Court Cases Yet</p>
    \t\t\t\t\t<p>Court cases only appear here when a member has a payment dispute with another member. Payment Disputes range from \"Upline not confirming payment before the given time elapses\" to \"Downline presenting payment evidence that has been marked as dubious by the upline\". When members have disputes of such, their court cases will appear here.</p>
    \t\t\t\t</div>
    \t\t\t{% else %}
\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing {{ cases.getPaginationData.firstItemNumber }} to {{ cases.getPaginationData.lastItemNumber }} out of {{ cases.getTotalItemCount }} Case{% if cases.getTotalItemCount != 1 %}s{% endif %}</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>#</th>
\t    \t\t\t\t\t\t\t\t<th>Receiver</th>
\t    \t\t\t\t\t\t\t\t<th>Feeder</th>
\t    \t\t\t\t\t\t\t\t<th>Status</th>
\t    \t\t\t\t\t\t\t\t<th>Date</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t{% for case in cases %}
\t    \t\t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t\t<td><a href=\"{{ path('demi_god_case', {'jury' : case.getId()}) }}\">Case #{{ case.getId() }}</a></td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ case.getReceiver().getName() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ case.getFeeder().getName() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t            {% if case.getStatus() == 'giver_reply' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-info\">Feeder Reply</button>
\t\t\t\t\t\t\t\t\t            {% elseif case.getStatus() == 'receiver_reply' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-primary\">Receiver Reply</button>
\t\t\t\t\t\t\t\t\t            {% elseif case.getStatus() == 'admin_reply' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-warning\">Admin Reply</button>
\t\t\t\t\t\t\t\t\t            {% elseif case.getStatus() == 'open' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-success\">Open</button>
\t\t\t\t\t\t\t\t\t            {% elseif case.getStatus() == 'closed' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-danger\">Closed</button>
\t\t\t\t\t\t\t\t\t            {% endif %}
\t    \t\t\t\t\t\t\t\t\t</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ case.getCreatedDate()|date('d F, Y') }}</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t{% endfor %}
\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    {{ knp_pagination_render(cases) }}
\t                </div>
    \t\t\t{% endif %}
    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section> <!-- End Main Content -->

{% endblock %}", "demi-god-mode/admin-list-jury.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\demi-god-mode\\admin-list-jury.html.twig");
    }
}
