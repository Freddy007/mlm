<?php

/* member/awaiting-merging.html.twig */
class __TwigTemplate_8607083d7213cba173987157832a833c546e8b662d3f9c52361dc75dc35a9a68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/awaiting-merging.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_391640293f50e8289a84db114a78ecd51f8d6cd3507082d3f201f72748fcb560 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_391640293f50e8289a84db114a78ecd51f8d6cd3507082d3f201f72748fcb560->enter($__internal_391640293f50e8289a84db114a78ecd51f8d6cd3507082d3f201f72748fcb560_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/awaiting-merging.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_391640293f50e8289a84db114a78ecd51f8d6cd3507082d3f201f72748fcb560->leave($__internal_391640293f50e8289a84db114a78ecd51f8d6cd3507082d3f201f72748fcb560_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_48c76e49ca78be9f83df9eb49969fdfc973d33d1fd992ade9b7c87363d9fdf5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_48c76e49ca78be9f83df9eb49969fdfc973d33d1fd992ade9b7c87363d9fdf5d->enter($__internal_48c76e49ca78be9f83df9eb49969fdfc973d33d1fd992ade9b7c87363d9fdf5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Awaiting Merging";
        
        $__internal_48c76e49ca78be9f83df9eb49969fdfc973d33d1fd992ade9b7c87363d9fdf5d->leave($__internal_48c76e49ca78be9f83df9eb49969fdfc973d33d1fd992ade9b7c87363d9fdf5d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_2e6631a7bd7a7e77ef9db8adc0f3c93f2ab8ddada68d86dd94eacb102ab7f019 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e6631a7bd7a7e77ef9db8adc0f3c93f2ab8ddada68d86dd94eacb102ab7f019->enter($__internal_2e6631a7bd7a7e77ef9db8adc0f3c93f2ab8ddada68d86dd94eacb102ab7f019_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
\t";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "autoRecycleSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "    \t<div class=\"callout callout-info\">
        \t<p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
    \t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
\t";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "subscribeSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "    \t<div class=\"callout callout-success\">
        \t<p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
    \t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>Awaiting Merging - ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getName", array(), "method"), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ")</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li><a href=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("packs");
        echo "\"><i class=\"fa fa-pie-chart\"></i> Packs</a></li>
        \t<li class=\"active\">Awaiting Merging</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"row\">
      \t\t<div class=\"col-sm-12\">
      \t\t\t<div class=\"alert alert-info\">
      \t\t\t\t<p>We have taken your request for processing. Kindly be patient as the system finds the next available member for you to make payment to.</p>
      \t\t\t</div>
      \t\t</div>
      \t</div>
    </section> <!-- End Main Content -->
";
        
        $__internal_2e6631a7bd7a7e77ef9db8adc0f3c93f2ab8ddada68d86dd94eacb102ab7f019->leave($__internal_2e6631a7bd7a7e77ef9db8adc0f3c93f2ab8ddada68d86dd94eacb102ab7f019_prof);

    }

    public function getTemplateName()
    {
        return "member/awaiting-merging.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 24,  104 => 23,  96 => 21,  91 => 18,  82 => 15,  79 => 14,  75 => 13,  72 => 12,  63 => 9,  60 => 8,  56 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Awaiting Merging{% endblock %}

{% block body %}

\t{% for flash_message in app.session.flashBag.get('autoRecycleSuccess') %}
    \t<div class=\"callout callout-info\">
        \t<p>{{ flash_message }}</p>
    \t</div>
\t{% endfor %}

\t{% for flash_message in app.session.flashBag.get('subscribeSuccess') %}
    \t<div class=\"callout callout-success\">
        \t<p>{{ flash_message }}</p>
    \t</div>
\t{% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>Awaiting Merging - {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }})</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li><a href=\"{{ path('packs') }}\"><i class=\"fa fa-pie-chart\"></i> Packs</a></li>
        \t<li class=\"active\">Awaiting Merging</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"row\">
      \t\t<div class=\"col-sm-12\">
      \t\t\t<div class=\"alert alert-info\">
      \t\t\t\t<p>We have taken your request for processing. Kindly be patient as the system finds the next available member for you to make payment to.</p>
      \t\t\t</div>
      \t\t</div>
      \t</div>
    </section> <!-- End Main Content -->
{% endblock %}", "member/awaiting-merging.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member\\awaiting-merging.html.twig");
    }
}
