<?php

/* member/check-payment.html.twig */
class __TwigTemplate_98b73a87d025801585c63f0a38066b5ef729437f87e18a9357d54aa8f652916b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/check-payment.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'modalPicHeader' => array($this, 'block_modalPicHeader'),
            'modalPicBody' => array($this, 'block_modalPicBody'),
            'modalConfirmHeader' => array($this, 'block_modalConfirmHeader'),
            'modalConfirmBody' => array($this, 'block_modalConfirmBody'),
            'modalConfirmLink' => array($this, 'block_modalConfirmLink'),
            'modalCancelHeader' => array($this, 'block_modalCancelHeader'),
            'modalCancelBody' => array($this, 'block_modalCancelBody'),
            'modalCancelLink' => array($this, 'block_modalCancelLink'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c628bc07c73933be5126c8d039aa2c1a9ff3e44558815ac885c5b84583d1862b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c628bc07c73933be5126c8d039aa2c1a9ff3e44558815ac885c5b84583d1862b->enter($__internal_c628bc07c73933be5126c8d039aa2c1a9ff3e44558815ac885c5b84583d1862b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/check-payment.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c628bc07c73933be5126c8d039aa2c1a9ff3e44558815ac885c5b84583d1862b->leave($__internal_c628bc07c73933be5126c8d039aa2c1a9ff3e44558815ac885c5b84583d1862b_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_5af9d24e1eda723847baa0ed527435fa714b2b8103b29871e2bfdcfbf3f85ba9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5af9d24e1eda723847baa0ed527435fa714b2b8103b29871e2bfdcfbf3f85ba9->enter($__internal_5af9d24e1eda723847baa0ed527435fa714b2b8103b29871e2bfdcfbf3f85ba9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Mark Payment";
        
        $__internal_5af9d24e1eda723847baa0ed527435fa714b2b8103b29871e2bfdcfbf3f85ba9->leave($__internal_5af9d24e1eda723847baa0ed527435fa714b2b8103b29871e2bfdcfbf3f85ba9_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_d6ad2aecf323555a30a427da37e4196f085c8d61b5a35f0ab640a260ef23b333 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6ad2aecf323555a30a427da37e4196f085c8d61b5a35f0ab640a260ef23b333->enter($__internal_d6ad2aecf323555a30a427da37e4196f085c8d61b5a35f0ab640a260ef23b333_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
\t";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "autoRecycleSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "    \t<div class=\"alert alert-success\">
        \t<p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
    \t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>Mark Payment - ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPack", array(), "method"), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ")</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li><a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("packs");
        echo "\"><i class=\"fa fa-pie-chart\"></i> Packs</a></li>
        \t<li class=\"active\">Mark Payment</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"row\">
      \t\t<!-- Giver's Column -->
      \t\t<div class=\"col-sm-8 col-sm-offset-2\">
      \t\t\t";
        // line 28
        if (($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "not_paid")) {
            // line 29
            echo "      \t\t\t\t<div class=\"alert alert-warning countdownHolder\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left for feeder complete payment</p>
\t\t\t\t\t\t<div id=\"clockGiver\" class='countdownTimer'>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder hourCountDownGiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Hrs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder minuteCountDownGiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Mins</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder secondCountDownGiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Secs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t            var deadlineGiver = \"";
            // line 49
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
            echo "\";
\t\t\t\t            initializeClock('clockGiver', 'hourCountDownGiver', 'minuteCountDownGiver', 'secondCountDownGiver', deadlineGiver);
\t\t\t\t\t\t</script>
      \t\t\t\t</div>
      \t\t\t";
        } elseif ((($this->getAttribute(        // line 53
(isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "payment_made") || (($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getScamPaid", array(), "method") == "scam_paid") && ($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "scam")))) {
            // line 54
            echo "      \t\t\t\t<div class=\"alert alert-info countdownHolder\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left for you to confirm payment</p>
      \t\t\t\t\t<p class=\"text-center small\"><span class=\"text-warning\">(If the payment is a scam, please click the '<strong>Scam</strong>' button below)</span></p>
\t\t\t\t\t\t<div id='clockReceiver' class='countdownTimer'>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder hourCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Hrs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder minuteCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Mins</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder secondCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Secs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t            var deadline = \"";
            // line 75
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiverTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
            echo "\";
\t\t\t\t            initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
\t\t\t\t\t\t</script>
      \t\t\t\t</div>
      \t\t\t";
        } elseif (($this->getAttribute(        // line 79
(isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "scam")) {
            // line 80
            echo "      \t\t\t\t<div class=\"alert alert-danger countdownHolder\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">You have marked this payment as scam</p>
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left for feeder to complete payment</p>
\t\t\t\t\t\t<div id='clockReceiver' class='countdownTimer'>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder hourCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Hrs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder minuteCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Mins</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder secondCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Secs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t            var deadline = \"";
            // line 101
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
            echo "\";
\t\t\t\t            initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
\t\t\t\t\t\t</script>
      \t\t\t\t</div>
      \t\t\t";
        } elseif (($this->getAttribute(        // line 105
(isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "jury")) {
            // line 106
            echo "      \t\t\t\t<div class=\"alert alert-warning\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">This case is in the Jury's Stand. Go and submit your statement and evidence</p>
      \t\t\t\t</div>
      \t\t\t";
        } elseif (($this->getAttribute(        // line 109
(isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "fully_paid")) {
            // line 110
            echo "      \t\t\t\t<div class=\"alert alert-success\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">You have confirmed this payment</p>
      \t\t\t\t</div>
      \t\t\t";
        }
        // line 114
        echo "
      \t\t\t";
        // line 115
        if (($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") != "not_paid")) {
            // line 116
            echo "
\t      \t\t\t<div class=\"box box-solid\">
\t  \t\t\t\t\t<div class=\"box-header with-border\">
\t  \t\t\t\t\t\t<h3 class=\"box-title\">Payment Details</h3>
\t  \t\t\t\t\t</div>

\t  \t\t\t\t\t<div class=\"box-body\">
\t\t      \t\t\t\t<form class=\"form-horizontal\">

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Payment Method</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentMethod", array(), "method"), "html", null, true);
            echo "</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Bank</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentBankName", array(), "method"), "html", null, true);
            echo "</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Account Number</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">";
            // line 145
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentAccountNumber", array(), "method"), "html", null, true);
            echo "</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Account Name</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentAccountName", array(), "method"), "html", null, true);
            echo "</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Depositor's Name</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentDepositorName", array(), "method"), "html", null, true);
            echo "</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Payment Location</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">";
            // line 169
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentLocation", array(), "method"), "html", null, true);
            echo "</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t          \t\t\t\t\t<div class=\"form-group\">
\t                    \t\t\t<label class=\"col-sm-4 control-label\">Payment Proof</label>

\t                    \t\t\t<div class=\"col-sm-8\">
\t                      \t\t\t\t<p class=\"form-control-static\"><img class=\"img-responsive\" src=\"";
            // line 177
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentUploadedFile", array(), "method")), "html", null, true);
            echo "\" style=\"margin: 0; border: none;\"></p>
\t                      \t\t\t\t<p class=\"form-control-static\"><button type=\"button\" class=\"btn btn-xs btn-warning viewPicModal\" data-toggle=\"modal\" data-target=\"#viewPicModal\">View Image</button></p>
\t                    \t\t\t</div>
\t              \t\t\t\t</div>
\t\t      \t\t\t\t</form>
\t  \t\t\t\t\t</div>
\t  \t\t\t\t</div>

      \t\t\t";
        }
        // line 186
        echo "
  \t\t\t\t<!-- Action Buttons -->
  \t\t\t\t<div class=\"row\">
  \t\t\t\t\t";
        // line 189
        if ((($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "payment_made") || ($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "scam"))) {
            // line 190
            echo "\t  \t\t\t\t\t<!-- Confirm Payment Button -->
\t  \t\t\t\t\t<div class=\"col-sm-4\">
\t  \t\t\t\t\t\t<button type=\"button\" class=\"btn btn-success btn-block btn-flat\" data-toggle=\"modal\" data-target=\"#viewConfirmModal\">Confirm Payment</button>
\t  \t\t\t\t\t</div> <!-- End Confirm Payment Button -->
\t  \t\t\t\t";
        }
        // line 195
        echo "  \t\t\t\t\t";
        if ((($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getFeederTimeExtended", array(), "method") == null) && ($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "not_paid"))) {
            // line 196
            echo "\t  \t\t\t\t\t<!-- Extend Time Payment Button -->
\t  \t\t\t\t\t<div class=\"col-sm-4\">
\t  \t\t\t\t\t\t<a href=\"";
            // line 198
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("extend_payment", array("feeder" => $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-warning btn-block\">Extend Payment Time<br>(12 Hours)</a>
\t  \t\t\t\t\t</div>
  \t\t\t\t\t";
        }
        // line 201
        echo "  \t\t\t\t\t";
        if ((($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "scam") || ($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "payment_made"))) {
            // line 202
            echo "\t  \t\t\t\t\t<!-- Dubious Payment Button -->
\t  \t\t\t\t\t<div class=\"col-sm-4\">
\t  \t\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger btn-block btn-flat\" data-toggle=\"modal\" data-target=\"#viewCancelModal\">Mark as Scam</button>
\t  \t\t\t\t\t</div> <!-- End Dubious Payment Button -->
\t  \t\t\t\t";
        }
        // line 207
        echo "\t  \t\t\t\t";
        if (($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "jury")) {
            // line 208
            echo "\t  \t\t\t\t\t<!-- Court Button -->
\t  \t\t\t\t\t<div class=\"col-sm-4\">
\t  \t\t\t\t\t\t<a href=\"";
            // line 210
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jury", array("case" => $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getJury", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-warning btn-block\">Go to Jury's Stand</a>
\t  \t\t\t\t\t</div> <!-- End Court Button -->
\t  \t\t\t\t";
        }
        // line 213
        echo "  \t\t\t\t</div> <!-- End Action Buttons -->
      \t\t</div>
      \t</div>
    </section> <!-- End Main Content -->

";
        
        $__internal_d6ad2aecf323555a30a427da37e4196f085c8d61b5a35f0ab640a260ef23b333->leave($__internal_d6ad2aecf323555a30a427da37e4196f085c8d61b5a35f0ab640a260ef23b333_prof);

    }

    // line 220
    public function block_modalPicHeader($context, array $blocks = array())
    {
        $__internal_77d02d9bfb4b4f3aa7e5a75511a7509241d602d596df3cf37f6ed7a0799f170c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77d02d9bfb4b4f3aa7e5a75511a7509241d602d596df3cf37f6ed7a0799f170c->enter($__internal_77d02d9bfb4b4f3aa7e5a75511a7509241d602d596df3cf37f6ed7a0799f170c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalPicHeader"));

        echo "Payment Proof for ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPack", array(), "method"), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ")";
        
        $__internal_77d02d9bfb4b4f3aa7e5a75511a7509241d602d596df3cf37f6ed7a0799f170c->leave($__internal_77d02d9bfb4b4f3aa7e5a75511a7509241d602d596df3cf37f6ed7a0799f170c_prof);

    }

    // line 221
    public function block_modalPicBody($context, array $blocks = array())
    {
        $__internal_0a7d026ffb5d8e32194f706bb3eab227772b0c24469c8f12c69a702644995e30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a7d026ffb5d8e32194f706bb3eab227772b0c24469c8f12c69a702644995e30->enter($__internal_0a7d026ffb5d8e32194f706bb3eab227772b0c24469c8f12c69a702644995e30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalPicBody"));

        echo "<img class=\"img-responsive\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentUploadedFile", array(), "method")), "html", null, true);
        echo "\">";
        
        $__internal_0a7d026ffb5d8e32194f706bb3eab227772b0c24469c8f12c69a702644995e30->leave($__internal_0a7d026ffb5d8e32194f706bb3eab227772b0c24469c8f12c69a702644995e30_prof);

    }

    // line 223
    public function block_modalConfirmHeader($context, array $blocks = array())
    {
        $__internal_639983cb1a2e4df79a8d0a2e7199fe9548405bcc01a551d8666decf02cbbcc6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_639983cb1a2e4df79a8d0a2e7199fe9548405bcc01a551d8666decf02cbbcc6c->enter($__internal_639983cb1a2e4df79a8d0a2e7199fe9548405bcc01a551d8666decf02cbbcc6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalConfirmHeader"));

        echo "Confirm Payment";
        
        $__internal_639983cb1a2e4df79a8d0a2e7199fe9548405bcc01a551d8666decf02cbbcc6c->leave($__internal_639983cb1a2e4df79a8d0a2e7199fe9548405bcc01a551d8666decf02cbbcc6c_prof);

    }

    // line 224
    public function block_modalConfirmBody($context, array $blocks = array())
    {
        $__internal_92e7f36af468c9b2e758a0a8930a968b412281c26a189482bcd2b9e55dceef85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92e7f36af468c9b2e758a0a8930a968b412281c26a189482bcd2b9e55dceef85->enter($__internal_92e7f36af468c9b2e758a0a8930a968b412281c26a189482bcd2b9e55dceef85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalConfirmBody"));

        echo "You are about to confirm this payment. Be sure to verify your bank account to ensure the feeder properly made payment as this action is irreversible.";
        
        $__internal_92e7f36af468c9b2e758a0a8930a968b412281c26a189482bcd2b9e55dceef85->leave($__internal_92e7f36af468c9b2e758a0a8930a968b412281c26a189482bcd2b9e55dceef85_prof);

    }

    // line 225
    public function block_modalConfirmLink($context, array $blocks = array())
    {
        $__internal_7259587f0d1a3b053cc4ac4207bc203488af2710567c31b69c37cdebc390a42a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7259587f0d1a3b053cc4ac4207bc203488af2710567c31b69c37cdebc390a42a->enter($__internal_7259587f0d1a3b053cc4ac4207bc203488af2710567c31b69c37cdebc390a42a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalConfirmLink"));

        // line 226
        echo "    <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("confirm_payment", array("feeder" => $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getId", array(), "method"))), "html", null, true);
        echo "\" class=\"btn btn-outline pull-right\">Proceed</a>
";
        
        $__internal_7259587f0d1a3b053cc4ac4207bc203488af2710567c31b69c37cdebc390a42a->leave($__internal_7259587f0d1a3b053cc4ac4207bc203488af2710567c31b69c37cdebc390a42a_prof);

    }

    // line 229
    public function block_modalCancelHeader($context, array $blocks = array())
    {
        $__internal_61e1e1239b2e8f4b7924a55e9131bb05faa457a3a173ad4d1e2f7696da539a8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61e1e1239b2e8f4b7924a55e9131bb05faa457a3a173ad4d1e2f7696da539a8a->enter($__internal_61e1e1239b2e8f4b7924a55e9131bb05faa457a3a173ad4d1e2f7696da539a8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelHeader"));

        echo "Mark as Scam";
        
        $__internal_61e1e1239b2e8f4b7924a55e9131bb05faa457a3a173ad4d1e2f7696da539a8a->leave($__internal_61e1e1239b2e8f4b7924a55e9131bb05faa457a3a173ad4d1e2f7696da539a8a_prof);

    }

    // line 230
    public function block_modalCancelBody($context, array $blocks = array())
    {
        $__internal_0bf15bc93f0b0fe5735e3ea3db1e7b3541d4c77a50a74423961d52ddd9f17a06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0bf15bc93f0b0fe5735e3ea3db1e7b3541d4c77a50a74423961d52ddd9f17a06->enter($__internal_0bf15bc93f0b0fe5735e3ea3db1e7b3541d4c77a50a74423961d52ddd9f17a06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelBody"));

        echo "You are about to mark this payment as scam";
        
        $__internal_0bf15bc93f0b0fe5735e3ea3db1e7b3541d4c77a50a74423961d52ddd9f17a06->leave($__internal_0bf15bc93f0b0fe5735e3ea3db1e7b3541d4c77a50a74423961d52ddd9f17a06_prof);

    }

    // line 231
    public function block_modalCancelLink($context, array $blocks = array())
    {
        $__internal_94d516a41c70e2a019b4db45bfe503bd174db8a01b6123284f463ae6fe6dacee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94d516a41c70e2a019b4db45bfe503bd174db8a01b6123284f463ae6fe6dacee->enter($__internal_94d516a41c70e2a019b4db45bfe503bd174db8a01b6123284f463ae6fe6dacee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelLink"));

        // line 232
        echo "    <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scam_payment", array("feeder" => $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getId", array(), "method"))), "html", null, true);
        echo "\" class=\"btn btn-outline pull-right\">Proceed</a>
";
        
        $__internal_94d516a41c70e2a019b4db45bfe503bd174db8a01b6123284f463ae6fe6dacee->leave($__internal_94d516a41c70e2a019b4db45bfe503bd174db8a01b6123284f463ae6fe6dacee_prof);

    }

    public function getTemplateName()
    {
        return "member/check-payment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  481 => 232,  475 => 231,  463 => 230,  451 => 229,  441 => 226,  435 => 225,  423 => 224,  411 => 223,  397 => 221,  380 => 220,  368 => 213,  362 => 210,  358 => 208,  355 => 207,  348 => 202,  345 => 201,  339 => 198,  335 => 196,  332 => 195,  325 => 190,  323 => 189,  318 => 186,  306 => 177,  295 => 169,  284 => 161,  273 => 153,  262 => 145,  251 => 137,  240 => 129,  225 => 116,  223 => 115,  220 => 114,  214 => 110,  212 => 109,  207 => 106,  205 => 105,  198 => 101,  175 => 80,  173 => 79,  166 => 75,  143 => 54,  141 => 53,  134 => 49,  112 => 29,  110 => 28,  97 => 18,  93 => 17,  85 => 15,  80 => 12,  71 => 9,  68 => 8,  64 => 7,  61 => 6,  55 => 5,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Mark Payment{% endblock %}

{% block body %}

\t{% for flash_message in app.session.flashBag.get('autoRecycleSuccess') %}
    \t<div class=\"alert alert-success\">
        \t<p>{{ flash_message }}</p>
    \t</div>
\t{% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>Mark Payment - {{ feeder.getPack().getName() }} ({{ app_currency_symbol }}{{ feeder.getPack().getFormattedAmount() }})</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li><a href=\"{{ path('packs') }}\"><i class=\"fa fa-pie-chart\"></i> Packs</a></li>
        \t<li class=\"active\">Mark Payment</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"row\">
      \t\t<!-- Giver's Column -->
      \t\t<div class=\"col-sm-8 col-sm-offset-2\">
      \t\t\t{% if feeder.getStatus() == 'not_paid' %}
      \t\t\t\t<div class=\"alert alert-warning countdownHolder\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left for feeder complete payment</p>
\t\t\t\t\t\t<div id=\"clockGiver\" class='countdownTimer'>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder hourCountDownGiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Hrs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder minuteCountDownGiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Mins</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder secondCountDownGiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Secs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t            var deadlineGiver = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
\t\t\t\t            initializeClock('clockGiver', 'hourCountDownGiver', 'minuteCountDownGiver', 'secondCountDownGiver', deadlineGiver);
\t\t\t\t\t\t</script>
      \t\t\t\t</div>
      \t\t\t{% elseif feeder.getStatus() == 'payment_made' or (feeder.getScamPaid() == 'scam_paid' and feeder.getStatus() == 'scam') %}
      \t\t\t\t<div class=\"alert alert-info countdownHolder\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left for you to confirm payment</p>
      \t\t\t\t\t<p class=\"text-center small\"><span class=\"text-warning\">(If the payment is a scam, please click the '<strong>Scam</strong>' button below)</span></p>
\t\t\t\t\t\t<div id='clockReceiver' class='countdownTimer'>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder hourCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Hrs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder minuteCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Mins</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder secondCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Secs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t            var deadline = \"{{ feeder.getReceiverTimeEnd()|date('Y-m-d H:i:s') }}\";
\t\t\t\t            initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
\t\t\t\t\t\t</script>
      \t\t\t\t</div>
      \t\t\t{% elseif feeder.getStatus() == 'scam' %}
      \t\t\t\t<div class=\"alert alert-danger countdownHolder\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">You have marked this payment as scam</p>
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left for feeder to complete payment</p>
\t\t\t\t\t\t<div id='clockReceiver' class='countdownTimer'>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder hourCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Hrs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder minuteCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Mins</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class='countdownTimerCell'>
\t\t\t\t\t\t\t\t<span class='timeHolder secondCountDownReceiver'></span>
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t\t<span class='timeDescription'>Secs</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t            var deadline = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
\t\t\t\t            initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
\t\t\t\t\t\t</script>
      \t\t\t\t</div>
      \t\t\t{% elseif feeder.getStatus() == 'jury' %}
      \t\t\t\t<div class=\"alert alert-warning\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">This case is in the Jury's Stand. Go and submit your statement and evidence</p>
      \t\t\t\t</div>
      \t\t\t{% elseif feeder.getStatus() == 'fully_paid' %}
      \t\t\t\t<div class=\"alert alert-success\">
      \t\t\t\t\t<p class=\"text-center lead\" style=\"margin-bottom: 5px\">You have confirmed this payment</p>
      \t\t\t\t</div>
      \t\t\t{% endif %}

      \t\t\t{% if feeder.getStatus() != 'not_paid' %}

\t      \t\t\t<div class=\"box box-solid\">
\t  \t\t\t\t\t<div class=\"box-header with-border\">
\t  \t\t\t\t\t\t<h3 class=\"box-title\">Payment Details</h3>
\t  \t\t\t\t\t</div>

\t  \t\t\t\t\t<div class=\"box-body\">
\t\t      \t\t\t\t<form class=\"form-horizontal\">

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Payment Method</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">{{ feeder.getPaymentMethod() }}</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Bank</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">{{ feeder.getPaymentBankName() }}</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Account Number</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">{{ feeder.getPaymentAccountNumber() }}</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Account Name</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">{{ feeder.getPaymentAccountName() }}</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Depositor's Name</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">{{ feeder.getPaymentDepositorName() }}</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t\t      \t\t\t\t\t<div class=\"form-group\">
\t\t                \t\t\t<label class=\"col-sm-4 control-label\">Payment Location</label>

\t\t                \t\t\t<div class=\"col-sm-8\">
\t\t                  \t\t\t\t<p class=\"form-control-static\">{{ feeder.getPaymentLocation() }}</p>
\t\t                \t\t\t</div>
\t\t          \t\t\t\t</div>

\t          \t\t\t\t\t<div class=\"form-group\">
\t                    \t\t\t<label class=\"col-sm-4 control-label\">Payment Proof</label>

\t                    \t\t\t<div class=\"col-sm-8\">
\t                      \t\t\t\t<p class=\"form-control-static\"><img class=\"img-responsive\" src=\"{{ asset(feeder.getPaymentUploadedFile()) }}\" style=\"margin: 0; border: none;\"></p>
\t                      \t\t\t\t<p class=\"form-control-static\"><button type=\"button\" class=\"btn btn-xs btn-warning viewPicModal\" data-toggle=\"modal\" data-target=\"#viewPicModal\">View Image</button></p>
\t                    \t\t\t</div>
\t              \t\t\t\t</div>
\t\t      \t\t\t\t</form>
\t  \t\t\t\t\t</div>
\t  \t\t\t\t</div>

      \t\t\t{% endif %}

  \t\t\t\t<!-- Action Buttons -->
  \t\t\t\t<div class=\"row\">
  \t\t\t\t\t{% if feeder.getStatus() == 'payment_made' or feeder.getStatus() == 'scam' %}
\t  \t\t\t\t\t<!-- Confirm Payment Button -->
\t  \t\t\t\t\t<div class=\"col-sm-4\">
\t  \t\t\t\t\t\t<button type=\"button\" class=\"btn btn-success btn-block btn-flat\" data-toggle=\"modal\" data-target=\"#viewConfirmModal\">Confirm Payment</button>
\t  \t\t\t\t\t</div> <!-- End Confirm Payment Button -->
\t  \t\t\t\t{% endif %}
  \t\t\t\t\t{% if feeder.getFeederTimeExtended() == null and feeder.getStatus() == 'not_paid' %}
\t  \t\t\t\t\t<!-- Extend Time Payment Button -->
\t  \t\t\t\t\t<div class=\"col-sm-4\">
\t  \t\t\t\t\t\t<a href=\"{{ path('extend_payment', {'feeder' : feeder.getId()}) }}\" class=\"btn btn-warning btn-block\">Extend Payment Time<br>(12 Hours)</a>
\t  \t\t\t\t\t</div>
  \t\t\t\t\t{% endif %}
  \t\t\t\t\t{% if feeder.getStatus() == 'scam' or feeder.getStatus() == 'payment_made' %}
\t  \t\t\t\t\t<!-- Dubious Payment Button -->
\t  \t\t\t\t\t<div class=\"col-sm-4\">
\t  \t\t\t\t\t\t<button type=\"button\" class=\"btn btn-danger btn-block btn-flat\" data-toggle=\"modal\" data-target=\"#viewCancelModal\">Mark as Scam</button>
\t  \t\t\t\t\t</div> <!-- End Dubious Payment Button -->
\t  \t\t\t\t{% endif %}
\t  \t\t\t\t{% if feeder.getStatus() == 'jury' %}
\t  \t\t\t\t\t<!-- Court Button -->
\t  \t\t\t\t\t<div class=\"col-sm-4\">
\t  \t\t\t\t\t\t<a href=\"{{ path('jury', {'case' : feeder.getJury()}) }}\" class=\"btn btn-warning btn-block\">Go to Jury's Stand</a>
\t  \t\t\t\t\t</div> <!-- End Court Button -->
\t  \t\t\t\t{% endif %}
  \t\t\t\t</div> <!-- End Action Buttons -->
      \t\t</div>
      \t</div>
    </section> <!-- End Main Content -->

{% endblock %}

{% block modalPicHeader %}Payment Proof for {{ feeder.getPack().getName() }} ({{ app_currency_symbol }}{{ feeder.getPack().getFormattedAmount() }}){% endblock %}
{% block modalPicBody %}<img class=\"img-responsive\" src=\"{{ asset(feeder.getPaymentUploadedFile()) }}\">{% endblock %}

{% block modalConfirmHeader %}Confirm Payment{% endblock %}
{% block modalConfirmBody %}You are about to confirm this payment. Be sure to verify your bank account to ensure the feeder properly made payment as this action is irreversible.{% endblock %}
{% block modalConfirmLink %}
    <a href=\"{{ path('confirm_payment', {'feeder' : feeder.getId()}) }}\" class=\"btn btn-outline pull-right\">Proceed</a>
{% endblock %}

{% block modalCancelHeader %}Mark as Scam{% endblock %}
{% block modalCancelBody %}You are about to mark this payment as scam{% endblock %}
{% block modalCancelLink %}
    <a href=\"{{ path('scam_payment', {'feeder' : feeder.getId()}) }}\" class=\"btn btn-outline pull-right\">Proceed</a>
{% endblock %}", "member/check-payment.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member\\check-payment.html.twig");
    }
}
