<?php

/* god-mode/adminprofile-bank-details.html.twig */
class __TwigTemplate_eb544ea1112b859fe7b65c69b32b47de566467de9336babfc3b71957e1a75333 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/adminprofile-bank-details.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_774edb5ab104796261644b28de326d9916a2720b4a46a557e4779ae922bf3048 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_774edb5ab104796261644b28de326d9916a2720b4a46a557e4779ae922bf3048->enter($__internal_774edb5ab104796261644b28de326d9916a2720b4a46a557e4779ae922bf3048_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/adminprofile-bank-details.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_774edb5ab104796261644b28de326d9916a2720b4a46a557e4779ae922bf3048->leave($__internal_774edb5ab104796261644b28de326d9916a2720b4a46a557e4779ae922bf3048_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_15cdb79db9397f805635bce18aefbf74d557ecb6442f5c9666597b2eff1340dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15cdb79db9397f805635bce18aefbf74d557ecb6442f5c9666597b2eff1340dc->enter($__internal_15cdb79db9397f805635bce18aefbf74d557ecb6442f5c9666597b2eff1340dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        
        $__internal_15cdb79db9397f805635bce18aefbf74d557ecb6442f5c9666597b2eff1340dc->leave($__internal_15cdb79db9397f805635bce18aefbf74d557ecb6442f5c9666597b2eff1340dc_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_6061ea23bac1c5eac38f1dbc384170451ea9ba23507bc3cd4a5c6e189d49477a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6061ea23bac1c5eac38f1dbc384170451ea9ba23507bc3cd4a5c6e189d49477a->enter($__internal_6061ea23bac1c5eac38f1dbc384170451ea9ba23507bc3cd4a5c6e189d49477a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
\t";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "    \t<div class=\"callout callout-success\">
        \t<p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
    \t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "'s Profile</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li class=\"active\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">

            <!-- Left Side -->
            <div class=\"col-md-4\">
                <!-- Links -->
                <div class=\"list-group\">
                    <a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_settings");
        echo "\" class=\"list-group-item list-group-item\">Settings</a>
                    <a href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminprofile");
        echo "\" class=\"list-group-item list-group-item\">Profile</a>
                    <a href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminbank_details");
        echo "\" class=\"list-group-item  list-group-item-success\">Bank Details</a>
                    <a href=\"";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminchange_password");
        echo "\" class=\"list-group-item\">Change Password</a>
                </div> <!-- End Links -->
                
                <!-- Profile Image -->
                <div class=\"box box-warning\">
                    
                    <div class=\"box-body box-profile\">
                        
                        <img class=\"profile-user-img img-responsive img-circle\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getAvatar", array(), "method")), "html", null, true);
        echo "\" alt=\"User profile picture\">

                        <h3 class=\"profile-username text-center\">";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getName", array(), "method"), "html", null, true);
        echo "</h3>

                        <p class=\"text-muted text-center\">";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getUsername", array(), "method"), "html", null, true);
        echo "</p>

                        <ul class=\"list-group list-group-unbordered\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneOne", array(), "method"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 52
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneTwo", array(), "method") != null)) {
            // line 53
            echo "                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPhoneTwo", array(), "method"), "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        // line 57
        echo "                            <li class=\"list-group-item\">
                                <i class=\"fa fa-envelope-o\"></i> <a class=\"pull-right\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getEmail", array(), "method"), "html", null, true);
        echo "</a>
                            </li>
                        </ul>
                    </div>
                </div> <!-- End Profile Image -->


                <div class=\"box box-success\">

                    <div class=\"box-header with-border\">
                        ";
        // line 68
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankChoice", array(), "method") == 1)) {
            // line 69
            echo "                        <h3 class=\"box-title\">Default Bank Account</h3>
                        ";
        } else {
            // line 71
            echo "                        <h3 class=\"box-title\">Provisional Bank Account</h3>
                        ";
        }
        // line 73
        echo "                    </div>

                    <div class=\"box-body\">
                        ";
        // line 76
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankName", array(), "method") != null)) {
            // line 77
            echo "                            <ul class=\"list-group list-group-unbordered\">
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-bank\"></i> Bank Name <a class=\"pull-right\">";
            // line 79
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankName", array(), "method"), "html", null, true);
            echo "</a>
                                </li>
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-bank\"></i> Account Name <a class=\"pull-right\">";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankAccountName", array(), "method"), "html", null, true);
            echo "</a>
                                </li>
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-bank\"></i> Account Number <a class=\"pull-right\">";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankAccountNumber", array(), "method"), "html", null, true);
            echo "</a>
                                </li>
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-bank\"></i> Account Type <a class=\"pull-right\">";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getBankAccountType", array(), "method"), "html", null, true);
            echo "</a>
                                </li>
                            </ul>
                        ";
        } else {
            // line 92
            echo "                            <div class=\"alert alert-danger text-center\">
                                <p>You have not added bank details yet!</p>
                            </div>
                        ";
        }
        // line 96
        echo "                    </div>

                </div>

            </div>


            <!-- Right Side -->
            <div class=\"col-md-8\">

                <div class=\"box box-success\">
                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Choose a bank account</h3>
                    </div>

                        <div class=\"box-body\">
                            ";
        // line 112
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["bankForm"]) ? $context["bankForm"] : $this->getContext($context, "bankForm")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("update_bank_choice"), "attr" => array("class" => "form-horizontal")));
        echo "

                            ";
        // line 114
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "paymentSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 115
            echo "                                <div class=\"alert alert-success alert-dismissible\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                    <p>";
            // line 117
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "
                            <span class=\"text-danger\">";
        // line 121
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["bankForm"]) ? $context["bankForm"] : $this->getContext($context, "bankForm")), "bank_choice", array()), 'errors');
        echo "</span>
                            <div class=\"form-group\">
                                <div class=\"col-sm-12\">
                                    ";
        // line 124
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["bankForm"]) ? $context["bankForm"] : $this->getContext($context, "bankForm")), "bank_choice", array()), 'widget');
        echo "
                                </div>
                            </div>

                            <div class=\"row\">
                                <div class=\"col-sm-12\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Choose a bank account</button>
                                </div>
                            </div>

                            ";
        // line 134
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["bankForm"]) ? $context["bankForm"] : $this->getContext($context, "bankForm")), 'form_end');
        echo "


                        </div>


                    </div>


                <div class=\"box box-info\">

                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Default Bank Account</h3>
                    </div>

                    <div class=\"box-body\">
                        ";
        // line 150
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminbank_details"), "attr" => array("class" => "form-horizontal")));
        echo "

                            ";
        // line 152
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "paymentSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 153
            echo "                                <div class=\"alert alert-success alert-dismissible\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                    <p>";
            // line 155
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 158
        echo "
                            <span class=\"text-danger\">";
        // line 159
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "bankNameOne", array()), 'errors');
        echo "</span>
                            <div class=\"form-group\">
                                ";
        // line 161
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "bankNameOne", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Bank Name"));
        echo "

                                <div class=\"col-sm-10\">
                                    ";
        // line 164
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "bankNameOne", array()), 'widget');
        echo "
                                </div>
                            </div>

                            <span class=\"text-danger\">";
        // line 168
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountNameOne", array()), 'errors');
        echo "</span>
                            <div class=\"form-group\">
                                ";
        // line 170
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountNameOne", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Account Name"));
        echo "

                                <div class=\"col-sm-10\">
                                    ";
        // line 173
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountNameOne", array()), 'widget');
        echo "
                                </div>
                            </div>

                            <span class=\"text-danger\">";
        // line 177
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountNumberOne", array()), 'errors');
        echo "</span>
                            <div class=\"form-group\">
                                ";
        // line 179
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountNumberOne", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Account Number"));
        echo "

                                <div class=\"col-sm-10\">
                                    ";
        // line 182
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountNumberOne", array()), 'widget');
        echo "
                                </div>
                            </div>

                            <span class=\"text-danger\">";
        // line 186
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountTypeOne", array()), 'errors');
        echo "</span>
                            <div class=\"form-group\">
                                ";
        // line 188
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountTypeOne", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Account Type"));
        echo "

                                <div class=\"col-sm-10\">
                                    ";
        // line 191
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), "accountTypeOne", array()), 'widget');
        echo "
                                </div>
                            </div>



                            <div class=\"row\">
                                <div class=\"col-sm-12\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Save Payment Details</button>
                                </div>
                            </div>

                        ";
        // line 203
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["paymentForm"]) ? $context["paymentForm"] : $this->getContext($context, "paymentForm")), 'form_end');
        echo "
                    </div>

                </div>


                <div class=\"box box-info\">
                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Provisional Bank Account</h3>
                    </div>

                    <div class=\"box-body\">
                        ";
        // line 215
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminbank_details_two"), "attr" => array("class" => "form-horizontal")));
        echo "


                        ";
        // line 218
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "paymentSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 219
            echo "                            <div class=\"alert alert-success alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <p>";
            // line 221
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 224
        echo "
                       <span class=\"text-danger\">";
        // line 225
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "bank_name_two", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            ";
        // line 227
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "bank_name_two", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Bank Name"));
        echo "

                            <div class=\"col-sm-10\">
                                ";
        // line 230
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "bank_name_two", array()), 'widget');
        echo "
                            </div>
                        </div>


                        <span class=\"text-danger\">";
        // line 235
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_name_two", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            ";
        // line 237
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_name_two", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Account Name"));
        echo "

                            <div class=\"col-sm-10\">
                                ";
        // line 240
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_name_two", array()), 'widget');
        echo "
                            </div>
                        </div>

                        <span class=\"text-danger\">";
        // line 244
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_number_two", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            ";
        // line 246
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_number_two", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Account Number"));
        echo "

                            <div class=\"col-sm-10\">
                                ";
        // line 249
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_number_two", array()), 'widget');
        echo "
                            </div>
                        </div>


                        <span class=\"text-danger\">";
        // line 254
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_type_two", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            ";
        // line 256
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_type_two", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Account Type"));
        echo "

                            <div class=\"col-sm-10\">
                                ";
        // line 259
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), "account_type_two", array()), 'widget');
        echo "
                            </div>
                        </div>


                        <div class=\"row\">
                            <div class=\"col-sm-12\">
                                <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Save Payment Details</button>
                            </div>
                        </div>

                        ";
        // line 270
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["paymentTwoForm"]) ? $context["paymentTwoForm"] : $this->getContext($context, "paymentTwoForm")), 'form_end');
        echo "

                    </div>
                </div>


            </div>

        </div>
    </section>

";
        
        $__internal_6061ea23bac1c5eac38f1dbc384170451ea9ba23507bc3cd4a5c6e189d49477a->leave($__internal_6061ea23bac1c5eac38f1dbc384170451ea9ba23507bc3cd4a5c6e189d49477a_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/adminprofile-bank-details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  534 => 270,  520 => 259,  514 => 256,  509 => 254,  501 => 249,  495 => 246,  490 => 244,  483 => 240,  477 => 237,  472 => 235,  464 => 230,  458 => 227,  453 => 225,  450 => 224,  441 => 221,  437 => 219,  433 => 218,  427 => 215,  412 => 203,  397 => 191,  391 => 188,  386 => 186,  379 => 182,  373 => 179,  368 => 177,  361 => 173,  355 => 170,  350 => 168,  343 => 164,  337 => 161,  332 => 159,  329 => 158,  320 => 155,  316 => 153,  312 => 152,  307 => 150,  288 => 134,  275 => 124,  269 => 121,  266 => 120,  257 => 117,  253 => 115,  249 => 114,  244 => 112,  226 => 96,  220 => 92,  213 => 88,  207 => 85,  201 => 82,  195 => 79,  191 => 77,  189 => 76,  184 => 73,  180 => 71,  176 => 69,  174 => 68,  161 => 58,  158 => 57,  152 => 54,  149 => 53,  147 => 52,  142 => 50,  135 => 46,  130 => 44,  125 => 42,  114 => 34,  110 => 33,  106 => 32,  102 => 31,  86 => 18,  82 => 17,  77 => 15,  72 => 12,  63 => 9,  60 => 8,  56 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}{{ app.user.getName() }}{% endblock %}

{% block body %}

\t{% for flash_message in app.session.flashBag.get('notice') %}
    \t<div class=\"callout callout-success\">
        \t<p>{{ flash_message }}</p>
    \t</div>
\t{% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>{{ app.user.getName() }}'s Profile</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li class=\"active\">{{ app.user.getName() }}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">

            <!-- Left Side -->
            <div class=\"col-md-4\">
                <!-- Links -->
                <div class=\"list-group\">
                    <a href=\"{{ path('god_mode_settings') }}\" class=\"list-group-item list-group-item\">Settings</a>
                    <a href=\"{{ path('adminprofile') }}\" class=\"list-group-item list-group-item\">Profile</a>
                    <a href=\"{{ path('adminbank_details') }}\" class=\"list-group-item  list-group-item-success\">Bank Details</a>
                    <a href=\"{{ path('adminchange_password') }}\" class=\"list-group-item\">Change Password</a>
                </div> <!-- End Links -->
                
                <!-- Profile Image -->
                <div class=\"box box-warning\">
                    
                    <div class=\"box-body box-profile\">
                        
                        <img class=\"profile-user-img img-responsive img-circle\" src=\"{{ asset(app.user.getAvatar()) }}\" alt=\"User profile picture\">

                        <h3 class=\"profile-username text-center\">{{ app.user.getName() }}</h3>

                        <p class=\"text-muted text-center\">{{ app.user.getUsername() }}</p>

                        <ul class=\"list-group list-group-unbordered\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">{{ app.user.getPhoneOne() }}</a>
                            </li>
                            {% if app.user.getPhoneTwo() != null %}
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-phone\"></i> <a class=\"pull-right\">{{ app.user.getPhoneTwo() }}</a>
                                </li>
                            {% endif %}
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-envelope-o\"></i> <a class=\"pull-right\">{{ app.user.getEmail() }}</a>
                            </li>
                        </ul>
                    </div>
                </div> <!-- End Profile Image -->


                <div class=\"box box-success\">

                    <div class=\"box-header with-border\">
                        {% if app.user.getBankChoice() == 1 %}
                        <h3 class=\"box-title\">Default Bank Account</h3>
                        {% else %}
                        <h3 class=\"box-title\">Provisional Bank Account</h3>
                        {% endif %}
                    </div>

                    <div class=\"box-body\">
                        {% if app.user.getBankName() != null %}
                            <ul class=\"list-group list-group-unbordered\">
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-bank\"></i> Bank Name <a class=\"pull-right\">{{ app.user.getBankName() }}</a>
                                </li>
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-bank\"></i> Account Name <a class=\"pull-right\">{{ app.user.getBankAccountName() }}</a>
                                </li>
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-bank\"></i> Account Number <a class=\"pull-right\">{{ app.user.getBankAccountNumber() }}</a>
                                </li>
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-bank\"></i> Account Type <a class=\"pull-right\">{{ app.user.getBankAccountType() }}</a>
                                </li>
                            </ul>
                        {% else %}
                            <div class=\"alert alert-danger text-center\">
                                <p>You have not added bank details yet!</p>
                            </div>
                        {% endif %}
                    </div>

                </div>

            </div>


            <!-- Right Side -->
            <div class=\"col-md-8\">

                <div class=\"box box-success\">
                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Choose a bank account</h3>
                    </div>

                        <div class=\"box-body\">
                            {{ form_start(bankForm, {'action': path('update_bank_choice'), 'attr': {'class': 'form-horizontal'}}) }}

                            {% for flash_message in app.session.flashBag.get('paymentSuccess') %}
                                <div class=\"alert alert-success alert-dismissible\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                    <p>{{ flash_message }}</p>
                                </div>
                            {% endfor %}

                            <span class=\"text-danger\">{{ form_errors(bankForm.bank_choice) }}</span>
                            <div class=\"form-group\">
                                <div class=\"col-sm-12\">
                                    {{ form_widget(bankForm.bank_choice) }}
                                </div>
                            </div>

                            <div class=\"row\">
                                <div class=\"col-sm-12\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Choose a bank account</button>
                                </div>
                            </div>

                            {{ form_end(bankForm) }}


                        </div>


                    </div>


                <div class=\"box box-info\">

                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Default Bank Account</h3>
                    </div>

                    <div class=\"box-body\">
                        {{ form_start(paymentForm, {'action': path('adminbank_details'), 'attr': {'class': 'form-horizontal'}}) }}

                            {% for flash_message in app.session.flashBag.get('paymentSuccess') %}
                                <div class=\"alert alert-success alert-dismissible\">
                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                    <p>{{ flash_message }}</p>
                                </div>
                            {% endfor %}

                            <span class=\"text-danger\">{{ form_errors(paymentForm.bankNameOne) }}</span>
                            <div class=\"form-group\">
                                {{ form_label(paymentForm.bankNameOne, 'Bank Name', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                                <div class=\"col-sm-10\">
                                    {{ form_widget(paymentForm.bankNameOne) }}
                                </div>
                            </div>

                            <span class=\"text-danger\">{{ form_errors(paymentForm.accountNameOne) }}</span>
                            <div class=\"form-group\">
                                {{ form_label(paymentForm.accountNameOne, 'Account Name', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                                <div class=\"col-sm-10\">
                                    {{ form_widget(paymentForm.accountNameOne) }}
                                </div>
                            </div>

                            <span class=\"text-danger\">{{ form_errors(paymentForm.accountNumberOne) }}</span>
                            <div class=\"form-group\">
                                {{ form_label(paymentForm.accountNumberOne, 'Account Number', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                                <div class=\"col-sm-10\">
                                    {{ form_widget(paymentForm.accountNumberOne) }}
                                </div>
                            </div>

                            <span class=\"text-danger\">{{ form_errors(paymentForm.accountTypeOne) }}</span>
                            <div class=\"form-group\">
                                {{ form_label(paymentForm.accountTypeOne, 'Account Type', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                                <div class=\"col-sm-10\">
                                    {{ form_widget(paymentForm.accountTypeOne) }}
                                </div>
                            </div>



                            <div class=\"row\">
                                <div class=\"col-sm-12\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Save Payment Details</button>
                                </div>
                            </div>

                        {{ form_end(paymentForm) }}
                    </div>

                </div>


                <div class=\"box box-info\">
                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Provisional Bank Account</h3>
                    </div>

                    <div class=\"box-body\">
                        {{ form_start(paymentTwoForm, {'action': path('adminbank_details_two'), 'attr': {'class': 'form-horizontal'}}) }}


                        {% for flash_message in app.session.flashBag.get('paymentSuccess') %}
                            <div class=\"alert alert-success alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <p>{{ flash_message }}</p>
                            </div>
                        {% endfor %}

                       <span class=\"text-danger\">{{ form_errors(paymentTwoForm.bank_name_two) }}</span>
                        <div class=\"form-group\">
                            {{ form_label(paymentTwoForm.bank_name_two, 'Bank Name', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                            <div class=\"col-sm-10\">
                                {{ form_widget(paymentTwoForm.bank_name_two) }}
                            </div>
                        </div>


                        <span class=\"text-danger\">{{ form_errors(paymentTwoForm.account_name_two) }}</span>
                        <div class=\"form-group\">
                            {{ form_label(paymentTwoForm.account_name_two, 'Account Name', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                            <div class=\"col-sm-10\">
                                {{ form_widget(paymentTwoForm.account_name_two) }}
                            </div>
                        </div>

                        <span class=\"text-danger\">{{ form_errors(paymentTwoForm.account_number_two) }}</span>
                        <div class=\"form-group\">
                            {{ form_label(paymentTwoForm.account_number_two, 'Account Number', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                            <div class=\"col-sm-10\">
                                {{ form_widget(paymentTwoForm.account_number_two) }}
                            </div>
                        </div>


                        <span class=\"text-danger\">{{ form_errors(paymentTwoForm.account_type_two) }}</span>
                        <div class=\"form-group\">
                            {{ form_label(paymentTwoForm.account_type_two, 'Account Type', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                            <div class=\"col-sm-10\">
                                {{ form_widget(paymentTwoForm.account_type_two) }}
                            </div>
                        </div>


                        <div class=\"row\">
                            <div class=\"col-sm-12\">
                                <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Save Payment Details</button>
                            </div>
                        </div>

                        {{ form_end(paymentTwoForm) }}

                    </div>
                </div>


            </div>

        </div>
    </section>

{% endblock %}", "god-mode/adminprofile-bank-details.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\adminprofile-bank-details.html.twig");
    }
}
