<?php

/* member/unauthorized.html.twig */
class __TwigTemplate_5814b62a675cc220961deb4198d3ed49f8fc2ebac810facad08298aadf3a2b4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/unauthorized.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_06b03437971bccdf1bb9f8b803064ede51c101298a61fa9e56373f951d068f4f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06b03437971bccdf1bb9f8b803064ede51c101298a61fa9e56373f951d068f4f->enter($__internal_06b03437971bccdf1bb9f8b803064ede51c101298a61fa9e56373f951d068f4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/unauthorized.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_06b03437971bccdf1bb9f8b803064ede51c101298a61fa9e56373f951d068f4f->leave($__internal_06b03437971bccdf1bb9f8b803064ede51c101298a61fa9e56373f951d068f4f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_6f6ea4991d0d7fcdbd2c51a7aec956b5e18e87011e3354c8a48a5d912fc0d606 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f6ea4991d0d7fcdbd2c51a7aec956b5e18e87011e3354c8a48a5d912fc0d606->enter($__internal_6f6ea4991d0d7fcdbd2c51a7aec956b5e18e87011e3354c8a48a5d912fc0d606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Unauthorized Access";
        
        $__internal_6f6ea4991d0d7fcdbd2c51a7aec956b5e18e87011e3354c8a48a5d912fc0d606->leave($__internal_6f6ea4991d0d7fcdbd2c51a7aec956b5e18e87011e3354c8a48a5d912fc0d606_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_2fa15b80e54c8f371d75029c06f954d0ff766fa41ae3fed946306abc7e500254 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fa15b80e54c8f371d75029c06f954d0ff766fa41ae3fed946306abc7e500254->enter($__internal_2fa15b80e54c8f371d75029c06f954d0ff766fa41ae3fed946306abc7e500254_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>Unauthorized Access</h1>
      \t<ol class=\"breadcrumb\">
      \t\t<li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Home</a></li>
        \t<li class=\"active\">Unauthorized Access</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"error-page\">
        \t<h2 class=\"headline text-red\"> 500</h2>

        \t<div class=\"error-content\">
          \t\t<h3><i class=\"fa fa-warning text-red\"></i> Access Denied!</h3>

          \t\t<p>You do not have access to this page. Meanwhile, you may <a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\">return to dashboard</a>.</p>
          \t</div>
        </div>
    </section>
";
        
        $__internal_2fa15b80e54c8f371d75029c06f954d0ff766fa41ae3fed946306abc7e500254->leave($__internal_2fa15b80e54c8f371d75029c06f954d0ff766fa41ae3fed946306abc7e500254_prof);

    }

    public function getTemplateName()
    {
        return "member/unauthorized.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 23,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Unauthorized Access{% endblock %}

{% block body %}
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>Unauthorized Access</h1>
      \t<ol class=\"breadcrumb\">
      \t\t<li><a href=\"#\"><i class=\"fa fa-dashboard\"></i> Home</a></li>
        \t<li class=\"active\">Unauthorized Access</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"error-page\">
        \t<h2 class=\"headline text-red\"> 500</h2>

        \t<div class=\"error-content\">
          \t\t<h3><i class=\"fa fa-warning text-red\"></i> Access Denied!</h3>

          \t\t<p>You do not have access to this page. Meanwhile, you may <a href=\"{{ path('dashboard') }}\">return to dashboard</a>.</p>
          \t</div>
        </div>
    </section>
{% endblock %}", "member/unauthorized.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\member\\unauthorized.html.twig");
    }
}
