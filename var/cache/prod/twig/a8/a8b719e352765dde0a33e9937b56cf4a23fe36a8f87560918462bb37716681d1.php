<?php

/* god-mode/merge-users.html.twig */
class __TwigTemplate_a60af9566469adcdbf4a2c2c64886751792d464ff47dd4a96ce2c28049b94277 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/merge-users.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_10f17a20c7ebb3deda512af2ef7e8478c1f49fcfe2c06450e4d5839a218a2bfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10f17a20c7ebb3deda512af2ef7e8478c1f49fcfe2c06450e4d5839a218a2bfc->enter($__internal_10f17a20c7ebb3deda512af2ef7e8478c1f49fcfe2c06450e4d5839a218a2bfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/merge-users.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_10f17a20c7ebb3deda512af2ef7e8478c1f49fcfe2c06450e4d5839a218a2bfc->leave($__internal_10f17a20c7ebb3deda512af2ef7e8478c1f49fcfe2c06450e4d5839a218a2bfc_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_f95fdf70b3110407113ecc383c58964aba37f28165defb1c161e2e822eef0df5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f95fdf70b3110407113ecc383c58964aba37f28165defb1c161e2e822eef0df5->enter($__internal_f95fdf70b3110407113ecc383c58964aba37f28165defb1c161e2e822eef0df5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["waitingSubscriptions"]) ? $context["waitingSubscriptions"] : $this->getContext($context, "waitingSubscriptions")), "getTotalItemCount", array()), "html", null, true);
        echo " User";
        if (($this->getAttribute((isset($context["waitingSubscriptions"]) ? $context["waitingSubscriptions"] : $this->getContext($context, "waitingSubscriptions")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        echo " in Waiting List For ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getName", array(), "method"), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ") Package";
        
        $__internal_f95fdf70b3110407113ecc383c58964aba37f28165defb1c161e2e822eef0df5->leave($__internal_f95fdf70b3110407113ecc383c58964aba37f28165defb1c161e2e822eef0df5_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_cce4f10c8d62227727bd30b1559613c5eacdf49dab11ac8012c7a35ad89b304a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cce4f10c8d62227727bd30b1559613c5eacdf49dab11ac8012c7a35ad89b304a->enter($__internal_cce4f10c8d62227727bd30b1559613c5eacdf49dab11ac8012c7a35ad89b304a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["waitingSubscriptions"]) ? $context["waitingSubscriptions"] : $this->getContext($context, "waitingSubscriptions")), "getTotalItemCount", array()), "html", null, true);
        echo " User";
        if (($this->getAttribute((isset($context["waitingSubscriptions"]) ? $context["waitingSubscriptions"] : $this->getContext($context, "waitingSubscriptions")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        echo " in Waiting List
            ";
        // line 24
        echo "            <br>
             ";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["activeUsersCount"]) ? $context["activeUsersCount"] : $this->getContext($context, "activeUsersCount")), "html", null, true);
        echo " Users in Receiving List
            ";
        // line 27
        echo "            <br>
            <a href=\"/god-mode/recommitments/123\">";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["recommitUsersCount"]) ? $context["recommitUsersCount"] : $this->getContext($context, "recommitUsersCount")), "html", null, true);
        echo "</a>  Users waiting to Recommit List
            ";
        // line 30
        echo "        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-users\"></i> Merge Users</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <div class=\"col-sm-12\">
                ";
        // line 43
        echo "                    ";
        // line 44
        echo "                    ";
        // line 45
        echo "                        ";
        // line 46
        echo "                    ";
        // line 47
        echo "                        ";
        // line 48
        echo "                    ";
        // line 49
        echo "                    ";
        // line 50
        echo "                    ";
        // line 51
        echo "                        ";
        // line 52
        echo "                    ";
        // line 53
        echo "                        ";
        // line 54
        echo "                    ";
        // line 55
        echo "                ";
        // line 56
        echo "                ";
        // line 57
        echo "                    ";
        // line 58
        echo "                ";
        // line 59
        echo "            </div>
            <div class=\"clearfix\"></div>
        </div>

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 67
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"";
        // line 68
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item\">List Admin</a>
                    <a href=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item\">List Users</a>
                    <a href=\"";
        // line 70
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 72
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 73
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item\">List Payments</a>
                    <a href=\"";
        // line 74
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 75
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 76
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 78
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 79
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item\">Approve Testimonies</a>
                    <a href=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_merge_users", array("pack" => $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getId", array(), "method"))), "html", null, true);
        echo "\" class=\"list-group-item active\">Merge Users</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                ";
        // line 88
        if (((isset($context["mergeType"]) ? $context["mergeType"] : $this->getContext($context, "mergeType")) == "automatic")) {
            // line 89
            echo "                    <div class=\"alert alert-warning\">
                        <p>Sorry, Merging Mode Set To Automatic. If you wish to manually handle things yourself, kindly switch mode back to Manual</p>
                    </div>
                ";
        } else {
            // line 93
            echo "                    <div class=\"row\">
                        <div class=\"col-sm-2\">
                        </div>
                        <div class=\"col-sm-8 text-center lead\">
                            ";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["waitingSubscriptions"]) ? $context["waitingSubscriptions"] : $this->getContext($context, "waitingSubscriptions")), "getTotalItemCount", array()), "html", null, true);
            echo " User";
            if (($this->getAttribute((isset($context["waitingSubscriptions"]) ? $context["waitingSubscriptions"] : $this->getContext($context, "waitingSubscriptions")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            echo " in Waiting List For ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getName", array(), "method"), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getFormattedAmount", array(), "method"), "html", null, true);
            echo ") Package
                        </div>
                        <div class=\"col-sm-2\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <form action=\"?query=\">
                        <div class=\"col-md-8\">

                                <select class=\"form-control\" name=\"query\" id=\"query\">

                                    ";
            // line 109
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["selectList"]);
            foreach ($context['_seq'] as $context["_key"] => $context["selectList"]) {
                // line 110
                echo "                                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["selectList"], "getUser", array(), "method"), "getId", array(), "method"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["selectList"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</option>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['selectList'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 112
            echo "
                                </select>
                                ";
            // line 115
            echo "                        </div>
                        <div class=\"col-md-4\">
                            <input type=\"submit\" class=\"btn btn-success\">
                        </div>
                        </form>

                    </div>



                    <table class=\"table table-hover table-responsive table-striped\">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Username</th>
                                ";
            // line 131
            echo "                                <th>Total Feeders</th>
                                <th>Select Receiver</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
            // line 136
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["waitingSubscriptions"]) ? $context["waitingSubscriptions"] : $this->getContext($context, "waitingSubscriptions")));
            foreach ($context['_seq'] as $context["_key"] => $context["waitingSubscription"]) {
                // line 137
                echo "                                <tr>
                                    <td>";
                // line 138
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["waitingSubscription"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</td>
                                    <td>";
                // line 139
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["waitingSubscription"], "getUser", array(), "method"), "getUsername", array(), "method"), "html", null, true);
                echo "</td>
                                    ";
                // line 141
                echo "                                    <td>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["waitingSubscription"], "getFeederTotal", array(), "method"), "html", null, true);
                echo "</td>
                                    <td>
                                        <div class=\"dropdown nav-item\">
                                            <input class=\"dropdown-toggle form-control live-search-box\" id=\"quicksearch\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" placeholder=\"Search By Username\">
                                            <div class=\"dropdown-menu live-search-list\" aria-labelledby=\"dropdownMenuButton\" style=\"max-height: 250px; overflow-y: scroll; width: 100%\">
                                                    <li>
                                                        <a class=\"dropdown-item\" href=\"";
                // line 147
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_merge_user", array("receiver" => $this->getAttribute((isset($context["adminone"]) ? $context["adminone"] : $this->getContext($context, "adminone")), "getId", array(), "method"), "feederSubscription" => $this->getAttribute($context["waitingSubscription"], "getId", array(), "method"), "pack" => $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getId", array(), "method"))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adminone"]) ? $context["adminone"] : $this->getContext($context, "adminone")), "getUsername", array(), "method"), "html", null, true);
                echo "</a>
                                                    </li>

                                                ";
                // line 151
                echo "                                                    ";
                // line 152
                echo "                                                ";
                // line 153
                echo "
                                                ";
                // line 155
                echo "                                                    ";
                // line 156
                echo "                                                ";
                // line 157
                echo "
                                                ";
                // line 158
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["admins"]) ? $context["admins"] : $this->getContext($context, "admins")));
                foreach ($context['_seq'] as $context["_key"] => $context["admin"]) {
                    // line 159
                    echo "                                                    <li>
                                                        <a class=\"dropdown-item\" href=\"";
                    // line 160
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_merge_user", array("receiver" => $this->getAttribute($context["admin"], "getId", array(), "method"), "feederSubscription" => $this->getAttribute($context["waitingSubscription"], "getId", array(), "method"), "pack" => $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getId", array(), "method"))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["admin"], "getUsername", array(), "method"), "html", null, true);
                    echo "</a>
                                                    </li>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['admin'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 163
                echo "                                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["activeUsers"]) ? $context["activeUsers"] : $this->getContext($context, "activeUsers")));
                foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                    // line 164
                    echo "                                                    ";
                    if (($this->getAttribute($context["user"], "getRecommitRequired", array()) == true)) {
                        // line 165
                        echo "                                                    <li style=\"background-color:red; color: white\">
                                                        ";
                    } elseif (($this->getAttribute(                    // line 166
$context["user"], "getFeederCounter", array(), "method") == 1)) {
                        // line 167
                        echo "                                                            <li style=\"background-color:greenyellow; color: white\">
                                                    ";
                    } else {
                        // line 169
                        echo "                                                <li>
                                                ";
                    }
                    // line 171
                    echo "
                                                <a class=\"dropdown-item\" href=\"";
                    // line 172
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_merge_user", array("receiver" => $this->getAttribute($this->getAttribute($context["user"], "getUser", array(), "method"), "getId", array(), "method"), "feederSubscription" => $this->getAttribute($context["waitingSubscription"], "getId", array(), "method"), "pack" => $this->getAttribute((isset($context["pack"]) ? $context["pack"] : $this->getContext($context, "pack")), "getId", array(), "method"))), "html", null, true);
                    echo "\">
                                                            ";
                    // line 173
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["user"], "getUser", array(), "method"), "getUsername", array(), "method"), "html", null, true);
                    echo " (";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["user"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
                    echo ") (";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "getCreated", array()), "F jS \\a\\t g:ia"), "html", null, true);
                    echo ")
                                                        </a>
                                                    </li>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 177
                echo "                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['waitingSubscription'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 182
            echo "                        </tbody>
                    </table>

                    <div class=\"navigation text-center\">
                        ";
            // line 186
            echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["waitingSubscriptions"]) ? $context["waitingSubscriptions"] : $this->getContext($context, "waitingSubscriptions")));
            echo "
                    </div>
                ";
        }
        // line 189
        echo "            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_cce4f10c8d62227727bd30b1559613c5eacdf49dab11ac8012c7a35ad89b304a->leave($__internal_cce4f10c8d62227727bd30b1559613c5eacdf49dab11ac8012c7a35ad89b304a_prof);

    }

    // line 195
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_89f3d8779353539bb4b63034033433431d52691609c8da0f1eb8ce25b20b586b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_89f3d8779353539bb4b63034033433431d52691609c8da0f1eb8ce25b20b586b->enter($__internal_89f3d8779353539bb4b63034033433431d52691609c8da0f1eb8ce25b20b586b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 196
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(document).ready(function(\$) {
            \$('.live-search-list li').each(function() {
                \$(this).attr('data-search-term', \$(this).text().toLowerCase());
            });

            \$('.live-search-box').on('keyup', function() {
                var searchTerm = \$(this).val().toLowerCase();

                \$('.live-search-list li').each(function() {
                    if(\$(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
                        \$(this).show();
                    } else {
                        \$(this).hide();
                    }
                });
            });
        });
    </script>
";
        
        $__internal_89f3d8779353539bb4b63034033433431d52691609c8da0f1eb8ce25b20b586b->leave($__internal_89f3d8779353539bb4b63034033433431d52691609c8da0f1eb8ce25b20b586b_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/merge-users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  485 => 196,  479 => 195,  468 => 189,  462 => 186,  456 => 182,  446 => 177,  432 => 173,  428 => 172,  425 => 171,  421 => 169,  417 => 167,  415 => 166,  412 => 165,  409 => 164,  404 => 163,  393 => 160,  390 => 159,  386 => 158,  383 => 157,  381 => 156,  379 => 155,  376 => 153,  374 => 152,  372 => 151,  364 => 147,  354 => 141,  350 => 139,  346 => 138,  343 => 137,  339 => 136,  332 => 131,  315 => 115,  311 => 112,  300 => 110,  296 => 109,  272 => 97,  266 => 93,  260 => 89,  258 => 88,  249 => 82,  245 => 81,  241 => 80,  237 => 79,  233 => 78,  229 => 77,  225 => 76,  221 => 75,  217 => 74,  213 => 73,  209 => 72,  205 => 71,  201 => 70,  197 => 69,  193 => 68,  189 => 67,  179 => 59,  177 => 58,  175 => 57,  173 => 56,  171 => 55,  169 => 54,  167 => 53,  165 => 52,  163 => 51,  161 => 50,  159 => 49,  157 => 48,  155 => 47,  153 => 46,  151 => 45,  149 => 44,  147 => 43,  134 => 32,  130 => 30,  126 => 28,  123 => 27,  119 => 25,  116 => 24,  108 => 22,  102 => 18,  93 => 15,  90 => 14,  86 => 13,  83 => 12,  74 => 9,  71 => 8,  67 => 7,  64 => 6,  58 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}{{ waitingSubscriptions.getTotalItemCount }} User{% if waitingSubscriptions.getTotalItemCount != 1 %}s{% endif %} in Waiting List For {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) Package{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            {{ waitingSubscriptions.getTotalItemCount }} User{% if waitingSubscriptions.getTotalItemCount != 1 %}s{% endif %} in Waiting List
            {#For {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) Package #}
            <br>
             {{ activeUsersCount }} Users in Receiving List
            {#For {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) Package#}
            <br>
            <a href=\"/god-mode/recommitments/123\">{{ recommitUsersCount }}</a>  Users waiting to Recommit List
            {#For {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) Package<br>#}
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-users\"></i> Merge Users</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <div class=\"col-sm-12\">
                {#<p>#}
                    {#Merging Mode Set to: #}
                    {#{% if mergeType == 'manual' %}#}
                        {#<button type=\"button\" class=\"btn btn-info btn-flat\">Manual</button>#}
                    {#{% elseif mergeType == 'automatic' %}#}
                        {#<button type=\"button\" class=\"btn btn-info btn-flat\">Automatic</button>#}
                    {#{% endif %}#}
                    {#Change Merging Mode to: #}
                    {#{% if mergeType == 'manual' %}#}
                        {#<a href=\"{{ path('god_mode_change_merging_mode', {'mode' : 'automatic', 'pack' : pack.getId()}) }}\" class=\"btn btn-warning btn-flat\">Automatic</a>#}
                    {#{% elseif mergeType == 'automatic' %}#}
                        {#<a href=\"{{ path('god_mode_change_merging_mode', {'mode' : 'manual', 'pack' : pack.getId()}) }}\" class=\"btn btn-warning btn-flat\">Manual</a>#}
                    {#{% endif %}#}
                {#</p>#}
                {#{% if mergeType == 'manual' %}#}
                    {#<p><a href=\"{{ path('god_mode_auto_merge_users', {'pack' : pack.getId()}) }}\" class=\"btn btn-success btn-flat pull-right\">Automatically Merge Waiting List</a></p>#}
                {#{% endif %}#}
            </div>
            <div class=\"clearfix\"></div>
        </div>

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item\">List Users</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>
                    <a href=\"{{ path('god_mode_merge_users', {'pack' : pack.getId()}) }}\" class=\"list-group-item active\">Merge Users</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                {% if mergeType == 'automatic' %}
                    <div class=\"alert alert-warning\">
                        <p>Sorry, Merging Mode Set To Automatic. If you wish to manually handle things yourself, kindly switch mode back to Manual</p>
                    </div>
                {% else %}
                    <div class=\"row\">
                        <div class=\"col-sm-2\">
                        </div>
                        <div class=\"col-sm-8 text-center lead\">
                            {{ waitingSubscriptions.getTotalItemCount }} User{% if waitingSubscriptions.getTotalItemCount != 1 %}s{% endif %} in Waiting List For {{ pack.getName() }} ({{ app_currency_symbol }}{{ pack.getFormattedAmount() }}) Package
                        </div>
                        <div class=\"col-sm-2\">
                        </div>
                    </div>

                    <div class=\"row\">
                        <form action=\"?query=\">
                        <div class=\"col-md-8\">

                                <select class=\"form-control\" name=\"query\" id=\"query\">

                                    {% for selectList in selectList %}
                                        <option value=\"{{ selectList.getUser().getId() }}\">{{ selectList.getUser().getName() }}</option>
                                    {% endfor %}

                                </select>
                                {#<input type=\"text\" class=\"form-control\" name=\"query\">#}
                        </div>
                        <div class=\"col-md-4\">
                            <input type=\"submit\" class=\"btn btn-success\">
                        </div>
                        </form>

                    </div>



                    <table class=\"table table-hover table-responsive table-striped\">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Username</th>
                                {#<th>Date Subscribed</th>#}
                                <th>Total Feeders</th>
                                <th>Select Receiver</th>
                            </tr>
                        </thead>
                        <tbody>
                            {% for waitingSubscription in waitingSubscriptions %}
                                <tr>
                                    <td>{{ waitingSubscription.getUser().getName() }}</td>
                                    <td>{{ waitingSubscription.getUser().getUsername() }}</td>
                                    {#<td>{{ waitingSubscription.getUser().getUsername() }}</td>#}
                                    <td>{{ waitingSubscription.getFeederTotal() }}</td>
                                    <td>
                                        <div class=\"dropdown nav-item\">
                                            <input class=\"dropdown-toggle form-control live-search-box\" id=\"quicksearch\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" placeholder=\"Search By Username\">
                                            <div class=\"dropdown-menu live-search-list\" aria-labelledby=\"dropdownMenuButton\" style=\"max-height: 250px; overflow-y: scroll; width: 100%\">
                                                    <li>
                                                        <a class=\"dropdown-item\" href=\"{{ path('god_mode_merge_user', {'receiver' : adminone.getId(), 'feederSubscription' : waitingSubscription.getId(), 'pack' : pack.getId()}) }}\">{{ adminone.getUsername() }}</a>
                                                    </li>

                                                {#<li>#}
                                                    {#<a class=\"dropdown-item\" href=\"{{ path('god_mode_merge_user', {'receiver' : admintwo.getId(), 'feederSubscription' : waitingSubscription.getId(), 'pack' : pack.getId()}) }}\">{{ admintwo.getUsername() }}</a>#}
                                                {#</li>#}

                                                {#<li>#}
                                                    {#<a class=\"dropdown-item\" href=\"{{ path('god_mode_merge_user', {'receiver' : adminthree.getId(), 'feederSubscription' : waitingSubscription.getId(), 'pack' : pack.getId()}) }}\">{{ adminthree.getUsername() }}</a>#}
                                                {#</li>#}

                                                {% for admin in admins %}
                                                    <li>
                                                        <a class=\"dropdown-item\" href=\"{{ path('god_mode_merge_user', {'receiver' : admin.getId(), 'feederSubscription' : waitingSubscription.getId(), 'pack' : pack.getId()}) }}\">{{ admin.getUsername() }}</a>
                                                    </li>
                                                {% endfor %}
                                                {% for user in activeUsers %}
                                                    {% if user.getRecommitRequired == true %}
                                                    <li style=\"background-color:red; color: white\">
                                                        {% elseif user.getFeederCounter() == 1 %}
                                                            <li style=\"background-color:greenyellow; color: white\">
                                                    {% else  %}
                                                <li>
                                                {% endif %}

                                                <a class=\"dropdown-item\" href=\"{{ path('god_mode_merge_user', {'receiver' : user.getUser().getId(), 'feederSubscription' : waitingSubscription.getId(), 'pack' : pack.getId()}) }}\">
                                                            {{ user.getUser().getUsername() }} ({{ user.getUser().getName() }}) ({{  user.getCreated|date(\"F jS \\\\a\\\\t g:ia\") }})
                                                        </a>
                                                    </li>
                                                {% endfor %}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            {% endfor %}
                        </tbody>
                    </table>

                    <div class=\"navigation text-center\">
                        {{ knp_pagination_render(waitingSubscriptions) }}
                    </div>
                {% endif %}
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}

{% block javascripts %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(document).ready(function(\$) {
            \$('.live-search-list li').each(function() {
                \$(this).attr('data-search-term', \$(this).text().toLowerCase());
            });

            \$('.live-search-box').on('keyup', function() {
                var searchTerm = \$(this).val().toLowerCase();

                \$('.live-search-list li').each(function() {
                    if(\$(this).filter('[data-search-term *= ' + searchTerm + ']').length > 0 || searchTerm.length < 1) {
                        \$(this).show();
                    } else {
                        \$(this).hide();
                    }
                });
            });
        });
    </script>
{% endblock %}", "god-mode/merge-users.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\god-mode\\merge-users.html.twig");
    }
}
