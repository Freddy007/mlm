<?php

/* member/pack-payment.html.twig */
class __TwigTemplate_ef10df778ed3aa5fa70476532b347ba4a88a7f33eedf932ccd4e250f865fd107 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/pack-payment.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'modalPicHeader' => array($this, 'block_modalPicHeader'),
            'modalPicBody' => array($this, 'block_modalPicBody'),
            'modalCancelHeader' => array($this, 'block_modalCancelHeader'),
            'modalCancelBody' => array($this, 'block_modalCancelBody'),
            'modalCancelLink' => array($this, 'block_modalCancelLink'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3e52bb02ce1cfaecf72e17065d57b0c5eec8841fda0ac8b7e5660b6d88d14e74 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e52bb02ce1cfaecf72e17065d57b0c5eec8841fda0ac8b7e5660b6d88d14e74->enter($__internal_3e52bb02ce1cfaecf72e17065d57b0c5eec8841fda0ac8b7e5660b6d88d14e74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/pack-payment.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3e52bb02ce1cfaecf72e17065d57b0c5eec8841fda0ac8b7e5660b6d88d14e74->leave($__internal_3e52bb02ce1cfaecf72e17065d57b0c5eec8841fda0ac8b7e5660b6d88d14e74_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_7aa546d33b6927383a3aee06b1116f381bcd2518f9c0e966e578208533c04c31 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7aa546d33b6927383a3aee06b1116f381bcd2518f9c0e966e578208533c04c31->enter($__internal_7aa546d33b6927383a3aee06b1116f381bcd2518f9c0e966e578208533c04c31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Make Payment";
        
        $__internal_7aa546d33b6927383a3aee06b1116f381bcd2518f9c0e966e578208533c04c31->leave($__internal_7aa546d33b6927383a3aee06b1116f381bcd2518f9c0e966e578208533c04c31_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_4a67bc5b2df2a7043124ad93acdda40307aa299ffd53a74fa2396b8481242f11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a67bc5b2df2a7043124ad93acdda40307aa299ffd53a74fa2396b8481242f11->enter($__internal_4a67bc5b2df2a7043124ad93acdda40307aa299ffd53a74fa2396b8481242f11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "autoRecycleSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-info\" xmlns=\"http://www.w3.org/1999/html\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "subscribeSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Make Payment - ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
        echo "
            (";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPack", array(), "method"), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ")</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li><a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("packs");
        echo "\"><i class=\"fa fa-pie-chart\"></i> Packs</a></li>
            <li class=\"active\">Make Payment</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
        <div class=\"row\">
            <!-- Receiver's Column -->
            <div class=\"col-sm-5\">
                <div class=\"box box-success\">
                    <div class=\"box-body box-profile\">
                        <img class=\"profile-user-img img-responsive img-circle center-block\"
                             src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array(), "method"), "getAvatar", array(), "method")), "html", null, true);
        echo "\" alt=\"User profile picture\">

                        <h3 class=\"profile-username text-center\">";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array(), "method"), "getName", array(), "method"), "html", null, true);
        echo "</h3>

                        <p class=\"text-muted text-center\">";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array(), "method"), "getUsername", array(), "method"), "html", null, true);
        echo "</p>

                        <ul class=\"list-group list-group-unbordered\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-phone\"></i> <a
                                        class=\"pull-right\">";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array(), "method"), "getPhoneOne", array(), "method"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 49
        if (($this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array(), "method"), "getPhoneTwo", array(), "method") != null)) {
            // line 50
            echo "                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-phone\"></i> <a
                                            class=\"pull-right\">";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array(), "method"), "getPhoneTwo", array(), "method"), "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        // line 55
        echo "                            <li class=\"list-group-item\">
                                <i class=\"fa fa-envelope-o\"></i> <a
                                        class=\"pull-right\">";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array(), "method"), "getEmail", array(), "method"), "html", null, true);
        echo "</a>
                            </li>


                            <div class=\"box box-success\">

                                <div class=\"box-header with-border\">
                                    ";
        // line 65
        echo "                                        ";
        // line 66
        echo "                                    ";
        // line 67
        echo "                                        ";
        // line 68
        echo "                                    ";
        // line 69
        echo "                                        ";
        // line 70
        echo "                                    ";
        // line 71
        echo "                                </div>
                                <div class=\"box-body\">
                                    ";
        // line 74
        echo "
                                        <ul class=\"list-group list-group-unbordered\">


                                            <li class=\"list-group-item\">
                                                <i class=\"fa fa-bank\"></i> Mobile Money Number <a
                                                        class=\"pull-right\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array()), "getPhoneOne", array(), "method"), "html", null, true);
        echo "</a>
                                            </li>

                                            <li class=\"list-group-item\">
                                                <i class=\"fa fa-bank\"></i> Alternative Number <a
                                                        class=\"pull-right\">";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array()), "getPhoneTwo", array(), "method"), "html", null, true);
        echo "</a>
                                            </li>

                                        </ul>

                                    ";
        // line 91
        echo "                                        <ul class=\"list-group list-group-unbordered\">

                                            ";
        // line 94
        echo "                                                ";
        // line 95
        echo "                                                        ";
        // line 96
        echo "                                            ";
        // line 97
        echo "
                                            ";
        // line 99
        echo "                                                ";
        // line 100
        echo "                                                        ";
        // line 101
        echo "                                            ";
        // line 102
        echo "
                                            ";
        // line 104
        echo "                                                ";
        // line 105
        echo "                                                        ";
        // line 106
        echo "                                            ";
        // line 107
        echo "
                                            ";
        // line 109
        echo "                                                ";
        // line 110
        echo "                                                        ";
        // line 111
        echo "                                            ";
        // line 112
        echo "
                                            ";
        // line 114
        echo "                                                ";
        // line 115
        echo "                                                        ";
        // line 116
        echo "                                            ";
        // line 117
        echo "
                                        </ul>

                                    ";
        // line 121
        echo "                                        ";
        // line 122
        echo "                                            ";
        // line 123
        echo "                                                ";
        // line 124
        echo "                                        ";
        // line 125
        echo "                                    ";
        // line 126
        echo "                                </div>

                            </div>
                        </ul>
                    </div>
                </div>

                ";
        // line 134
        echo "                    ";
        // line 135
        echo "                    ";
        // line 136
        echo "                        ";
        // line 137
        echo "                            ";
        // line 138
        echo "
                        ";
        // line 140
        echo "                    ";
        // line 141
        echo "                    ";
        // line 142
        echo "                        ";
        // line 143
        echo "                            ";
        // line 144
        echo "                            ";
        // line 145
        echo "                                ";
        // line 146
        echo "                            ";
        // line 147
        echo "                            ";
        // line 148
        echo "                                ";
        // line 149
        echo "                                    ";
        // line 150
        echo "                                            ";
        // line 151
        echo "                                ";
        // line 152
        echo "                                ";
        // line 153
        echo "                                    ";
        // line 154
        echo "                                        ";
        // line 155
        echo "                                                ";
        // line 156
        echo "                                    ";
        // line 157
        echo "                                ";
        // line 158
        echo "                                ";
        // line 159
        echo "                                    ";
        // line 160
        echo "                                            ";
        // line 161
        echo "                                ";
        // line 162
        echo "                                ";
        // line 163
        echo "                                    ";
        // line 164
        echo "                                        ";
        // line 165
        echo "                                                ";
        // line 166
        echo "                                    ";
        // line 167
        echo "                                ";
        // line 168
        echo "
                            ";
        // line 170
        echo "                        ";
        // line 171
        echo "                    ";
        // line 172
        echo "                ";
        // line 173
        echo "                    ";
        // line 174
        echo "                        ";
        // line 175
        echo "                            ";
        // line 176
        echo "                    ";
        // line 177
        echo "                ";
        // line 178
        echo "                <!-- End Receiver's Bitcoin Section -->
            </div>
            <!-- End Receiver's Column -->

            <!-- Payment Form Column -->
            <div class=\"col-sm-7\">
                ";
        // line 184
        if (((isset($context["renderForm"]) ? $context["renderForm"] : $this->getContext($context, "renderForm")) == true)) {
            // line 185
            echo "                    ";
            if (($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "not_paid")) {
                // line 186
                echo "                        <div class=\"alert alert-warning countdownHolder\">
                            <p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left to complete payment</p>
                            <div id=\"clockGiver\" class='countdownTimer'>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder hourCountDownGiver'></span>
                                    <br>
                                    <span class='timeDescription'>Hrs</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder minuteCountDownGiver'></span>
                                    <br>
                                    <span class='timeDescription'>Mins</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder secondCountDownGiver'></span>
                                    <br>
                                    <span class='timeDescription'>Secs</span>
                                </div>
                            </div>
                            <script type=\"text/javascript\">
                                var deadlineGiver = \"";
                // line 206
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                echo "\";
                                initializeClock('clockGiver', 'hourCountDownGiver', 'minuteCountDownGiver', 'secondCountDownGiver', deadlineGiver);
                            </script>
                        </div>
                    ";
            } elseif (($this->getAttribute(            // line 210
(isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "scam")) {
                // line 211
                echo "                        <div class=\"alert alert-danger countdownHolder\">
                            <p class=\"text-center lead\" style=\"margin-bottom: 5px\">Your Payment has been marked as a
                                scam</p>
                            <p class=\"text-center small\">(Kindly complete your payment before your time elapses or you
                                will be banned permanently from the system!)</p>
                            <div id='clockReceiver' class='countdownTimer'>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder hourCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Hrs</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder minuteCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Mins</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder secondCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Secs</span>
                                </div>
                            </div>
                            <script type=\"text/javascript\">
                                var deadline = \"";
                // line 234
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                echo "\";
                                initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
                            </script>
                        </div>
                    ";
            }
            // line 239
            echo "                    <div class=\"box box-solid\">
                        <div class=\"box-header with-border\">
                            <h3 class=\"box-title\">Enter Payment Details</h3>
                        </div>
                        <script>
                            \$(document).ready( function() {
                                \$('#paymentmethod').change(function() {
                                    // to get the selected value
                                    var selected = \$(this).val();

                                    if(selected == \"Bitcoin Wallet\"){
                                        banknamegroup.style.display(\"none\");
                                    }else {

                                    }

                                    // do your magic pony stuff
                                });
                            });
                        </script>
                        <div class=\"box-body\">
                            ";
            // line 260
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("make_payment", array("feeder" => $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getId", array(), "method"))), "attr" => array("class" => "form-horizontal")));
            echo "

                            <span class=\"text-danger\">";
            // line 262
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "payment_method", array()), 'errors');
            echo "</span>
                            <div class=\"form-group\">
                                ";
            // line 264
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "payment_method", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Payment Method"));
            echo "

                                <div class=\"col-sm-10\">
                                    ";
            // line 267
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "payment_method", array()), 'widget', array("attr" => array("id" => "paymentmethod")));
            echo "
                                </div>
                            </div>

                            <span class=\"text-danger\">";
            // line 271
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "paymentImage", array()), 'errors');
            echo "</span>
                            <div class=\"form-group\">
                                ";
            // line 273
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "paymentImage", array()), 'label', array("label_attr" => array("class" => "col-sm-2 control-label"), "label" => "Screenshot"));
            echo "

                                <div class=\"col-sm-10\">
                                    ";
            // line 276
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "paymentImage", array()), 'widget');
            echo "
                                </div>
                            </div>

                            <div class=\"row\">
                                <div class=\"col-sm-6\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Make Payment
                                    </button>
                                </div>
                                <div class=\"col-sm-6\">
                                    <button type=\"button\" class=\"btn btn-danger btn-block btn-flat\" data-toggle=\"modal\"
                                            data-target=\"#viewCancelModal\">I Can't Pay
                                    </button>
                                </div>
                            </div>

                            ";
            // line 292
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
            echo "
                        </div>
                    </div>
                ";
        } else {
            // line 296
            echo "                    ";
            if ((($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "payment_made") || (($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getScamPaid", array(), "method") == "scam_paid") && ($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "scam")))) {
                // line 297
                echo "                        <div class=\"alert alert-info countdownHolder\">
                            <p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left for receiver to confirm
                                you</p>
                            <div id='clockReceiver' class='countdownTimer'>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder hourCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Hrs</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder minuteCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Mins</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder secondCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Secs</span>
                                </div>
                            </div>
                            <script type=\"text/javascript\">
                                var deadline = \"";
                // line 318
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiverTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                echo "\";
                                initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
                            </script>
                        </div>
                    ";
            } elseif (($this->getAttribute(            // line 322
(isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "scam")) {
                // line 323
                echo "                        <div class=\"alert alert-danger countdownHolder\">
                            <p class=\"text-center lead\" style=\"margin-bottom: 5px\">Your Payment has been marked a
                                scam</p>
                            <div id='clockReceiver' class='countdownTimer'>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder hourCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Hrs</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder minuteCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Mins</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder secondCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Secs</span>
                                </div>
                            </div>
                            <script type=\"text/javascript\">
                                var deadline = \"";
                // line 344
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                echo "\";
                                initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
                            </script>
                        </div>
                    ";
            } elseif (($this->getAttribute(            // line 348
(isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "fully_paid")) {
                // line 349
                echo "                        <div class=\"alert alert-success\">
                            <p class=\"lead\">Payment Confirmed</p>
                            <p>This payment has been confirmed</p>
                        </div>
                    ";
            } elseif (($this->getAttribute(            // line 353
(isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getStatus", array(), "method") == "jury")) {
                // line 354
                echo "                        <div class=\"alert alert-warning text-center\">
                            <p class=\"lead\">Case in Jury's Stand</p>
                            <p>This case is in court. Head to the Judges Panel to submit your statement and evidence</p>
                            <p><a href=\"";
                // line 357
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_case", array("case" => $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getJury", array(), "method"))), "html", null, true);
                echo "\" class=\"btn btn-info\"
                                  style=\"text-decoration: none\">View in Jury's Stand</a></p>
                        </div>
                    ";
            }
            // line 361
            echo "                    <div class=\"box box-solid\">
                        <div class=\"box-header with-border\">
                            <h3 class=\"box-title\">Payment Details</h3>
                        </div>

                        <div class=\"box-body\">
                            <form class=\"form-horizontal\">

                                <div class=\"form-group\">
                                    <label class=\"col-sm-4 control-label\">Payment Method</label>

                                    <div class=\"col-sm-8\">
                                        <p class=\"form-control-static\">";
            // line 373
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentMethod", array(), "method"), "html", null, true);
            echo "</p>
                                    </div>
                                </div>


                                <div class=\"form-group\">
                                    <label class=\"col-sm-4 control-label\">Mobile Money Number</label>

                                    <div class=\"col-sm-8\">
                                        <p class=\"form-control-static\">";
            // line 382
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getReceiver", array(), "method"), "getPhoneOne", array(), "method"), "html", null, true);
            echo "</p>
                                    </div>
                                </div>


                                ";
            // line 388
            echo "                                    ";
            // line 389
            echo "
                                    ";
            // line 391
            echo "                                        ";
            // line 392
            echo "                                    ";
            // line 393
            echo "                                ";
            // line 394
            echo "
                                ";
            // line 396
            echo "                                    ";
            // line 397
            echo "
                                    ";
            // line 399
            echo "                                        ";
            // line 400
            echo "                                    ";
            // line 401
            echo "                                ";
            // line 402
            echo "
                                <div class=\"form-group\">
                                    <label class=\"col-sm-4 control-label\">Payment Proof</label>

                                    <div class=\"col-sm-8\">
                                        <p class=\"form-control-static\"><img class=\"img-responsive\"
                                                                            src=\"";
            // line 408
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentUploadedFile", array(), "method")), "html", null, true);
            echo "\"
                                                                            style=\"margin: 0; border: none;\"></p>
                                        <p class=\"form-control-static\">
                                            <button type=\"button\" class=\"btn btn-xs btn-warning viewPicModal\"
                                                    data-toggle=\"modal\" data-target=\"#viewPicModal\">View Image
                                            </button>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                ";
        }
        // line 421
        echo "            </div> <!-- End Receiver's Column -->
        </div>
    </section> <!-- End Main Content -->
";
        
        $__internal_4a67bc5b2df2a7043124ad93acdda40307aa299ffd53a74fa2396b8481242f11->leave($__internal_4a67bc5b2df2a7043124ad93acdda40307aa299ffd53a74fa2396b8481242f11_prof);

    }

    // line 426
    public function block_modalPicHeader($context, array $blocks = array())
    {
        $__internal_01fe50a77d2c7302edde66a8400a24429d4d9e2beb91ccef312687832ac37c85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01fe50a77d2c7302edde66a8400a24429d4d9e2beb91ccef312687832ac37c85->enter($__internal_01fe50a77d2c7302edde66a8400a24429d4d9e2beb91ccef312687832ac37c85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalPicHeader"));

        echo "Payment Proof for ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPack", array(), "method"), "getFormattedAmount", array(), "method"), "html", null, true);
        echo ")";
        
        $__internal_01fe50a77d2c7302edde66a8400a24429d4d9e2beb91ccef312687832ac37c85->leave($__internal_01fe50a77d2c7302edde66a8400a24429d4d9e2beb91ccef312687832ac37c85_prof);

    }

    // line 427
    public function block_modalPicBody($context, array $blocks = array())
    {
        $__internal_02c35b496658bb4a48ed738f7f50709a380000bf36a1a1033ce950d8bc4cabb3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02c35b496658bb4a48ed738f7f50709a380000bf36a1a1033ce950d8bc4cabb3->enter($__internal_02c35b496658bb4a48ed738f7f50709a380000bf36a1a1033ce950d8bc4cabb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalPicBody"));

        echo "<img class=\"img-responsive\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getPaymentUploadedFile", array(), "method")), "html", null, true);
        echo "\">";
        
        $__internal_02c35b496658bb4a48ed738f7f50709a380000bf36a1a1033ce950d8bc4cabb3->leave($__internal_02c35b496658bb4a48ed738f7f50709a380000bf36a1a1033ce950d8bc4cabb3_prof);

    }

    // line 429
    public function block_modalCancelHeader($context, array $blocks = array())
    {
        $__internal_15a8cb14384df0f2bd44635f6451149b8c0c0b0f454194f2b62e70bc8168b505 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15a8cb14384df0f2bd44635f6451149b8c0c0b0f454194f2b62e70bc8168b505->enter($__internal_15a8cb14384df0f2bd44635f6451149b8c0c0b0f454194f2b62e70bc8168b505_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelHeader"));

        echo "Cancel Payment";
        
        $__internal_15a8cb14384df0f2bd44635f6451149b8c0c0b0f454194f2b62e70bc8168b505->leave($__internal_15a8cb14384df0f2bd44635f6451149b8c0c0b0f454194f2b62e70bc8168b505_prof);

    }

    // line 430
    public function block_modalCancelBody($context, array $blocks = array())
    {
        $__internal_79504272d0df1ceb5d8f6a34fd466647082a7f830213adbfd3c679b0717f2faf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79504272d0df1ceb5d8f6a34fd466647082a7f830213adbfd3c679b0717f2faf->enter($__internal_79504272d0df1ceb5d8f6a34fd466647082a7f830213adbfd3c679b0717f2faf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelBody"));

        echo "Are you sure you want to cancel this payment? If you do not have another package subscription where your payment has been confirmed and activated, you will be blocked from this system";
        
        $__internal_79504272d0df1ceb5d8f6a34fd466647082a7f830213adbfd3c679b0717f2faf->leave($__internal_79504272d0df1ceb5d8f6a34fd466647082a7f830213adbfd3c679b0717f2faf_prof);

    }

    // line 431
    public function block_modalCancelLink($context, array $blocks = array())
    {
        $__internal_b07d444cffcb0b78815bc611268591c1cca8cf280fef77b6633b106d4433b5c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b07d444cffcb0b78815bc611268591c1cca8cf280fef77b6633b106d4433b5c0->enter($__internal_b07d444cffcb0b78815bc611268591c1cca8cf280fef77b6633b106d4433b5c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelLink"));

        // line 432
        echo "    <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cancel_payment", array("feeder" => $this->getAttribute((isset($context["feeder"]) ? $context["feeder"] : $this->getContext($context, "feeder")), "getId", array(), "method"))), "html", null, true);
        echo "\" class=\"btn btn-outline pull-right\">Proceed</a>
";
        
        $__internal_b07d444cffcb0b78815bc611268591c1cca8cf280fef77b6633b106d4433b5c0->leave($__internal_b07d444cffcb0b78815bc611268591c1cca8cf280fef77b6633b106d4433b5c0_prof);

    }

    public function getTemplateName()
    {
        return "member/pack-payment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  778 => 432,  772 => 431,  760 => 430,  748 => 429,  734 => 427,  717 => 426,  707 => 421,  691 => 408,  683 => 402,  681 => 401,  679 => 400,  677 => 399,  674 => 397,  672 => 396,  669 => 394,  667 => 393,  665 => 392,  663 => 391,  660 => 389,  658 => 388,  650 => 382,  638 => 373,  624 => 361,  617 => 357,  612 => 354,  610 => 353,  604 => 349,  602 => 348,  595 => 344,  572 => 323,  570 => 322,  563 => 318,  540 => 297,  537 => 296,  530 => 292,  511 => 276,  505 => 273,  500 => 271,  493 => 267,  487 => 264,  482 => 262,  477 => 260,  454 => 239,  446 => 234,  421 => 211,  419 => 210,  412 => 206,  390 => 186,  387 => 185,  385 => 184,  377 => 178,  375 => 177,  373 => 176,  371 => 175,  369 => 174,  367 => 173,  365 => 172,  363 => 171,  361 => 170,  358 => 168,  356 => 167,  354 => 166,  352 => 165,  350 => 164,  348 => 163,  346 => 162,  344 => 161,  342 => 160,  340 => 159,  338 => 158,  336 => 157,  334 => 156,  332 => 155,  330 => 154,  328 => 153,  326 => 152,  324 => 151,  322 => 150,  320 => 149,  318 => 148,  316 => 147,  314 => 146,  312 => 145,  310 => 144,  308 => 143,  306 => 142,  304 => 141,  302 => 140,  299 => 138,  297 => 137,  295 => 136,  293 => 135,  291 => 134,  282 => 126,  280 => 125,  278 => 124,  276 => 123,  274 => 122,  272 => 121,  267 => 117,  265 => 116,  263 => 115,  261 => 114,  258 => 112,  256 => 111,  254 => 110,  252 => 109,  249 => 107,  247 => 106,  245 => 105,  243 => 104,  240 => 102,  238 => 101,  236 => 100,  234 => 99,  231 => 97,  229 => 96,  227 => 95,  225 => 94,  221 => 91,  213 => 85,  205 => 80,  197 => 74,  193 => 71,  191 => 70,  189 => 69,  187 => 68,  185 => 67,  183 => 66,  181 => 65,  171 => 57,  167 => 55,  161 => 52,  157 => 50,  155 => 49,  150 => 47,  142 => 42,  137 => 40,  132 => 38,  116 => 25,  112 => 24,  105 => 22,  101 => 21,  96 => 18,  87 => 15,  84 => 14,  80 => 13,  77 => 12,  68 => 9,  65 => 8,  61 => 7,  58 => 6,  52 => 5,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Make Payment{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('autoRecycleSuccess') %}
        <div class=\"callout callout-info\" xmlns=\"http://www.w3.org/1999/html\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('subscribeSuccess') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Make Payment - {{ feeder.getPack().getName() }}
            ({{ app_currency_symbol }} {{ feeder.getPack().getFormattedAmount() }})</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li><a href=\"{{ path('packs') }}\"><i class=\"fa fa-pie-chart\"></i> Packs</a></li>
            <li class=\"active\">Make Payment</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
        <div class=\"row\">
            <!-- Receiver's Column -->
            <div class=\"col-sm-5\">
                <div class=\"box box-success\">
                    <div class=\"box-body box-profile\">
                        <img class=\"profile-user-img img-responsive img-circle center-block\"
                             src=\"{{ asset(feeder.getReceiver().getAvatar()) }}\" alt=\"User profile picture\">

                        <h3 class=\"profile-username text-center\">{{ feeder.getReceiver().getName() }}</h3>

                        <p class=\"text-muted text-center\">{{ feeder.getReceiver().getUsername() }}</p>

                        <ul class=\"list-group list-group-unbordered\">
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-phone\"></i> <a
                                        class=\"pull-right\">{{ feeder.getReceiver().getPhoneOne() }}</a>
                            </li>
                            {% if feeder.getReceiver().getPhoneTwo() != null %}
                                <li class=\"list-group-item\">
                                    <i class=\"fa fa-phone\"></i> <a
                                            class=\"pull-right\">{{ feeder.getReceiver().getPhoneTwo() }}</a>
                                </li>
                            {% endif %}
                            <li class=\"list-group-item\">
                                <i class=\"fa fa-envelope-o\"></i> <a
                                        class=\"pull-right\">{{ feeder.getReceiver().getEmail() }}</a>
                            </li>


                            <div class=\"box box-success\">

                                <div class=\"box-header with-border\">
                                    {#{% if feeder.getReceiver.getBankChoice() == 1 %}#}
                                        {#<h3 class=\"box-title\">Default Bank Account</h3>#}
                                    {#{% elseif feeder.getReceiver.getBankChoice() == 2 %}#}
                                        {#<h3 class=\"box-title\">Provisional Bank Account</h3>#}
                                    {#{% else %}#}
                                        {#<h3 class=\"box-title\">Notice: </h3>#}
                                    {#{% endif %}#}
                                </div>
                                <div class=\"box-body\">
                                    {#{% if feeder.getReceiver.getBankName() == feeder.getReceiver.getBankNameOne() %}#}

                                        <ul class=\"list-group list-group-unbordered\">


                                            <li class=\"list-group-item\">
                                                <i class=\"fa fa-bank\"></i> Mobile Money Number <a
                                                        class=\"pull-right\">{{ feeder.getReceiver.getPhoneOne() }}</a>
                                            </li>

                                            <li class=\"list-group-item\">
                                                <i class=\"fa fa-bank\"></i> Alternative Number <a
                                                        class=\"pull-right\">{{ feeder.getReceiver.getPhoneTwo() }}</a>
                                            </li>

                                        </ul>

                                    {#{% elseif feeder.getReceiver.getBankName() == feeder.getReceiver.getBankNameTwo() %}#}
                                        <ul class=\"list-group list-group-unbordered\">

                                            {#<li class=\"list-group-item\">#}
                                                {#<i class=\"fa fa-bank\"></i> Bank Name <a#}
                                                        {#class=\"pull-right\">{{ feeder.getReceiver.getBankNameTwo() }}</a>#}
                                            {#</li>#}

                                            {#<li class=\"list-group-item\">#}
                                                {#<i class=\"fa fa-bank\"></i> Phone Number <a#}
                                                        {#class=\"pull-right\">{{ feeder.getReceiver.getBankNameTwo() }}</a>#}
                                            {#</li>#}

                                            {#<li class=\"list-group-item\">#}
                                                {#<i class=\"fa fa-bank\"></i> Account Name <a#}
                                                        {#class=\"pull-right\">{{ feeder.getReceiver.getAccountNameTwo() }}</a>#}
                                            {#</li>#}

                                            {#<li class=\"list-group-item\">#}
                                                {#<i class=\"fa fa-bank\"></i> Account Number <a#}
                                                        {#class=\"pull-right\">{{ feeder.getReceiver.getAccountNumberTwo() }}</a>#}
                                            {#</li>#}

                                            {#<li class=\"list-group-item\">#}
                                                {#<i class=\"fa fa-bank\"></i> Account Type <a#}
                                                        {#class=\"pull-right\">{{ feeder.getReceiver.getAccountTypeTwo() }}</a>#}
                                            {#</li>#}

                                        </ul>

                                    {#{% else %}#}
                                        {#<div class=\"alert alert-danger text-center\">#}
                                            {#<p>User has not provided bank acccount details! Message/Call User or Contact#}
                                                {#Support</p>#}
                                        {#</div>#}
                                    {#{% endif %}#}
                                </div>

                            </div>
                        </ul>
                    </div>
                </div>

                {#{% if receiver.bitcoinAddress != null or receiver.bitcoinAddress != '' %}#}
                    {#<!-- Receiver's Bitcoin Section  -->#}
                    {#<div class=\"box box-warning\">#}
                        {#{% autoescape %}#}
                            {#{{ box.create_payment_box(feeder.getReceiver.getBitcoinAddress(), btcamount)| raw }}#}

                        {#{% endautoescape %}#}
                    {#</div>#}
                    {#<div class=\"box box-warning\">#}
                        {#<div class=\"box-body box-profile\">#}
                            {#<img src=\"https://chart.googleapis.com/chart?chs=225x225&chld=L|2&cht=qr&chl=bitcoin%3A{{  feeder.getReceiver().getBitcoinAddress }}%3Famount%3D{{ btcamount }}\" class=\"img-responsive center-block\"  style=\"height: 240px;width: 240px\" />#}
                            {#<div class=\"alert alert-info text-center\">#}
                                {#<p>Once you have made payment via Bitcoin Click on the \"Make Payment\" button .<br/> <strong> Select Bitcoin Wallet for Bank and Payment Method</strong> </p>#}
                            {#</div>#}
                            {#<ul class=\"list-group list-group-unbordered\">#}
                                {#<li class=\"list-group-item\">#}
                                    {#<i class=\"fa fa-money\"></i> Amount in {{ app_currency_symbol }}: <a#}
                                            {#class=\"pull-right\">{{ feeder.getPack().getAmount() }}</a>#}
                                {#</li>#}
                                {#{% if app_currency != 'USD' %}#}
                                    {#<li class=\"list-group-item\">#}
                                        {#<i class=\"fa fa-money\"></i> Amount in \$: <a#}
                                                {#class=\"pull-right\">{{ feeder.getPack().getAmount()/app_rate }} </a>#}
                                    {#</li>#}
                                {#{% endif %}#}
                                {#<li class=\"list-group-item\">#}
                                    {#<i class=\"fa fa-money\"></i> Amount in BTC: <a#}
                                            {#class=\"pull-right\"> {{ btcamount }} </a>#}
                                {#</li>#}
                                {#{% if feeder.getReceiver().getBitcoinAddress != null %}#}
                                    {#<li class=\"list-group-item\">#}
                                        {#<i class=\"fa fa-envelope-o\"></i> Receiver Bitcoin Address: <a#}
                                                {#class=\"pull-right\">{{ feeder.getReceiver().getBitcoinAddress() }}</a>#}
                                    {#</li>#}
                                {#{% endif %}#}

                            {#</ul>#}
                        {#</div>#}
                    {#</div>#}
                {#{% else %}#}
                    {#<div class=\"alert alert-danger text-center\">#}
                        {#<p>User has not provided bitcoin address! if you would like to pay through bitcoin, Message/Call#}
                            {#User or Contact Support</p>#}
                    {#</div>#}
                {#{% endif %}#}
                <!-- End Receiver's Bitcoin Section -->
            </div>
            <!-- End Receiver's Column -->

            <!-- Payment Form Column -->
            <div class=\"col-sm-7\">
                {% if renderForm == true %}
                    {% if feeder.getStatus() == 'not_paid' %}
                        <div class=\"alert alert-warning countdownHolder\">
                            <p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left to complete payment</p>
                            <div id=\"clockGiver\" class='countdownTimer'>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder hourCountDownGiver'></span>
                                    <br>
                                    <span class='timeDescription'>Hrs</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder minuteCountDownGiver'></span>
                                    <br>
                                    <span class='timeDescription'>Mins</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder secondCountDownGiver'></span>
                                    <br>
                                    <span class='timeDescription'>Secs</span>
                                </div>
                            </div>
                            <script type=\"text/javascript\">
                                var deadlineGiver = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                initializeClock('clockGiver', 'hourCountDownGiver', 'minuteCountDownGiver', 'secondCountDownGiver', deadlineGiver);
                            </script>
                        </div>
                    {% elseif feeder.getStatus() == 'scam' %}
                        <div class=\"alert alert-danger countdownHolder\">
                            <p class=\"text-center lead\" style=\"margin-bottom: 5px\">Your Payment has been marked as a
                                scam</p>
                            <p class=\"text-center small\">(Kindly complete your payment before your time elapses or you
                                will be banned permanently from the system!)</p>
                            <div id='clockReceiver' class='countdownTimer'>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder hourCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Hrs</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder minuteCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Mins</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder secondCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Secs</span>
                                </div>
                            </div>
                            <script type=\"text/javascript\">
                                var deadline = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
                            </script>
                        </div>
                    {% endif %}
                    <div class=\"box box-solid\">
                        <div class=\"box-header with-border\">
                            <h3 class=\"box-title\">Enter Payment Details</h3>
                        </div>
                        <script>
                            \$(document).ready( function() {
                                \$('#paymentmethod').change(function() {
                                    // to get the selected value
                                    var selected = \$(this).val();

                                    if(selected == \"Bitcoin Wallet\"){
                                        banknamegroup.style.display(\"none\");
                                    }else {

                                    }

                                    // do your magic pony stuff
                                });
                            });
                        </script>
                        <div class=\"box-body\">
                            {{ form_start(form, {'action': path('make_payment', { 'feeder': feeder.getId() }), 'attr': {'class': 'form-horizontal'}}) }}

                            <span class=\"text-danger\">{{ form_errors(form.payment_method) }}</span>
                            <div class=\"form-group\">
                                {{ form_label(form.payment_method, 'Payment Method', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                                <div class=\"col-sm-10\">
                                    {{ form_widget(form.payment_method, { 'attr': {'id': 'paymentmethod'} }) }}
                                </div>
                            </div>

                            <span class=\"text-danger\">{{ form_errors(form.paymentImage) }}</span>
                            <div class=\"form-group\">
                                {{ form_label(form.paymentImage, \"Screenshot\", { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}

                                <div class=\"col-sm-10\">
                                    {{ form_widget(form.paymentImage) }}
                                </div>
                            </div>

                            <div class=\"row\">
                                <div class=\"col-sm-6\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Make Payment
                                    </button>
                                </div>
                                <div class=\"col-sm-6\">
                                    <button type=\"button\" class=\"btn btn-danger btn-block btn-flat\" data-toggle=\"modal\"
                                            data-target=\"#viewCancelModal\">I Can't Pay
                                    </button>
                                </div>
                            </div>

                            {{ form_end(form) }}
                        </div>
                    </div>
                {% else %}
                    {% if feeder.getStatus() == 'payment_made' or (feeder.getScamPaid() == 'scam_paid' and feeder.getStatus() == 'scam') %}
                        <div class=\"alert alert-info countdownHolder\">
                            <p class=\"text-center lead\" style=\"margin-bottom: 5px\">Time left for receiver to confirm
                                you</p>
                            <div id='clockReceiver' class='countdownTimer'>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder hourCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Hrs</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder minuteCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Mins</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder secondCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Secs</span>
                                </div>
                            </div>
                            <script type=\"text/javascript\">
                                var deadline = \"{{ feeder.getReceiverTimeEnd()|date('Y-m-d H:i:s') }}\";
                                initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
                            </script>
                        </div>
                    {% elseif feeder.getStatus() == 'scam' %}
                        <div class=\"alert alert-danger countdownHolder\">
                            <p class=\"text-center lead\" style=\"margin-bottom: 5px\">Your Payment has been marked a
                                scam</p>
                            <div id='clockReceiver' class='countdownTimer'>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder hourCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Hrs</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder minuteCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Mins</span>
                                </div>
                                <div class='countdownTimerCell'>
                                    <span class='timeHolder secondCountDownReceiver'></span>
                                    <br>
                                    <span class='timeDescription'>Secs</span>
                                </div>
                            </div>
                            <script type=\"text/javascript\">
                                var deadline = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                initializeClock('clockReceiver', 'hourCountDownReceiver', 'minuteCountDownReceiver', 'secondCountDownReceiver', deadline);
                            </script>
                        </div>
                    {% elseif feeder.getStatus() == 'fully_paid' %}
                        <div class=\"alert alert-success\">
                            <p class=\"lead\">Payment Confirmed</p>
                            <p>This payment has been confirmed</p>
                        </div>
                    {% elseif feeder.getStatus() == 'jury' %}
                        <div class=\"alert alert-warning text-center\">
                            <p class=\"lead\">Case in Jury's Stand</p>
                            <p>This case is in court. Head to the Judges Panel to submit your statement and evidence</p>
                            <p><a href=\"{{ path('user_case', {'case' : feeder.getJury()}) }}\" class=\"btn btn-info\"
                                  style=\"text-decoration: none\">View in Jury's Stand</a></p>
                        </div>
                    {% endif %}
                    <div class=\"box box-solid\">
                        <div class=\"box-header with-border\">
                            <h3 class=\"box-title\">Payment Details</h3>
                        </div>

                        <div class=\"box-body\">
                            <form class=\"form-horizontal\">

                                <div class=\"form-group\">
                                    <label class=\"col-sm-4 control-label\">Payment Method</label>

                                    <div class=\"col-sm-8\">
                                        <p class=\"form-control-static\">{{ feeder.getPaymentMethod() }}</p>
                                    </div>
                                </div>


                                <div class=\"form-group\">
                                    <label class=\"col-sm-4 control-label\">Mobile Money Number</label>

                                    <div class=\"col-sm-8\">
                                        <p class=\"form-control-static\">{{ feeder.getReceiver().getPhoneOne() }}</p>
                                    </div>
                                </div>


                                {#<div class=\"form-group\">#}
                                    {#<label class=\"col-sm-4 control-label\">Depositor's Name</label>#}

                                    {#<div class=\"col-sm-8\">#}
                                        {#<p class=\"form-control-static\">{{ feeder.getPaymentDepositorName() }}</p>#}
                                    {#</div>#}
                                {#</div>#}

                                {#<div class=\"form-group\">#}
                                    {#<label class=\"col-sm-4 control-label\">Payment Location</label>#}

                                    {#<div class=\"col-sm-8\">#}
                                        {#<p class=\"form-control-static\">{{ feeder.getPaymentLocation() }}</p>#}
                                    {#</div>#}
                                {#</div>#}

                                <div class=\"form-group\">
                                    <label class=\"col-sm-4 control-label\">Payment Proof</label>

                                    <div class=\"col-sm-8\">
                                        <p class=\"form-control-static\"><img class=\"img-responsive\"
                                                                            src=\"{{ asset(feeder.getPaymentUploadedFile()) }}\"
                                                                            style=\"margin: 0; border: none;\"></p>
                                        <p class=\"form-control-static\">
                                            <button type=\"button\" class=\"btn btn-xs btn-warning viewPicModal\"
                                                    data-toggle=\"modal\" data-target=\"#viewPicModal\">View Image
                                            </button>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                {% endif %}
            </div> <!-- End Receiver's Column -->
        </div>
    </section> <!-- End Main Content -->
{% endblock %}

{% block modalPicHeader %}Payment Proof for {{ feeder.getPack().getName() }} ({{ app_currency_symbol }}{{ feeder.getPack().getFormattedAmount() }}){% endblock %}
{% block modalPicBody %}<img class=\"img-responsive\" src=\"{{ asset(feeder.getPaymentUploadedFile()) }}\">{% endblock %}

{% block modalCancelHeader %}Cancel Payment{% endblock %}
{% block modalCancelBody %}Are you sure you want to cancel this payment? If you do not have another package subscription where your payment has been confirmed and activated, you will be blocked from this system{% endblock %}
{% block modalCancelLink %}
    <a href=\"{{ path('cancel_payment', { 'feeder': feeder.getId() }) }}\" class=\"btn btn-outline pull-right\">Proceed</a>
{% endblock %}
", "member/pack-payment.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\member\\pack-payment.html.twig");
    }
}
