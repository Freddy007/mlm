<?php

/* member/reviews.html.twig */
class __TwigTemplate_04e94a2ef2ea2ea6211e3fd8fbef6e51466f30e238c079439d3279c092bc1358 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/reviews.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e77d2b745d2734866bc19419abb431e3d93383f30ab70e8d4ebb80779f15ede2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e77d2b745d2734866bc19419abb431e3d93383f30ab70e8d4ebb80779f15ede2->enter($__internal_e77d2b745d2734866bc19419abb431e3d93383f30ab70e8d4ebb80779f15ede2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/reviews.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e77d2b745d2734866bc19419abb431e3d93383f30ab70e8d4ebb80779f15ede2->leave($__internal_e77d2b745d2734866bc19419abb431e3d93383f30ab70e8d4ebb80779f15ede2_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_214e2cee8a481c3653fefb9008df2545f0c869a80fcf56b2d0db43cc310a1e67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_214e2cee8a481c3653fefb9008df2545f0c869a80fcf56b2d0db43cc310a1e67->enter($__internal_214e2cee8a481c3653fefb9008df2545f0c869a80fcf56b2d0db43cc310a1e67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "My Testimonies";
        
        $__internal_214e2cee8a481c3653fefb9008df2545f0c869a80fcf56b2d0db43cc310a1e67->leave($__internal_214e2cee8a481c3653fefb9008df2545f0c869a80fcf56b2d0db43cc310a1e67_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_36ecf5eb2a63adf204b04e4cb7ceb09e17445fb931f03aa5502dfa7229ffa3e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36ecf5eb2a63adf204b04e4cb7ceb09e17445fb931f03aa5502dfa7229ffa3e6->enter($__internal_36ecf5eb2a63adf204b04e4cb7ceb09e17445fb931f03aa5502dfa7229ffa3e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            My Testimonies
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-comments-o\"></i> My Testimonies</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">

                <!-- small box -->
                <div class=\"small-box bg-blue\">
                    <div class=\"inner\">
                        <h3>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()), "html", null, true);
        echo "</h3>
                        <p>Testimonies</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"fa fa-comments-o\"></i>
                    </div>
                    <a class=\"small-box-footer\">My Testimonies <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
          \t\t\t<a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_review");
        echo "\" class=\"list-group-item\">Create New Testimony</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reviews");
        echo "\" class=\"list-group-item list-group-item-success\">View Testimonies</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
                
                ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "testimonySuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 46
            echo "                    <div class=\"alert alert-success alert-dismissible\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>";
            // line 48
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "
    \t\t\t";
        // line 52
        if (($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()) == 0)) {
            // line 53
            echo "    \t\t\t\t<div class=\"alert alert-warning\">
    \t\t\t\t\t<p class=\"lead text-center\">You have not created any testimony yet</p>
    \t\t\t\t\t<hr>
    \t\t\t\t\t<a href=\"";
            // line 56
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_review");
            echo "\" class=\"btn btn-info btn-block\" style=\"text-decoration: none\">Create Testimony</a>
    \t\t\t\t</div>
    \t\t\t";
        } else {
            // line 59
            echo "\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing ";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()), "html", null, true);
            echo " Testimon";
            if (($this->getAttribute((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "getTotalItemCount", array()) != 1)) {
                echo "ies";
            } else {
                echo "y";
            }
            echo "</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
                            <div class=\"page-content-body\">
                                <div class=\"row\">
                                    ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")));
            foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                // line 68
                echo "                                        <!-- Testimonials Cell -->
                                        <div class=\"col-sm-12 clearfix media-section testimonials-cell\">
                                            <img src=\"";
                // line 70
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($context["review"], "getUser", array(), "method"), "getAvatar", array(), "method")), "html", null, true);
                echo "\" class=\"pull-left\" style=\"max-width:100px;height:auto\">
                                            <div class=\"push-right\">
                                                <h3 class=\"pink\">";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["review"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</h3>
                                                <p class=\"catamaran\"><small>";
                // line 73
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["review"], "getCreatedDate", array(), "method"), "d M, Y"), "html", null, true);
                echo " |
                                                        ";
                // line 75
                echo "                                                    </small></p>
                                                <p class=\"catamaran\">";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "getTestimony", array()), "html", null, true);
                echo "</p>
                                            </div>
                                        </div> <!-- End Testimonials Cell -->
                                        <hr>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "                                </div>
                            </div>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    ";
            // line 87
            echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")));
            echo "
\t                </div>
    \t\t\t";
        }
        // line 90
        echo "    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section> <!-- End Main Content -->

";
        
        $__internal_36ecf5eb2a63adf204b04e4cb7ceb09e17445fb931f03aa5502dfa7229ffa3e6->leave($__internal_36ecf5eb2a63adf204b04e4cb7ceb09e17445fb931f03aa5502dfa7229ffa3e6_prof);

    }

    public function getTemplateName()
    {
        return "member/reviews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 90,  205 => 87,  197 => 81,  186 => 76,  183 => 75,  179 => 73,  175 => 72,  170 => 70,  166 => 68,  162 => 67,  143 => 61,  139 => 59,  133 => 56,  128 => 53,  126 => 52,  123 => 51,  114 => 48,  110 => 46,  106 => 45,  96 => 38,  92 => 37,  78 => 26,  61 => 12,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}My Testimonies{% endblock %}

{% block body %}
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            My Testimonies
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-comments-o\"></i> My Testimonies</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">

                <!-- small box -->
                <div class=\"small-box bg-blue\">
                    <div class=\"inner\">
                        <h3>{{ reviews.getTotalItemCount }}</h3>
                        <p>Testimonies</p>
                    </div>
                    <div class=\"icon\">
                        <i class=\"fa fa-comments-o\"></i>
                    </div>
                    <a class=\"small-box-footer\">My Testimonies <i class=\"fa fa-arrow-circle-right\"></i></a>
                </div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
          \t\t\t<a href=\"{{ path('create_review') }}\" class=\"list-group-item\">Create New Testimony</a>
                    <a href=\"{{ path('reviews') }}\" class=\"list-group-item list-group-item-success\">View Testimonies</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
                
                {% for flash_message in app.session.flashBag.get('testimonySuccess') %}
                    <div class=\"alert alert-success alert-dismissible\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        <p>{{ flash_message }}</p>
                    </div>
                {% endfor %}

    \t\t\t{% if reviews.getTotalItemCount == 0 %}
    \t\t\t\t<div class=\"alert alert-warning\">
    \t\t\t\t\t<p class=\"lead text-center\">You have not created any testimony yet</p>
    \t\t\t\t\t<hr>
    \t\t\t\t\t<a href=\"{{ path('create_review') }}\" class=\"btn btn-info btn-block\" style=\"text-decoration: none\">Create Testimony</a>
    \t\t\t\t</div>
    \t\t\t{% else %}
\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing {{ reviews.getPaginationData.firstItemNumber }} to {{ reviews.getPaginationData.lastItemNumber }} out of {{ reviews.getTotalItemCount }} Testimon{% if reviews.getTotalItemCount != 1 %}ies{% else %}y{% endif %}</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
                            <div class=\"page-content-body\">
                                <div class=\"row\">
                                    {% for review in reviews %}
                                        <!-- Testimonials Cell -->
                                        <div class=\"col-sm-12 clearfix media-section testimonials-cell\">
                                            <img src=\"{{ asset(review.getUser().getAvatar()) }}\" class=\"pull-left\" style=\"max-width:100px;height:auto\">
                                            <div class=\"push-right\">
                                                <h3 class=\"pink\">{{ review.getUser().getName() }}</h3>
                                                <p class=\"catamaran\"><small>{{ review.getCreatedDate()|date('d M, Y') }} |
                                                        {#{{ review.getUser().getResidentState() }}#}
                                                    </small></p>
                                                <p class=\"catamaran\">{{ review.getTestimony }}</p>
                                            </div>
                                        </div> <!-- End Testimonials Cell -->
                                        <hr>
                                    {% endfor %}
                                </div>
                            </div>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    {{ knp_pagination_render(reviews) }}
\t                </div>
    \t\t\t{% endif %}
    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section> <!-- End Main Content -->

{% endblock %}", "member/reviews.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\member\\reviews.html.twig");
    }
}
