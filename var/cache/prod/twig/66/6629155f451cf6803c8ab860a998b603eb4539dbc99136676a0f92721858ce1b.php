<?php

/* public/register-packs.html.twig */
class __TwigTemplate_81b9208f26ce095d8c9c5a40e48a5317d60f8dde7032ab66c8ac987c101d6927 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("public.base.html.twig", "public/register-packs.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "public.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7922f3aefc5db91f8634c60870d877ac118cba26014dd4d26d16f504c79b0b47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7922f3aefc5db91f8634c60870d877ac118cba26014dd4d26d16f504c79b0b47->enter($__internal_7922f3aefc5db91f8634c60870d877ac118cba26014dd4d26d16f504c79b0b47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/register-packs.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7922f3aefc5db91f8634c60870d877ac118cba26014dd4d26d16f504c79b0b47->leave($__internal_7922f3aefc5db91f8634c60870d877ac118cba26014dd4d26d16f504c79b0b47_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_033ba74c3bf0527e2c09204c9c365008e1dcc8055d37bdfb0734603fcd8c7d57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_033ba74c3bf0527e2c09204c9c365008e1dcc8055d37bdfb0734603fcd8c7d57->enter($__internal_033ba74c3bf0527e2c09204c9c365008e1dcc8055d37bdfb0734603fcd8c7d57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Register Package";
        
        $__internal_033ba74c3bf0527e2c09204c9c365008e1dcc8055d37bdfb0734603fcd8c7d57->leave($__internal_033ba74c3bf0527e2c09204c9c365008e1dcc8055d37bdfb0734603fcd8c7d57_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_391fb60c5ff3f7f63680350fc17b0a85f23459e592e5853308039c8838cab6ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_391fb60c5ff3f7f63680350fc17b0a85f23459e592e5853308039c8838cab6ec->enter($__internal_391fb60c5ff3f7f63680350fc17b0a85f23459e592e5853308039c8838cab6ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <section>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">

                    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "packError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 13
            echo "                        <div class=\"alert alert-danger\">
                            <p>";
            // line 14
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "
                    ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "userReferrerError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 19
            echo "                        <div class=\"alert alert-danger\">
                            <p>";
            // line 20
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
                    ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "userReferrer"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 25
            echo "                        <div class=\"alert alert-success\">
                            <p>";
            // line 26
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "
                    <h2>Select a Package to Start With</h2>
                    <hr class=\"star-primary\">
                </div>
            </div>
            <div class=\"row\">
                ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packs"]) ? $context["packs"] : $this->getContext($context, "packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 36
            echo "
                    <div class=\"col-sm-3 col-md-3\">
                        <div class=\"plan-cell\">
                            <ul class=\"list-group green\">
                                <li class=\"list-group-item plan-name\">";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getName", array(), "method"), "html", null, true);
            echo "</li>
                                <li class=\"list-group-item plan-price catamaran\">";
            // line 41
            echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getFormattedAmount", array(), "method"), "html", null, true);
            echo "</li>
                                <li class=\"list-group-item catamaran\"><i class=\"ion-ios-loop\"></i> Auto Matching</li>
                                <li class=\"list-group-item catamaran\"><i class=\"ion-arrow-up-c\"></i> ";
            // line 43
            echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getROI", array(), "method"), "html", null, true);
            echo " Return on Investment</li>
                                <li class=\"list-group-item catamaran\" style=\"padding:0\"><a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register", array("pack" => $this->getAttribute($context["pack"], "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-warning btn-block\">Sign Up</a></li>
                            </ul>
                        </div>
                    </div>

                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "
            </div>
        </div>
    </section>

";
        
        $__internal_391fb60c5ff3f7f63680350fc17b0a85f23459e592e5853308039c8838cab6ec->leave($__internal_391fb60c5ff3f7f63680350fc17b0a85f23459e592e5853308039c8838cab6ec_prof);

    }

    public function getTemplateName()
    {
        return "public/register-packs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 50,  148 => 44,  143 => 43,  137 => 41,  133 => 40,  127 => 36,  123 => 35,  115 => 29,  106 => 26,  103 => 25,  99 => 24,  96 => 23,  87 => 20,  84 => 19,  80 => 18,  77 => 17,  68 => 14,  65 => 13,  61 => 12,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'public.base.html.twig' %}

{% block title %}Register Package{% endblock %}

{% block body %}

    <section>
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">

                    {% for flash_message in app.session.flashBag.get('packError') %}
                        <div class=\"alert alert-danger\">
                            <p>{{ flash_message }}</p>
                        </div>
                    {% endfor %}

                    {% for flash_message in app.session.flashBag.get('userReferrerError') %}
                        <div class=\"alert alert-danger\">
                            <p>{{ flash_message }}</p>
                        </div>
                    {% endfor %}

                    {% for flash_message in app.session.flashBag.get('userReferrer') %}
                        <div class=\"alert alert-success\">
                            <p>{{ flash_message }}</p>
                        </div>
                    {% endfor %}

                    <h2>Select a Package to Start With</h2>
                    <hr class=\"star-primary\">
                </div>
            </div>
            <div class=\"row\">
                {% for pack in packs %}

                    <div class=\"col-sm-3 col-md-3\">
                        <div class=\"plan-cell\">
                            <ul class=\"list-group green\">
                                <li class=\"list-group-item plan-name\">{{ pack.getName() }}</li>
                                <li class=\"list-group-item plan-price catamaran\">{{ app_currency_symbol }}{{ pack.getFormattedAmount() }}</li>
                                <li class=\"list-group-item catamaran\"><i class=\"ion-ios-loop\"></i> Auto Matching</li>
                                <li class=\"list-group-item catamaran\"><i class=\"ion-arrow-up-c\"></i> {{ app_currency_symbol }}{{ pack.getROI() }} Return on Investment</li>
                                <li class=\"list-group-item catamaran\" style=\"padding:0\"><a href=\"{{ path('register', {'pack' : pack.getId() }) }}\" class=\"btn btn-warning btn-block\">Sign Up</a></li>
                            </ul>
                        </div>
                    </div>

                {% endfor %}

            </div>
        </div>
    </section>

{% endblock %}", "public/register-packs.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public\\register-packs.html.twig");
    }
}
