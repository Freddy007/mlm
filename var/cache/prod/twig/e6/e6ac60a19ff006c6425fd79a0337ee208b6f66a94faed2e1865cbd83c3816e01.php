<?php

/* public/forgot-password.html.twig */
class __TwigTemplate_47c8fbd761b9ed7b09133eb8243091e06633783269a5337645845353613313ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("public.base.html.twig", "public/forgot-password.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "public.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56f6b7412e3b62ac702c1cb9176feb370dba33fa2833547fcb1b91e730cc86a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56f6b7412e3b62ac702c1cb9176feb370dba33fa2833547fcb1b91e730cc86a4->enter($__internal_56f6b7412e3b62ac702c1cb9176feb370dba33fa2833547fcb1b91e730cc86a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/forgot-password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_56f6b7412e3b62ac702c1cb9176feb370dba33fa2833547fcb1b91e730cc86a4->leave($__internal_56f6b7412e3b62ac702c1cb9176feb370dba33fa2833547fcb1b91e730cc86a4_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e5db665507e6a5d16850be77923636feac0a771d1880a75e12458439ae75deba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5db665507e6a5d16850be77923636feac0a771d1880a75e12458439ae75deba->enter($__internal_e5db665507e6a5d16850be77923636feac0a771d1880a75e12458439ae75deba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Forgot Password";
        
        $__internal_e5db665507e6a5d16850be77923636feac0a771d1880a75e12458439ae75deba->leave($__internal_e5db665507e6a5d16850be77923636feac0a771d1880a75e12458439ae75deba_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_88a2270ff4d72abcccc5bc935a9eda731d32563301e4ea01e7a74e37572df8a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88a2270ff4d72abcccc5bc935a9eda731d32563301e4ea01e7a74e37572df8a3->enter($__internal_88a2270ff4d72abcccc5bc935a9eda731d32563301e4ea01e7a74e37572df8a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Forgot Password</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">
                <div class=\"login-box\" style=\"margin: 0 auto !important\">
                    <!-- /.login-logo -->
                    <div class=\"login-box-body\">

                        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "forgotError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 19
            echo "                            <div class=\"alert alert-danger\">
                                <p>";
            // line 20
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
                        ";
        // line 24
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "forgotSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 25
            echo "                            <div class=\"alert alert-success\">
                                <p>";
            // line 26
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "        
                        ";
        // line 30
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("forgot_password"), "attr" => array("class" => "form-horizontal")));
        echo "
                                
                            <span class=\"text-danger\">";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'errors');
        echo "</span>

                            <div class=\"form-group\">
                                ";
        // line 35
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'label', array("label_attr" => array("class" => "col-sm-3 control-label"), "label" => "Email"));
        echo "

                                <div class=\"col-sm-9\">
                                    ";
        // line 38
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'widget');
        echo "
                                </div>
                            </div>

                            <div class=\"row\">
                                <div class=\"col-sm-12\">
                                    <button type=\"submit\" class=\"btn btn-danger btn-block btn-flat\">Reset Password</button>
                                </div>
                            </div>
                    
                        ";
        // line 48
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

                    </div>
                </div>
            </div>
        </div>
    </section>

";
        
        $__internal_88a2270ff4d72abcccc5bc935a9eda731d32563301e4ea01e7a74e37572df8a3->leave($__internal_88a2270ff4d72abcccc5bc935a9eda731d32563301e4ea01e7a74e37572df8a3_prof);

    }

    public function getTemplateName()
    {
        return "public/forgot-password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 48,  122 => 38,  116 => 35,  110 => 32,  105 => 30,  102 => 29,  93 => 26,  90 => 25,  86 => 24,  83 => 23,  74 => 20,  71 => 19,  67 => 18,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'public.base.html.twig' %}

{% block title %}Forgot Password{% endblock %}

{% block body %}

    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Forgot Password</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">
                <div class=\"login-box\" style=\"margin: 0 auto !important\">
                    <!-- /.login-logo -->
                    <div class=\"login-box-body\">

                        {% for flash_message in app.session.flashBag.get('forgotError') %}
                            <div class=\"alert alert-danger\">
                                <p>{{ flash_message }}</p>
                            </div>
                        {% endfor %}

                        {% for flash_message in app.session.flashBag.get('forgotSuccess') %}
                            <div class=\"alert alert-success\">
                                <p>{{ flash_message }}</p>
                            </div>
                        {% endfor %}
        
                        {{ form_start(form, {'action': path('forgot_password'), 'attr': {'class': 'form-horizontal'}}) }}
                                
                            <span class=\"text-danger\">{{ form_errors(form.email) }}</span>

                            <div class=\"form-group\">
                                {{ form_label(form.email, 'Email', { 'label_attr': {'class': 'col-sm-3 control-label'} }) }}

                                <div class=\"col-sm-9\">
                                    {{ form_widget(form.email) }}
                                </div>
                            </div>

                            <div class=\"row\">
                                <div class=\"col-sm-12\">
                                    <button type=\"submit\" class=\"btn btn-danger btn-block btn-flat\">Reset Password</button>
                                </div>
                            </div>
                    
                        {{ form_end(form) }}

                    </div>
                </div>
            </div>
        </div>
    </section>

{% endblock %}", "public/forgot-password.html.twig", "/var/www/html/mlm/app/Resources/views/public/forgot-password.html.twig");
    }
}
