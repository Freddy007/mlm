<?php

/* member/referrals.html.twig */
class __TwigTemplate_426090e8c7fff199226d61b01fd44c4dfe41e979949db022492fd5851115865e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/referrals.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f585f42a52032d9651492128d74fc3bf9a134538cb170ac3b7bedd7e0db95b2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f585f42a52032d9651492128d74fc3bf9a134538cb170ac3b7bedd7e0db95b2f->enter($__internal_f585f42a52032d9651492128d74fc3bf9a134538cb170ac3b7bedd7e0db95b2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/referrals.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f585f42a52032d9651492128d74fc3bf9a134538cb170ac3b7bedd7e0db95b2f->leave($__internal_f585f42a52032d9651492128d74fc3bf9a134538cb170ac3b7bedd7e0db95b2f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_5e2298d3ddbaf78b9fa8417f1789969174d53036abef3ffe3f0255bfb54c2f8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e2298d3ddbaf78b9fa8417f1789969174d53036abef3ffe3f0255bfb54c2f8c->enter($__internal_5e2298d3ddbaf78b9fa8417f1789969174d53036abef3ffe3f0255bfb54c2f8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Referrals";
        
        $__internal_5e2298d3ddbaf78b9fa8417f1789969174d53036abef3ffe3f0255bfb54c2f8c->leave($__internal_5e2298d3ddbaf78b9fa8417f1789969174d53036abef3ffe3f0255bfb54c2f8c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_0247ae5e8cc1785cec8aa593deff6bf2a3f84e6bcbdd0d04304f011c9f697e53 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0247ae5e8cc1785cec8aa593deff6bf2a3f84e6bcbdd0d04304f011c9f697e53->enter($__internal_0247ae5e8cc1785cec8aa593deff6bf2a3f84e6bcbdd0d04304f011c9f697e53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tYour Referrals
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-users\"></i> Referrals</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">
          \t\t<!-- small box -->
          \t\t<div class=\"small-box bg-aqua\">
            \t\t<div class=\"inner\">
              \t\t\t<h3>";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getReferralsCount", array(), "method"), "html", null, true);
        echo "</h3>
\t\t\t\t\t\t<p>Referral";
        // line 26
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getReferralsCount", array(), "method") != 1)) {
            echo "s";
        }
        echo "</p>
            \t\t</div>
            \t\t<div class=\"icon\">
              \t\t\t<i class=\"ion ion-ios-people\"></i>
            \t\t</div>
            \t\t<a class=\"small-box-footer\">Your Referrals <i class=\"fa fa-arrow-circle-right\"></i></a>
          \t\t</div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
          \t\t\t<a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create-referral");
        echo "\" class=\"list-group-item\"><i class=\"fa fa-plus\"></i> Add Referral</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">

                <!-- Referral Link -->
                <div class=\"box box-info\">

                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Your Referral Link</h3>
                    </div>

                    <div class=\"box-body\">
                        <p class=\"text-muted\">";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "siteurl", array()), "html", null, true);
        echo "/ref/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getUsername", array(), "method"), "html", null, true);
        echo "</p>
                    </div>
                </div> <!-- End Referral Link -->
                
    \t\t\t";
        // line 55
        if (($this->getAttribute((isset($context["referrals"]) ? $context["referrals"] : $this->getContext($context, "referrals")), "getTotalItemCount", array()) == 0)) {
            // line 56
            echo "    \t\t\t\t<div class=\"alert alert-warning\">
    \t\t\t\t\t<p class=\"lead text-center\">Sorry, you do not have any referrals yet</p>
    \t\t\t\t</div>
    \t\t\t";
        } else {
            // line 60
            echo "\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["referrals"]) ? $context["referrals"] : $this->getContext($context, "referrals")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["referrals"]) ? $context["referrals"] : $this->getContext($context, "referrals")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["referrals"]) ? $context["referrals"] : $this->getContext($context, "referrals")), "getTotalItemCount", array()), "html", null, true);
            echo " Referral";
            if (($this->getAttribute((isset($context["referrals"]) ? $context["referrals"] : $this->getContext($context, "referrals")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            echo "</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>Name</th>
\t    \t\t\t\t\t\t\t\t<th>Username</th>
\t    \t\t\t\t\t\t\t\t<th>Email</th>
\t    \t\t\t\t\t\t\t\t<th>Date Joined</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t";
            // line 77
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["referrals"]) ? $context["referrals"] : $this->getContext($context, "referrals")));
            foreach ($context['_seq'] as $context["_key"] => $context["referral"]) {
                // line 78
                echo "\t    \t\t\t\t\t\t\t\t<tr class=\"";
                if (($this->getAttribute($context["referral"], "getStatus", array(), "method") == "active")) {
                    echo "text-success";
                } else {
                    echo "text-danger";
                }
                echo "\">
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 79
                echo twig_escape_filter($this->env, $this->getAttribute($context["referral"], "getName", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 80
                echo twig_escape_filter($this->env, $this->getAttribute($context["referral"], "getUsername", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 81
                echo twig_escape_filter($this->env, $this->getAttribute($context["referral"], "getEmail", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 82
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["referral"], "getCreated", array(), "method"), "d F, Y"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['referral'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo "\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    ";
            // line 91
            echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["referrals"]) ? $context["referrals"] : $this->getContext($context, "referrals")));
            echo "
\t                </div>
    \t\t\t";
        }
        // line 94
        echo "    \t\t</div> <!-- End Main Content -->
    \t</div>
    </section> <!-- End Main Content -->
";
        
        $__internal_0247ae5e8cc1785cec8aa593deff6bf2a3f84e6bcbdd0d04304f011c9f697e53->leave($__internal_0247ae5e8cc1785cec8aa593deff6bf2a3f84e6bcbdd0d04304f011c9f697e53_prof);

    }

    public function getTemplateName()
    {
        return "member/referrals.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 94,  203 => 91,  195 => 85,  186 => 82,  182 => 81,  178 => 80,  174 => 79,  165 => 78,  161 => 77,  135 => 62,  131 => 60,  125 => 56,  123 => 55,  114 => 51,  96 => 36,  81 => 26,  77 => 25,  61 => 12,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Referrals{% endblock %}

{% block body %}
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tYour Referrals
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-users\"></i> Referrals</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">
          \t\t<!-- small box -->
          \t\t<div class=\"small-box bg-aqua\">
            \t\t<div class=\"inner\">
              \t\t\t<h3>{{ app.user.getReferralsCount() }}</h3>
\t\t\t\t\t\t<p>Referral{% if app.user.getReferralsCount() != 1 %}s{% endif %}</p>
            \t\t</div>
            \t\t<div class=\"icon\">
              \t\t\t<i class=\"ion ion-ios-people\"></i>
            \t\t</div>
            \t\t<a class=\"small-box-footer\">Your Referrals <i class=\"fa fa-arrow-circle-right\"></i></a>
          \t\t</div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
          \t\t\t<a href=\"{{ path('create-referral') }}\" class=\"list-group-item\"><i class=\"fa fa-plus\"></i> Add Referral</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">

                <!-- Referral Link -->
                <div class=\"box box-info\">

                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Your Referral Link</h3>
                    </div>

                    <div class=\"box-body\">
                        <p class=\"text-muted\">{{ adm.siteurl }}/ref/{{ app.user.getUsername() }}</p>
                    </div>
                </div> <!-- End Referral Link -->
                
    \t\t\t{% if referrals.getTotalItemCount == 0 %}
    \t\t\t\t<div class=\"alert alert-warning\">
    \t\t\t\t\t<p class=\"lead text-center\">Sorry, you do not have any referrals yet</p>
    \t\t\t\t</div>
    \t\t\t{% else %}
\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing {{ referrals.getPaginationData.firstItemNumber }} to {{ referrals.getPaginationData.lastItemNumber }} out of {{ referrals.getTotalItemCount }} Referral{% if referrals.getTotalItemCount != 1 %}s{% endif %}</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>Name</th>
\t    \t\t\t\t\t\t\t\t<th>Username</th>
\t    \t\t\t\t\t\t\t\t<th>Email</th>
\t    \t\t\t\t\t\t\t\t<th>Date Joined</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t{% for referral in referrals %}
\t    \t\t\t\t\t\t\t\t<tr class=\"{% if referral.getStatus() == 'active' %}text-success{% else %}text-danger{% endif %}\">
\t    \t\t\t\t\t\t\t\t\t<td>{{ referral.getName() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ referral.getUsername() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ referral.getEmail() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ referral.getCreated()|date('d F, Y') }}</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t{% endfor %}
\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    {{ knp_pagination_render(referrals) }}
\t                </div>
    \t\t\t{% endif %}
    \t\t</div> <!-- End Main Content -->
    \t</div>
    </section> <!-- End Main Content -->
{% endblock %}", "member/referrals.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member\\referrals.html.twig");
    }
}
