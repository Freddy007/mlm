<?php

/* member/purse.html.twig */
class __TwigTemplate_4e665fb2d9ef015beea947de0655df8a765e6af1d26165ea97360cec9257fa05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/purse.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b039329ca224fee398ee0396a75d5c4b800d2084455d04f06baca6dd3f8dc5a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b039329ca224fee398ee0396a75d5c4b800d2084455d04f06baca6dd3f8dc5a0->enter($__internal_b039329ca224fee398ee0396a75d5c4b800d2084455d04f06baca6dd3f8dc5a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/purse.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b039329ca224fee398ee0396a75d5c4b800d2084455d04f06baca6dd3f8dc5a0->leave($__internal_b039329ca224fee398ee0396a75d5c4b800d2084455d04f06baca6dd3f8dc5a0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e855a4b30d9561a5e564c3cb71c069d803c289959dc7e0553df040ccdf4eff31 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e855a4b30d9561a5e564c3cb71c069d803c289959dc7e0553df040ccdf4eff31->enter($__internal_e855a4b30d9561a5e564c3cb71c069d803c289959dc7e0553df040ccdf4eff31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Wallet";
        
        $__internal_e855a4b30d9561a5e564c3cb71c069d803c289959dc7e0553df040ccdf4eff31->leave($__internal_e855a4b30d9561a5e564c3cb71c069d803c289959dc7e0553df040ccdf4eff31_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_7df684acea6ed0ab56965256211183c4f44a303e742e6fc42d7a2d1074e1b41a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7df684acea6ed0ab56965256211183c4f44a303e742e6fc42d7a2d1074e1b41a->enter($__internal_7df684acea6ed0ab56965256211183c4f44a303e742e6fc42d7a2d1074e1b41a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tYour Wallet
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-money\"></i> Wallet</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">
          \t\t<!-- small box -->
          \t\t<div class=\"small-box bg-green\">
            \t\t<div class=\"inner\">
              \t\t\t<h3>";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getFormattedPurse", array(), "method"), "html", null, true);
        echo "</h3>
              \t\t\t<p>Wallet</p>
            \t\t</div>
            \t\t<div class=\"icon\">
              \t\t\t<i class=\"ion ion-cash\"></i>
            \t\t</div>
            \t\t<a class=\"small-box-footer\">Wallet Balance <i class=\"fa fa-arrow-circle-right\"></i></a>
          \t\t</div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
\t\t\t\t\t";
        // line 36
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "getPurse", array()) != null)) {
            // line 37
            echo "\t\t\t\t\t\t<a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("withdraw_purse");
            echo "\" class=\"list-group-item\">Withdraw Wallet</a>
\t\t\t\t\t";
        } else {
            // line 39
            echo "\t\t\t\t\t\t<a href=\"#\" class=\"list-group-item\">Empty Wallet</a>
\t\t\t\t\t";
        }
        // line 41
        echo "          \t\t\t<a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("transfer_purse");
        echo "\" class=\"list-group-item\">Transfer To User</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
    \t\t\t";
        // line 47
        if (($this->getAttribute((isset($context["transactions"]) ? $context["transactions"] : $this->getContext($context, "transactions")), "getTotalItemCount", array()) == 0)) {
            // line 48
            echo "    \t\t\t\t<div class=\"alert alert-warning\">
    \t\t\t\t\t<p class=\"lead text-center\">Sorry, either you do not have a referral yet or your referrals have not subscribed for any pack yet</p>
    \t\t\t\t\t<hr>
    \t\t\t\t\t<a href=\"";
            // line 51
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create-referral");
            echo "\" class=\"btn btn-info btn-block\" style=\"text-decoration: none\">Add Referral</a>
    \t\t\t\t</div>
    \t\t\t";
        } else {
            // line 54
            echo "\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing ";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["transactions"]) ? $context["transactions"] : $this->getContext($context, "transactions")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["transactions"]) ? $context["transactions"] : $this->getContext($context, "transactions")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["transactions"]) ? $context["transactions"] : $this->getContext($context, "transactions")), "getTotalItemCount", array()), "html", null, true);
            echo " Transaction";
            if (($this->getAttribute((isset($context["transactions"]) ? $context["transactions"] : $this->getContext($context, "transactions")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            echo "</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>Username</th>
\t    \t\t\t\t\t\t\t\t<th>Name</th>
\t    \t\t\t\t\t\t\t\t<th>Amount</th>
\t    \t\t\t\t\t\t\t\t<th>Your Share</th>
\t    \t\t\t\t\t\t\t\t<th>Status</th>
\t    \t\t\t\t\t\t\t\t<th>Date</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t";
            // line 73
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["transactions"]) ? $context["transactions"] : $this->getContext($context, "transactions")));
            foreach ($context['_seq'] as $context["_key"] => $context["transaction"]) {
                // line 74
                echo "\t    \t\t\t\t\t\t\t\t<tr class=\"";
                if (($this->getAttribute($context["transaction"], "getStatus", array(), "method") == "not_confirmed")) {
                    echo "text-danger";
                } else {
                    echo "text-success";
                }
                echo "\">
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 75
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["transaction"], "getFeeder", array(), "method"), "getUsername", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 76
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["transaction"], "getFeeder", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 77
                echo twig_escape_filter($this->env, $this->getAttribute($context["transaction"], "getFormattedAmount", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 78
                echo twig_escape_filter($this->env, $this->getAttribute($context["transaction"], "getFormattedShare", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 79
                echo twig_escape_filter($this->env, $this->getAttribute($context["transaction"], "getStatus", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 80
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["transaction"], "getCreated", array(), "method"), "d F, Y"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['transaction'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    ";
            // line 89
            echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["transactions"]) ? $context["transactions"] : $this->getContext($context, "transactions")));
            echo "
\t                </div>
    \t\t\t";
        }
        // line 92
        echo "    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section>
";
        
        $__internal_7df684acea6ed0ab56965256211183c4f44a303e742e6fc42d7a2d1074e1b41a->leave($__internal_7df684acea6ed0ab56965256211183c4f44a303e742e6fc42d7a2d1074e1b41a_prof);

    }

    public function getTemplateName()
    {
        return "member/purse.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 92,  209 => 89,  201 => 83,  192 => 80,  188 => 79,  184 => 78,  180 => 77,  176 => 76,  172 => 75,  163 => 74,  159 => 73,  131 => 56,  127 => 54,  121 => 51,  116 => 48,  114 => 47,  104 => 41,  100 => 39,  94 => 37,  92 => 36,  77 => 25,  61 => 12,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Wallet{% endblock %}

{% block body %}
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tYour Wallet
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-money\"></i> Wallet</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">
          \t\t<!-- small box -->
          \t\t<div class=\"small-box bg-green\">
            \t\t<div class=\"inner\">
              \t\t\t<h3>{{ app_currency_symbol }}{{ app.user.getFormattedPurse() }}</h3>
              \t\t\t<p>Wallet</p>
            \t\t</div>
            \t\t<div class=\"icon\">
              \t\t\t<i class=\"ion ion-cash\"></i>
            \t\t</div>
            \t\t<a class=\"small-box-footer\">Wallet Balance <i class=\"fa fa-arrow-circle-right\"></i></a>
          \t\t</div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
\t\t\t\t\t{% if app.user.getPurse != null %}
\t\t\t\t\t\t<a href=\"{{ path('withdraw_purse') }}\" class=\"list-group-item\">Withdraw Wallet</a>
\t\t\t\t\t{% else %}
\t\t\t\t\t\t<a href=\"#\" class=\"list-group-item\">Empty Wallet</a>
\t\t\t\t\t{% endif %}
          \t\t\t<a href=\"{{ path('transfer_purse') }}\" class=\"list-group-item\">Transfer To User</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
    \t\t\t{% if transactions.getTotalItemCount == 0 %}
    \t\t\t\t<div class=\"alert alert-warning\">
    \t\t\t\t\t<p class=\"lead text-center\">Sorry, either you do not have a referral yet or your referrals have not subscribed for any pack yet</p>
    \t\t\t\t\t<hr>
    \t\t\t\t\t<a href=\"{{ path('create-referral') }}\" class=\"btn btn-info btn-block\" style=\"text-decoration: none\">Add Referral</a>
    \t\t\t\t</div>
    \t\t\t{% else %}
\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing {{ transactions.getPaginationData.firstItemNumber }} to {{ transactions.getPaginationData.lastItemNumber }} out of {{ transactions.getTotalItemCount }} Transaction{% if transactions.getTotalItemCount != 1 %}s{% endif %}</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>Username</th>
\t    \t\t\t\t\t\t\t\t<th>Name</th>
\t    \t\t\t\t\t\t\t\t<th>Amount</th>
\t    \t\t\t\t\t\t\t\t<th>Your Share</th>
\t    \t\t\t\t\t\t\t\t<th>Status</th>
\t    \t\t\t\t\t\t\t\t<th>Date</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t{% for transaction in transactions %}
\t    \t\t\t\t\t\t\t\t<tr class=\"{% if transaction.getStatus() == 'not_confirmed' %}text-danger{% else %}text-success{% endif %}\">
\t    \t\t\t\t\t\t\t\t\t<td>{{ transaction.getFeeder().getUsername() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ transaction.getFeeder().getName() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ transaction.getFormattedAmount() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ transaction.getFormattedShare() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ transaction.getStatus() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ transaction.getCreated()|date('d F, Y') }}</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t{% endfor %}
\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    {{ knp_pagination_render(transactions) }}
\t                </div>
    \t\t\t{% endif %}
    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section>
{% endblock %}", "member/purse.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member\\purse.html.twig");
    }
}
