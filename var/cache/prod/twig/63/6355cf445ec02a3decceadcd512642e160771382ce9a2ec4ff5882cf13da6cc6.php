<?php

/* member/pack-feeders.html.twig */
class __TwigTemplate_1873359fe7333a21bd01230c23f2f4dd4be4bde78bd0ca2c6e5cd7b90e6770ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/pack-feeders.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5bc7a3cc139e89b5be272cca0d476d03457338241a604192d989132da42358f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5bc7a3cc139e89b5be272cca0d476d03457338241a604192d989132da42358f0->enter($__internal_5bc7a3cc139e89b5be272cca0d476d03457338241a604192d989132da42358f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/pack-feeders.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5bc7a3cc139e89b5be272cca0d476d03457338241a604192d989132da42358f0->leave($__internal_5bc7a3cc139e89b5be272cca0d476d03457338241a604192d989132da42358f0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_268afffd1bc0ae0c296408f82d084d5cb7b73a8665deae7a506a7ed01fabadb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_268afffd1bc0ae0c296408f82d084d5cb7b73a8665deae7a506a7ed01fabadb6->enter($__internal_268afffd1bc0ae0c296408f82d084d5cb7b73a8665deae7a506a7ed01fabadb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Your Feeders in the ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : $this->getContext($context, "subscription")), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
        echo " Pack";
        
        $__internal_268afffd1bc0ae0c296408f82d084d5cb7b73a8665deae7a506a7ed01fabadb6->leave($__internal_268afffd1bc0ae0c296408f82d084d5cb7b73a8665deae7a506a7ed01fabadb6_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_b90ded08314731ec4163367666888a9e44a651984cb34fe337e86a7c52045a3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b90ded08314731ec4163367666888a9e44a651984cb34fe337e86a7c52045a3c->enter($__internal_b90ded08314731ec4163367666888a9e44a651984cb34fe337e86a7c52045a3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Your Feeders in ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : $this->getContext($context, "subscription")), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
        echo " Pack</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li><a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("packs");
        echo "\"><i class=\"fa fa-pie-chart\"></i> Packages</a></li>
            <li class=\"active\">";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : $this->getContext($context, "subscription")), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
        echo " Feeders</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"row\">
      \t\t<div class=\"col-sm-12\">
      \t\t\t";
        // line 21
        if (((isset($context["feeders"]) ? $context["feeders"] : $this->getContext($context, "feeders")) == null)) {
            // line 22
            echo "                    ";
            // line 23
            echo "                    <div class=\"col-sm-12\">
                        <div class=\"box box-solid\">
                            <div class=\"box-header with-border text-center\">
                              <i class=\"fa fa-users\"></i>

                              <h3 class=\"box-title\">No Feeders Yet (";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : $this->getContext($context, "subscription")), "getPack", array(), "method"), "getName", array(), "method"), "html", null, true);
            echo " Package)</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class=\"box-body\">
                                <div class=\"alert alert-warning text-center\">
                                    <h4><i class=\"icon fa fa-warning\"></i> No feeder assigned to pay you yet</h4>
                                    <p>Your pack subscription has been activated. Kindly be patient as the system matches feeders to pay you on your maturity date(7 days).
                                        The System will match a total of ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : $this->getContext($context, "subscription")), "getFeederTotal", array(), "method"), "html", null, true);
            echo " feeders to pay you.</p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div> <!-- End Downlines Panel -->
\t\t\t\t";
        } else {
            // line 42
            echo "\t\t\t\t\t";
            // line 43
            echo "\t\t\t\t\t<div class=\"row text-center\">
                        <div class=\"col-sm-8 col-sm-offset-2\">
                            <div class=\"alert alert-info\">
                                <p>The System will match a total of ";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : $this->getContext($context, "subscription")), "getFeederTotal", array(), "method"), "html", null, true);
            echo " feeders to pay you. ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["subscription"]) ? $context["subscription"] : $this->getContext($context, "subscription")), "getFeederCounter", array(), "method"), "html", null, true);
            echo " remaining feeders for the system to match to you</p>
                            </div>
                        </div>
\t\t\t\t\t\t";
            // line 49
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["feeders"]) ? $context["feeders"] : $this->getContext($context, "feeders")));
            foreach ($context['_seq'] as $context["_key"] => $context["feeder"]) {
                // line 50
                echo "                            <div class=\"col-sm-6\">
                                <div class=\"box box-widget widget-user\">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class=\"widget-user-header bg-aqua-active\">
                                        <h3 class=\"widget-user-username\">";
                // line 54
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</h3>
                                        <h5 class=\"widget-user-desc\">";
                // line 55
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getUsername", array(), "method"), "html", null, true);
                echo "</h5>
                                    </div>
                                    <div class=\"widget-user-image\">
                                        <img class=\"img-circle\" src=\"";
                // line 58
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getAvatar", array(), "method")), "html", null, true);
                echo "\" alt=\"User Avatar\">
                                    </div>
                                    <div class=\"box-footer\">
                                        <div class=\"row\">
                                            <div class=\"col-sm-6 border-right\">
                                                <div class=\"description-block\">
                                                    <h5 class=\"description-header\"><i class=\"fa fa-phone\"></i> ";
                // line 64
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getPhoneOne", array(), "method"), "html", null, true);
                echo "</h5>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            ";
                // line 68
                if (($this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getPhoneTwo", array(), "method") != null)) {
                    // line 69
                    echo "                                                <div class=\"col-sm-6 border-right\">
                                                    <div class=\"description-block\">
                                                        <h5 class=\"description-header\"><i class=\"fa fa-phone\"></i> ";
                    // line 71
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["feeder"], "getFeeder", array(), "method"), "getPhoneTwo", array(), "method"), "html", null, true);
                    echo "</h5>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                            ";
                }
                // line 76
                echo "                                        </div>
                                        <div class=\"row\">
                                            <div class=\"col-sm-12\">
                                                ";
                // line 80
                echo "                                                ";
                if (($this->getAttribute($context["feeder"], "getStatus", array(), "method") == "not_paid")) {
                    // line 81
                    echo "                                                    <p>This feeder is to make payment to you</p>
                                                    <p>Time left to complete payment</p>
                                                    <p id=\"countdownClock";
                    // line 83
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\">
                                                        <strong>Hrs: </strong>
                                                        <span class=\"hourCountDown";
                    // line 85
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                        <strong>| Mins: </strong>
                                                        <span class=\"minuteCountDown";
                    // line 87
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                        <strong>| Secs: </strong>
                                                        <span class=\"secondCountDown";
                    // line 89
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                    </p>
                                                    <script type=\"text/javascript\">
                                                        var deadlineGiver";
                    // line 92
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo " = \"";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["feeder"], "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                    echo "\";
                                                        initializeClock('countdownClock";
                    // line 93
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'hourCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'minuteCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'secondCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', deadlineGiver";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo ");
                                                    </script>
                                                ";
                } elseif ((($this->getAttribute(                // line 95
$context["feeder"], "getStatus", array(), "method") == "payment_made") || (($this->getAttribute($context["feeder"], "getScamPaid", array(), "method") == "scam_paid") && ($this->getAttribute($context["feeder"], "getStatus", array(), "method") == "scam")))) {
                    // line 96
                    echo "                                                    <p>This feeder has made payment to you</p>
                                                    <p>Time left for you to confirm payment</p>
                                                    <p id=\"countdownClock";
                    // line 98
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\">
                                                        <strong>Hrs: </strong>
                                                        <span class=\"hourCountDown";
                    // line 100
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                        <strong>| Mins: </strong>
                                                        <span class=\"minuteCountDown";
                    // line 102
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                        <strong>| Secs: </strong>
                                                        <span class=\"secondCountDown";
                    // line 104
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                    </p>
                                                    <script type=\"text/javascript\">
                                                        var deadlineGiver";
                    // line 107
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo " = \"";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["feeder"], "getReceiverTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                    echo "\";
                                                        initializeClock('countdownClock";
                    // line 108
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'hourCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'minuteCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'secondCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', deadlineGiver";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo ");
                                                    </script>
                                                ";
                } elseif (($this->getAttribute(                // line 110
$context["feeder"], "getStatus", array(), "method") == "jury")) {
                    // line 111
                    echo "                                                    <p>There seems to be a dispute on this payment. Hence, it has been moved to the Jury's</p>
                                                ";
                } elseif (($this->getAttribute(                // line 112
$context["feeder"], "getStatus", array(), "method") == "scam")) {
                    // line 113
                    echo "                                                    <p>You have marked this payment as a scam</p>
                                                    <p>Time left for feeder to properly complete payment before being blocked</p>
                                                    <p class=\"text-center\" id=\"countdownClock";
                    // line 115
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\">
                                                        <strong>Hours: </strong>
                                                        <span class=\"hourCountDown";
                    // line 117
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                        <strong>| Minutes: </strong>
                                                        <span class=\"minuteCountDown";
                    // line 119
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                        <strong>| Seconds: </strong>
                                                        <span class=\"secondCountDown";
                    // line 121
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "\"></span>
                                                    </p>
                                                    <script type=\"text/javascript\">
                                                        var deadlineGiver";
                    // line 124
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo " = \"";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["feeder"], "getFeederTimeEnd", array(), "method"), "Y-m-d H:i:s"), "html", null, true);
                    echo "\";
                                                        initializeClock('countdownClock";
                    // line 125
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'hourCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'minuteCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', 'secondCountDown";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo "', deadlineGiver";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["feeder"], "getId", array(), "method"), "html", null, true);
                    echo ");
                                                    </script>
                                                ";
                } elseif (($this->getAttribute(                // line 127
$context["feeder"], "getStatus", array(), "method") == "fully_paid")) {
                    // line 128
                    echo "                                                    <p>You have fully confirmed this payment</p>
                                                ";
                }
                // line 130
                echo "                                            </div>
                                        </div>
                                    </div>
                                    ";
                // line 133
                if (($this->getAttribute($context["feeder"], "getStatus", array(), "method") == "jury")) {
                    // line 134
                    echo "                                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_case", array("case" => $this->getAttribute($context["feeder"], "getJury", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-lg btn-block btn-warning\">Go to the Court</a>
                                    ";
                } elseif (($this->getAttribute(                // line 135
$context["feeder"], "getStatus", array(), "method") == "fully_paid")) {
                    // line 136
                    echo "                                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("check_payment", array("feeder" => $this->getAttribute($context["feeder"], "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-lg btn-block btn-success\">Payment Confirmed</a>
                                    ";
                } else {
                    // line 138
                    echo "                                        <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("check_payment", array("feeder" => $this->getAttribute($context["feeder"], "getId", array(), "method"))), "html", null, true);
                    echo "\" class=\"btn btn-lg btn-block btn-warning\">Check Payment</a>
                                    ";
                }
                // line 140
                echo "                                </div>
                            </div>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['feeder'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 143
            echo "\t\t\t\t\t</div>
\t\t\t\t";
        }
        // line 145
        echo "      \t\t</div>
      \t</div>
    </section>
";
        
        $__internal_b90ded08314731ec4163367666888a9e44a651984cb34fe337e86a7c52045a3c->leave($__internal_b90ded08314731ec4163367666888a9e44a651984cb34fe337e86a7c52045a3c_prof);

    }

    public function getTemplateName()
    {
        return "member/pack-feeders.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  373 => 145,  369 => 143,  361 => 140,  355 => 138,  349 => 136,  347 => 135,  342 => 134,  340 => 133,  335 => 130,  331 => 128,  329 => 127,  316 => 125,  310 => 124,  304 => 121,  299 => 119,  294 => 117,  289 => 115,  285 => 113,  283 => 112,  280 => 111,  278 => 110,  265 => 108,  259 => 107,  253 => 104,  248 => 102,  243 => 100,  238 => 98,  234 => 96,  232 => 95,  219 => 93,  213 => 92,  207 => 89,  202 => 87,  197 => 85,  192 => 83,  188 => 81,  185 => 80,  180 => 76,  172 => 71,  168 => 69,  166 => 68,  159 => 64,  150 => 58,  144 => 55,  140 => 54,  134 => 50,  130 => 49,  122 => 46,  117 => 43,  115 => 42,  105 => 35,  95 => 28,  88 => 23,  86 => 22,  84 => 21,  73 => 13,  69 => 12,  65 => 11,  60 => 9,  55 => 6,  49 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Your Feeders in the {{ subscription.getPack().getName() }} Pack{% endblock %}

{% block body %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>Your Feeders in {{ subscription.getPack().getName() }} Pack</h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
            <li><a href=\"{{ path('packs') }}\"><i class=\"fa fa-pie-chart\"></i> Packages</a></li>
            <li class=\"active\">{{ subscription.getPack().getName() }} Feeders</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"row\">
      \t\t<div class=\"col-sm-12\">
      \t\t\t{% if feeders == null %}
                    {# No feeder assigned yet #}
                    <div class=\"col-sm-12\">
                        <div class=\"box box-solid\">
                            <div class=\"box-header with-border text-center\">
                              <i class=\"fa fa-users\"></i>

                              <h3 class=\"box-title\">No Feeders Yet ({{ subscription.getPack().getName() }} Package)</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class=\"box-body\">
                                <div class=\"alert alert-warning text-center\">
                                    <h4><i class=\"icon fa fa-warning\"></i> No feeder assigned to pay you yet</h4>
                                    <p>Your pack subscription has been activated. Kindly be patient as the system matches feeders to pay you on your maturity date(7 days).
                                        The System will match a total of {{ subscription.getFeederTotal() }} feeders to pay you.</p>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div> <!-- End Downlines Panel -->
\t\t\t\t{% else %}
\t\t\t\t\t{# Feeders assigned #}
\t\t\t\t\t<div class=\"row text-center\">
                        <div class=\"col-sm-8 col-sm-offset-2\">
                            <div class=\"alert alert-info\">
                                <p>The System will match a total of {{ subscription.getFeederTotal() }} feeders to pay you. {{ subscription.getFeederCounter() }} remaining feeders for the system to match to you</p>
                            </div>
                        </div>
\t\t\t\t\t\t{% for feeder in feeders %}
                            <div class=\"col-sm-6\">
                                <div class=\"box box-widget widget-user\">
                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                    <div class=\"widget-user-header bg-aqua-active\">
                                        <h3 class=\"widget-user-username\">{{ feeder.getFeeder().getName() }}</h3>
                                        <h5 class=\"widget-user-desc\">{{ feeder.getFeeder().getUsername() }}</h5>
                                    </div>
                                    <div class=\"widget-user-image\">
                                        <img class=\"img-circle\" src=\"{{ asset(feeder.getFeeder().getAvatar()) }}\" alt=\"User Avatar\">
                                    </div>
                                    <div class=\"box-footer\">
                                        <div class=\"row\">
                                            <div class=\"col-sm-6 border-right\">
                                                <div class=\"description-block\">
                                                    <h5 class=\"description-header\"><i class=\"fa fa-phone\"></i> {{ feeder.getFeeder().getPhoneOne() }}</h5>
                                                </div>
                                                <!-- /.description-block -->
                                            </div>
                                            {% if feeder.getFeeder().getPhoneTwo() != null %}
                                                <div class=\"col-sm-6 border-right\">
                                                    <div class=\"description-block\">
                                                        <h5 class=\"description-header\"><i class=\"fa fa-phone\"></i> {{ feeder.getFeeder().getPhoneTwo() }}</h5>
                                                    </div>
                                                    <!-- /.description-block -->
                                                </div>
                                            {% endif %}
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"col-sm-12\">
                                                {# Check feeder entry conditions #}
                                                {% if feeder.getStatus() == 'not_paid' %}
                                                    <p>This feeder is to make payment to you</p>
                                                    <p>Time left to complete payment</p>
                                                    <p id=\"countdownClock{{ feeder.getId() }}\">
                                                        <strong>Hrs: </strong>
                                                        <span class=\"hourCountDown{{ feeder.getId() }}\"></span>
                                                        <strong>| Mins: </strong>
                                                        <span class=\"minuteCountDown{{ feeder.getId() }}\"></span>
                                                        <strong>| Secs: </strong>
                                                        <span class=\"secondCountDown{{ feeder.getId() }}\"></span>
                                                    </p>
                                                    <script type=\"text/javascript\">
                                                        var deadlineGiver{{ feeder.getId() }} = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                        initializeClock('countdownClock{{ feeder.getId() }}', 'hourCountDown{{ feeder.getId() }}', 'minuteCountDown{{ feeder.getId() }}', 'secondCountDown{{ feeder.getId() }}', deadlineGiver{{ feeder.getId() }});
                                                    </script>
                                                {% elseif feeder.getStatus() == 'payment_made' or (feeder.getScamPaid() == 'scam_paid' and feeder.getStatus() == 'scam') %}
                                                    <p>This feeder has made payment to you</p>
                                                    <p>Time left for you to confirm payment</p>
                                                    <p id=\"countdownClock{{ feeder.getId() }}\">
                                                        <strong>Hrs: </strong>
                                                        <span class=\"hourCountDown{{ feeder.getId() }}\"></span>
                                                        <strong>| Mins: </strong>
                                                        <span class=\"minuteCountDown{{ feeder.getId() }}\"></span>
                                                        <strong>| Secs: </strong>
                                                        <span class=\"secondCountDown{{ feeder.getId() }}\"></span>
                                                    </p>
                                                    <script type=\"text/javascript\">
                                                        var deadlineGiver{{ feeder.getId() }} = \"{{ feeder.getReceiverTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                        initializeClock('countdownClock{{ feeder.getId() }}', 'hourCountDown{{ feeder.getId() }}', 'minuteCountDown{{ feeder.getId() }}', 'secondCountDown{{ feeder.getId() }}', deadlineGiver{{ feeder.getId() }});
                                                    </script>
                                                {% elseif feeder.getStatus() == 'jury' %}
                                                    <p>There seems to be a dispute on this payment. Hence, it has been moved to the Jury's</p>
                                                {% elseif feeder.getStatus() == 'scam' %}
                                                    <p>You have marked this payment as a scam</p>
                                                    <p>Time left for feeder to properly complete payment before being blocked</p>
                                                    <p class=\"text-center\" id=\"countdownClock{{ feeder.getId() }}\">
                                                        <strong>Hours: </strong>
                                                        <span class=\"hourCountDown{{ feeder.getId() }}\"></span>
                                                        <strong>| Minutes: </strong>
                                                        <span class=\"minuteCountDown{{ feeder.getId() }}\"></span>
                                                        <strong>| Seconds: </strong>
                                                        <span class=\"secondCountDown{{ feeder.getId() }}\"></span>
                                                    </p>
                                                    <script type=\"text/javascript\">
                                                        var deadlineGiver{{ feeder.getId() }} = \"{{ feeder.getFeederTimeEnd()|date('Y-m-d H:i:s') }}\";
                                                        initializeClock('countdownClock{{ feeder.getId() }}', 'hourCountDown{{ feeder.getId() }}', 'minuteCountDown{{ feeder.getId() }}', 'secondCountDown{{ feeder.getId() }}', deadlineGiver{{ feeder.getId() }});
                                                    </script>
                                                {% elseif feeder.getStatus() == 'fully_paid' %}
                                                    <p>You have fully confirmed this payment</p>
                                                {% endif %}
                                            </div>
                                        </div>
                                    </div>
                                    {% if feeder.getStatus() == 'jury'%}
                                        <a href=\"{{ path('user_case', {'case' : feeder.getJury()}) }}\" class=\"btn btn-lg btn-block btn-warning\">Go to the Court</a>
                                    {% elseif feeder.getStatus() == 'fully_paid' %}
                                        <a href=\"{{ path('check_payment', {'feeder' : feeder.getId()}) }}\" class=\"btn btn-lg btn-block btn-success\">Payment Confirmed</a>
                                    {% else %}
                                        <a href=\"{{ path('check_payment', {'feeder' : feeder.getId()}) }}\" class=\"btn btn-lg btn-block btn-warning\">Check Payment</a>
                                    {% endif %}
                                </div>
                            </div>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</div>
\t\t\t\t{% endif %}
      \t\t</div>
      \t</div>
    </section>
{% endblock %}", "member/pack-feeders.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\member\\pack-feeders.html.twig");
    }
}
