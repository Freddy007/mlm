<?php

/* member/packs.html.twig */
class __TwigTemplate_7192338dd02ad1240906d3a0c74fa2b0778de2ca7326db22b1426b47a1ca1e14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/packs.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0023a64000dc60236d3c48da90ce0fc3bb72c838d893dcbbb3728531e2b4e6db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0023a64000dc60236d3c48da90ce0fc3bb72c838d893dcbbb3728531e2b4e6db->enter($__internal_0023a64000dc60236d3c48da90ce0fc3bb72c838d893dcbbb3728531e2b4e6db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/packs.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0023a64000dc60236d3c48da90ce0fc3bb72c838d893dcbbb3728531e2b4e6db->leave($__internal_0023a64000dc60236d3c48da90ce0fc3bb72c838d893dcbbb3728531e2b4e6db_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_db1e7f10afa1aed8e22cc96c7e8eb5ae4ba7d5f1fd5d71e27739545d7cec335c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db1e7f10afa1aed8e22cc96c7e8eb5ae4ba7d5f1fd5d71e27739545d7cec335c->enter($__internal_db1e7f10afa1aed8e22cc96c7e8eb5ae4ba7d5f1fd5d71e27739545d7cec335c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Packages";
        
        $__internal_db1e7f10afa1aed8e22cc96c7e8eb5ae4ba7d5f1fd5d71e27739545d7cec335c->leave($__internal_db1e7f10afa1aed8e22cc96c7e8eb5ae4ba7d5f1fd5d71e27739545d7cec335c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_6b6af32c38d3400870a29c8316cd8132b766bf892960da218ffe484cf55f1971 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b6af32c38d3400870a29c8316cd8132b766bf892960da218ffe484cf55f1971->enter($__internal_6b6af32c38d3400870a29c8316cd8132b766bf892960da218ffe484cf55f1971_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
\t";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "subscribeError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "    \t<div class=\"callout callout-danger\">
        \t<p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
    \t</div>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>Select a Package</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\">Packages</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"row\">

      \t\t";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packs"]) ? $context["packs"] : $this->getContext($context, "packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 27
            echo "                <div class=\"col-sm-4 col-md-3\">
                    <div class=\"plan-cell\">
                        <ul class=\"list-group green\">
                            <li class=\"list-group-item plan-name\">";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getName", array(), "method"), "html", null, true);
            echo "</li>
                            <li class=\"list-group-item plan-price catamaran\">";
            // line 31
            echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getFormattedAmount", array(), "method"), "html", null, true);
            echo "</li>
                            <li class=\"list-group-item catamaran\"><i class=\"ion-ios-loop\"></i> Auto Matching</li>
                            <li class=\"list-group-item catamaran\"><i class=\"ion-arrow-up-c\"></i> ";
            // line 33
            echo twig_escape_filter($this->env, (isset($context["app_currency_symbol"]) ? $context["app_currency_symbol"] : $this->getContext($context, "app_currency_symbol")), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getROI", array(), "method"), "html", null, true);
            echo " Return on Investment</li>
                            ";
            // line 34
            if (($this->getAttribute($context["pack"], "getStatus", array(), "method") == "granted")) {
                // line 35
                echo "                                <li class=\"list-group-item catamaran\" style=\"padding:0\"><a class=\"btn btn-success btn-block\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pack_subscription_new", array("pack" => $this->getAttribute($context["pack"], "getId", array(), "method"))), "html", null, true);
                echo "\">Subscribe</a></li>
                            ";
            } elseif (($this->getAttribute(            // line 36
$context["pack"], "getStatus", array(), "method") == "feeders")) {
                // line 37
                echo "                                <li class=\"list-group-item catamaran\" style=\"padding:0\"><a class=\"btn btn-success btn-block\" href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pack_subscription_feeders", array("pack" => $this->getAttribute($context["pack"], "getId", array(), "method"))), "html", null, true);
                echo "\">Check Feeders</a></li>
                            ";
            } elseif (($this->getAttribute(            // line 38
$context["pack"], "getStatus", array(), "method") == "payment")) {
                // line 39
                echo "                                ";
                if (($this->getAttribute($context["pack"], "getUserFeeder", array(), "method") == null)) {
                    echo "    
                                    <li class=\"list-group-item catamaran\" style=\"padding:0\"><a class=\"btn btn-warning btn-block\" href=\"";
                    // line 40
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("await_merging", array("pack" => $this->getAttribute($context["pack"], "getId", array(), "method"))), "html", null, true);
                    echo "\">Awaiting Merging</a></li>
                                ";
                } else {
                    // line 42
                    echo "                                    <li class=\"list-group-item catamaran\" style=\"padding:0\"><a class=\"btn btn-warning btn-block\" href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("make_payment", array("feeder" => $this->getAttribute($this->getAttribute($context["pack"], "getUserFeeder", array(), "method"), "getId", array(), "method"))), "html", null, true);
                    echo "\">Payment Page</a></li>
                                ";
                }
                // line 44
                echo "                            ";
            }
            // line 45
            echo "                        </ul>
                    </div>
                </div>
      \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "      \t</div>
    </section> <!-- End Main content -->
";
        
        $__internal_6b6af32c38d3400870a29c8316cd8132b766bf892960da218ffe484cf55f1971->leave($__internal_6b6af32c38d3400870a29c8316cd8132b766bf892960da218ffe484cf55f1971_prof);

    }

    public function getTemplateName()
    {
        return "member/packs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 49,  150 => 45,  147 => 44,  141 => 42,  136 => 40,  131 => 39,  129 => 38,  124 => 37,  122 => 36,  117 => 35,  115 => 34,  110 => 33,  104 => 31,  100 => 30,  95 => 27,  91 => 26,  79 => 17,  72 => 12,  63 => 9,  60 => 8,  56 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Packages{% endblock %}

{% block body %}

\t{% for flash_message in app.session.flashBag.get('subscribeError') %}
    \t<div class=\"callout callout-danger\">
        \t<p>{{ flash_message }}</p>
    \t</div>
\t{% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>Select a Package</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\">Packages</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
      \t<div class=\"row\">

      \t\t{% for pack in packs %}
                <div class=\"col-sm-4 col-md-3\">
                    <div class=\"plan-cell\">
                        <ul class=\"list-group green\">
                            <li class=\"list-group-item plan-name\">{{ pack.getName() }}</li>
                            <li class=\"list-group-item plan-price catamaran\">{{ app_currency_symbol }}{{ pack.getFormattedAmount() }}</li>
                            <li class=\"list-group-item catamaran\"><i class=\"ion-ios-loop\"></i> Auto Matching</li>
                            <li class=\"list-group-item catamaran\"><i class=\"ion-arrow-up-c\"></i> {{ app_currency_symbol }}{{ pack.getROI() }} Return on Investment</li>
                            {% if pack.getStatus() == 'granted' %}
                                <li class=\"list-group-item catamaran\" style=\"padding:0\"><a class=\"btn btn-success btn-block\" href=\"{{ path('pack_subscription_new', {'pack' : pack.getId()}) }}\">Subscribe</a></li>
                            {% elseif pack.getStatus() == 'feeders' %}
                                <li class=\"list-group-item catamaran\" style=\"padding:0\"><a class=\"btn btn-success btn-block\" href=\"{{ path('pack_subscription_feeders', {'pack' : pack.getId()}) }}\">Check Feeders</a></li>
                            {% elseif pack.getStatus() == 'payment' %}
                                {% if pack.getUserFeeder() == null %}    
                                    <li class=\"list-group-item catamaran\" style=\"padding:0\"><a class=\"btn btn-warning btn-block\" href=\"{{ path('await_merging', {'pack' : pack.getId()}) }}\">Awaiting Merging</a></li>
                                {% else %}
                                    <li class=\"list-group-item catamaran\" style=\"padding:0\"><a class=\"btn btn-warning btn-block\" href=\"{{ path('make_payment', {'feeder' : pack.getUserFeeder().getId()}) }}\">Payment Page</a></li>
                                {% endif %}
                            {% endif %}
                        </ul>
                    </div>
                </div>
      \t\t{% endfor %}
      \t</div>
    </section> <!-- End Main content -->
{% endblock %}", "member/packs.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\member\\packs.html.twig");
    }
}
