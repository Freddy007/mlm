<?php

/* public/home.html.twig */
class __TwigTemplate_3119cfc56878452441e30f0355b646e3649b381de5b3e14e998d7f962fbd5428 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("public.base.html.twig", "public/home.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'carousel' => array($this, 'block_carousel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "public.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30d26c693e3676854bc600158a8160f5398991a6ce55d14e185d36c7c23e1686 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30d26c693e3676854bc600158a8160f5398991a6ce55d14e185d36c7c23e1686->enter($__internal_30d26c693e3676854bc600158a8160f5398991a6ce55d14e185d36c7c23e1686_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/home.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_30d26c693e3676854bc600158a8160f5398991a6ce55d14e185d36c7c23e1686->leave($__internal_30d26c693e3676854bc600158a8160f5398991a6ce55d14e185d36c7c23e1686_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d2aceed7473ff774eb3a4023a7f81097c8005fbbe93d2ba5cb557e3c4b2ec40a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d2aceed7473ff774eb3a4023a7f81097c8005fbbe93d2ba5cb557e3c4b2ec40a->enter($__internal_d2aceed7473ff774eb3a4023a7f81097c8005fbbe93d2ba5cb557e3c4b2ec40a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <!-- Header -->
    <header>
        <div class=\"container\" id=\"maincontent\" tabindex=\"-1\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    ";
        // line 11
        $this->displayBlock('carousel', $context, $blocks);
        // line 126
        echo "
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id=\"about\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>About</h2>
                    <hr class=\"star-primary\">
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6 clearfix media-section\">
                    <img src=\"";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/about.jpg"), "html", null, true);
        echo "\" class=\"pull-left\">
                    <div class=\"push-right\">
                        <p style=\"text-align:justify\">This organization was created by a group of zealous people seeking to promote human welfare and provide a springboard for individuals and corporate organizations alike to thrive via financial empowerment.</p>
                        <p style=\"text-align:justify\">It is a well known fact that there is a huge gap between the rich and the poor. Our organization aims to significantly reduce that gap with the aim of creating a platform that enables all our members attain a higher standard of living and all round financial freedom. This works by evenly distributing all investments among members to bring everybody up at the same time.</p>
                        <p style=\"text-align:justify\">Join our organization today and say goodbye to poverty and financial struggles.</p>
                    </div>
                </div>
                <div class=\"col-xs-12 col-sm-6 home-page-features-cell\">
                    <center>
                        <div class=\"row\"><img  width=\"180px\" class=\"responsive-img\" src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/site-target.png"), "html", null, true);
        echo "\"></div>
                    </center>
                    <h3 class=\"darkblue cell-header\">Aim</h3>
                    <p class=\"catamaran cell-text\">To create a platform that gives all participants an avenue to enjoy a higher standard of living.</p>
                </div>
                <div class=\"col-xs-12 col-sm-6 home-page-features-cell\">
                    <center>
                        <div class=\"row\"><img  width=\"180px\" class=\"responsive-img\" src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/eye-focus.png"), "html", null, true);
        echo "\"></div>
                    </center>
                    <h3 class=\"darkblue cell-header\">Vision</h3>
                    <p class=\"catamaran cell-text\">Our vision is to see people live totally independent lives, free from debt and financial struggles.</p>
                </div>
            </div>
        </div>
    </section> <!-- End About Section -->

    <!-- How It Works Section -->
    <section class=\"success\" id=\"how-it-works\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>How It Works</h2>
                    <hr class=\"star-light\">
                </div>
            </div>
            <div class=\"row\">
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                               <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/pick-package.png"), "html", null, true);
        echo "\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Select a Package</h4>
                            <p class=\"catamaran\">We have a range of packages for you to choose from. Check out the details of the packages below:</p>
                            <div class=\"row\">
                                ";
        // line 188
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packs"]) ? $context["packs"] : $this->getContext($context, "packs")));
        foreach ($context['_seq'] as $context["_key"] => $context["pack"]) {
            // line 189
            echo "                                    <div class=\"col-sm-4 col-md-3\">
                                        <div class=\"plan-cell\">
                                            <ul class=\"list-group green\">
                                                <li class=\"list-group-item plan-name\">";
            // line 192
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getName", array(), "method"), "html", null, true);
            echo "</li>
                                                <li class=\"list-group-item plan-price catamaran\">";
            // line 193
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getAppcurrencysymbol", array(), "method"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getFormattedAmount", array(), "method"), "html", null, true);
            echo "</li>
                                                <li class=\"list-group-item catamaran\"><i class=\"ion-ios-loop\"></i> Auto Matching</li>
                                                <li class=\"list-group-item catamaran\"><i class=\"ion-arrow-up-c\"></i> ";
            // line 195
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getAppcurrencysymbol", array(), "method"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["pack"], "getROI", array(), "method"), "html", null, true);
            echo " Return on Investment</li>
                                                <li class=\"list-group-item catamaran\" style=\"padding:0\"><a href=\"";
            // line 196
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register", array("pack" => $this->getAttribute($context["pack"], "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-warning btn-block\">Sign Up</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pack'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 201
        echo "                            </div>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/match-users.png"), "html", null, true);
        echo "\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Get Matched to Pay an Existing Member</h4>
                            <p class=\"catamaran\">After selecting a suitable package, the system will automatically match you to an existing member you are to make payment to.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/make-payment.png"), "html", null, true);
        echo "\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Make Payment</h4>
                            <p class=\"catamaran\">Get the account details of the member you are to pay to from your dashboard and pay the stated amount.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/upload-payment.png"), "html", null, true);
        echo "\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Upload Payment Proof</h4>
                            <p class=\"catamaran\">While making payment via transfer, mobile or internet banking, ensure that you take screenshots to be uploaded as evidence of payment. If payment is done via bank deposit, kindly scan the teller and upload as evidence of payment.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"";
        // line 245
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/get-activated.png"), "html", null, true);
        echo "\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Get Payment Activation</h4>
                            <p class=\"catamaran\">When you have uploaded the proof of payment, get in touch with the existing member you made payment to, to activate your package. In the extremely rare case of the member not activating your package, kindly write a support ticket to the admin and we will investigate the payment. If the payment evidence is confirmed to be true, we will activate your package and permanently block the account of the existing member. If the payment evidence is found to be false, your account will be permanently blocked as there is no room for fraudsters on ";
        // line 249
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo ".</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -
                <!-- Start Media Section -->
                ";
        // line 255
        echo "                    ";
        // line 256
        echo "                        ";
        // line 257
        echo "                            ";
        // line 258
        echo "                        ";
        // line 259
        echo "                        ";
        // line 260
        echo "                            ";
        // line 261
        echo "                            ";
        // line 262
        echo "                        ";
        // line 263
        echo "                    ";
        // line 264
        echo "                ";
        // line 265
        echo "                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/receive-payment.png"), "html", null, true);
        echo "\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Receive Payments from Downlines</h4>
                            <p class=\"catamaran\">Each downline is given 24 hours to make payment. In the event that a downline fails to make payment within the stipulated time, the downline's account will be blocked. However, a downline can request for a time extension of 6 hours from you and you can extend the time frame by 6 hours.</p>
                            <p class=\"catamaran\">When a downline makes payment and uploads evidence of payment, DO NOT confirm payment without checking your bank account balance from your financial institution. If you do so, there is nothing the admin can do about it as payment confirmation is an irreversible process on the system.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"";
        // line 282
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/activate-downlines.png"), "html", null, true);
        echo "\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Activate Downlines</h4>
                            <p class=\"catamaran\">When you are fully convinced that a downline has made payment, you can proceed to activate.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/recycle.png"), "html", null, true);
        echo "\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Auto Recycle</h4>
                            <p class=\"catamaran\">You are free to choose if the system should autorecycle you or not</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
            </div>
        </div>
    </section> <!-- End How It Works Section -->

    <!-- FAQ Section -->
    <section id=\"faq\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>FAQ</h2>
                    <hr class=\"star-primary\">
                </div>
            </div>
            <div class=\"row\">
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">What is ";
        // line 325
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "?</h4>
                            <p class=\"catamaran\">";
        // line 326
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " is a member to member donation and mutual aid fund scheme for members to help other members in an efficient way. By using this scheme, members gives and receives donations from each other.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">What is the aim of ";
        // line 339
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "?</h4>
                            <p class=\"catamaran\">Our aim is to create a platform that gives all participants an avenue to enjoy a higher standard of living.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How are the packages on ";
        // line 353
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " Different?</h4>
                            <p class=\"catamaran\">The different packages are designed to accomodate a specified investment amount.
                                The higher the package, the more money you invest. Hence, the more profit you make.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Who can join ";
        // line 368
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "?</h4>
                            <p class=\"catamaran\">Anybody of any age and sex can join ";
        // line 369
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo ". Equal benefits and donations are assigned to all.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Is Runniing Multiple Accounts Allowed?</h4>
                            <p class=\"catamaran\">Yes, you can run multiple accounts, but you must run them with different usernames and email addresses as those are unique in the system</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Much Does Setting Up An Account Cost?</h4>
                            <p class=\"catamaran\">Setting up an account with ";
        // line 397
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " is absolutely 100% FREE! No charges whatsoever!!</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Long Will It Take Get Downlines To Pay Me?</h4>
                            <p class=\"catamaran\">The time it takes for you to get downlines to pay you depends on the flow of new users in the system. To shorten this timeframe, this is why we introduced the autocycle feature which forces new and existing members to keep investing in the system while retaining their profits of course. Downlines are assigned to pay usually on or
                                before 7 days!</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Can I Join ";
        // line 425
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "?</h4>
                            <p class=\"catamaran\">There are two ways to join ";
        // line 426
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo ":</p>
                            <ul class=\"catamaran\">
                                <li><p>You get invited by an existing member via his/her referal link. After clicking the referal link, you will be redirected to a registration form page where you fill a simple registration form. This process takes less than two minutes. After successfully filling the form, you will be granted access to your dashboard where you can join a package and start investing.</p></li>
                                <li><p>In the event where you are not refered by anyone, you can head over to <a href=\"";
        // line 429
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register_pack");
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "siteurl", array()), "html", null, true);
        echo "/register-pack</a> and fill out the simple registration form there.</p></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                ";
        // line 436
        echo "                    ";
        // line 437
        echo "                        ";
        // line 438
        echo "                            ";
        // line 439
        echo "                                ";
        // line 440
        echo "                            ";
        // line 441
        echo "                        ";
        // line 442
        echo "                        ";
        // line 443
        echo "                            ";
        // line 444
        echo "                            ";
        // line 445
        echo "                                ";
        // line 446
        echo "                                ";
        // line 447
        echo "                                ";
        // line 448
        echo "                                ";
        // line 449
        echo "                        ";
        // line 450
        echo "                    ";
        // line 451
        echo "                ";
        // line 452
        echo "                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Many Packages Can I Sign Up to at the same Time?</h4>
                            <p class=\"catamaran\">You can only run 1 packages at a time.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Do I Receive Payment?</h4>
                            <p class=\"catamaran\">The downlines the system assigns to pay you will make payments via the
                                mobile money account details you provide while filling the registration form.
                                They can proceed to make payment either via bank deposit or cash transfer.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Is ";
        // line 491
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " Legal?</h4>
                            <p class=\"catamaran\">";
        // line 492
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " is a multi-level interpersonal organization where individuals who will help each other deliberately, will join with their details. Also, the registered members from ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " have a bound together monetary relationship, and this has demonstrated the motivation behind why ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " is not a subject of legitimate relations thus the ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " community can't be illicit. Giving cash by one member to another is not disallowed by either universal or nearby lawful frameworks.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">I Have Made Payment But My Payment Has Not Been Approved?</h4>
                            <p class=\"catamaran\">";
        // line 506
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo " is a timed system. Members are given 24 hours to confirm payments made by their
                                downlines (provided valid evidence of payment has been uploaded). Once this time frame elapses and there is still no approval,
                                the case will be automatically moved to the ";
        // line 508
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "
                                Court where judges will preside over the issue and reach a verdict to confirm or cancel the payment based on the statements and
                                evidences provided by you and the member you were matched to.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Help!!! The Downline Assigned to Pay Me Refused to Pay</h4>
                            <p class=\"catamaran\">Keep Calm, there is no need to worry here. Once the time assigned to a downline has been exhausted, the system will block the account of the downline and assign a new downline to pay you within 24 hours.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
            </div>
        </div>
    </section> <!-- End FAQ Section -->

    <!-- Testimonies Section -->
    <section class=\"success\" id=\"testimonies\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>Testimonies</h2>
                    <hr class=\"star-light\">
                </div>
            </div>
            <div class=\"row\">
                ";
        // line 542
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")));
        foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
            // line 543
            echo "                    <div class=\"col-xs-12 col-sm-4 testimonials-cell\">
                        <img src=\"";
            // line 544
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute($context["review"], "getUser", array(), "method"), "getAvatar", array(), "method")), "html", null, true);
            echo "\">
                        <h3 class=\"darkblue\">";
            // line 545
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["review"], "getUser", array(), "method"), "getName", array(), "method"), "html", null, true);
            echo "</h3>
                        <p class=\"catamaran\"><small>";
            // line 546
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["review"], "getCreatedDate", array(), "method"), "d M. Y H:i"), "html", null, true);
            echo " | ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["review"], "getUser", array(), "method"), "getResidentState", array(), "method"), "html", null, true);
            echo "</small></p>
                        <p class=\"catamaran\">";
            // line 547
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "getTestimony", array(), "method"), "html", null, true);
            echo "</p>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 550
        echo "            </div>
        </div>
    </section> <!-- End Testimonies Section -->

    <!-- Contact Section -->
    <section id=\"contact\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>Contact Us</h2>
                    <hr class=\"star-primary\">
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-lg-8 col-lg-offset-2\">
                    ";
        // line 565
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "contactError"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 566
            echo "                        <div class=\"alert alert-danger\">
                            <p>";
            // line 567
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 570
        echo "
                    ";
        // line 571
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "contactSuccess"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 572
            echo "                        <div class=\"alert alert-success\">
                            <p>";
            // line 573
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 576
        echo "
                    ";
        // line 577
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contact"), "attr" => array("class" => "form-horizontal")));
        echo "

                    ";
        // line 580
        echo "                        <div class=\"row control-group\">
                            <div class=\"form-group col-xs-12 floating-label-form-group controls\">
                                    ";
        // line 582
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget');
        echo "
                                <p class=\"help-block text-danger\"></p>
                            </div>
                        </div>
                        <div class=\"row control-group\">
                            <div class=\"form-group col-xs-12 floating-label-form-group controls\">
                                ";
        // line 588
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "
                                <p class=\"help-block text-danger\"></p>
                            </div>
                        </div>
                        <div class=\"row control-group\">
                            <div class=\"form-group col-xs-12 floating-label-form-group controls\">
                                ";
        // line 594
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "message", array()), 'widget');
        echo "
                                <p class=\"help-block text-danger\"></p>
                            </div>
                        </div>
                        <br>
                        <div id=\"success\"></div>
                        <div class=\"row\">
                            <div class=\"form-group col-xs-12\">
                                <button type=\"submit\" class=\"btn btn-success btn-lg\">Send</button>
                            </div>
                        </div>
                    ";
        // line 605
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                  ";
        // line 607
        echo "                </div>
            </div>
        </div>
    </section>


";
        
        $__internal_d2aceed7473ff774eb3a4023a7f81097c8005fbbe93d2ba5cb557e3c4b2ec40a->leave($__internal_d2aceed7473ff774eb3a4023a7f81097c8005fbbe93d2ba5cb557e3c4b2ec40a_prof);

    }

    // line 11
    public function block_carousel($context, array $blocks = array())
    {
        $__internal_32d64dfb1679aaed42858f4e39581d8cef17b3ce093d298b70f0bb27ae790dc0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32d64dfb1679aaed42858f4e39581d8cef17b3ce093d298b70f0bb27ae790dc0->enter($__internal_32d64dfb1679aaed42858f4e39581d8cef17b3ce093d298b70f0bb27ae790dc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "carousel"));

        // line 12
        echo "                        <div id=\"index-carousel\" class=\"carousel slide\"   data-ride=\"carousel\">

                            ";
        // line 15
        echo "                            <div class=\"intro-text carousel-inner\">
                                <div class=\"item active\">
                                    <h2><img src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/back_one.jpg"), "html", null, true);
        echo "\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                        <div class=\"intro-text\">
                                            <h1 class=\"name\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "</h1>
                                            <hr class=\"star-light\">
                                            <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                            ";
        // line 25
        echo "

                                        </div>

                                </div>

                                <div class=\"item\">
                                    <h2><img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/back_one.jpg"), "html", null, true);
        echo "\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        ";
        // line 40
        echo "
                                    </div>

                                </div>
                                <div class=\"item\">
                                    <h2><img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/back_one.jpg"), "html", null, true);
        echo "\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        ";
        // line 53
        echo "

                                    </div>

                                </div>
                                <div class=\"item\">
                                    <h2><img src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/back_one.jpg"), "html", null, true);
        echo "\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        ";
        // line 67
        echo "

                                    </div>

                                </div>
                                <div class=\"item\">
                                    <h2><img src=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/back_one.jpg"), "html", null, true);
        echo "\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        <p>Lorem ipsum dolor sit amet</p>


                                    </div>

                                </div>
                                <div class=\"item\">
                                    <h2><img src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/back_one.jpg"), "html", null, true);
        echo "\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">";
        // line 91
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "getSitename", array(), "method"), "html", null, true);
        echo "</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        ";
        // line 95
        echo "

                                    </div>

                                </div>
                                <br/><br/>
                                <a href=\"";
        // line 101
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" class=\"btn btn-lg btn-warning\">";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("Login", array(), "messages");
        echo "</a> <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register_pack");
        echo "\" class=\"btn btn-lg btn-warning\">Register</a>
                            </div>


                            ";
        // line 106
        echo "                            <a class=\"left carousel-control\" href=\"#index-carousel\" data-slide=\"prev\">
                                <span class=\"glyphicon glyphicon-chevron-left\"></span>
                            </a>
                            <a class=\"right carousel-control\"  href=\"#index-carousel\" data-slide=\"next\">
                                <span class=\"glyphicon glyphicon-chevron-right\"></span>
                            </a>

                            ";
        // line 114
        echo "                            <ol class=\"carousel-indicators\">
                                <li data-target=\"#index-carousel\" data-slide-to=\"0\" class=\"active\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"1\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"2\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"3\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"4\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"5\"></li>
                            </ol>

                        </div>

                    ";
        
        $__internal_32d64dfb1679aaed42858f4e39581d8cef17b3ce093d298b70f0bb27ae790dc0->leave($__internal_32d64dfb1679aaed42858f4e39581d8cef17b3ce093d298b70f0bb27ae790dc0_prof);

    }

    public function getTemplateName()
    {
        return "public/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  910 => 114,  901 => 106,  890 => 101,  882 => 95,  876 => 91,  869 => 87,  856 => 77,  849 => 73,  841 => 67,  835 => 63,  828 => 59,  820 => 53,  814 => 49,  807 => 45,  800 => 40,  794 => 36,  787 => 32,  778 => 25,  772 => 21,  765 => 17,  761 => 15,  757 => 12,  751 => 11,  738 => 607,  734 => 605,  720 => 594,  711 => 588,  702 => 582,  698 => 580,  693 => 577,  690 => 576,  681 => 573,  678 => 572,  674 => 571,  671 => 570,  662 => 567,  659 => 566,  655 => 565,  638 => 550,  629 => 547,  623 => 546,  619 => 545,  615 => 544,  612 => 543,  608 => 542,  571 => 508,  566 => 506,  543 => 492,  539 => 491,  498 => 452,  496 => 451,  494 => 450,  492 => 449,  490 => 448,  488 => 447,  486 => 446,  484 => 445,  482 => 444,  480 => 443,  478 => 442,  476 => 441,  474 => 440,  472 => 439,  470 => 438,  468 => 437,  466 => 436,  455 => 429,  449 => 426,  445 => 425,  414 => 397,  383 => 369,  379 => 368,  361 => 353,  344 => 339,  328 => 326,  324 => 325,  290 => 294,  275 => 282,  259 => 269,  253 => 265,  251 => 264,  249 => 263,  247 => 262,  245 => 261,  243 => 260,  241 => 259,  239 => 258,  237 => 257,  235 => 256,  233 => 255,  225 => 249,  218 => 245,  203 => 233,  188 => 221,  173 => 209,  163 => 201,  152 => 196,  147 => 195,  141 => 193,  137 => 192,  132 => 189,  128 => 188,  119 => 182,  93 => 159,  83 => 152,  71 => 143,  52 => 126,  50 => 11,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'public.base.html.twig' %}

{% block body %}

    <!-- Header -->
    <header>
        <div class=\"container\" id=\"maincontent\" tabindex=\"-1\">
            <div class=\"row\">
                <div class=\"col-lg-12\">

                    {% block carousel %}
                        <div id=\"index-carousel\" class=\"carousel slide\"   data-ride=\"carousel\">

                            {#Wrapper for slides#}
                            <div class=\"intro-text carousel-inner\">
                                <div class=\"item active\">
                                    <h2><img src=\"{{ asset('img/back_one.jpg') }}\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                        <div class=\"intro-text\">
                                            <h1 class=\"name\">{{ adm.getSitename() }}</h1>
                                            <hr class=\"star-light\">
                                            <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                            {#<p>Lorem ipsum dolor sit amet</p>#}


                                        </div>

                                </div>

                                <div class=\"item\">
                                    <h2><img src=\"{{ asset('img/back_one.jpg') }}\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">{{ adm.getSitename() }}</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        {#<p>Lorem ipsum dolor sit amet</p>#}

                                    </div>

                                </div>
                                <div class=\"item\">
                                    <h2><img src=\"{{ asset('img/back_one.jpg') }}\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">{{ adm.getSitename() }}</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        {#<p>Lorem ipsum dolor sit amet</p>#}


                                    </div>

                                </div>
                                <div class=\"item\">
                                    <h2><img src=\"{{ asset('img/back_one.jpg') }}\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">{{ adm.getSitename() }}</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        {#<p>Lorem ipsum dolor sit amet</p>#}


                                    </div>

                                </div>
                                <div class=\"item\">
                                    <h2><img src=\"{{ asset('img/back_one.jpg') }}\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">{{ adm.getSitename() }}</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        <p>Lorem ipsum dolor sit amet</p>


                                    </div>

                                </div>
                                <div class=\"item\">
                                    <h2><img src=\"{{ asset('img/back_one.jpg') }}\" class=\"pull-left\"></h2>
                                    <div class=\"carousel-caption\">
                                    </div>
                                    <div class=\"intro-text\">
                                        <h1 class=\"name\">{{ adm.getSitename() }}</h1>
                                        <hr class=\"star-light\">
                                        <span class=\"skills\">Trusted and Genuine Returns for Everyone</span>
                                        {#<p>Lorem ipsum dolor sit amet</p>#}


                                    </div>

                                </div>
                                <br/><br/>
                                <a href=\"{{ path('login') }}\" class=\"btn btn-lg btn-warning\">{% trans %}Login{% endtrans %}</a> <a href=\"{{ path('register_pack') }}\" class=\"btn btn-lg btn-warning\">Register</a>
                            </div>


                            {#Controls#}
                            <a class=\"left carousel-control\" href=\"#index-carousel\" data-slide=\"prev\">
                                <span class=\"glyphicon glyphicon-chevron-left\"></span>
                            </a>
                            <a class=\"right carousel-control\"  href=\"#index-carousel\" data-slide=\"next\">
                                <span class=\"glyphicon glyphicon-chevron-right\"></span>
                            </a>

                            {#Indicators#}
                            <ol class=\"carousel-indicators\">
                                <li data-target=\"#index-carousel\" data-slide-to=\"0\" class=\"active\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"1\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"2\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"3\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"4\"></li>
                                <li data-target=\"#index-carousel\" data-slide-to=\"5\"></li>
                            </ol>

                        </div>

                    {% endblock %}

                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id=\"about\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>About</h2>
                    <hr class=\"star-primary\">
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-md-6 clearfix media-section\">
                    <img src=\"{{ asset('img/about.jpg') }}\" class=\"pull-left\">
                    <div class=\"push-right\">
                        <p style=\"text-align:justify\">This organization was created by a group of zealous people seeking to promote human welfare and provide a springboard for individuals and corporate organizations alike to thrive via financial empowerment.</p>
                        <p style=\"text-align:justify\">It is a well known fact that there is a huge gap between the rich and the poor. Our organization aims to significantly reduce that gap with the aim of creating a platform that enables all our members attain a higher standard of living and all round financial freedom. This works by evenly distributing all investments among members to bring everybody up at the same time.</p>
                        <p style=\"text-align:justify\">Join our organization today and say goodbye to poverty and financial struggles.</p>
                    </div>
                </div>
                <div class=\"col-xs-12 col-sm-6 home-page-features-cell\">
                    <center>
                        <div class=\"row\"><img  width=\"180px\" class=\"responsive-img\" src=\"{{ asset('img/site-target.png') }}\"></div>
                    </center>
                    <h3 class=\"darkblue cell-header\">Aim</h3>
                    <p class=\"catamaran cell-text\">To create a platform that gives all participants an avenue to enjoy a higher standard of living.</p>
                </div>
                <div class=\"col-xs-12 col-sm-6 home-page-features-cell\">
                    <center>
                        <div class=\"row\"><img  width=\"180px\" class=\"responsive-img\" src=\"{{ asset('img/eye-focus.png') }}\"></div>
                    </center>
                    <h3 class=\"darkblue cell-header\">Vision</h3>
                    <p class=\"catamaran cell-text\">Our vision is to see people live totally independent lives, free from debt and financial struggles.</p>
                </div>
            </div>
        </div>
    </section> <!-- End About Section -->

    <!-- How It Works Section -->
    <section class=\"success\" id=\"how-it-works\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>How It Works</h2>
                    <hr class=\"star-light\">
                </div>
            </div>
            <div class=\"row\">
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                               <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/pick-package.png') }}\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Select a Package</h4>
                            <p class=\"catamaran\">We have a range of packages for you to choose from. Check out the details of the packages below:</p>
                            <div class=\"row\">
                                {% for pack in packs %}
                                    <div class=\"col-sm-4 col-md-3\">
                                        <div class=\"plan-cell\">
                                            <ul class=\"list-group green\">
                                                <li class=\"list-group-item plan-name\">{{ pack.getName() }}</li>
                                                <li class=\"list-group-item plan-price catamaran\">{{ adm.getAppcurrencysymbol() }}{{ pack.getFormattedAmount() }}</li>
                                                <li class=\"list-group-item catamaran\"><i class=\"ion-ios-loop\"></i> Auto Matching</li>
                                                <li class=\"list-group-item catamaran\"><i class=\"ion-arrow-up-c\"></i> {{ adm.getAppcurrencysymbol() }}{{ pack.getROI() }} Return on Investment</li>
                                                <li class=\"list-group-item catamaran\" style=\"padding:0\"><a href=\"{{ path('register', {'pack' : pack.getId() }) }}\" class=\"btn btn-warning btn-block\">Sign Up</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/match-users.png') }}\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Get Matched to Pay an Existing Member</h4>
                            <p class=\"catamaran\">After selecting a suitable package, the system will automatically match you to an existing member you are to make payment to.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/make-payment.png') }}\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Make Payment</h4>
                            <p class=\"catamaran\">Get the account details of the member you are to pay to from your dashboard and pay the stated amount.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/upload-payment.png') }}\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Upload Payment Proof</h4>
                            <p class=\"catamaran\">While making payment via transfer, mobile or internet banking, ensure that you take screenshots to be uploaded as evidence of payment. If payment is done via bank deposit, kindly scan the teller and upload as evidence of payment.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/get-activated.png') }}\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Get Payment Activation</h4>
                            <p class=\"catamaran\">When you have uploaded the proof of payment, get in touch with the existing member you made payment to, to activate your package. In the extremely rare case of the member not activating your package, kindly write a support ticket to the admin and we will investigate the payment. If the payment evidence is confirmed to be true, we will activate your package and permanently block the account of the existing member. If the payment evidence is found to be false, your account will be permanently blocked as there is no room for fraudsters on {{ adm.getSitename() }}.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -
                <!-- Start Media Section -->
                {#<div class=\"col-sm-12 clearfix media-section\">#}
                    {#<div class=\"media\">#}
                        {#<div class=\"media-left\">#}
                            {#<div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/downlines-assigned.png') }}\"></div>#}
                        {#</div>#}
                        {#<div class=\"media-body\">#}
                            {#<h4 class=\"media-heading\">The System Assigns Two Downlines to Pay You</h4>#}
                            {#<p class=\"catamaran\">Upon activation of your package, the system will assign two members to pay you. For instance, if you subscribe to the N5,000 package, the system will assign two people to pay you N5,000 each within 15 days!!!</p>#}
                        {#</div>#}
                    {#</div>#}
                {#</div> <!-- End Media Section -->#}
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/receive-payment.png') }}\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Receive Payments from Downlines</h4>
                            <p class=\"catamaran\">Each downline is given 24 hours to make payment. In the event that a downline fails to make payment within the stipulated time, the downline's account will be blocked. However, a downline can request for a time extension of 6 hours from you and you can extend the time frame by 6 hours.</p>
                            <p class=\"catamaran\">When a downline makes payment and uploads evidence of payment, DO NOT confirm payment without checking your bank account balance from your financial institution. If you do so, there is nothing the admin can do about it as payment confirmation is an irreversible process on the system.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/activate-downlines.png') }}\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Activate Downlines</h4>
                            <p class=\"catamaran\">When you are fully convinced that a downline has made payment, you can proceed to activate.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <div class=\"row\"><img width=\"180px\" style=\"padding-left: 15px\" class=\"responsive-img\" src=\"{{ asset('img/recycle.png') }}\"></div>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Auto Recycle</h4>
                            <p class=\"catamaran\">You are free to choose if the system should autorecycle you or not</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
            </div>
        </div>
    </section> <!-- End How It Works Section -->

    <!-- FAQ Section -->
    <section id=\"faq\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>FAQ</h2>
                    <hr class=\"star-primary\">
                </div>
            </div>
            <div class=\"row\">
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">What is {{ adm.getSitename() }}?</h4>
                            <p class=\"catamaran\">{{ adm.getSitename() }} is a member to member donation and mutual aid fund scheme for members to help other members in an efficient way. By using this scheme, members gives and receives donations from each other.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">What is the aim of {{ adm.getSitename() }}?</h4>
                            <p class=\"catamaran\">Our aim is to create a platform that gives all participants an avenue to enjoy a higher standard of living.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How are the packages on {{ adm.getSitename() }} Different?</h4>
                            <p class=\"catamaran\">The different packages are designed to accomodate a specified investment amount.
                                The higher the package, the more money you invest. Hence, the more profit you make.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Who can join {{ adm.getSitename() }}?</h4>
                            <p class=\"catamaran\">Anybody of any age and sex can join {{ adm.getSitename() }}. Equal benefits and donations are assigned to all.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Is Runniing Multiple Accounts Allowed?</h4>
                            <p class=\"catamaran\">Yes, you can run multiple accounts, but you must run them with different usernames and email addresses as those are unique in the system</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Much Does Setting Up An Account Cost?</h4>
                            <p class=\"catamaran\">Setting up an account with {{ adm.getSitename() }} is absolutely 100% FREE! No charges whatsoever!!</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Long Will It Take Get Downlines To Pay Me?</h4>
                            <p class=\"catamaran\">The time it takes for you to get downlines to pay you depends on the flow of new users in the system. To shorten this timeframe, this is why we introduced the autocycle feature which forces new and existing members to keep investing in the system while retaining their profits of course. Downlines are assigned to pay usually on or
                                before 7 days!</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Can I Join {{ adm.getSitename() }}?</h4>
                            <p class=\"catamaran\">There are two ways to join {{ adm.getSitename() }}:</p>
                            <ul class=\"catamaran\">
                                <li><p>You get invited by an existing member via his/her referal link. After clicking the referal link, you will be redirected to a registration form page where you fill a simple registration form. This process takes less than two minutes. After successfully filling the form, you will be granted access to your dashboard where you can join a package and start investing.</p></li>
                                <li><p>In the event where you are not refered by anyone, you can head over to <a href=\"{{ path('register_pack') }}\">{{ adm.siteurl }}/register-pack</a> and fill out the simple registration form there.</p></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                {#<div class=\"col-sm-12 clearfix media-section\">#}
                    {#<div class=\"media\">#}
                        {#<div class=\"media-left\">#}
                            {#<span class=\"media-rounded green\">#}
                                {#<i class=\"fa fa-question\"></i>#}
                            {#</span>#}
                        {#</div>#}
                        {#<div class=\"media-body\">#}
                            {#<h4 class=\"media-heading\">How Much Compensation Do I Receive From Refering People?</h4>#}
                            {#<p class=\"catamaran\">You get 10% of the FIRST package your referals sign up for. For instance, #}
                                {#if you refer someone to the system and the person starts by joining the Small package of N5,000, #}
                                {#your wallet will be credited with 10% of N5,000 which is {{ adm.getAppcurrencysymbol() }}#}
                                {#500 naira. However, after that package, the next time your referals invest into the system, #}
                                {#you will not receive any compensation bonus.</p>#}
                        {#</div>#}
                    {#</div>#}
                {#</div> <!-- End Media Section -->#}
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Many Packages Can I Sign Up to at the same Time?</h4>
                            <p class=\"catamaran\">You can only run 1 packages at a time.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">How Do I Receive Payment?</h4>
                            <p class=\"catamaran\">The downlines the system assigns to pay you will make payments via the
                                mobile money account details you provide while filling the registration form.
                                They can proceed to make payment either via bank deposit or cash transfer.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Is {{ adm.getSitename() }} Legal?</h4>
                            <p class=\"catamaran\">{{ adm.getSitename() }} is a multi-level interpersonal organization where individuals who will help each other deliberately, will join with their details. Also, the registered members from {{ adm.getSitename() }} have a bound together monetary relationship, and this has demonstrated the motivation behind why {{ adm.getSitename() }} is not a subject of legitimate relations thus the {{ adm.getSitename() }} community can't be illicit. Giving cash by one member to another is not disallowed by either universal or nearby lawful frameworks.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">I Have Made Payment But My Payment Has Not Been Approved?</h4>
                            <p class=\"catamaran\">{{ adm.getSitename() }} is a timed system. Members are given 24 hours to confirm payments made by their
                                downlines (provided valid evidence of payment has been uploaded). Once this time frame elapses and there is still no approval,
                                the case will be automatically moved to the {{ adm.getSitename() }}
                                Court where judges will preside over the issue and reach a verdict to confirm or cancel the payment based on the statements and
                                evidences provided by you and the member you were matched to.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
                <!-- Start Media Section -->
                <div class=\"col-sm-12 clearfix media-section\">
                    <div class=\"media\">
                        <div class=\"media-left\">
                            <span class=\"media-rounded green\">
                                <i class=\"fa fa-question\"></i>
                            </span>
                        </div>
                        <div class=\"media-body\">
                            <h4 class=\"media-heading\">Help!!! The Downline Assigned to Pay Me Refused to Pay</h4>
                            <p class=\"catamaran\">Keep Calm, there is no need to worry here. Once the time assigned to a downline has been exhausted, the system will block the account of the downline and assign a new downline to pay you within 24 hours.</p>
                        </div>
                    </div>
                </div> <!-- End Media Section -->
            </div>
        </div>
    </section> <!-- End FAQ Section -->

    <!-- Testimonies Section -->
    <section class=\"success\" id=\"testimonies\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>Testimonies</h2>
                    <hr class=\"star-light\">
                </div>
            </div>
            <div class=\"row\">
                {% for review in reviews %}
                    <div class=\"col-xs-12 col-sm-4 testimonials-cell\">
                        <img src=\"{{ asset(review.getUser().getAvatar()) }}\">
                        <h3 class=\"darkblue\">{{ review.getUser().getName() }}</h3>
                        <p class=\"catamaran\"><small>{{ review.getCreatedDate()|date('d M. Y H:i') }} | {{ review.getUser().getResidentState() }}</small></p>
                        <p class=\"catamaran\">{{ review.getTestimony() }}</p>
                    </div>
                {% endfor %}
            </div>
        </div>
    </section> <!-- End Testimonies Section -->

    <!-- Contact Section -->
    <section id=\"contact\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-12 text-center\">
                    <h2>Contact Us</h2>
                    <hr class=\"star-primary\">
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-lg-8 col-lg-offset-2\">
                    {% for flash_message in app.session.flashBag.get('contactError') %}
                        <div class=\"alert alert-danger\">
                            <p>{{ flash_message }}</p>
                        </div>
                    {% endfor %}

                    {% for flash_message in app.session.flashBag.get('contactSuccess') %}
                        <div class=\"alert alert-success\">
                            <p>{{ flash_message }}</p>
                        </div>
                    {% endfor %}

                    {{ form_start(form, {'action': path('contact'), 'attr': {'class': 'form-horizontal'}}) }}

                    {#<form name=\"sentMessage\" id=\"contactForm\" novalidate action=\"\">#}
                        <div class=\"row control-group\">
                            <div class=\"form-group col-xs-12 floating-label-form-group controls\">
                                    {{ form_widget(form.name) }}
                                <p class=\"help-block text-danger\"></p>
                            </div>
                        </div>
                        <div class=\"row control-group\">
                            <div class=\"form-group col-xs-12 floating-label-form-group controls\">
                                {{ form_widget(form.email) }}
                                <p class=\"help-block text-danger\"></p>
                            </div>
                        </div>
                        <div class=\"row control-group\">
                            <div class=\"form-group col-xs-12 floating-label-form-group controls\">
                                {{ form_widget(form.message) }}
                                <p class=\"help-block text-danger\"></p>
                            </div>
                        </div>
                        <br>
                        <div id=\"success\"></div>
                        <div class=\"row\">
                            <div class=\"form-group col-xs-12\">
                                <button type=\"submit\" class=\"btn btn-success btn-lg\">Send</button>
                            </div>
                        </div>
                    {{ form_end(form) }}
                  {#  </form>#}
                </div>
            </div>
        </div>
    </section>


{% endblock %}", "public/home.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public\\home.html.twig");
    }
}
