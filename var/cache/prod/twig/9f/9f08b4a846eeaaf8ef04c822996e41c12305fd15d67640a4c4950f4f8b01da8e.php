<?php

/* god-mode/list-users.html.twig */
class __TwigTemplate_cdfdd5992e907910e0dda8f9515caffe203f857e052ea2ecf84ee1c54cc29d7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/list-users.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf2d0a9be2e9253041ff5fb3ec03b36f4e5883357f70e0ed9436147ac5052174 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf2d0a9be2e9253041ff5fb3ec03b36f4e5883357f70e0ed9436147ac5052174->enter($__internal_bf2d0a9be2e9253041ff5fb3ec03b36f4e5883357f70e0ed9436147ac5052174_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/list-users.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bf2d0a9be2e9253041ff5fb3ec03b36f4e5883357f70e0ed9436147ac5052174->leave($__internal_bf2d0a9be2e9253041ff5fb3ec03b36f4e5883357f70e0ed9436147ac5052174_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_00816bade388f205cd7bef3b3de5398d668efab4ee05578cf1082c09f9f18634 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00816bade388f205cd7bef3b3de5398d668efab4ee05578cf1082c09f9f18634->enter($__internal_00816bade388f205cd7bef3b3de5398d668efab4ee05578cf1082c09f9f18634_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getTotalItemCount", array()), "html", null, true);
        echo " User";
        if (($this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        
        $__internal_00816bade388f205cd7bef3b3de5398d668efab4ee05578cf1082c09f9f18634->leave($__internal_00816bade388f205cd7bef3b3de5398d668efab4ee05578cf1082c09f9f18634_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_0aa70b37c911b56a82edbde24cfe06fb992fa302d5369997d59d6ed3877f1689 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0aa70b37c911b56a82edbde24cfe06fb992fa302d5369997d59d6ed3877f1689->enter($__internal_0aa70b37c911b56a82edbde24cfe06fb992fa302d5369997d59d6ed3877f1689_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getTotalItemCount", array()), "html", null, true);
        echo " User";
        if (($this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getTotalItemCount", array()) != 1)) {
            echo "s";
        }
        // line 23
        echo "        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-users\"></i> Users</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_admin");
        echo "\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin");
        echo "\" class=\"list-group-item\">List Admin</a>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users");
        echo "\" class=\"list-group-item active\">List Users</a>
                    <a href=\"";
        // line 40
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_pack");
        echo "\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"";
        // line 41
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack");
        echo "\" class=\"list-group-item\">List Pack</a>
                    <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_pack_subs");
        echo "\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_admin_payments");
        echo "\" class=\"list-group-item\">List Payments</a>
                    <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_news");
        echo "\" class=\"list-group-item\">Create News</a>
                    <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_news");
        echo "\" class=\"list-group-item\">List News</a>
                    <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("set_notice_board");
        echo "\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"";
        // line 47
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("unblock_user");
        echo "\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("reset_password");
        echo "\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_compose_mail");
        echo "\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_mails");
        echo "\" class=\"list-group-item\">List Mail</a>
                    <a href=\"";
        // line 51
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_approve_reviews");
        echo "\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <div class=\"row\">
                    <div class=\"col-sm-2\">
                    </div>
                    <div class=\"col-sm-8 text-center lead\">
                        ";
        // line 61
        if (($this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getTotalItemCount", array()) != 0)) {
            // line 62
            echo "                        Showing ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getTotalItemCount", array()), "html", null, true);
            echo " User";
            if (($this->getAttribute((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            // line 63
            echo "                        ";
        } else {
            // line 64
            echo "                            0 Users
                        ";
        }
        // line 66
        echo "                    </div>
                    <div class=\"col-sm-2\">
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <div class=\"dropdown\" style=\"display:inline-block\">
                            <button class=\"btn btn-warning dropdown-toggle\" type=\"button\" id=\"sortMenu\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Sort By <span class=\"caret\"></span></button>
                            <ul class=\"dropdown-menu\" aria-labelledby=\"sortMenu\">
                                <li>
                                    <a href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users", array("page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "sort" => "date_DESC")), "html", null, true);
        echo "\">Created Date DESC</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users", array("page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "sort" => "date_ASC")), "html", null, true);
        echo "\">Created Date ASC</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users", array("page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "sort" => "name_DESC")), "html", null, true);
        echo "\">Name DESC</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users", array("page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "sort" => "name_ASC")), "html", null, true);
        echo "\">Name ASC</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users", array("page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "sort" => "username_DESC")), "html", null, true);
        echo "\">Username DESC</a>
                                </li>
                                <li>
                                    <a href=\"";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_users", array("page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "sort" => "username_ASC")), "html", null, true);
        echo "\">Username ASC</a>
                                </li>
                            </ul>
                        </div>
                        <span>
                            <strong>Sorting By: </strong>
                            ";
        // line 97
        if (((isset($context["sort"]) ? $context["sort"] : $this->getContext($context, "sort")) == "date_DESC")) {
            // line 98
            echo "                                Created Date DESC
                            ";
        } elseif ((        // line 99
(isset($context["sort"]) ? $context["sort"] : $this->getContext($context, "sort")) == "date_ASC")) {
            // line 100
            echo "                                Created Date ASC
                            ";
        } elseif ((        // line 101
(isset($context["sort"]) ? $context["sort"] : $this->getContext($context, "sort")) == "name_DESC")) {
            // line 102
            echo "                                Name DESC
                            ";
        } elseif ((        // line 103
(isset($context["sort"]) ? $context["sort"] : $this->getContext($context, "sort")) == "name_ASC")) {
            // line 104
            echo "                                Name ASC
                            ";
        } elseif ((        // line 105
(isset($context["sort"]) ? $context["sort"] : $this->getContext($context, "sort")) == "username_DESC")) {
            // line 106
            echo "                                Username DESC
                            ";
        } elseif ((        // line 107
(isset($context["sort"]) ? $context["sort"] : $this->getContext($context, "sort")) == "username_ASC")) {
            // line 108
            echo "                                Username ASC
                            ";
        }
        // line 110
        echo "                        </span>
                    </div>
                </div>
                <table class=\"table table-hover table-responsive table-striped\">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Phone Number</th>
                            <th>Status</th>
                            <th>Options</th>
                            <th>Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
        // line 125
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 126
            echo "                            <tr>
                                <td>
                                    ";
            // line 128
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "getName", array(), "method"), "html", null, true);
            echo "
                                </td>
                                <td>
                                    ";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "getUsername", array(), "method"), "html", null, true);
            echo "
                                </td>

                                <td>
                                    ";
            // line 135
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "getPhoneOne", array(), "method"), "html", null, true);
            echo "
                                </td>
                                <td>
                                    ";
            // line 138
            if (($this->getAttribute($context["user"], "getStatus", array(), "method") == "active")) {
                // line 139
                echo "                                        <span class=\"text-success\">Active</span>
                                    ";
            } else {
                // line 141
                echo "                                        <span class=\"text-danger\">Blocked</span>
                                    ";
            }
            // line 143
            echo "                                </td>
                                <td>
                                    ";
            // line 145
            if (($this->getAttribute($context["user"], "getStatus", array(), "method") == "active")) {
                // line 146
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("do_block_user", array("user" => $this->getAttribute($context["user"], "getId", array(), "method"), "page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "sort" => (isset($context["sort"]) ? $context["sort"] : $this->getContext($context, "sort")))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-flat btn-danger\">Block User</a>
                                    ";
            } else {
                // line 148
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("do_activate_user", array("user" => $this->getAttribute($context["user"], "getId", array(), "method"), "page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "sort" => (isset($context["sort"]) ? $context["sort"] : $this->getContext($context, "sort")))), "html", null, true);
                echo "\" class=\"btn btn-xs btn-flat btn-success\">Activate User</a>
                                    ";
            }
            // line 150
            echo "                                </td>
                                <td>
                                    ";
            // line 152
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "getCreated", array(), "method"), "d M. Y H:i"), "html", null, true);
            echo "
                                </td>
                                <td>
                                    <a href=\"";
            // line 155
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_user", array("user" => $this->getAttribute($context["user"], "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-xs btn-warning\">Edit</a>
                                </td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 159
        echo "                    </tbody>
                </table>

                <div class=\"navigation text-center\">
                    ";
        // line 163
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
        echo "
                </div>
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
";
        
        $__internal_0aa70b37c911b56a82edbde24cfe06fb992fa302d5369997d59d6ed3877f1689->leave($__internal_0aa70b37c911b56a82edbde24cfe06fb992fa302d5369997d59d6ed3877f1689_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/list-users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  395 => 163,  389 => 159,  379 => 155,  373 => 152,  369 => 150,  363 => 148,  357 => 146,  355 => 145,  351 => 143,  347 => 141,  343 => 139,  341 => 138,  335 => 135,  328 => 131,  322 => 128,  318 => 126,  314 => 125,  297 => 110,  293 => 108,  291 => 107,  288 => 106,  286 => 105,  283 => 104,  281 => 103,  278 => 102,  276 => 101,  273 => 100,  271 => 99,  268 => 98,  266 => 97,  257 => 91,  251 => 88,  245 => 85,  239 => 82,  233 => 79,  227 => 76,  215 => 66,  211 => 64,  208 => 63,  197 => 62,  195 => 61,  182 => 51,  178 => 50,  174 => 49,  170 => 48,  166 => 47,  162 => 46,  158 => 45,  154 => 44,  150 => 43,  146 => 42,  142 => 41,  138 => 40,  134 => 39,  130 => 38,  126 => 37,  111 => 25,  107 => 23,  101 => 22,  95 => 18,  86 => 15,  83 => 14,  79 => 13,  76 => 12,  67 => 9,  64 => 8,  60 => 7,  57 => 6,  51 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}{{ users.getTotalItemCount }} User{% if users.getTotalItemCount != 1 %}s{% endif %}{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}
    
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
            {{ users.getTotalItemCount }} User{% if users.getTotalItemCount != 1 %}s{% endif %}
        </h1>
        <ol class=\"breadcrumb\">
            <li><a href=\"{{ path('god_mode_dashboard') }}\"><i class=\"fa fa-dashboard\"></i> God Mode Dashboard</a></li>
            <li class=\"active\"><i class=\"fa fa-users\"></i> Users</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">
            <!-- Mini Side Bar -->
            <div class=\"col-sm-3\">
                <div class=\"list-group\">
                    <a href=\"{{ path('create_admin') }}\" class=\"list-group-item\">Create Admin</a>
                    <a href=\"{{ path('list_admin') }}\" class=\"list-group-item\">List Admin</a>
                    <a href=\"{{ path('list_users') }}\" class=\"list-group-item active\">List Users</a>
                    <a href=\"{{ path('create_pack') }}\" class=\"list-group-item\">Create Pack</a>
                    <a href=\"{{ path('list_pack') }}\" class=\"list-group-item\">List Pack</a>
                    <a href=\"{{ path('list_pack_subs') }}\" class=\"list-group-item\">List Pack Subs</a>
                    <a href=\"{{ path('list_admin_payments') }}\" class=\"list-group-item\">List Payments</a>
                    <a href=\"{{ path('create_news') }}\" class=\"list-group-item\">Create News</a>
                    <a href=\"{{ path('list_news') }}\" class=\"list-group-item\">List News</a>
                    <a href=\"{{ path('set_notice_board') }}\" class=\"list-group-item\">Set Notice Board</a>
                    <a href=\"{{ path('unblock_user') }}\" class=\"list-group-item\">Unblock User</a>
                    <a href=\"{{ path('reset_password') }}\" class=\"list-group-item\">Reset Password</a>
                    <a href=\"{{ path('god_mode_compose_mail') }}\" class=\"list-group-item\">Compose Mail</a>
                    <a href=\"{{ path('god_mode_mails') }}\" class=\"list-group-item\">List Mail</a>
                    <a href=\"{{ path('list_approve_reviews') }}\" class=\"list-group-item\">Approve Testimonies</a>
                </div>
            </div> <!-- End Mini Side Bar -->

            <!-- Main Content -->
            <div class=\"col-sm-9\">
                <div class=\"row\">
                    <div class=\"col-sm-2\">
                    </div>
                    <div class=\"col-sm-8 text-center lead\">
                        {% if users.getTotalItemCount != 0 %}
                        Showing {{ users.getPaginationData.firstItemNumber }} to {{ users.getPaginationData.lastItemNumber }} out of {{ users.getTotalItemCount }} User{% if users.getTotalItemCount != 1 %}s{% endif %}
                        {% else %}
                            0 Users
                        {% endif %}
                    </div>
                    <div class=\"col-sm-2\">
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        <div class=\"dropdown\" style=\"display:inline-block\">
                            <button class=\"btn btn-warning dropdown-toggle\" type=\"button\" id=\"sortMenu\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Sort By <span class=\"caret\"></span></button>
                            <ul class=\"dropdown-menu\" aria-labelledby=\"sortMenu\">
                                <li>
                                    <a href=\"{{ path('list_users', {'page' : page, 'sort' : 'date_DESC'}) }}\">Created Date DESC</a>
                                </li>
                                <li>
                                    <a href=\"{{ path('list_users', {'page' : page, 'sort' : 'date_ASC'}) }}\">Created Date ASC</a>
                                </li>
                                <li>
                                    <a href=\"{{ path('list_users', {'page' : page, 'sort' : 'name_DESC'}) }}\">Name DESC</a>
                                </li>
                                <li>
                                    <a href=\"{{ path('list_users', {'page' : page, 'sort' : 'name_ASC'}) }}\">Name ASC</a>
                                </li>
                                <li>
                                    <a href=\"{{ path('list_users', {'page' : page, 'sort' : 'username_DESC'}) }}\">Username DESC</a>
                                </li>
                                <li>
                                    <a href=\"{{ path('list_users', {'page' : page, 'sort' : 'username_ASC'}) }}\">Username ASC</a>
                                </li>
                            </ul>
                        </div>
                        <span>
                            <strong>Sorting By: </strong>
                            {% if sort == 'date_DESC' %}
                                Created Date DESC
                            {% elseif sort == 'date_ASC' %}
                                Created Date ASC
                            {% elseif sort == 'name_DESC' %}
                                Name DESC
                            {% elseif sort == 'name_ASC' %}
                                Name ASC
                            {% elseif sort == 'username_DESC' %}
                                Username DESC
                            {% elseif sort == 'username_ASC' %}
                                Username ASC
                            {% endif %}
                        </span>
                    </div>
                </div>
                <table class=\"table table-hover table-responsive table-striped\">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Phone Number</th>
                            <th>Status</th>
                            <th>Options</th>
                            <th>Created</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% for user in users %}
                            <tr>
                                <td>
                                    {{ user.getName() }}
                                </td>
                                <td>
                                    {{ user.getUsername() }}
                                </td>

                                <td>
                                    {{ user.getPhoneOne() }}
                                </td>
                                <td>
                                    {% if user.getStatus() == 'active' %}
                                        <span class=\"text-success\">Active</span>
                                    {% else %}
                                        <span class=\"text-danger\">Blocked</span>
                                    {% endif %}
                                </td>
                                <td>
                                    {% if user.getStatus() == 'active' %}
                                        <a href=\"{{ path('do_block_user', {'user' : user.getId(), 'page' : page, 'sort' : sort}) }}\" class=\"btn btn-xs btn-flat btn-danger\">Block User</a>
                                    {% else %}
                                        <a href=\"{{ path('do_activate_user', {'user' : user.getId(), 'page' : page, 'sort' : sort}) }}\" class=\"btn btn-xs btn-flat btn-success\">Activate User</a>
                                    {% endif %}
                                </td>
                                <td>
                                    {{ user.getCreated()|date('d M. Y H:i') }}
                                </td>
                                <td>
                                    <a href=\"{{ path('edit_user', {'user' : user.getId()}) }}\" class=\"btn btn-xs btn-warning\">Edit</a>
                                </td>
                            </tr>
                        {% endfor %}
                    </tbody>
                </table>

                <div class=\"navigation text-center\">
                    {{ knp_pagination_render(users) }}
                </div>
            </div> <!-- End Main Content -->
        </div>

    </section> <!-- End Main content -->
{% endblock %}", "god-mode/list-users.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\list-users.html.twig");
    }
}
