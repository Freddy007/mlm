<?php

/* public/login.html.twig */
class __TwigTemplate_ba90f42251eb04a6966c906425afc0a0e4273f5d5b6b88fecaf5ad3dea84bf07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("public.base.html.twig", "public/login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "public.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e86bc2214bb0de72d00205f0b1960a54e8b732d8698c1b4133051f7f1f5d2f96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e86bc2214bb0de72d00205f0b1960a54e8b732d8698c1b4133051f7f1f5d2f96->enter($__internal_e86bc2214bb0de72d00205f0b1960a54e8b732d8698c1b4133051f7f1f5d2f96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e86bc2214bb0de72d00205f0b1960a54e8b732d8698c1b4133051f7f1f5d2f96->leave($__internal_e86bc2214bb0de72d00205f0b1960a54e8b732d8698c1b4133051f7f1f5d2f96_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b88aa6307bc7edae79b8fe5f1a3c4e1aa8af436aa010a25ef59d8ab232f777e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b88aa6307bc7edae79b8fe5f1a3c4e1aa8af436aa010a25ef59d8ab232f777e8->enter($__internal_b88aa6307bc7edae79b8fe5f1a3c4e1aa8af436aa010a25ef59d8ab232f777e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Sign In";
        
        $__internal_b88aa6307bc7edae79b8fe5f1a3c4e1aa8af436aa010a25ef59d8ab232f777e8->leave($__internal_b88aa6307bc7edae79b8fe5f1a3c4e1aa8af436aa010a25ef59d8ab232f777e8_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_6e742566b561bf9a8066d34667592521b286752f6f16ee5679c6dcd3051b75d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e742566b561bf9a8066d34667592521b286752f6f16ee5679c6dcd3051b75d0->enter($__internal_6e742566b561bf9a8066d34667592521b286752f6f16ee5679c6dcd3051b75d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Sign In</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">
                <div class=\"login-box\" style=\"margin: 0 auto !important\">
                    <!-- /.login-logo -->
                    <div class=\"login-box-body\">
                        ";
        // line 17
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 18
            echo "                            <div class=\"alert alert-danger\">
                                ";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array())), "html", null, true);
            echo "
                            </div>
                        ";
        }
        // line 22
        echo "
                        <form action=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("security_login_check");
        echo "\" method=\"post\">
                            <div class=\"form-group has-feedback\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"username\" value=\"";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\">
                                <span class=\"fa fa-user-o form-control-feedback\"></span>
                            </div>
                            <div class=\"form-group has-feedback\">
                                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\">
                                <span class=\"fa fa-key form-control-feedback\"></span>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-xs-8\">
                                    <div class=\"checkbox icheck\">
                                        <label>
                                            <input type=\"checkbox\" name=\"_remember_me\"> Remember Me
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class=\"col-xs-4\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Sign In</button>
                                </div>
                                <!-- /.col -->
                            </div>
                            <input type=\"hidden\" name=\"ver\" value=\"u\">
                        </form>


                        <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("forgot_password");
        echo "\">Forgot password</a> | <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register_pack");
        echo "\" class=\"text-center\">Register</a>
                    </div>
                    <!-- /.login-box-body -->
                </div>
                <!-- /.login-box -->
            </div>
        </div>
    </section>

";
        
        $__internal_6e742566b561bf9a8066d34667592521b286752f6f16ee5679c6dcd3051b75d0->leave($__internal_6e742566b561bf9a8066d34667592521b286752f6f16ee5679c6dcd3051b75d0_prof);

    }

    public function getTemplateName()
    {
        return "public/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 50,  85 => 25,  80 => 23,  77 => 22,  71 => 19,  68 => 18,  66 => 17,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'public.base.html.twig' %}

{% block title %}Sign In{% endblock %}

{% block body %}

    <section>
        <div class=\"container\">
            <div class=\"row\">
                <h2 class=\"text-center\">Sign In</h2>
                <hr class=\"star-primary\">
            </div>
            <div class=\"row\">
                <div class=\"login-box\" style=\"margin: 0 auto !important\">
                    <!-- /.login-logo -->
                    <div class=\"login-box-body\">
                        {% if error %}
                            <div class=\"alert alert-danger\">
                                {{ error.messageKey|trans(error.messageData) }}
                            </div>
                        {% endif %}

                        <form action=\"{{ path('security_login_check') }}\" method=\"post\">
                            <div class=\"form-group has-feedback\">
                                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" name=\"username\" value=\"{{ last_username }}\">
                                <span class=\"fa fa-user-o form-control-feedback\"></span>
                            </div>
                            <div class=\"form-group has-feedback\">
                                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"password\">
                                <span class=\"fa fa-key form-control-feedback\"></span>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-xs-8\">
                                    <div class=\"checkbox icheck\">
                                        <label>
                                            <input type=\"checkbox\" name=\"_remember_me\"> Remember Me
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class=\"col-xs-4\">
                                    <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Sign In</button>
                                </div>
                                <!-- /.col -->
                            </div>
                            <input type=\"hidden\" name=\"ver\" value=\"u\">
                        </form>


                        <a href=\"{{ path('forgot_password') }}\">Forgot password</a> | <a href=\"{{ path('register_pack') }}\" class=\"text-center\">Register</a>
                    </div>
                    <!-- /.login-box-body -->
                </div>
                <!-- /.login-box -->
            </div>
        </div>
    </section>

{% endblock %}", "public/login.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public\\login.html.twig");
    }
}
