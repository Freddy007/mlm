<?php

/* member.base.html.twig */
class __TwigTemplate_0cbc01c3bf0d64b0c5388ca3d3dbd9eaebf7fc19c327a115d92fa7a18dd59205 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'modalPicHeader' => array($this, 'block_modalPicHeader'),
            'modalPicBody' => array($this, 'block_modalPicBody'),
            'modalConfirmHeader' => array($this, 'block_modalConfirmHeader'),
            'modalConfirmBody' => array($this, 'block_modalConfirmBody'),
            'modalConfirmLink' => array($this, 'block_modalConfirmLink'),
            'modalCancelHeader' => array($this, 'block_modalCancelHeader'),
            'modalCancelBody' => array($this, 'block_modalCancelBody'),
            'modalCancelLink' => array($this, 'block_modalCancelLink'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_960c844d4e56f475ee8f73e6d0abaaff70c1592a076b3e88ee9f6608264a4d34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_960c844d4e56f475ee8f73e6d0abaaff70c1592a076b3e88ee9f6608264a4d34->enter($__internal_960c844d4e56f475ee8f73e6d0abaaff70c1592a076b3e88ee9f6608264a4d34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member.base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo " | ";
        echo twig_escape_filter($this->env, (isset($context["app_name"]) ? $context["app_name"] : $this->getContext($context, "app_name")), "html", null, true);
        echo "</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">

    <!-- Favicon and Touch Icons -->
    ";
        // line 12
        echo "    ";
        // line 13
        echo "    ";
        // line 14
        echo "    ";
        // line 15
        echo "    ";
        // line 16
        echo "    
    ";
        // line 17
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 47
        echo "    
    <!-- CountDown Timer -->
    <script type=\"text/javascript\">

       function getTimeRemaining(endtime) {
            //var t = Date.parse(endtime) - Date.parse(new Date());
            var t = new Date(endtime.replace(/-/g,'/').replace('T‌​',' ').replace(/\\..*|\\+.*/,\"\")) - new Date();
            var seconds = Math.floor( (t/1000) % 60 );
            var minutes = Math.floor( (t/1000/60) % 60 );
            var hours = Math.floor( (t/(1000*60*60)));
            return {
                'total': t,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, \$hoursClass, \$minutesClass, \$secondsClass, endtime) {
            var clock = document.getElementById(id);

            var hoursSpan = clock.querySelector('.' + \$hoursClass);
            var minutesSpan = clock.querySelector('.' + \$minutesClass);
            var secondsSpan = clock.querySelector('.' + \$secondsClass);

            function updateClock() {
                var t = getTimeRemaining(endtime);

                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if(t.total<=0){

                    hoursSpan.innerHTML = ('00');
                    minutesSpan.innerHTML = ('00');
                    secondsSpan.innerHTML = ('00');

                    clearInterval(timeinterval);
                }
            }

            updateClock(); // run function once at first to avoid delay
            var timeinterval = setInterval(updateClock,1000);

        }
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
</head>
<body class=\"hold-transition skin-black-light sidebar-mini\">

    <!-- Page Wrapper -->
    <div class=\"wrapper\">

        ";
        // line 107
        $this->loadTemplate("member/member.page-top-header.html.twig", "member.base.html.twig", 107)->display($context);
        // line 108
        echo "
        ";
        // line 109
        $this->loadTemplate("member/member.page-main-sidebar.html.twig", "member.base.html.twig", 109)->display($context);
        // line 110
        echo "
        <!-- Content Wrapper. Contains page content -->
        <div class=\"content-wrapper\">
            ";
        // line 113
        $this->displayBlock('body', $context, $blocks);
        // line 115
        echo "        </div> <!-- End Content Wrapper -->

        ";
        // line 117
        $this->loadTemplate("member/member.page-main-footer.html.twig", "member.base.html.twig", 117)->display($context);
        // line 118
        echo "
    </div> <!-- End Page Wrapper -->

    <!-- Pic Modal -->
    <div class=\"modal modal-warning\" id=\"viewPicModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">×</span></button>
                    <h4 class=\"modal-title\">";
        // line 128
        $this->displayBlock('modalPicHeader', $context, $blocks);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    <p class=\"text-center\">";
        // line 131
        $this->displayBlock('modalPicBody', $context, $blocks);
        echo "</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Close</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div>

    <!-- Confirm Modal -->
    <div class=\"modal modal-success\" id=\"viewConfirmModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">×</span></button>
                    <h4 class=\"modal-title\">";
        // line 147
        $this->displayBlock('modalConfirmHeader', $context, $blocks);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    <p class=\"text-center\">";
        // line 150
        $this->displayBlock('modalConfirmBody', $context, $blocks);
        echo "</p>
                </div>
                <div class=\"modal-footer\">
                    ";
        // line 153
        $this->displayBlock('modalConfirmLink', $context, $blocks);
        // line 154
        echo "                    <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div>

    <!-- Cancel Modal -->
    <div class=\"modal modal-danger\" id=\"viewCancelModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">×</span></button>
                    <h4 class=\"modal-title\">";
        // line 167
        $this->displayBlock('modalCancelHeader', $context, $blocks);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    <p class=\"text-center\">";
        // line 170
        $this->displayBlock('modalCancelBody', $context, $blocks);
        echo "</p>
                </div>
                <div class=\"modal-footer\">
                    ";
        // line 173
        $this->displayBlock('modalCancelLink', $context, $blocks);
        // line 174
        echo "                    <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div>



    ";
        // line 182
        $this->displayBlock('javascripts', $context, $blocks);
        // line 221
        echo "
</body>
</html>";
        
        $__internal_960c844d4e56f475ee8f73e6d0abaaff70c1592a076b3e88ee9f6608264a4d34->leave($__internal_960c844d4e56f475ee8f73e6d0abaaff70c1592a076b3e88ee9f6608264a4d34_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_9640f5e7c95beb2901579b814280b1b9fe68a6b865a8debf3a514360e836493a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9640f5e7c95beb2901579b814280b1b9fe68a6b865a8debf3a514360e836493a->enter($__internal_9640f5e7c95beb2901579b814280b1b9fe68a6b865a8debf3a514360e836493a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_9640f5e7c95beb2901579b814280b1b9fe68a6b865a8debf3a514360e836493a->leave($__internal_9640f5e7c95beb2901579b814280b1b9fe68a6b865a8debf3a514360e836493a_prof);

    }

    // line 17
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_d869cd8e0d60dcc976a9d552d1984ec24740cedb5d414e6560631e98bdd3d787 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d869cd8e0d60dcc976a9d552d1984ec24740cedb5d414e6560631e98bdd3d787->enter($__internal_d869cd8e0d60dcc976a9d552d1984ec24740cedb5d414e6560631e98bdd3d787_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 18
        echo "        <!-- Bootstrap 3.3.6 -->
        <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\">
        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo "\">
        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/ionicons.min.css"), "html", null, true);
        echo "\">
        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("dist/css/AdminLTE.min.css"), "html", null, true);
        echo "\">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("dist/css/skins/skin-black-light.min.css"), "html", null, true);
        echo "\">
        <!-- iCheck -->
        <link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/iCheck/flat/blue.css"), "html", null, true);
        echo "\">
        <!-- Morris chart -->
        <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/morris/morris.css"), "html", null, true);
        echo "\">
        <!-- jvectormap -->
        <link rel=\"stylesheet\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/jvectormap/jquery-jvectormap-1.2.2.css"), "html", null, true);
        echo "\">
        <!-- Date Picker -->
        <link rel=\"stylesheet\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/datepicker/datepicker3.css"), "html", null, true);
        echo "\">
        <!-- Daterange picker -->
        <link rel=\"stylesheet\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/daterangepicker/daterangepicker.css"), "html", null, true);
        echo "\">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel=\"stylesheet\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"), "html", null, true);
        echo "\">
        <!-- Theme override style -->
        <link rel=\"stylesheet\" href=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("dist/css/overrides.css"), "html", null, true);
        echo "\">

        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css\" />

        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css\" />
    ";
        
        $__internal_d869cd8e0d60dcc976a9d552d1984ec24740cedb5d414e6560631e98bdd3d787->leave($__internal_d869cd8e0d60dcc976a9d552d1984ec24740cedb5d414e6560631e98bdd3d787_prof);

    }

    // line 113
    public function block_body($context, array $blocks = array())
    {
        $__internal_ab7cb651eaa21e1624ced816ff557564a2b0c30cdf6eeaabdd704fbef1ea742b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab7cb651eaa21e1624ced816ff557564a2b0c30cdf6eeaabdd704fbef1ea742b->enter($__internal_ab7cb651eaa21e1624ced816ff557564a2b0c30cdf6eeaabdd704fbef1ea742b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 114
        echo "            ";
        
        $__internal_ab7cb651eaa21e1624ced816ff557564a2b0c30cdf6eeaabdd704fbef1ea742b->leave($__internal_ab7cb651eaa21e1624ced816ff557564a2b0c30cdf6eeaabdd704fbef1ea742b_prof);

    }

    // line 128
    public function block_modalPicHeader($context, array $blocks = array())
    {
        $__internal_859d190c5e740faddf8467922f0e206976252646ededa2cf0df78ad4a08f2648 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_859d190c5e740faddf8467922f0e206976252646ededa2cf0df78ad4a08f2648->enter($__internal_859d190c5e740faddf8467922f0e206976252646ededa2cf0df78ad4a08f2648_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalPicHeader"));

        
        $__internal_859d190c5e740faddf8467922f0e206976252646ededa2cf0df78ad4a08f2648->leave($__internal_859d190c5e740faddf8467922f0e206976252646ededa2cf0df78ad4a08f2648_prof);

    }

    // line 131
    public function block_modalPicBody($context, array $blocks = array())
    {
        $__internal_f7929c459af534d5673eb7c6f278b705b668db8b54a5f687dfba4922d86c3457 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7929c459af534d5673eb7c6f278b705b668db8b54a5f687dfba4922d86c3457->enter($__internal_f7929c459af534d5673eb7c6f278b705b668db8b54a5f687dfba4922d86c3457_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalPicBody"));

        
        $__internal_f7929c459af534d5673eb7c6f278b705b668db8b54a5f687dfba4922d86c3457->leave($__internal_f7929c459af534d5673eb7c6f278b705b668db8b54a5f687dfba4922d86c3457_prof);

    }

    // line 147
    public function block_modalConfirmHeader($context, array $blocks = array())
    {
        $__internal_a38cdfc664012ad96b3d90e89f7cc0e40d2e248f653d9dc0a5908d381cb2d007 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a38cdfc664012ad96b3d90e89f7cc0e40d2e248f653d9dc0a5908d381cb2d007->enter($__internal_a38cdfc664012ad96b3d90e89f7cc0e40d2e248f653d9dc0a5908d381cb2d007_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalConfirmHeader"));

        
        $__internal_a38cdfc664012ad96b3d90e89f7cc0e40d2e248f653d9dc0a5908d381cb2d007->leave($__internal_a38cdfc664012ad96b3d90e89f7cc0e40d2e248f653d9dc0a5908d381cb2d007_prof);

    }

    // line 150
    public function block_modalConfirmBody($context, array $blocks = array())
    {
        $__internal_8c09c158ce6fd53b6197089c6f20bcf459d3f2825315cde810d70b7752a9f5ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c09c158ce6fd53b6197089c6f20bcf459d3f2825315cde810d70b7752a9f5ce->enter($__internal_8c09c158ce6fd53b6197089c6f20bcf459d3f2825315cde810d70b7752a9f5ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalConfirmBody"));

        
        $__internal_8c09c158ce6fd53b6197089c6f20bcf459d3f2825315cde810d70b7752a9f5ce->leave($__internal_8c09c158ce6fd53b6197089c6f20bcf459d3f2825315cde810d70b7752a9f5ce_prof);

    }

    // line 153
    public function block_modalConfirmLink($context, array $blocks = array())
    {
        $__internal_b48a17f136b8150eeef3889a2c0dbb81ff4d48050843bf2d94a09ce20c484038 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b48a17f136b8150eeef3889a2c0dbb81ff4d48050843bf2d94a09ce20c484038->enter($__internal_b48a17f136b8150eeef3889a2c0dbb81ff4d48050843bf2d94a09ce20c484038_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalConfirmLink"));

        
        $__internal_b48a17f136b8150eeef3889a2c0dbb81ff4d48050843bf2d94a09ce20c484038->leave($__internal_b48a17f136b8150eeef3889a2c0dbb81ff4d48050843bf2d94a09ce20c484038_prof);

    }

    // line 167
    public function block_modalCancelHeader($context, array $blocks = array())
    {
        $__internal_2f4981e8179b07b298c615ed1aa1264bb964700fd65c95bd86ffb93e16fe5aef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2f4981e8179b07b298c615ed1aa1264bb964700fd65c95bd86ffb93e16fe5aef->enter($__internal_2f4981e8179b07b298c615ed1aa1264bb964700fd65c95bd86ffb93e16fe5aef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelHeader"));

        
        $__internal_2f4981e8179b07b298c615ed1aa1264bb964700fd65c95bd86ffb93e16fe5aef->leave($__internal_2f4981e8179b07b298c615ed1aa1264bb964700fd65c95bd86ffb93e16fe5aef_prof);

    }

    // line 170
    public function block_modalCancelBody($context, array $blocks = array())
    {
        $__internal_f5008571f7812f64f43ab969be83eaf33e72ae43cc1fbbbe9ed16c71ff6076f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5008571f7812f64f43ab969be83eaf33e72ae43cc1fbbbe9ed16c71ff6076f9->enter($__internal_f5008571f7812f64f43ab969be83eaf33e72ae43cc1fbbbe9ed16c71ff6076f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelBody"));

        
        $__internal_f5008571f7812f64f43ab969be83eaf33e72ae43cc1fbbbe9ed16c71ff6076f9->leave($__internal_f5008571f7812f64f43ab969be83eaf33e72ae43cc1fbbbe9ed16c71ff6076f9_prof);

    }

    // line 173
    public function block_modalCancelLink($context, array $blocks = array())
    {
        $__internal_506f1d2c22571a19949383855031136a16a06fe006bdd8318732c3b9cfe00c86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_506f1d2c22571a19949383855031136a16a06fe006bdd8318732c3b9cfe00c86->enter($__internal_506f1d2c22571a19949383855031136a16a06fe006bdd8318732c3b9cfe00c86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "modalCancelLink"));

        
        $__internal_506f1d2c22571a19949383855031136a16a06fe006bdd8318732c3b9cfe00c86->leave($__internal_506f1d2c22571a19949383855031136a16a06fe006bdd8318732c3b9cfe00c86_prof);

    }

    // line 182
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_8e24e676e3f0361dba361cdef1d4b3bcadcebf66b1d73b9baa5f45db1c2d245f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e24e676e3f0361dba361cdef1d4b3bcadcebf66b1d73b9baa5f45db1c2d245f->enter($__internal_8e24e676e3f0361dba361cdef1d4b3bcadcebf66b1d73b9baa5f45db1c2d245f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 183
        echo "        <!-- jQuery -->
        <script src=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.1.0.min.js"), "html", null, true);
        echo "\"></script>
        <!-- jQuery UI -->
        <script src=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/jQueryUI/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          \$.widget.bridge('uibutton', \$.ui.button);
        </script>
        <!-- Bootstrap -->
        <script src=\"";
        // line 192
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Sparkline -->
        <script src=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/sparkline/jquery.sparkline.min.js"), "html", null, true);
        echo "\"></script>
        <!-- jvectormap -->
        <script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/jvectormap/jquery-jvectormap-world-mill-en.js"), "html", null, true);
        echo "\"></script>
        <!-- jQuery Knob Chart -->
        <script src=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/knob/jquery.knob.js"), "html", null, true);
        echo "\"></script>
        <!-- datepicker -->
        <script src=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/datepicker/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src=\"";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"), "html", null, true);
        echo "\"></script>
        <!-- Slimscroll -->
        <script src=\"";
        // line 205
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/slimScroll/jquery.slimscroll.min.js"), "html", null, true);
        echo "\"></script>
        <!-- FastClick -->
        <script src=\"";
        // line 207
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/fastclick/fastclick.js"), "html", null, true);
        echo "\"></script>
        <!-- AdminLTE App -->
        <script src=\"";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("dist/js/app.min.js"), "html", null, true);
        echo "\"></script>

        <script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>

        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js\"></script>

        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js\"></script>

        <script>
            \$('#query').select2();
        </script>
    ";
        
        $__internal_8e24e676e3f0361dba361cdef1d4b3bcadcebf66b1d73b9baa5f45db1c2d245f->leave($__internal_8e24e676e3f0361dba361cdef1d4b3bcadcebf66b1d73b9baa5f45db1c2d245f_prof);

    }

    public function getTemplateName()
    {
        return "member.base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  503 => 209,  498 => 207,  493 => 205,  488 => 203,  483 => 201,  478 => 199,  473 => 197,  469 => 196,  464 => 194,  459 => 192,  450 => 186,  445 => 184,  442 => 183,  436 => 182,  425 => 173,  414 => 170,  403 => 167,  392 => 153,  381 => 150,  370 => 147,  359 => 131,  348 => 128,  341 => 114,  335 => 113,  322 => 41,  317 => 39,  312 => 37,  307 => 35,  302 => 33,  297 => 31,  292 => 29,  287 => 27,  282 => 25,  277 => 23,  272 => 21,  267 => 19,  264 => 18,  258 => 17,  247 => 6,  238 => 221,  236 => 182,  226 => 174,  224 => 173,  218 => 170,  212 => 167,  197 => 154,  195 => 153,  189 => 150,  183 => 147,  164 => 131,  158 => 128,  146 => 118,  144 => 117,  140 => 115,  138 => 113,  133 => 110,  131 => 109,  128 => 108,  126 => 107,  64 => 47,  62 => 17,  59 => 16,  57 => 15,  55 => 14,  53 => 13,  51 => 12,  41 => 6,  34 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <title>{% block title %}{% endblock %} | {{ app_name }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">

    <!-- Favicon and Touch Icons -->
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"shortcut icon\" type=\"image/png\">#}
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"apple-touch-icon\">#}
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"apple-touch-icon\" sizes=\"72x72\">#}
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"apple-touch-icon\" sizes=\"72x72\">#}
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"apple-touch-icon\" sizes=\"72x72\">#}
    
    {% block stylesheets %}
        <!-- Bootstrap 3.3.6 -->
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\">
        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-awesome.min.css') }}\">
        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"{{ asset('css/ionicons.min.css') }}\">
        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"{{ asset('dist/css/AdminLTE.min.css') }}\">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel=\"stylesheet\" href=\"{{ asset('dist/css/skins/skin-black-light.min.css') }}\">
        <!-- iCheck -->
        <link rel=\"stylesheet\" href=\"{{ asset('plugins/iCheck/flat/blue.css') }}\">
        <!-- Morris chart -->
        <link rel=\"stylesheet\" href=\"{{ asset('plugins/morris/morris.css') }}\">
        <!-- jvectormap -->
        <link rel=\"stylesheet\" href=\"{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}\">
        <!-- Date Picker -->
        <link rel=\"stylesheet\" href=\"{{ asset('plugins/datepicker/datepicker3.css') }}\">
        <!-- Daterange picker -->
        <link rel=\"stylesheet\" href=\"{{ asset('plugins/daterangepicker/daterangepicker.css') }}\">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel=\"stylesheet\" href=\"{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}\">
        <!-- Theme override style -->
        <link rel=\"stylesheet\" href=\"{{ asset('dist/css/overrides.css') }}\">

        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css\" />

        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css\" />
    {% endblock %}
    
    <!-- CountDown Timer -->
    <script type=\"text/javascript\">

       function getTimeRemaining(endtime) {
            //var t = Date.parse(endtime) - Date.parse(new Date());
            var t = new Date(endtime.replace(/-/g,'/').replace('T‌​',' ').replace(/\\..*|\\+.*/,\"\")) - new Date();
            var seconds = Math.floor( (t/1000) % 60 );
            var minutes = Math.floor( (t/1000/60) % 60 );
            var hours = Math.floor( (t/(1000*60*60)));
            return {
                'total': t,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }

        function initializeClock(id, \$hoursClass, \$minutesClass, \$secondsClass, endtime) {
            var clock = document.getElementById(id);

            var hoursSpan = clock.querySelector('.' + \$hoursClass);
            var minutesSpan = clock.querySelector('.' + \$minutesClass);
            var secondsSpan = clock.querySelector('.' + \$secondsClass);

            function updateClock() {
                var t = getTimeRemaining(endtime);

                hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
                minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
                secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

                if(t.total<=0){

                    hoursSpan.innerHTML = ('00');
                    minutesSpan.innerHTML = ('00');
                    secondsSpan.innerHTML = ('00');

                    clearInterval(timeinterval);
                }
            }

            updateClock(); // run function once at first to avoid delay
            var timeinterval = setInterval(updateClock,1000);

        }
    </script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
</head>
<body class=\"hold-transition skin-black-light sidebar-mini\">

    <!-- Page Wrapper -->
    <div class=\"wrapper\">

        {% include('member/member.page-top-header.html.twig') %}

        {% include('member/member.page-main-sidebar.html.twig') %}

        <!-- Content Wrapper. Contains page content -->
        <div class=\"content-wrapper\">
            {% block body %}
            {% endblock %}
        </div> <!-- End Content Wrapper -->

        {% include('member/member.page-main-footer.html.twig') %}

    </div> <!-- End Page Wrapper -->

    <!-- Pic Modal -->
    <div class=\"modal modal-warning\" id=\"viewPicModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">×</span></button>
                    <h4 class=\"modal-title\">{% block modalPicHeader %}{% endblock %}</h4>
                </div>
                <div class=\"modal-body\">
                    <p class=\"text-center\">{% block modalPicBody %}{% endblock %}</p>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Close</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div>

    <!-- Confirm Modal -->
    <div class=\"modal modal-success\" id=\"viewConfirmModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">×</span></button>
                    <h4 class=\"modal-title\">{% block modalConfirmHeader %}{% endblock %}</h4>
                </div>
                <div class=\"modal-body\">
                    <p class=\"text-center\">{% block modalConfirmBody %}{% endblock %}</p>
                </div>
                <div class=\"modal-footer\">
                    {% block modalConfirmLink %}{% endblock %}
                    <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div>

    <!-- Cancel Modal -->
    <div class=\"modal modal-danger\" id=\"viewCancelModal\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">×</span></button>
                    <h4 class=\"modal-title\">{% block modalCancelHeader %}{% endblock %}</h4>
                </div>
                <div class=\"modal-body\">
                    <p class=\"text-center\">{% block modalCancelBody %}{% endblock %}</p>
                </div>
                <div class=\"modal-footer\">
                    {% block modalCancelLink %}{% endblock %}
                    <button type=\"button\" class=\"btn btn-outline pull-left\" data-dismiss=\"modal\">Cancel</button>
                </div>
            </div> <!-- /.modal-content -->
        </div> <!-- /.modal-dialog -->
    </div>



    {% block javascripts %}
        <!-- jQuery -->
        <script src=\"{{ asset('js/jquery-3.1.0.min.js') }}\"></script>
        <!-- jQuery UI -->
        <script src=\"{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}\"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          \$.widget.bridge('uibutton', \$.ui.button);
        </script>
        <!-- Bootstrap -->
        <script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
        <!-- Sparkline -->
        <script src=\"{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}\"></script>
        <!-- jvectormap -->
        <script src=\"{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}\"></script>
        <script src=\"{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}\"></script>
        <!-- jQuery Knob Chart -->
        <script src=\"{{ asset('plugins/knob/jquery.knob.js') }}\"></script>
        <!-- datepicker -->
        <script src=\"{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}\"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src=\"{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}\"></script>
        <!-- Slimscroll -->
        <script src=\"{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}\"></script>
        <!-- FastClick -->
        <script src=\"{{ asset('plugins/fastclick/fastclick.js') }}\"></script>
        <!-- AdminLTE App -->
        <script src=\"{{ asset('dist/js/app.min.js') }}\"></script>

        <script src=\"https://unpkg.com/sweetalert/dist/sweetalert.min.js\"></script>

        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js\"></script>

        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js\"></script>

        <script>
            \$('#query').select2();
        </script>
    {% endblock %}

</body>
</html>", "member.base.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\member.base.html.twig");
    }
}
