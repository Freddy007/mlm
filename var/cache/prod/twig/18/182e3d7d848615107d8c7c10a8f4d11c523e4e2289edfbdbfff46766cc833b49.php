<?php

/* member/list-jury.html.twig */
class __TwigTemplate_33610b4bc38e007b652bc1a24120b6a3180a1175cd3f276368b96c54524f6370 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "member/list-jury.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0000d5257d1a0f14b0cb8be82e4daa75eb47dfedec0c65968f90766077b5ea1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0000d5257d1a0f14b0cb8be82e4daa75eb47dfedec0c65968f90766077b5ea1e->enter($__internal_0000d5257d1a0f14b0cb8be82e4daa75eb47dfedec0c65968f90766077b5ea1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "member/list-jury.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0000d5257d1a0f14b0cb8be82e4daa75eb47dfedec0c65968f90766077b5ea1e->leave($__internal_0000d5257d1a0f14b0cb8be82e4daa75eb47dfedec0c65968f90766077b5ea1e_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_489815ca00254c3698805086a99e71f116200f5ecbff2fd46dda63f108714256 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_489815ca00254c3698805086a99e71f116200f5ecbff2fd46dda63f108714256->enter($__internal_489815ca00254c3698805086a99e71f116200f5ecbff2fd46dda63f108714256_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Your Cases";
        
        $__internal_489815ca00254c3698805086a99e71f116200f5ecbff2fd46dda63f108714256->leave($__internal_489815ca00254c3698805086a99e71f116200f5ecbff2fd46dda63f108714256_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_714d7034cb65d7106f13036194e722c64f362f3b5503249e8b05a6f828675782 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_714d7034cb65d7106f13036194e722c64f362f3b5503249e8b05a6f828675782->enter($__internal_714d7034cb65d7106f13036194e722c64f362f3b5503249e8b05a6f828675782_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tYour Court Cases
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"";
        // line 12
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("dashboard");
        echo "\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-university\"></i> Court</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">
          \t\t<!-- small box -->
          \t\t<div class=\"small-box bg-purple\">
            \t\t<div class=\"inner\">
              \t\t\t<h3>";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["courtCount"]) ? $context["courtCount"] : $this->getContext($context, "courtCount")), "html", null, true);
        echo "</h3>
              \t\t\t<p>Court Cases</p>
            \t\t</div>
            \t\t<div class=\"icon\">
              \t\t\t<i class=\"fa fa-university\"></i>
            \t\t</div>
            \t\t<a class=\"small-box-footer\">Court Cases <i class=\"fa fa-arrow-circle-right\"></i></a>
          \t\t</div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
                    <a href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("jury");
        echo "\" class=\"list-group-item list-group-item-success\">View Cases</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
    \t\t\t";
        // line 42
        if (($this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getTotalItemCount", array()) == 0)) {
            // line 43
            echo "    \t\t\t\t<div class=\"alert alert-info\">
    \t\t\t\t\t<p class=\"lead text-center\">You have no court cases</p>
    \t\t\t\t\t<p>Court cases only appear here when you have a payment dispute with another member. Payment Disputes range from \"Upline not confirming payment before the given time elapses\" to \"Downline presenting payment evidence that has been marked as dubious by the upline\". When you have disputes of such, your court cases will appear here.</p>
    \t\t\t\t</div>
    \t\t\t";
        } else {
            // line 48
            echo "\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getPaginationData", array()), "firstItemNumber", array()), "html", null, true);
            echo " to ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getPaginationData", array()), "lastItemNumber", array()), "html", null, true);
            echo " out of ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getTotalItemCount", array()), "html", null, true);
            echo " Case";
            if (($this->getAttribute((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")), "getTotalItemCount", array()) != 1)) {
                echo "s";
            }
            echo "</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>#</th>
\t    \t\t\t\t\t\t\t\t<th>Receiver</th>
\t    \t\t\t\t\t\t\t\t<th>Feeder</th>
\t    \t\t\t\t\t\t\t\t<th>Status</th>
\t    \t\t\t\t\t\t\t\t<th>Date</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")));
            foreach ($context['_seq'] as $context["_key"] => $context["case"]) {
                // line 67
                echo "\t    \t\t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t\t<td><a href=\"";
                // line 68
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_case", array("case" => $this->getAttribute($context["case"], "getId", array(), "method"))), "html", null, true);
                echo "\">Case #";
                echo twig_escape_filter($this->env, $this->getAttribute($context["case"], "getId", array(), "method"), "html", null, true);
                echo "</a></td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["case"], "getReceiver", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 70
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["case"], "getFeeder", array(), "method"), "getName", array(), "method"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t            ";
                // line 72
                if (($this->getAttribute($context["case"], "getStatus", array(), "method") == "giver_reply")) {
                    // line 73
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-info\">Feeder Reply</button>
\t\t\t\t\t\t\t\t\t            ";
                } elseif (($this->getAttribute(                // line 74
$context["case"], "getStatus", array(), "method") == "receiver_reply")) {
                    // line 75
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-primary\">Receiver Reply</button>
\t\t\t\t\t\t\t\t\t            ";
                } elseif (($this->getAttribute(                // line 76
$context["case"], "getStatus", array(), "method") == "admin_reply")) {
                    // line 77
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-warning\">Admin Reply</button>
\t\t\t\t\t\t\t\t\t            ";
                } elseif (($this->getAttribute(                // line 78
$context["case"], "getStatus", array(), "method") == "open")) {
                    // line 79
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-success\">Open</button>
\t\t\t\t\t\t\t\t\t            ";
                } elseif (($this->getAttribute(                // line 80
$context["case"], "getStatus", array(), "method") == "closed")) {
                    // line 81
                    echo "\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-danger\">Closed</button>
\t\t\t\t\t\t\t\t\t            ";
                }
                // line 83
                echo "\t    \t\t\t\t\t\t\t\t\t</td>
\t    \t\t\t\t\t\t\t\t\t<td>";
                // line 84
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["case"], "getCreatedDate", array(), "method"), "d F, Y"), "html", null, true);
                echo "</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['case'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 87
            echo "\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    ";
            // line 93
            echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, (isset($context["cases"]) ? $context["cases"] : $this->getContext($context, "cases")));
            echo "
\t                </div>
    \t\t\t";
        }
        // line 96
        echo "    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section> <!-- End Main Content -->

";
        
        $__internal_714d7034cb65d7106f13036194e722c64f362f3b5503249e8b05a6f828675782->leave($__internal_714d7034cb65d7106f13036194e722c64f362f3b5503249e8b05a6f828675782_prof);

    }

    public function getTemplateName()
    {
        return "member/list-jury.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 96,  208 => 93,  200 => 87,  191 => 84,  188 => 83,  184 => 81,  182 => 80,  179 => 79,  177 => 78,  174 => 77,  172 => 76,  169 => 75,  167 => 74,  164 => 73,  162 => 72,  157 => 70,  153 => 69,  147 => 68,  144 => 67,  140 => 66,  113 => 50,  109 => 48,  102 => 43,  100 => 42,  91 => 36,  77 => 25,  61 => 12,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}Your Cases{% endblock %}

{% block body %}
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
      \t<h1>
        \tYour Court Cases
      \t</h1>
      \t<ol class=\"breadcrumb\">
        \t<li><a href=\"{{ path('dashboard') }}\"><i class=\"fa fa-dashboard\"></i> Dashboard</a></li>
        \t<li class=\"active\"><i class=\"fa fa-university\"></i> Court</li>
      \t</ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">
    \t<div class=\"row\">
    \t\t<!-- Mini Side Bar -->
    \t\t<div class=\"col-sm-3\">
          \t\t<!-- small box -->
          \t\t<div class=\"small-box bg-purple\">
            \t\t<div class=\"inner\">
              \t\t\t<h3>{{ courtCount }}</h3>
              \t\t\t<p>Court Cases</p>
            \t\t</div>
            \t\t<div class=\"icon\">
              \t\t\t<i class=\"fa fa-university\"></i>
            \t\t</div>
            \t\t<a class=\"small-box-footer\">Court Cases <i class=\"fa fa-arrow-circle-right\"></i></a>
          \t\t</div> <!-- End small box -->

          \t\t<!-- Links -->
          \t\t<div class=\"list-group\">
                    <a href=\"{{ path('jury') }}\" class=\"list-group-item list-group-item-success\">View Cases</a>
          \t\t</div> <!-- End Links -->
    \t\t</div> <!-- End Mini Side Bar -->

    \t\t<!-- Mini  Main Content -->
    \t\t<div class=\"col-sm-9\">
    \t\t\t{% if cases.getTotalItemCount == 0 %}
    \t\t\t\t<div class=\"alert alert-info\">
    \t\t\t\t\t<p class=\"lead text-center\">You have no court cases</p>
    \t\t\t\t\t<p>Court cases only appear here when you have a payment dispute with another member. Payment Disputes range from \"Upline not confirming payment before the given time elapses\" to \"Downline presenting payment evidence that has been marked as dubious by the upline\". When you have disputes of such, your court cases will appear here.</p>
    \t\t\t\t</div>
    \t\t\t{% else %}
\t    \t\t\t<div class=\"box\">
\t    \t\t\t\t<div class=\"box-header with-border text-center\">
\t    \t\t\t\t\t<h3 class=\"box-title\">Showing {{ cases.getPaginationData.firstItemNumber }} to {{ cases.getPaginationData.lastItemNumber }} out of {{ cases.getTotalItemCount }} Case{% if cases.getTotalItemCount != 1 %}s{% endif %}</h3>
\t    \t\t\t\t</div>

\t    \t\t\t\t<div class=\"box-body\">
\t    \t\t\t\t\t<table class=\"table table-responsive table-hover table-striped\">
\t    \t\t\t\t\t\t<thead>
\t    \t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t<th>#</th>
\t    \t\t\t\t\t\t\t\t<th>Receiver</th>
\t    \t\t\t\t\t\t\t\t<th>Feeder</th>
\t    \t\t\t\t\t\t\t\t<th>Status</th>
\t    \t\t\t\t\t\t\t\t<th>Date</th>
\t    \t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t</thead>

\t    \t\t\t\t\t\t<tbody>
\t    \t\t\t\t\t\t\t{% for case in cases %}
\t    \t\t\t\t\t\t\t\t<tr>
\t    \t\t\t\t\t\t\t\t\t<td><a href=\"{{ path('user_case', {'case' : case.getId()}) }}\">Case #{{ case.getId() }}</a></td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ case.getReceiver().getName() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ case.getFeeder().getName() }}</td>
\t    \t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t            {% if case.getStatus() == 'giver_reply' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-info\">Feeder Reply</button>
\t\t\t\t\t\t\t\t\t            {% elseif case.getStatus() == 'receiver_reply' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-primary\">Receiver Reply</button>
\t\t\t\t\t\t\t\t\t            {% elseif case.getStatus() == 'admin_reply' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-warning\">Admin Reply</button>
\t\t\t\t\t\t\t\t\t            {% elseif case.getStatus() == 'open' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-success\">Open</button>
\t\t\t\t\t\t\t\t\t            {% elseif case.getStatus() == 'closed' %}
\t\t\t\t\t\t\t\t\t                <button class=\"btn btn-xs btn-danger\">Closed</button>
\t\t\t\t\t\t\t\t\t            {% endif %}
\t    \t\t\t\t\t\t\t\t\t</td>
\t    \t\t\t\t\t\t\t\t\t<td>{{ case.getCreatedDate()|date('d F, Y') }}</td>
\t    \t\t\t\t\t\t\t\t</tr>
\t    \t\t\t\t\t\t\t{% endfor %}
\t    \t\t\t\t\t\t</tbody>
\t    \t\t\t\t\t</table>
\t    \t\t\t\t</div>
\t    \t\t\t</div>

\t                <div class=\"navigation text-center\">
\t                    {{ knp_pagination_render(cases) }}
\t                </div>
    \t\t\t{% endif %}
    \t\t</div> <!-- End Mini Main Content -->
    \t</div>
    </section> <!-- End Main Content -->

{% endblock %}", "member/list-jury.html.twig", "C:\\laragon\\www\\mainscript\\app\\Resources\\views\\member\\list-jury.html.twig");
    }
}
