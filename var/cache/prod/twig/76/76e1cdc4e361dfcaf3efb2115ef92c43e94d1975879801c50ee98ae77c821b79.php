<?php

/* god-mode/settings.html.twig */
class __TwigTemplate_49b82b58441903a37da6d28644540cce5d6da6394570905528ad21a8343c6932 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("member.base.html.twig", "god-mode/settings.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "member.base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d45f05ef5720f4f4ea944a7512693ef5ad34f5a68024afc337663d1094175f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d45f05ef5720f4f4ea944a7512693ef5ad34f5a68024afc337663d1094175f6->enter($__internal_4d45f05ef5720f4f4ea944a7512693ef5ad34f5a68024afc337663d1094175f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "god-mode/settings.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4d45f05ef5720f4f4ea944a7512693ef5ad34f5a68024afc337663d1094175f6->leave($__internal_4d45f05ef5720f4f4ea944a7512693ef5ad34f5a68024afc337663d1094175f6_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_8880ccab6c83048ea3f0f66eae671eadf501325d20cb829bd2f99ec9c4acd15b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8880ccab6c83048ea3f0f66eae671eadf501325d20cb829bd2f99ec9c4acd15b->enter($__internal_8880ccab6c83048ea3f0f66eae671eadf501325d20cb829bd2f99ec9c4acd15b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "God Mode Dashboard";
        
        $__internal_8880ccab6c83048ea3f0f66eae671eadf501325d20cb829bd2f99ec9c4acd15b->leave($__internal_8880ccab6c83048ea3f0f66eae671eadf501325d20cb829bd2f99ec9c4acd15b_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_4e89430594b9f84009f6bd9ac3c06e385adf37671aa7ed5098fc8aa8bce8a3b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e89430594b9f84009f6bd9ac3c06e385adf37671aa7ed5098fc8aa8bce8a3b9->enter($__internal_4e89430594b9f84009f6bd9ac3c06e385adf37671aa7ed5098fc8aa8bce8a3b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 8
            echo "        <div class=\"callout callout-success\">
            <p>";
            // line 9
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 14
            echo "        <div class=\"callout callout-danger\">
            <p>";
            // line 15
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
        </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
           ";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["app_name"]) ? $context["app_name"] : $this->getContext($context, "app_name")), "html", null, true);
        echo " Settings Page
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"active\"><i class=\"fa fa-cog\"></i>  ";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["app_name"]) ? $context["app_name"] : $this->getContext($context, "app_name")), "html", null, true);
        echo " Settings Page</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">

            <!-- Left Side -->
            <div class=\"col-md-4\">
                <!-- Links -->
                <div class=\"list-group\">
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_settings");
        echo "\" class=\"list-group-item list-group-item-success\">Settings</a>
                    <a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminprofile");
        echo "\" class=\"list-group-item list-group-item\">Profile</a>
                    ";
        // line 41
        echo "                    <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("adminchange_password");
        echo "\" class=\"list-group-item\">Change Password</a>
                </div> <!-- End Links -->


            </div>

            <!-- Right Side -->
            <div class=\"col-md-8\">

                <div class=\"box box-info\">

                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Edit Personal Details</h3>
                    </div>

                    <div class=\"box-body\">

                        ";
        // line 58
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("god_mode_settings"), "attr" => array("class" => "form-horizontal")));
        echo "

                        ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "successNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 61
            echo "                            <div class=\"alert alert-success alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <p>";
            // line 63
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "
                        ";
        // line 67
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "get", array(0 => "errorNotice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flash_message"]) {
            // line 68
            echo "                            <div class=\"alert alert-danger alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <p>";
            // line 70
            echo twig_escape_filter($this->env, $context["flash_message"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash_message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "
                        <span class=\"text-danger\">";
        // line 74
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "envatoPurchasecode", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\"  class=\"col-sm-3 control-label\">";
        // line 76
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "envatoPurchasecode", array()), 'label');
        echo " </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 78
        if (($this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "envatoPurchasecode", array()) == "manualpeerupxyzinstall")) {
            echo "  ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "envatoPurchasecode", array()), 'widget', array("disabled" => "disabled"));
            echo " ";
        } else {
            echo " ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "envatoPurchasecode", array()), 'widget');
            echo " ";
        }
        // line 79
        echo "

                                <small class=\"pull-right\">Enter value here to change ";
        // line 81
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "envatoPurchasecode", array()), 'label');
        echo " </small>
                            </div>

                        </div>

                        <span class=\"text-danger\">";
        // line 86
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "siteurl", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">";
        // line 88
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "siteurl", array()), 'label');
        echo " </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 90
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "siteurl", array()), 'widget');
        echo "

                                <small class=\"pull-right\">Enter value here to change ";
        // line 92
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "siteurl", array()), 'label');
        echo " </small>
                            </div>

                        </div>

                        <span class=\"text-danger\">";
        // line 97
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "sitename", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">";
        // line 99
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "sitename", array()), 'label');
        echo " </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 101
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "sitename", array()), 'widget');
        echo "

                                <small class=\"pull-right\">Enter value here to change ";
        // line 103
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "sitename", array()), 'label');
        echo " </small>
                            </div>

                        </div>


                        <span class=\"text-danger\">";
        // line 109
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "sitemail", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">";
        // line 111
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "sitemail", array()), 'label');
        echo " </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 113
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "sitemail", array()), 'widget');
        echo "

                                <small class=\"pull-right\">Enter value here to change ";
        // line 115
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "sitemail", array()), 'label');
        echo " </small>
                            </div>

                        </div>



                        <div class=\"form-group\">
                            <label for=\"inputSiteLogo\" class=\"col-sm-2 control-label\">
                                ";
        // line 125
        echo "                            </label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\"><img class=\"img-responsive\" src=\"";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["adm"]) ? $context["adm"] : $this->getContext($context, "adm")), "sitelogo", array()), "html", null, true);
        echo "\" style=\"margin: 0; border: none;\"></p>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">
                                ";
        // line 134
        echo "                            </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 137
        echo "                            </div>
                        </div>
                        <span class=\"text-danger\">
                            ";
        // line 141
        echo "                        </span>
                        <div class=\"form-group\">
                            ";
        // line 144
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 147
        echo "                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"inputSiteLogo\" class=\"col-sm-2 control-label\">
                                ";
        // line 153
        echo "                            </label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\">
                                    <img class=\"img-responsive\"
                                                        ";
        // line 159
        echo "                                         style=\"margin: 0; background-color: lightgrey; border: none;\"></p>
                            </div>
                        </div>

                        ";
        // line 164
        echo "                            ";
        // line 165
        echo "                                ";
        // line 166
        echo "                            ";
        // line 167
        echo "                            ";
        // line 168
        echo "                                ";
        // line 169
        echo "                            ";
        // line 170
        echo "                        ";
        // line 171
        echo "
                        <span class=\"text-danger\">
                            ";
        // line 174
        echo "                        </span>
                        <div class=\"form-group\">
                            ";
        // line 177
        echo "
                            <div class=\"col-sm-10\">
                                ";
        // line 180
        echo "                            </div>
                        </div>

                        ";
        // line 184
        echo "                            ";
        // line 185
        echo "
                            ";
        // line 187
        echo "                                ";
        // line 188
        echo "                            ";
        // line 189
        echo "                        ";
        // line 190
        echo "                        ";
        // line 191
        echo "                            ";
        // line 192
        echo "                            ";
        // line 193
        echo "                                ";
        // line 194
        echo "                            ";
        // line 195
        echo "                        ";
        // line 196
        echo "
                        ";
        // line 198
        echo "                        ";
        // line 199
        echo "                            ";
        // line 200
        echo "
                            ";
        // line 202
        echo "                                ";
        // line 203
        echo "                            ";
        // line 204
        echo "                        ";
        // line 205
        echo "
                        <span class=\"text-danger\">";
        // line 206
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "matchMode", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">";
        // line 208
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "matchMode", array()), 'label');
        echo " </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 210
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "matchMode", array()), 'widget');
        echo "

                                <small class=\"pull-right\">Enter value here to change ";
        // line 212
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "matchMode", array()), 'label');
        echo " </small>
                            </div>

                        </div>

                        <span class=\"text-danger\">";
        // line 217
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrency", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">";
        // line 219
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrency", array()), 'label');
        echo " </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 221
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrency", array()), 'widget');
        echo "

                                <small class=\"pull-right\">Enter value here to change ";
        // line 223
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrency", array()), 'label');
        echo " </small>
                            </div>

                        </div>

                        <span class=\"text-danger\">";
        // line 228
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrencysymbol", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">";
        // line 230
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrencysymbol", array()), 'label');
        echo " </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 232
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrencysymbol", array()), 'widget');
        echo "

                                <small class=\"pull-right\">Enter value here to change ";
        // line 234
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrencysymbol", array()), 'label');
        echo " </small>
                            </div>

                        </div>


                        <span class=\"text-danger\">";
        // line 240
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrencyexchangerate", array()), 'errors');
        echo "</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">";
        // line 242
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrencyexchangerate", array()), 'label');
        echo " </label>
                            <div class=\"col-sm-9\">
                                ";
        // line 244
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrencyexchangerate", array()), 'widget');
        echo "

                                <small class=\"pull-right\">Enter value here to change ";
        // line 246
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), "appcurrencyexchangerate", array()), 'label');
        echo " </small>
                            </div>

                        </div>



                        <div class=\"row\">
                            <div class=\"col-sm-12\">
                                <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Save ";
        // line 255
        echo twig_escape_filter($this->env, (isset($context["app_name"]) ? $context["app_name"] : $this->getContext($context, "app_name")), "html", null, true);
        echo " Settings</button>
                            </div>
                        </div>

                        ";
        // line 259
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["admForm"]) ? $context["admForm"] : $this->getContext($context, "admForm")), 'form_end');
        echo "
                    </div>

                </div>

            </div>

        </div>

    </section>


";
        
        $__internal_4e89430594b9f84009f6bd9ac3c06e385adf37671aa7ed5098fc8aa8bce8a3b9->leave($__internal_4e89430594b9f84009f6bd9ac3c06e385adf37671aa7ed5098fc8aa8bce8a3b9_prof);

    }

    public function getTemplateName()
    {
        return "god-mode/settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  526 => 259,  519 => 255,  507 => 246,  502 => 244,  497 => 242,  492 => 240,  483 => 234,  478 => 232,  473 => 230,  468 => 228,  460 => 223,  455 => 221,  450 => 219,  445 => 217,  437 => 212,  432 => 210,  427 => 208,  422 => 206,  419 => 205,  417 => 204,  415 => 203,  413 => 202,  410 => 200,  408 => 199,  406 => 198,  403 => 196,  401 => 195,  399 => 194,  397 => 193,  395 => 192,  393 => 191,  391 => 190,  389 => 189,  387 => 188,  385 => 187,  382 => 185,  380 => 184,  375 => 180,  371 => 177,  367 => 174,  363 => 171,  361 => 170,  359 => 169,  357 => 168,  355 => 167,  353 => 166,  351 => 165,  349 => 164,  343 => 159,  336 => 153,  329 => 147,  325 => 144,  321 => 141,  316 => 137,  312 => 134,  304 => 128,  299 => 125,  287 => 115,  282 => 113,  277 => 111,  272 => 109,  263 => 103,  258 => 101,  253 => 99,  248 => 97,  240 => 92,  235 => 90,  230 => 88,  225 => 86,  217 => 81,  213 => 79,  203 => 78,  198 => 76,  193 => 74,  190 => 73,  181 => 70,  177 => 68,  173 => 67,  170 => 66,  161 => 63,  157 => 61,  153 => 60,  148 => 58,  127 => 41,  123 => 39,  119 => 38,  103 => 25,  97 => 22,  91 => 18,  82 => 15,  79 => 14,  75 => 13,  72 => 12,  63 => 9,  60 => 8,  56 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'member.base.html.twig' %}

{% block title %}God Mode Dashboard{% endblock %}

{% block body %}

    {% for flash_message in app.session.flashBag.get('successNotice') %}
        <div class=\"callout callout-success\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    {% for flash_message in app.session.flashBag.get('errorNotice') %}
        <div class=\"callout callout-danger\">
            <p>{{ flash_message }}</p>
        </div>
    {% endfor %}

    <!-- Content Header (Page header) -->
    <section class=\"content-header\">
        <h1>
           {{ app_name }} Settings Page
        </h1>
        <ol class=\"breadcrumb\">
            <li class=\"active\"><i class=\"fa fa-cog\"></i>  {{ app_name }} Settings Page</li>
        </ol>
    </section> <!-- End Content Header (Page header) -->

    <!-- Main content -->
    <section class=\"content\">

        <div class=\"row\">

            <!-- Left Side -->
            <div class=\"col-md-4\">
                <!-- Links -->
                <div class=\"list-group\">
                    <a href=\"{{ path('god_mode_settings') }}\" class=\"list-group-item list-group-item-success\">Settings</a>
                    <a href=\"{{ path('adminprofile') }}\" class=\"list-group-item list-group-item\">Profile</a>
                    {#<a href=\"{{ path('adminbank_details') }}\" class=\"list-group-item\">Bank Details</a>#}
                    <a href=\"{{ path('adminchange_password') }}\" class=\"list-group-item\">Change Password</a>
                </div> <!-- End Links -->


            </div>

            <!-- Right Side -->
            <div class=\"col-md-8\">

                <div class=\"box box-info\">

                    <div class=\"box-header with-border\">
                        <h3 class=\"box-title\">Edit Personal Details</h3>
                    </div>

                    <div class=\"box-body\">

                        {{ form_start(admForm, {'action': path('god_mode_settings'), 'attr': {'class': 'form-horizontal'}}) }}

                        {% for flash_message in app.session.flashBag.get('successNotice') %}
                            <div class=\"alert alert-success alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <p>{{ flash_message }}</p>
                            </div>
                        {% endfor %}

                        {% for flash_message in app.session.flashBag.get('errorNotice') %}
                            <div class=\"alert alert-danger alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <p>{{ flash_message }}</p>
                            </div>
                        {% endfor %}

                        <span class=\"text-danger\">{{ form_errors(admForm.envatoPurchasecode) }}</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\"  class=\"col-sm-3 control-label\">{{ form_label(admForm.envatoPurchasecode) }} </label>
                            <div class=\"col-sm-9\">
                                {% if adm.envatoPurchasecode == 'manualpeerupxyzinstall' %}  {{ form_widget(admForm.envatoPurchasecode, { disabled:'disabled'}) }} {% else %} {{ form_widget(admForm.envatoPurchasecode) }} {% endif %}


                                <small class=\"pull-right\">Enter value here to change {{ form_label(admForm.envatoPurchasecode) }} </small>
                            </div>

                        </div>

                        <span class=\"text-danger\">{{ form_errors(admForm.siteurl) }}</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">{{ form_label(admForm.siteurl) }} </label>
                            <div class=\"col-sm-9\">
                                {{ form_widget(admForm.siteurl) }}

                                <small class=\"pull-right\">Enter value here to change {{ form_label(admForm.siteurl) }} </small>
                            </div>

                        </div>

                        <span class=\"text-danger\">{{ form_errors(admForm.sitename) }}</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">{{ form_label(admForm.sitename) }} </label>
                            <div class=\"col-sm-9\">
                                {{ form_widget(admForm.sitename) }}

                                <small class=\"pull-right\">Enter value here to change {{ form_label(admForm.sitename) }} </small>
                            </div>

                        </div>


                        <span class=\"text-danger\">{{ form_errors(admForm.sitemail) }}</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">{{ form_label(admForm.sitemail) }} </label>
                            <div class=\"col-sm-9\">
                                {{ form_widget(admForm.sitemail) }}

                                <small class=\"pull-right\">Enter value here to change {{ form_label(admForm.sitemail) }} </small>
                            </div>

                        </div>



                        <div class=\"form-group\">
                            <label for=\"inputSiteLogo\" class=\"col-sm-2 control-label\">
                                {#{{ form_label(admForm.sitelogo) }}#}
                            </label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\"><img class=\"img-responsive\" src=\"{{ adm.sitelogo }}\" style=\"margin: 0; border: none;\"></p>
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">
                                {#{{ form_label(admForm.sitelogo) }} #}
                            </label>
                            <div class=\"col-sm-9\">
                                {#{{ form_widget(admForm.sitelogo) }}#}
                            </div>
                        </div>
                        <span class=\"text-danger\">
                            {#{{ form_errors(admForm.sitelogoImage) }}#}
                        </span>
                        <div class=\"form-group\">
                            {#{{ form_label(admForm.sitelogoImage, 'Change SiteLogo', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}#}

                            <div class=\"col-sm-10\">
                                {#{{ form_widget(admForm.sitelogoImage) }}#}
                            </div>
                        </div>

                        <div class=\"form-group\">
                            <label for=\"inputSiteLogo\" class=\"col-sm-2 control-label\">
                                {#{{ form_label(admForm.sitelogolight) }}#}
                            </label>

                            <div class=\"col-sm-10\">
                                <p class=\"form-control-static\">
                                    <img class=\"img-responsive\"
                                                        {#src=\"{{ adm.sitelogolight }}\" #}
                                         style=\"margin: 0; background-color: lightgrey; border: none;\"></p>
                            </div>
                        </div>

                        {#<div class=\"form-group\">#}
                            {#<label for=\"inputSiteName\" class=\"col-sm-3 control-label\">#}
                                {#{{ form_label(admForm.sitelogolight) }} #}
                            {#</label>#}
                            {#<div class=\"col-sm-9\">#}
                                {#{{ form_widget(admForm.sitelogolight) }}#}
                            {#</div>#}
                        {#</div>#}

                        <span class=\"text-danger\">
                            {#{{ form_errors(admForm.sitelogolightImage) }}#}
                        </span>
                        <div class=\"form-group\">
                            {#{{ form_label(admForm.sitelogolightImage, 'Change Site Light Logo', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}#}

                            <div class=\"col-sm-10\">
                                {#{{ form_widget(admForm.sitelogolightImage) }}#}
                            </div>
                        </div>

                        {#<div class=\"form-group\">#}
                            {#<label for=\"inputSiteLogo\" class=\"col-sm-2 control-label\">{{ form_label(admForm.sitesmallicon) }}</label>#}

                            {#<div class=\"col-sm-10\">#}
                                {#<p class=\"form-control-static\"><img class=\"img-responsive\" src=\"{{ adm.sitesmallicon }}\" style=\"margin: 0; border: none;\"></p>#}
                            {#</div>#}
                        {#</div>#}
                        {#<div class=\"form-group\">#}
                            {#<label for=\"inputSiteName\" class=\"col-sm-3 control-label\">{{ form_label(admForm.sitesmallicon) }} </label>#}
                            {#<div class=\"col-sm-9\">#}
                                {#{{ form_widget(admForm.sitesmallicon) }}#}
                            {#</div>#}
                        {#</div>#}

                        {#<span class=\"text-danger\">{{ form_errors(admForm.sitesmalliconimage) }}</span>#}
                        {#<div class=\"form-group\">#}
                            {#{{ form_label(admForm.sitesmalliconimage, 'Change Site Light Logo', { 'label_attr': {'class': 'col-sm-2 control-label'} }) }}#}

                            {#<div class=\"col-sm-10\">#}
                                {#{{ form_widget(admForm.sitesmalliconimage) }}#}
                            {#</div>#}
                        {#</div>#}

                        <span class=\"text-danger\">{{ form_errors(admForm.matchMode) }}</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">{{ form_label(admForm.matchMode) }} </label>
                            <div class=\"col-sm-9\">
                                {{ form_widget(admForm.matchMode) }}

                                <small class=\"pull-right\">Enter value here to change {{ form_label(admForm.matchMode) }} </small>
                            </div>

                        </div>

                        <span class=\"text-danger\">{{ form_errors(admForm.appcurrency) }}</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">{{ form_label(admForm.appcurrency) }} </label>
                            <div class=\"col-sm-9\">
                                {{ form_widget(admForm.appcurrency) }}

                                <small class=\"pull-right\">Enter value here to change {{ form_label(admForm.appcurrency) }} </small>
                            </div>

                        </div>

                        <span class=\"text-danger\">{{ form_errors(admForm.appcurrencysymbol) }}</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">{{ form_label(admForm.appcurrencysymbol) }} </label>
                            <div class=\"col-sm-9\">
                                {{ form_widget(admForm.appcurrencysymbol) }}

                                <small class=\"pull-right\">Enter value here to change {{ form_label(admForm.appcurrencysymbol) }} </small>
                            </div>

                        </div>


                        <span class=\"text-danger\">{{ form_errors(admForm.appcurrencyexchangerate) }}</span>
                        <div class=\"form-group\">
                            <label for=\"inputSiteName\" class=\"col-sm-3 control-label\">{{ form_label(admForm.appcurrencyexchangerate) }} </label>
                            <div class=\"col-sm-9\">
                                {{ form_widget(admForm.appcurrencyexchangerate) }}

                                <small class=\"pull-right\">Enter value here to change {{ form_label(admForm.appcurrencyexchangerate) }} </small>
                            </div>

                        </div>



                        <div class=\"row\">
                            <div class=\"col-sm-12\">
                                <button type=\"submit\" class=\"btn btn-primary btn-block btn-flat\">Save {{ app_name }} Settings</button>
                            </div>
                        </div>

                        {{ form_end(admForm) }}
                    </div>

                </div>

            </div>

        </div>

    </section>


{% endblock %}", "god-mode/settings.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\god-mode\\settings.html.twig");
    }
}
