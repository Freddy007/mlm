<?php

/* public.base.html.twig */
class __TwigTemplate_25d7d41bc2427d44669cdcbdfb4e4bd9814693f07ed23fb11b38dde5693d8809 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_271a6dcd1d7193d22c0f0be497be158adc44286a5eb72f6b56953caa25bc430f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_271a6dcd1d7193d22c0f0be497be158adc44286a5eb72f6b56953caa25bc430f->enter($__internal_271a6dcd1d7193d22c0f0be497be158adc44286a5eb72f6b56953caa25bc430f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "public.base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\" style=\"background-color:#fff\">

<head>

    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"Trusted and Genuine Returns\">
    <meta name=\"author\" content=\"";
        // line 10
        echo twig_escape_filter($this->env, ($context["app_name"] ?? $this->getContext($context, "app_name")), "html", null, true);
        echo "\">

    <!-- Favicon and Touch Icons -->
    ";
        // line 14
        echo "    ";
        // line 15
        echo "    ";
        // line 16
        echo "    ";
        // line 17
        echo "    ";
        // line 18
        echo "
    <title>";
        // line 19
        echo twig_escape_filter($this->env, ($context["app_name"] ?? $this->getContext($context, "app_name")), "html", null, true);
        echo " - </title>

    <!-- Bootstrap Core CSS -->
    <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Theme CSS -->
    <link href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/freelancer.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Theme style -->
    <link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("dist/css/AdminLTE.min.css"), "html", null, true);
        echo "\">
    <!-- iCheck -->
    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/iCheck/square/blue.css"), "html", null, true);
        echo "\">
    <!-- Fonts -->
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:300\" rel=\"stylesheet\">
    <link href=\"https://fonts.googleapis.com/css?family=Titillium+Web:900\" rel=\"stylesheet\">
    <link href=\"https://fonts.googleapis.com/css?family=Cherry+Swash:700\" rel=\"stylesheet\">

    <!-- Custom Fonts -->
    <link href=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/ionicons.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">

    <style type=\"text/css\">
        @media (max-width: 425px) {
            .media-body {
                display: block !important;
                width: 100% !important;
            }
            .navbar-brand img {
                max-width: 244px !important;
            }
        }
    </style>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>

    <![endif]-->

</head>

<body id=\"page-top\" class=\"index hold-transition ";
        // line 66
        if (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "register")) {
            echo "register-page";
        } elseif (($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method") == "login")) {
            echo "login-page";
        }
        echo "\" style=\"background-color:#fff\">

    <div id=\"skipnav\"><a href=\"#maincontent\">Skip to main content</a></div>

    ";
        // line 70
        $this->loadTemplate("public/public.nav.html.twig", "public.base.html.twig", 70)->display($context);
        // line 71
        echo "
    ";
        // line 72
        $this->displayBlock('body', $context, $blocks);
        // line 73
        echo "
    ";
        // line 74
        $this->loadTemplate("public/public.footer.html.twig", "public.base.html.twig", 74)->display($context);
        // line 75
        echo "
    <!-- jQuery -->
    <script type=\"text/javascript\" src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.1.0.min.js"), "html", null, true);
        echo "\"></script>

    <script type=\"text/javascript\" src=\"//www.wipixar.com/wipixar/livechat/php/app.php?widget-init.js\"></script>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css\" rel=\"stylesheet\" />
    <!-- Loading jquery here-->

    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js\"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type=\"text/javascript\" src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

    <!-- Plugin JavaScript -->
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js\"></script>

    <!-- Contact Form JavaScript -->
    <script src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jqBootstrapValidation.js"), "html", null, true);
        echo "\"></script>

    <!-- Theme JavaScript -->
    <script src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/freelancer.min.js"), "html", null, true);
        echo "\"></script>

    <!-- iCheck -->
    <script src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("plugins/iCheck/icheck.min.js"), "html", null, true);
        echo "\"></script>
    
    <script>
      \$(function () {
        \$('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });

      \$('select[data-select=\"true\"]').select2();



    </script>

</body>

</html>";
        
        $__internal_271a6dcd1d7193d22c0f0be497be158adc44286a5eb72f6b56953caa25bc430f->leave($__internal_271a6dcd1d7193d22c0f0be497be158adc44286a5eb72f6b56953caa25bc430f_prof);

    }

    // line 72
    public function block_body($context, array $blocks = array())
    {
        $__internal_7a89bbd2ba6d718dd7cc46eea8fe31943061be2954208b685b99e7bec9378b2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a89bbd2ba6d718dd7cc46eea8fe31943061be2954208b685b99e7bec9378b2f->enter($__internal_7a89bbd2ba6d718dd7cc46eea8fe31943061be2954208b685b99e7bec9378b2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_7a89bbd2ba6d718dd7cc46eea8fe31943061be2954208b685b99e7bec9378b2f->leave($__internal_7a89bbd2ba6d718dd7cc46eea8fe31943061be2954208b685b99e7bec9378b2f_prof);

    }

    public function getTemplateName()
    {
        return "public.base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 72,  182 => 98,  176 => 95,  170 => 92,  161 => 86,  149 => 77,  145 => 75,  143 => 74,  140 => 73,  138 => 72,  135 => 71,  133 => 70,  122 => 66,  92 => 39,  88 => 38,  78 => 31,  73 => 29,  67 => 26,  63 => 25,  57 => 22,  51 => 19,  48 => 18,  46 => 17,  44 => 16,  42 => 15,  40 => 14,  34 => 10,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\" style=\"background-color:#fff\">

<head>

    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"Trusted and Genuine Returns\">
    <meta name=\"author\" content=\"{{ app_name }}\">

    <!-- Favicon and Touch Icons -->
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"shortcut icon\" type=\"image/png\">#}
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"apple-touch-icon\">#}
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"apple-touch-icon\" sizes=\"72x72\">#}
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"apple-touch-icon\" sizes=\"72x72\">#}
    {#<link href=\"{{ asset('img/favicon.png') }}\" rel=\"apple-touch-icon\" sizes=\"72x72\">#}

    <title>{{ app_name }} - </title>

    <!-- Bootstrap Core CSS -->
    <link href=\"{{ asset('css/bootstrap.min.css') }}\" rel=\"stylesheet\">

    <!-- Theme CSS -->
    <link href=\"{{ asset('css/freelancer.min.css') }}\" rel=\"stylesheet\">
    <link href=\"{{ asset('css/style.css') }}\" rel=\"stylesheet\">

    <!-- Theme style -->
    <link rel=\"stylesheet\" href=\"{{ asset('dist/css/AdminLTE.min.css') }}\">
    <!-- iCheck -->
    <link rel=\"stylesheet\" href=\"{{ asset('plugins/iCheck/square/blue.css') }}\">
    <!-- Fonts -->
    <link href=\"https://fonts.googleapis.com/css?family=Catamaran:300\" rel=\"stylesheet\">
    <link href=\"https://fonts.googleapis.com/css?family=Titillium+Web:900\" rel=\"stylesheet\">
    <link href=\"https://fonts.googleapis.com/css?family=Cherry+Swash:700\" rel=\"stylesheet\">

    <!-- Custom Fonts -->
    <link href=\"{{ asset('css/font-awesome.min.css') }}\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"{{ asset('css/ionicons.min.css') }}\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700\" rel=\"stylesheet\" type=\"text/css\">
    <link href=\"https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">

    <style type=\"text/css\">
        @media (max-width: 425px) {
            .media-body {
                display: block !important;
                width: 100% !important;
            }
            .navbar-brand img {
                max-width: 244px !important;
            }
        }
    </style>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>

    <![endif]-->

</head>

<body id=\"page-top\" class=\"index hold-transition {% if app.request.get('_route') == 'register' %}register-page{% elseif app.request.get('_route') == 'login' %}login-page{% endif %}\" style=\"background-color:#fff\">

    <div id=\"skipnav\"><a href=\"#maincontent\">Skip to main content</a></div>

    {% include('public/public.nav.html.twig') %}

    {% block body %}{% endblock %}

    {% include('public/public.footer.html.twig') %}

    <!-- jQuery -->
    <script type=\"text/javascript\" src=\"{{ asset('js/jquery-3.1.0.min.js') }}\"></script>

    <script type=\"text/javascript\" src=\"//www.wipixar.com/wipixar/livechat/php/app.php?widget-init.js\"></script>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css\" rel=\"stylesheet\" />
    <!-- Loading jquery here-->

    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js\"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type=\"text/javascript\" src=\"{{ asset('js/bootstrap.min.js') }}\"></script>

    <!-- Plugin JavaScript -->
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js\"></script>

    <!-- Contact Form JavaScript -->
    <script src=\"{{ asset('js/jqBootstrapValidation.js') }}\"></script>

    <!-- Theme JavaScript -->
    <script src=\"{{ asset('js/freelancer.min.js') }}\"></script>

    <!-- iCheck -->
    <script src=\"{{ asset('plugins/iCheck/icheck.min.js') }}\"></script>
    
    <script>
      \$(function () {
        \$('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });

      \$('select[data-select=\"true\"]').select2();



    </script>

</body>

</html>", "public.base.html.twig", "C:\\laragon\\www\\paidlife\\app\\Resources\\views\\public.base.html.twig");
    }
}
