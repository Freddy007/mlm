<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/god-mode')) {
            // move_jury_cases
            if ($pathinfo === '/god-mode/move-jury-cases') {
                return array (  '_controller' => 'AppBundle\\Controller\\CronController::moveToJuryAction',  '_route' => 'move_jury_cases',);
            }

            // block_scam_users
            if ($pathinfo === '/god-mode/block-scam-users') {
                return array (  '_controller' => 'AppBundle\\Controller\\CronController::blockScamUsersAction',  '_route' => 'block_scam_users',);
            }

        }

        if (0 === strpos($pathinfo, '/demi-god-mode')) {
            if (0 === strpos($pathinfo, '/demi-god-mode/support')) {
                // close_support
                if (0 === strpos($pathinfo, '/demi-god-mode/support/close') && preg_match('#^/demi\\-god\\-mode/support/close/(?P<support>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'close_support')), array (  '_controller' => 'AppBundle\\Controller\\DemiGodController::closeSupportAction',));
                }

                // demi_god_view_support
                if (0 === strpos($pathinfo, '/demi-god-mode/support/view') && preg_match('#^/demi\\-god\\-mode/support/view/(?P<support>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'demi_god_view_support')), array (  '_controller' => 'AppBundle\\Controller\\DemiGodController::viewSupportAction',));
                }

                // demi_god_support
                if (preg_match('#^/demi\\-god\\-mode/support(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'demi_god_support')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\DemiGodController::supportAction',));
                }

            }

            if (0 === strpos($pathinfo, '/demi-god-mode/jury')) {
                if (0 === strpos($pathinfo, '/demi-god-mode/jury/case')) {
                    // demi_god_jury_confirm_payment
                    if (preg_match('#^/demi\\-god\\-mode/jury/case/(?P<jury>[^/]++)/confirm\\-payment$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'demi_god_jury_confirm_payment')), array (  '_controller' => 'AppBundle\\Controller\\DemiGodController::juryConfirmPaymentAction',));
                    }

                    // demi_god_jury_cancel_payment
                    if (preg_match('#^/demi\\-god\\-mode/jury/case/(?P<jury>[^/]++)/cancel\\-payment$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'demi_god_jury_cancel_payment')), array (  '_controller' => 'AppBundle\\Controller\\DemiGodController::juryCancelPaymentAction',));
                    }

                    // demi_god_case
                    if (preg_match('#^/demi\\-god\\-mode/jury/case/(?P<jury>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'demi_god_case')), array (  '_controller' => 'AppBundle\\Controller\\DemiGodController::demiGodCase',));
                    }

                }

                // demi_god_jury
                if (preg_match('#^/demi\\-god\\-mode/jury(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'demi_god_jury')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\DemiGodController::juryAction',));
                }

            }

            // demi_god_reviews
            if ($pathinfo === '/demi-god-mode/reviews') {
                return array (  '_controller' => 'AppBundle\\Controller\\DemiGodController::reviewsAction',  '_route' => 'demi_god_reviews',);
            }

        }

        if (0 === strpos($pathinfo, '/god-mode')) {
            // delete_transactions
            if (rtrim($pathinfo, '/') === '/god-mode/delete-transactions') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'delete_transactions');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\GodController::deleteTransactionsAction',  '_route' => 'delete_transactions',);
            }

            // god_mode_settings
            if (rtrim($pathinfo, '/') === '/god-mode-settings') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'god_mode_settings');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\GodController::godSettingsAction',  '_route' => 'god_mode_settings',);
            }

            // adminchange_password
            if ($pathinfo === '/god-mode/change-password') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::changePasswordAction',  '_route' => 'adminchange_password',);
            }

            // adminprofile
            if ($pathinfo === '/god-mode/profile') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::adminProfileAction',  '_route' => 'adminprofile',);
            }

            if (0 === strpos($pathinfo, '/god-mode/bank-details')) {
                // adminbank_details
                if ($pathinfo === '/god-mode/bank-details') {
                    return array (  '_controller' => 'AppBundle\\Controller\\GodController::bankDetailsAction',  '_route' => 'adminbank_details',);
                }

                // adminbank_details_two
                if ($pathinfo === '/god-mode/bank-details-two') {
                    return array (  '_controller' => 'AppBundle\\Controller\\GodController::bankDetailsTwoAction',  '_route' => 'adminbank_details_two',);
                }

            }

            // god_mode_dashboard
            if ($pathinfo === '/god-mode') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::godDashboardAction',  '_route' => 'god_mode_dashboard',);
            }

            // list_super_admin
            if (0 === strpos($pathinfo, '/god-mode/list-super-admin') && preg_match('#^/god\\-mode/list\\-super\\-admin(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_super_admin')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::listSuperAdminAction',));
            }

            // edit_super_admin
            if (0 === strpos($pathinfo, '/god-mode/edit-super-admin') && preg_match('#^/god\\-mode/edit\\-super\\-admin/(?P<admin>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_super_admin')), array (  '_controller' => 'AppBundle\\Controller\\GodController::editSuperAdminAction',));
            }

            // create_admin
            if ($pathinfo === '/god-mode/create-admin') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::createAdminAction',  '_route' => 'create_admin',);
            }

            if (0 === strpos($pathinfo, '/god-mode/edit-')) {
                // edit_admin
                if (0 === strpos($pathinfo, '/god-mode/edit-admin') && preg_match('#^/god\\-mode/edit\\-admin/(?P<admin>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_admin')), array (  '_controller' => 'AppBundle\\Controller\\GodController::editAdminAction',));
                }

                // edit_user
                if (0 === strpos($pathinfo, '/god-mode/edit-user') && preg_match('#^/god\\-mode/edit\\-user/(?P<user>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_user')), array (  '_controller' => 'AppBundle\\Controller\\GodController::editUserAction',));
                }

            }

            // list_admin
            if (0 === strpos($pathinfo, '/god-mode/list-admin') && preg_match('#^/god\\-mode/list\\-admin(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_admin')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::listAdminAction',));
            }

            // create_pack
            if ($pathinfo === '/god-mode/create-pack') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::createPackAction',  '_route' => 'create_pack',);
            }

            // list_pack
            if (0 === strpos($pathinfo, '/god-mode/list-pack') && preg_match('#^/god\\-mode/list\\-pack(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_pack')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::listPackAction',));
            }

            // edit_pack
            if (0 === strpos($pathinfo, '/god-mode/edit-pack') && preg_match('#^/god\\-mode/edit\\-pack/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_pack')), array (  '_controller' => 'AppBundle\\Controller\\GodController::editPackAction',));
            }

            if (0 === strpos($pathinfo, '/god-mode/list')) {
                // delete_pack
                if (0 === strpos($pathinfo, '/god-mode/list_pack') && preg_match('#^/god\\-mode/list_pack/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_pack')), array (  '_controller' => 'AppBundle\\Controller\\GodController::deletePackAction',));
                }

                if (0 === strpos($pathinfo, '/god-mode/list-')) {
                    // list_pack_subs
                    if (0 === strpos($pathinfo, '/god-mode/list-pack-subs') && preg_match('#^/god\\-mode/list\\-pack\\-subs(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_pack_subs')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::listPackSubsAction',));
                    }

                    // list_admin_payments
                    if (0 === strpos($pathinfo, '/god-mode/list-admin-payments') && preg_match('#^/god\\-mode/list\\-admin\\-payments(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_admin_payments')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::listAdminPaymentsAction',));
                    }

                }

            }

            // confirm_pack_payments
            if (0 === strpos($pathinfo, '/god-mode/confirm-payments') && preg_match('#^/god\\-mode/confirm\\-payments/(?P<packSubscription>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'confirm_pack_payments')), array (  '_controller' => 'AppBundle\\Controller\\GodController::confirmPayment',));
            }

            if (0 === strpos($pathinfo, '/god-mode/do-')) {
                // do_activate_packsubscription
                if (0 === strpos($pathinfo, '/god-mode/do-activate-packsub') && preg_match('#^/god\\-mode/do\\-activate\\-packsub/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'do_activate_packsubscription')), array (  '_controller' => 'AppBundle\\Controller\\GodController::doActivatePackSubscriptionrAction',));
                }

                // do_block_packsubscription
                if (0 === strpos($pathinfo, '/god-mode/do-block-packsub') && preg_match('#^/god\\-mode/do\\-block\\-packsub/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'do_block_packsubscription')), array (  '_controller' => 'AppBundle\\Controller\\GodController::doBlockPackSubscriptionrAction',));
                }

            }

            // create_news
            if ($pathinfo === '/god-mode/create-news') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::createNewsAction',  '_route' => 'create_news',);
            }

            if (0 === strpos($pathinfo, '/god-mode/news')) {
                // edit_news
                if (0 === strpos($pathinfo, '/god-mode/news/edit') && preg_match('#^/god\\-mode/news/edit/(?P<news>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_news')), array (  '_controller' => 'AppBundle\\Controller\\GodController::editNewsAction',));
                }

                // delete_news
                if (0 === strpos($pathinfo, '/god-mode/news/delete') && preg_match('#^/god\\-mode/news/delete/(?P<news>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_news')), array (  '_controller' => 'AppBundle\\Controller\\GodController::deleteNewsAction',));
                }

                // list_news
                if (0 === strpos($pathinfo, '/god-mode/news/list') && preg_match('#^/god\\-mode/news/list(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_news')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::listNewsAction',));
                }

            }

            // set_notice_board
            if ($pathinfo === '/god-mode/set-notice-board') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::setNoticeBoardAction',  '_route' => 'set_notice_board',);
            }

            // unblock_user
            if ($pathinfo === '/god-mode/unblock-user') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::unblockUserAction',  '_route' => 'unblock_user',);
            }

            // reset_password
            if ($pathinfo === '/god-mode/reset-password') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::resetPasswordAction',  '_route' => 'reset_password',);
            }

            if (0 === strpos($pathinfo, '/god-mode/mails')) {
                // god_mode_compose_mail
                if ($pathinfo === '/god-mode/mails/compose') {
                    return array (  '_controller' => 'AppBundle\\Controller\\GodController::composeMailAction',  '_route' => 'god_mode_compose_mail',);
                }

                // god_mode_view_mail
                if (0 === strpos($pathinfo, '/god-mode/mails/view') && preg_match('#^/god\\-mode/mails/view/(?P<mail>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'god_mode_view_mail')), array (  '_controller' => 'AppBundle\\Controller\\GodController::viewMailAction',));
                }

                // god_mode_mails
                if (preg_match('#^/god\\-mode/mails(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'god_mode_mails')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::listMailAction',));
                }

            }

            if (0 === strpos($pathinfo, '/god-mode/reviews')) {
                // list_approve_reviews
                if (0 === strpos($pathinfo, '/god-mode/reviews/list/approve') && preg_match('#^/god\\-mode/reviews/list/approve(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_approve_reviews')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::listApproveReviewsAction',));
                }

                if (0 === strpos($pathinfo, '/god-mode/reviews/do')) {
                    // do_approve_review
                    if (0 === strpos($pathinfo, '/god-mode/reviews/do/approve') && preg_match('#^/god\\-mode/reviews/do/approve/(?P<review>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'do_approve_review')), array (  '_controller' => 'AppBundle\\Controller\\GodController::doApproveReviewAction',));
                    }

                    // do_disapprove_review
                    if (0 === strpos($pathinfo, '/god-mode/reviews/do/disapprove') && preg_match('#^/god\\-mode/reviews/do/disapprove/(?P<review>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'do_disapprove_review')), array (  '_controller' => 'AppBundle\\Controller\\GodController::doDisapproveReviewAction',));
                    }

                }

            }

            // list_users
            if (0 === strpos($pathinfo, '/god-mode/list-users') && preg_match('#^/god\\-mode/list\\-users(?:/(?P<page>[^/]++)(?:/(?P<sort>[^/]++))?)?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'list_users')), array (  'page' => 1,  'sort' => 'date_DESC',  '_controller' => 'AppBundle\\Controller\\GodController::listUsersAction',));
            }

            if (0 === strpos($pathinfo, '/god-mode/do-')) {
                // do_block_user
                if (0 === strpos($pathinfo, '/god-mode/do-block-user') && preg_match('#^/god\\-mode/do\\-block\\-user/(?P<user>[^/]++)/(?P<page>[^/]++)/(?P<sort>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'do_block_user')), array (  '_controller' => 'AppBundle\\Controller\\GodController::doBlockUserAction',));
                }

                // do_activate_user
                if (0 === strpos($pathinfo, '/god-mode/do-activate-user') && preg_match('#^/god\\-mode/do\\-activate\\-user/(?P<user>[^/]++)/(?P<page>[^/]++)/(?P<sort>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'do_activate_user')), array (  '_controller' => 'AppBundle\\Controller\\GodController::doActivateUserAction',));
                }

            }

            // god_mode_merge_users
            if (0 === strpos($pathinfo, '/god-mode/merge-users') && preg_match('#^/god\\-mode/merge\\-users/(?P<pack>[^/]++)(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'god_mode_merge_users')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::mergeUsersAction',));
            }

            // recommitments
            if (0 === strpos($pathinfo, '/god-mode/recommitments') && preg_match('#^/god\\-mode/recommitments/(?P<pack>[^/]++)(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'recommitments')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\GodController::Recommitments',));
            }

            // change-recommitment
            if (0 === strpos($pathinfo, '/god-mode/change-recommitment') && preg_match('#^/god\\-mode/change\\-recommitment/(?P<recommit_id>[^/]++)/(?P<status>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'change-recommitment')), array (  '_controller' => 'AppBundle\\Controller\\GodController::changeRecommitment',));
            }

            // subscription-history
            if (0 === strpos($pathinfo, '/god-mode/subscription-history') && preg_match('#^/god\\-mode/subscription\\-history/(?P<user>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'subscription-history')), array (  '_controller' => 'AppBundle\\Controller\\GodController::subscriptionHistory',));
            }

            // god_mode_merge_user
            if (0 === strpos($pathinfo, '/god-mode/merge-user') && preg_match('#^/god\\-mode/merge\\-user/(?P<receiver>[^/]++)/(?P<feederSubscription>[^/]++)/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'god_mode_merge_user')), array (  '_controller' => 'AppBundle\\Controller\\GodController::mergeUserAction',));
            }

            // god_mode_auto_merge_users
            if (0 === strpos($pathinfo, '/god-mode/auto-merge-users') && preg_match('#^/god\\-mode/auto\\-merge\\-users/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'god_mode_auto_merge_users')), array (  '_controller' => 'AppBundle\\Controller\\GodController::autoMergeUsersAction',));
            }

            // god_mode_change_merging_mode
            if (0 === strpos($pathinfo, '/god-mode/change-merging-mode') && preg_match('#^/god\\-mode/change\\-merging\\-mode/(?P<mode>[^/]++)/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'god_mode_change_merging_mode')), array (  '_controller' => 'AppBundle\\Controller\\GodController::changeMergingModeAction',));
            }

            // run_cron
            if ($pathinfo === '/god-mode/run-cron') {
                return array (  '_controller' => 'AppBundle\\Controller\\GodController::runCron',  '_route' => 'run_cron',);
            }

        }

        if (0 === strpos($pathinfo, '/member')) {
            // dashboard
            if ($pathinfo === '/member/dashboard') {
                return array (  '_controller' => 'AppBundle\\Controller\\MemberController::dashboardAction',  '_route' => 'dashboard',);
            }

            // pay-commission
            if (0 === strpos($pathinfo, '/member/pay-commission') && preg_match('#^/member/pay\\-commission/(?P<user>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pay-commission')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::payCommission',));
            }

            // update_bank_choice
            if ($pathinfo === '/member/update-bank-choice') {
                return array (  '_controller' => 'AppBundle\\Controller\\MemberController::updateBankChoiceAction',  '_route' => 'update_bank_choice',);
            }

            // profile
            if ($pathinfo === '/member/profile') {
                return array (  '_controller' => 'AppBundle\\Controller\\MemberController::profileAction',  '_route' => 'profile',);
            }

            if (0 === strpos($pathinfo, '/member/bank-details')) {
                // bank_details
                if ($pathinfo === '/member/bank-details') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MemberController::bankDetailsAction',  '_route' => 'bank_details',);
                }

                // bank_details_two
                if ($pathinfo === '/member/bank-details-two') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MemberController::bankDetailsTwoAction',  '_route' => 'bank_details_two',);
                }

            }

            // change_password
            if ($pathinfo === '/member/change-password') {
                return array (  '_controller' => 'AppBundle\\Controller\\MemberController::changePasswordAction',  '_route' => 'change_password',);
            }

            // packs
            if ($pathinfo === '/member/packs') {
                return array (  '_controller' => 'AppBundle\\Controller\\MemberController::packsAction',  '_route' => 'packs',);
            }

            // visit_notif
            if (0 === strpos($pathinfo, '/member/visit-notif') && preg_match('#^/member/visit\\-notif/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'visit_notif')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::visitNotificationAction',));
            }

            // feeders
            if ($pathinfo === '/member/feeders') {
                return array (  '_controller' => 'AppBundle\\Controller\\MemberController::feedersAction',  '_route' => 'feeders',);
            }

            if (0 === strpos($pathinfo, '/member/p')) {
                if (0 === strpos($pathinfo, '/member/pack')) {
                    if (0 === strpos($pathinfo, '/member/pack-subscription')) {
                        // pack_subscription_jury
                        if (0 === strpos($pathinfo, '/member/pack-subscription/jury') && preg_match('#^/member/pack\\-subscription/jury/(?P<jury>[^/]++)/new/(?P<pack>[^/]++)/(?P<user>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pack_subscription_jury')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::packSubscriptionJuryAction',));
                        }

                        // pack_subscription_new
                        if (0 === strpos($pathinfo, '/member/pack-subscription/new') && preg_match('#^/member/pack\\-subscription/new/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'pack_subscription_new')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::packSubscriptionNewAction',));
                        }

                        // await_merging
                        if (0 === strpos($pathinfo, '/member/pack-subscription/await-merging') && preg_match('#^/member/pack\\-subscription/await\\-merging/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'await_merging')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::awaitMerging',));
                        }

                        if (0 === strpos($pathinfo, '/member/pack-subscription/feeder')) {
                            // make_payment
                            if (preg_match('#^/member/pack\\-subscription/feeder/(?P<feeder>[^/]++)/make\\-payment$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'make_payment')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::makePaymentAction',));
                            }

                            // cancel_payment
                            if (preg_match('#^/member/pack\\-subscription/feeder/(?P<feeder>[^/]++)/cancel\\-payment$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'cancel_payment')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::cancelPaymentAction',));
                            }

                        }

                    }

                    if (0 === strpos($pathinfo, '/member/packs/pack-subscription/c')) {
                        // check_payment
                        if (0 === strpos($pathinfo, '/member/packs/pack-subscription/check-payment') && preg_match('#^/member/packs/pack\\-subscription/check\\-payment/(?P<feeder>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'check_payment')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::checkPaymentAction',));
                        }

                        // confirm_payment
                        if (0 === strpos($pathinfo, '/member/packs/pack-subscription/confirm-payment') && preg_match('#^/member/packs/pack\\-subscription/confirm\\-payment/(?P<feeder>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'confirm_payment')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::feederConfirmAction',));
                        }

                    }

                    // extend_payment
                    if (0 === strpos($pathinfo, '/member/pack/pack-subscription/feeder') && preg_match('#^/member/pack/pack\\-subscription/feeder/(?P<feeder>[^/]++)/extend\\-payment$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'extend_payment')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::feederExtendAction',));
                    }

                    if (0 === strpos($pathinfo, '/member/packs/pack-subscription')) {
                        // scam_payment
                        if (0 === strpos($pathinfo, '/member/packs/pack-subscription/scam-payment') && preg_match('#^/member/packs/pack\\-subscription/scam\\-payment/(?P<feeder>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'scam_payment')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::feederScamAction',));
                        }

                        // cancel_pack_subscription
                        if (0 === strpos($pathinfo, '/member/packs/pack-subscription/cancel') && preg_match('#^/member/packs/pack\\-subscription/cancel/(?P<packSubscription>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cancel_pack_subscription')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::cancelPackSubscriptionAction',));
                        }

                        // book_autorecirculation
                        if (preg_match('#^/member/packs/pack\\-subscription/(?P<packSubscription>[^/]++)/book\\-autorecirculation$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'book_autorecirculation')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::bookAutoRecirculationAction',));
                        }

                        // do_auto_recirculation
                        if (preg_match('#^/member/packs/pack\\-subscription/(?P<packSubscription>[^/]++)/do\\-auto\\-recirculation/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'do_auto_recirculation')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::doAutoRecirculationAction',));
                        }

                        // cancel_autorecirculation
                        if (preg_match('#^/member/packs/pack\\-subscription/(?P<packSubscription>[^/]++)/cancel\\-auto\\-recirculation$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'cancel_autorecirculation')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::cancelAutoRecirculationAction',));
                        }

                    }

                    // pack_subscription_feeders
                    if (0 === strpos($pathinfo, '/member/pack-subscription') && preg_match('#^/member/pack\\-subscription/(?P<pack>[^/]++)/feeders$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'pack_subscription_feeders')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::packSubscriptionFeedersAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/member/purse')) {
                    // purse
                    if (preg_match('#^/member/purse(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'purse')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\MemberController::purseAction',));
                    }

                    if (0 === strpos($pathinfo, '/member/purse-')) {
                        // withdraw_purse
                        if ($pathinfo === '/member/purse-withdrawal') {
                            return array (  '_controller' => 'AppBundle\\Controller\\MemberController::withdrawPurseAction',  '_route' => 'withdraw_purse',);
                        }

                        // transfer_purse
                        if ($pathinfo === '/member/purse-transfer') {
                            return array (  '_controller' => 'AppBundle\\Controller\\MemberController::purseTransferAction',  '_route' => 'transfer_purse',);
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/member/referrals')) {
                // create-referral
                if ($pathinfo === '/member/referrals/create') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MemberController::createReferralAction',  '_route' => 'create-referral',);
                }

                // referrals
                if (preg_match('#^/member/referrals(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'referrals')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\MemberController::referralsAction',));
                }

            }

            if (0 === strpos($pathinfo, '/member/support')) {
                // create_support
                if ($pathinfo === '/member/support/create') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MemberController::createSupportAction',  '_route' => 'create_support',);
                }

                // add_support_message
                if (preg_match('#^/member/support/(?P<support>[^/]++)/add\\-message$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'add_support_message')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::addSupportMessageAction',));
                }

                // view_support
                if (preg_match('#^/member/support/(?P<support>[^/]++)/view$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'view_support')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::viewTicketAction',));
                }

                // support
                if (preg_match('#^/member/support(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'support')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\MemberController::supportAction',));
                }

            }

            if (0 === strpos($pathinfo, '/member/jury')) {
                // user_case
                if (0 === strpos($pathinfo, '/member/jury/case') && preg_match('#^/member/jury/case/(?P<case>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_case')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::caseAction',));
                }

                // jury
                if (preg_match('#^/member/jury(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'jury')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\MemberController::juryAction',));
                }

            }

            if (0 === strpos($pathinfo, '/member/reviews')) {
                // create_review
                if ($pathinfo === '/member/reviews/create') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MemberController::createReviewAction',  '_route' => 'create_review',);
                }

                // reviews
                if (preg_match('#^/member/reviews(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'reviews')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\MemberController::reviewsAction',));
                }

            }

            if (0 === strpos($pathinfo, '/member/mails')) {
                // member_view_mail
                if (0 === strpos($pathinfo, '/member/mails/view') && preg_match('#^/member/mails/view/(?P<mailRecipient>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'member_view_mail')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::viewMail',));
                }

                // member_delete_mail
                if (0 === strpos($pathinfo, '/member/mails/delete') && preg_match('#^/member/mails/delete/(?P<mailRecipient>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'member_delete_mail')), array (  '_controller' => 'AppBundle\\Controller\\MemberController::deleteMail',));
                }

                // member_mails
                if (preg_match('#^/member/mails(?:/(?P<page>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'member_mails')), array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\MemberController::memberMails',));
                }

            }

        }

        // home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\PublicController::homeAction',  '_route' => 'home',);
        }

        if (0 === strpos($pathinfo, '/re')) {
            // referral_link
            if (0 === strpos($pathinfo, '/ref') && preg_match('#^/ref/(?P<username>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'referral_link')), array (  '_controller' => 'AppBundle\\Controller\\PublicController::referralLinkAction',));
            }

            // register_pack
            if ($pathinfo === '/register-pack') {
                return array (  '_controller' => 'AppBundle\\Controller\\PublicController::registerPackAction',  '_route' => 'register_pack',);
            }

        }

        // contact
        if ($pathinfo === '/contact') {
            return array (  '_controller' => 'AppBundle\\Controller\\PublicController::contactAction',  '_route' => 'contact',);
        }

        // forgot_password
        if ($pathinfo === '/forgot-password') {
            return array (  '_controller' => 'AppBundle\\Controller\\PublicController::forgotPasswordAction',  '_route' => 'forgot_password',);
        }

        // anti_spam
        if ($pathinfo === '/anti-spam') {
            return array (  '_controller' => 'AppBundle\\Controller\\PublicController::AntiSpamAction',  '_route' => 'anti_spam',);
        }

        // confidentiality_policy
        if ($pathinfo === '/confidentiality-policy') {
            return array (  '_controller' => 'AppBundle\\Controller\\PublicController::ConfidentialityPolicyAction',  '_route' => 'confidentiality_policy',);
        }

        // earn_daily
        if ($pathinfo === '/earn-daily') {
            return array (  '_controller' => 'AppBundle\\Controller\\PublicController::EarnDailyAction',  '_route' => 'earn_daily',);
        }

        // free_airtime
        if ($pathinfo === '/free-airtime') {
            return array (  '_controller' => 'AppBundle\\Controller\\PublicController::FreeAirtimeAction',  '_route' => 'free_airtime',);
        }

        if (0 === strpos($pathinfo, '/s')) {
            // social_channels
            if ($pathinfo === '/social-channels') {
                return array (  '_controller' => 'AppBundle\\Controller\\PublicController::SocialChannelsAction',  '_route' => 'social_channels',);
            }

            // send-payments
            if ($pathinfo === '/send-payments') {
                return array (  '_controller' => 'AppBundle\\Controller\\PublicController::sendPayment',  '_route' => 'send-payments',);
            }

        }

        // send-mail
        if (0 === strpos($pathinfo, '/get-pack') && preg_match('#^/get\\-pack/(?P<user>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'send-mail')), array (  '_controller' => 'AppBundle\\Controller\\PublicController::getPack',));
        }

        // register
        if (0 === strpos($pathinfo, '/register') && preg_match('#^/register/(?P<pack>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'register')), array (  '_controller' => 'AppBundle\\Controller\\RegistrationController::registerAction',));
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // login
                if (rtrim($pathinfo, '/') === '/login') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'login');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
                }

                // security_login_check
                if ($pathinfo === '/login_check') {
                    return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::loginCheckAction',  '_route' => 'security_login_check',);
                }

            }

            // logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'AppBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'logout',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
