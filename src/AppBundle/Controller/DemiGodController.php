<?php
	
	namespace AppBundle\Controller;

	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;

	use Symfony\Component\HttpFoundation\Request;

	use Symfony\Component\Security\Core\Authentication\Token\UserCheck;

	use AppBundle\Entity\SupportMessage;
	use AppBundle\Entity\JuryMessage;

	use AppBundle\Form\SupportMessageType;
	use AppBundle\Form\JuryMessageType;

	class DemiGodController extends controller {

		/**
		 * @Route("/demi-god-mode/support/close/{support}", name="close_support")
		 */
		private function closeSupportAction($support) {
			$support = $this->getDoctrine()->getRepository('AppBundle:Support')->find($support);

			if($support != null) {
				
				$support->setStatus('closed');

				try {
					$em = $this->getDoctrine()->getManager();

					$em->flush();

					$this->addFlash(
						'ticketMessageSuccess',
						"Support #{$support->getId()} has been closed"
					);

					return $this->redirectToRoute('demi_god_view_support', array('support' => $support->getId()));
				} catch(Exception $e) {

					$this->addFlash(
						'ticketMessageError',
						'An internal error occured while closing the support. Please try again later'
					);

					return $this->redirectToRoute('demi_god_view_support', array('support' => $support->getId()));
				}
			}

			$this->addFlash(
				'ticketMessageError',
				'Support Non Existent'
			);

			return $this->redirectToRoute('demi_god_support');
		}

		/**
		 * @Route("/demi-god-mode/support/view/{support}", name="demi_god_view_support")
		 */
		private function viewSupportAction(Request $request, $support) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$support = $this->getDoctrine()->getRepository('AppBundle:Support')->find($support);

			if($support == null) {

                $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                    array('subject' => $this->getUser()->getId()),
                    array('created' => 'DESC'),
                    10
                );

                // Call Formatter
                $formatter = $this->get('app.timeline_format');
				return $this->render(
					'member/page-not-found.html.twig',
					[
                        'adm' => $adminsettings,
                        'timeline' => $formatter->format($notifications)
                    ]
				);
			}

			$supportMessage = new SupportMessage;
			$supportMessageForm = $this->createForm(SupportMessageType::class, $supportMessage);
			$supportMessageFormClone = clone $supportMessageForm;
			$supportMessageForm->handleRequest($request);

			if($supportMessageForm->isSubmitted() && $supportMessageForm->isValid()) {
				
				// Check if attachment was uploaded
				if($supportMessage->getAttachmentFile() != null) {

					$attachmentFile = $supportMessage->getAttachmentFile();

					$attachmentFileName = md5(uniqid()).'.'.$attachmentFile->guessExtension();

					// Move file to directory
					$attachmentDirectory = $this->getParameter('tickets_attachment_real_directory').'/'.$this->getUser()->getUsername();
					$attachmentDbDirectory = $this->getParameter('tickets_attachment_directory').'/'.$this->getUser()->getUsername();

					if(!is_dir($attachmentDirectory) && is_readable($attachmentDirectory)) {
						mkdir($attachmentDirectory);
					}

					$attachmentFile->move(
						$attachmentDirectory,
						$attachmentFileName
					);

					// Store file name
					$supportMessage->setAttachment($attachmentDbDirectory.'/'.$attachmentFileName);
				}

				$supportMessage->setSupportId($support->getId());

				$supportMessage->setSender('admin');

				$now = new \DateTime('now');
				$supportMessage->setCreatedDate($now);

				try {

					$em = $this->getDoctrine()->getManager();
					$em->persist($supportMessage);

					$em->flush();

					$support->setStatus('admin_reply');

					$em->flush();

					$this->addFlash(
						'ticketMessageSuccess',
						"You have successfully added a reply to the support #{$support->getId()}"
					);

					$supportMessageForm = $supportMessageFormClone;

				} catch(Exception $e) {

					$this->addFlash(
						'ticketMessageError',
						"An error occured while adding a reply to the support #{$support->getId()}"
					);

				}
			}

			$supportMessages = $this->getDoctrine()->getRepository('AppBundle:SupportMessage')->getSupportMessages($support);
			$supportUser = $this->getDoctrine()->getRepository('AppBundle:User')->find($support->getUserId());
			$supportCount = $this->getDoctrine()->getRepository('AppBundle:Support')->getActiveSupportCount();
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

            // Call Formatter
            $formatter = $this->get('app.timeline_format');


			return $this->render(
				'demi-god-mode/view-support.html.twig',
				[
                    'adm' => $adminsettings,
					'support' => $support,
					'supportMessages' => $supportMessages,
					'supportMessageForm' => $supportMessageForm->createView(),
					'supportUser' => $supportUser,
					'supportCount' => $supportCount,
                    'timeline' => $formatter->format($notifications)
				]
			);
		}

		/**
		 * @Route("/demi-god-mode/support/{page}", name="demi_god_support", defaults={"page"=1})
		 */
		private function supportAction($page) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$paginator = $this->get('knp_paginator');

			$dql = "SELECT s FROM AppBundle:Support s ORDER BY s.createdDate DESC";

			$em = $this->getDoctrine()->getManager();

			$query = $em->createQuery($dql);

			$limit = 50;

			$supports = $paginator->paginate($query, $page, $limit);
			$supportCount = $this->getDoctrine()->getRepository('AppBundle:Support')->getActiveSupportCount();
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

            // Call Formatter
            $formatter = $this->get('app.timeline_format');

			return $this->render(
				'demi-god-mode/supports.html.twig',
				[
                    'adm' => $adminsettings,
					'supports' => $supports,
					'supportCount' => $supportCount,
                        'timeline' => $formatter->format($notifications)
				]
			);
		}

		/**
		 * @Route("/demi-god-mode/jury/case/{jury}/confirm-payment", name="demi_god_jury_confirm_payment")
		 */
		private function juryConfirmPaymentAction($jury) {
			$jury = $this->getDoctrine()->getRepository('AppBundle:Jury')->find($jury);

			if($jury != null) {
				
				$feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->find($jury->getFeederEntryId());

				$feederPackSubscription = $feeder->getFeederPackSubscription();

				// Activate Pack Subscription
				$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->activateSubscription($feederPackSubscription);

				// Mark Feeder as Fully Paid
				$this->getDoctrine()->getRepository('AppBundle:Feeder')->markAsPaid($feeder);

				// Check if transaction exists for feeder pack subscription
				$transaction = $this->getDoctrine()->getRepository('AppBundle:Transaction')->findOneBy(
					array(
						'feeder' => $feeder->getFeeder()->getId(),
						'feederPackSubscription' => $feederPackSubscription->getId(),
						'status' => 'not_confirmed'
					)
				);

				if($transaction != null) {
					// confirm transaction
					$this->getDoctrine()->getRepository('AppBundle:Transaction')->confirmTransaction($transaction);

					// Get Feeder Referrer
					$feederReferrer = $feeder->getFeeder()->getReferrer();

					// Credit Referrer Wallet
					$creditWallet = $this->getDoctrine()->getRepository('AppBundle:User')->creditPurse($feederReferrer, $transaction->getReceiverShare());
				}

				if($feeder->getReceiverPackSubscription()->getUserType() == 'user' && $feeder->getReceiverPackSubscription()->getType() == 'pack') {					
					
					// Get Count of Receiver Pack Subscription From Feeders table Where feeder is 'fully_paid'
					$paidFeedersCount = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getPaidFeedersCount($feeder->getReceiverPackSubscription());

					$realTotal = $feeder->getReceiverPackSubscription()->getFeederTotal();

					// If Count is == total of Receiver Plan Subscription, mark status as 'completed'
					if($paidFeedersCount >= $realTotal) {
						
						// Complete Subscription
						$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->completeSubscription($feeder->getReceiverPackSubscription());
						// Check if user has booked cancel
						$bookCancel = $feeder->getReceiverPackSubscription()->getBookCancel();

						if($bookCancel == null) {
							// Auto Recycle if plan subscription type is not wallet
							if($feeder->getReceiverPackSubscription()->getAutoRecirculate() != null) {
								$autoRecirculatePack = $feeder->getReceiverPackSubscription()->getAutoRecirculate();
							} else {
								$autoRecirculatePack = $feeder->getReceiverPackSubscription()->getPack();
							}
						} else {
							// Cancel Plan
							$cancelSubscription = $bookCancel;
						}
					}
				}

				$verdict = "The Payment has been confirmed by the admin as the statements and evidences brought by the Feeder: {$feeder->getFeeder()->getName()} by far outweigh the statements and evidences brought by the Receiver: {$feeder->getReceiver()->getName()}";

				$jury->setVerdict($verdict);

				$jury->setStatus("closed");

				$em = $this->getDoctrine()->getManager();
				$em->flush();
				//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();
				$em->clear();

				if(isset($cancelSubscription)) {

					return $this->redirectToRoute('demi_god_case', array('jury' => $jury->getId()));

				} else if(isset($autoRecirculatePack)) {

					return $this->redirectToRoute('pack_subscription_jury', array('pack' => $autoRecirculatePack->getId(), ' jury' => $jury->getId(), 'user' => $feeder->getReceiver()->getId()));

				} else {

					return $this->redirectToRoute('demi_god_case', array('jury' => $jury->getId()));

				}

			} else {

				return $this->redirectToRoute('demi_god_jury');
			}
		}

		/**
		 * @Route("/demi-god-mode/jury/case/{jury}/cancel-payment", name="demi_god_jury_cancel_payment")
		 */
		private function juryCancelPaymentAction($jury) {
			$jury = $this->getDoctrine()->getRepository('AppBundle:Jury')->find($jury);

			if($jury != null) {
				
				$feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->find($jury->getFeederEntryId());

				// Cancel Downline
				$feeder->setStatus('cancelled');

				// Credit Receiver Plan Subscriber Counter
				$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->incrementSubscriptionFeeders($feeder->getReceiverPackSubscription(), 1);

				// Check if feeder has active subscriptions
				$feederActiveSubscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserActivePackSubscriptionsCount($feeder->getFeeder());

				if($feederActiveSubscriptions == 0) {
					// Block Feeder
					$this->getDoctrine()->getRepository('AppBundle:User')->blockUser($feeder->getFeeder());
				}

				// Cancel Feeder Pack Subscription
				$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->cancelSubscription($feeder->getFeederPackSubscription());

				// Set Verdict
				$verdict = "The Payment has been cancelled by the admin as the statements and evidences brought by the Receiver: {$feeder->getReceiver()->getName()} by far outweigh the statements and evidences brought by the Feeder: {$feeder->getFeeder()->getName()}. Hence the Feeder: {$feeder->getFeeder()->getName()} has been blocked for failing to complete the payment and another feeder will be assigned to the receiver";
				$jury->setVerdict($verdict);

				// Close Court Case
				$jury->setStatus("closed");

				$em = $this->getDoctrine()->getManager();
				$em->flush();
				//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();
				$em->clear();

				return $this->redirectToRoute('demi_god_case', array('jury' => $jury->getId()));

			} else {

				return $this->redirectToRoute('demi_god_jury');
			}
		}

		/**
		 * @Route("/demi-god-mode/jury/case/{jury}", name="demi_god_case")
		 */
		private function demiGodCase(Request $request, $jury) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$userCheck = new UserCheck;

			$jury = $this->getDoctrine()->getRepository('AppBundle:Jury')->find($jury);

			if($jury == null) {

                $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                    array('subject' => $this->getUser()->getId()),
                    array('created' => 'DESC'),
                    10
                );

                // Call Formatter
                $formatter = $this->get('app.timeline_format');
				return $this->render(
					'member/page-not-found.html.twig',
					[
                        'adm' => $adminsettings,
                        'timeline' => $formatter->format($notifications)]
				);
			}

			$juryMessage = new JuryMessage;
			$juryMessageForm = $this->createForm(JuryMessageType::class, $juryMessage);
			$juryMessageFormClone = clone $juryMessageForm;
			$juryMessageForm->handleRequest($request);

			if($juryMessageForm->isSubmitted() && $juryMessageForm->isValid()) {
				
				// Check if attachment was uploaded
				if($juryMessage->getAttachmentFile() != null) {

					$attachmentFile = $juryMessage->getAttachmentFile();

					$attachmentFileName = md5(uniqid()).'.'.$attachmentFile->guessExtension();

					// Move file to directory
					$attachmentDirectory = $this->getParameter('court_attachment_real_directory').'/admin';
					$attachmentDbDirectory = $this->getParameter('court_attachment_directory').'/admin';

					if(!is_dir($attachmentDirectory) && is_readable($attachmentDirectory)) {
						mkdir($attachmentDirectory);
					}

					$attachmentFile->move(
						$attachmentDirectory,
						$attachmentFileName
					);

					// Store file name
					$juryMessage->setAttachment($attachmentDbDirectory.'/'.$attachmentFileName);
				}

				$juryMessage->setJuryId($jury->getId());

				$juryMessage->setSenderId($this->getUser()->getId());

				$juryMessage->setSenderType('admin');

				$now = new \DateTime('now');
				$juryMessage->setCreatedDate($now);

				try {

					$em = $this->getDoctrine()->getManager();
					$em->persist($juryMessage);

					$em->flush();

					$jury->setStatus('admin_reply');

					$em->flush();

					//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

					$this->addFlash(
						'courtMessageSuccess',
						"You have successfully added a statement to the case #{$jury->getId()}"
					);

					$juryMessageForm = $juryMessageFormClone;

				} catch(Exception $e) {

					$this->addFlash(
						'courtMessageError',
						"An error occured while adding a statement to the case #{$jury->getId()}"
					);

				}
			}

			$juryMessages = $this->getDoctrine()->getRepository('AppBundle:JuryMessage')->getJuryMessages($jury);

			$feeder = $this->getDoctrine()->getRepository('AppBundle:User')->find($jury->getFeederId());

			$receiver = $userCheck->wash($this->getDoctrine()->getRepository('AppBundle:User')->find($jury->getReceiverId()));

			$juryCount = $this->getDoctrine()->getRepository('AppBundle:Jury')->getActiveJuryCount();
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

            // Call Formatter
            $formatter = $this->get('app.timeline_format');
			
			return $this->render(
				'demi-god-mode/admin-jury.html.twig',
				[
                    'adm' => $adminsettings,
					'jury' => $jury,
					'juryMessages' => $juryMessages,
					'juryMessageForm' => $juryMessageForm->createView(),
					'feeder' => $feeder,
					'receiver' => $receiver,
					'juryCount' => $juryCount,
                    'timeline' => $formatter->format($notifications)
				]
			);
		}

		/**
		 * @Route("/demi-god-mode/jury/{page}", name="demi_god_jury", defaults={"page"=1})
		 */
		private function juryAction($page) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT j FROM AppBundle:Jury j ORDER BY j.createdDate DESC";

			$query = $em->createQuery($dql);

			$limit = 25;

			$cases = $paginator->paginate($query, $page, $limit);

			$items = $cases->getItems();

			$casesItems = array();

			$userRepo = $this->getDoctrine()->getRepository('AppBundle:User');

			foreach($items as $item) {
				
				$item->setFeeder($userRepo->find($item->getFeederId()));
				
				$item->setReceiver($userRepo->find($item->getReceiverId()));

				$casesItems[] = $item;
			}

			$cases->setItems($casesItems);
			$juryCount = $this->getDoctrine()->getRepository('AppBundle:Jury')->getActiveJuryCount();
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

            // Call Formatter
            $formatter = $this->get('app.timeline_format');

			return $this->render(
				'demi-god-mode/admin-list-jury.html.twig',
				[
                    'adm' => $adminsettings,
					'cases' => $cases,
					'juryCount' => $juryCount,
                    'timeline' => $formatter->format($notifications)
				]
			);
		}

		/**
		 * @Route("/demi-god-mode/reviews", name="demi_god_reviews")
		 */
		private function reviewsAction() {
			//
		}

		/**
		 * Load stuff before calling any of the controller's methods
		 */
		public function __call($method, $args) {

			$securityToken = $this->get('security.token_storage')->getToken();

			$userCheck = new UserCheck;
			
			$user = $userCheck->verify($securityToken->getUser());

			$user->setReferralsCount($this->getDoctrine()->getRepository('AppBundle:User')->getReferralsCount($user));

			$user->setPacks($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserCurrentSubscriptionsCount($user));

			$user->setSupportCount($this->getDoctrine()->getRepository('AppBundle:Support')->getActiveSupportCount());

			$user->setJuryCount($this->getDoctrine()->getRepository('AppBundle:Jury')->getActiveJuryCount());

			// Get Mail Messages
			$user->setMailCount($this->getDoctrine()->getRepository('AppBundle:MailRecipient')->getUnreadCount($this->getUser()));

			$user->setMenuMail($this->getDoctrine()->getRepository('AppBundle:MailRecipient')->getMenuMail($this->getUser()));

         	$securityToken->setUser($user);

        	return call_user_func_array(array($this, $method), $args);
		}
	}