<?php

	namespace AppBundle\Controller;

	use Aws\S3\Exception\S3Exception;
	use Aws\S3\S3Client;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;

	use AppBundle\Form\PersonalType;
    use AppBundle\Form\BankDetailTwoType;
    use AppBundle\Form\BankChoiceType;
	use AppBundle\Form\PaymentType;
	use AppBundle\Form\SettingsType;
	use AppBundle\Form\PackPaymentType;
	use AppBundle\Form\SupportType;
	use AppBundle\Form\SupportMessageType;
	use AppBundle\Form\ReviewType;
	use AppBundle\Form\JuryMessageType;

	use Symfony\Component\Form\Extension\Core\Type\TextType;

	use Symfony\Component\Security\Core\Authentication\Token\Telegram;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;

	use Symfony\Component\Security\Core\Authentication\Token\UserCheck;

	use AppBundle\Entity\User;
	use AppBundle\Entity\Feeder;
	use AppBundle\Entity\PackSubscription;
	use AppBundle\Entity\Notification;
	use AppBundle\Entity\Transaction;
	use AppBundle\Entity\Support;
	use AppBundle\Entity\SupportMessage;
	use AppBundle\Entity\Review;
	use AppBundle\Entity\Jury;
	use AppBundle\Entity\JuryMessage;


	use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


    use AppBundle\Service\Cryptos\Classes\BtcGateway;
    use AppBundle\Service\Cryptos\Classes\BlockIo;
    use AppBundle\Service\Cryptos\Classes\BlockKey;

	class MemberController extends Controller {

		/**
		 * @Route("/member/dashboard", name="dashboard")
		 */
		private function dashboardAction() {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			// Feeders Count
			if($this->getUser()->getRole() == 'ROLE_USER') {
				$feedersCount = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getFeedersCount($this->getUser());
			} else if($this->getUser()->getRole() == 'ROLE_ADMIN' || $this->getUser()->getRole() == 'ROLE_SUPER_ADMIN') {
				$feedersCount = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getAdminFeedersCount($this->getUser());
			}

            if($adminsettings->getAppMode() !== "verified") {

                //return $this->redirectToRoute('logout');
            }
			// Active Referrals Count
			$activeReferralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($this->getUser());

			// Get Notice Board
			$noticeBoard = $this->getDoctrine()->getRepository('AppBundle:Admin')->getNoticeBoard();

			$notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
				array('subject' => $this->getUser()->getId()),
				array('created' => 'DESC'),
				10
			);

			// Call Formatter
			$formatter = $this->get('app.timeline_format');

			// Subscriptions
			$subscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserCurrentSubscriptions($this->getUser());

			// Future Subscriptions Cancels
			$futureSubscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserFutureSubscriptions($this->getUser());

			$realSubscriptions = array();

			if(!empty($subscriptions)) {
				foreach($subscriptions as $subscription) {

					$feeders = NULL;
					$receiver = NULL;
					$feederEntry = NULL;

					if($subscription->getStatus() == 'active') {
						// check feeders
						$feeders = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getSubscriptionFeeders($subscription);

					}else if($subscription->getStatus() == 'waiting'){

						return $this->redirectToRoute('await_merging', array('pack' => $subscription->getPack()->getId() ));

					} else if($subscription->getStatus() == 'inactive') {
						$userCheck = new UserCheck;
						// check receiver
                        $feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getReceiver($subscription, $this->getUser());
                        if($feeder != null) {
                            $receiver = $userCheck->sanitize($feeder);
                        }
						// check feeder entry
						$feederEntry = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getFeederEntry($this->getUser(), $subscription);
					}

					$realSubscriptions[$subscription->getPack()->getName()][] = array(
						'subscription' => $subscription,
						'feeders' => $feeders,
						'receiver' => $receiver,
						'feederEntry' => $feederEntry
					);
				}
			}

			$usersCount = $this->getDoctrine()->getRepository('AppBundle:User')->getUsersCount();
			$activeUsersCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveUsersCount();
			$blockedUsersCount = $this->getDoctrine()->getRepository('AppBundle:User')->getBlockedUsersCount();
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

			$feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getFeedersCount($this->getUser());

			$formatter = $this->get('app.timeline_format');
			return $this->render(
				'member/dashboard.html.twig',
				[
					'feedersCount' => $feedersCount,
                    'adm' => $adminsettings,
					'subscriptions' => $realSubscriptions,
					'futureSubscriptions' => $futureSubscriptions,
					'activeReferralsCount' => $activeReferralsCount,
					'noticeBoard' => $noticeBoard,
					'usersCount' => $usersCount,
					'activeUsersCount' => $activeUsersCount,
					'blockedUsersCount' => $blockedUsersCount,
					'timeline' => $formatter->format($notifications),
					'feeder'   => $feeder
				]
			);
		}

		/**
		 * @Route("/member/pay-commission/{user}", name="pay-commission")
		 */

		private function payCommission($user){
			$subscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy([
				'id' => 7166
			])->getPaidCommission();

			return new JsonResponse(array('user_id' => $user,'sub' => $subscription));
//			return ;
		}

        /**
         * @Route("/member/update-bank-choice", name="update_bank_choice")
         */
        private function updateBankChoiceAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $user = $this->getUser();


            $paymentForm = $this->createForm(PaymentType::class, $user);
            $paymentTwoForm = $this->createForm(BankDetailTwoType::class, $user);
            $bankForm = $this->createForm(BankChoiceType::class, $user);


            $bankForm->handleRequest($request);

            if($user->getBankChoice() == 'Use the Default Account'){
                $user->setBankChoice(1);
                $user->setBankName($user->getBankNameOne());
                $user->setBankAccountName($user->getAccountNameOne());
                $user->setBankAccountNumber($user->getAccountNumberOne());
                $user->setBankAccountType($user->getAccountTypeOne());
                $this->addFlash(
                    'paymentSuccess',
                    'Your bank details have been set to your default bank account details.'
                );
                // Save User
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            }elseif ($user->getBankChoice() == 'Use the Provisional Account'){
                $user->setBankChoice(2);
                $user->setBankName($user->getBankNameTwo());
                $user->setBankAccountName($user->getAccountNameTwo());
                $user->setBankAccountNumber($user->getAccountNumberTwo());
                $user->setBankAccountType($user->getAccountTypeTwo());
                $this->addFlash(
                    'paymentSuccess',
                    'Your bank details have been set to your provisional bank account details.'
                );
                // Save User
                $em = $this->getDoctrine()->getManager();
                $em->flush();

            }

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );



            // Call Formatter
            $formatter = $this->get('app.timeline_format');
                if($this->getUser()->getRole() != "ROLE_SUPER_ADMIN"){
                    return $this->render(
                        'member/profile-bank-details.html.twig',
                        [
                            'paymentForm' => $paymentForm->createView(),
                            'adm' => $adminsettings,
                            'bankForm' => $bankForm->createView(),
                            'paymentTwoForm' => $paymentTwoForm->createView(),
                            'timeline' => $formatter->format($notifications)
                        ]
                    );
                }else{
                    return $this->render(
                        'god-mode/adminprofile-bank-details.html.twig',
                        [
                            'paymentForm' => $paymentForm->createView(),
                            'adm' => $adminsettings,
                            'bankForm' => $bankForm->createView(),
                            'paymentTwoForm' => $paymentTwoForm->createView(),
                            'timeline' => $formatter->format($notifications)
                        ]
                    );
                }

        }

        /**
		 * @Route("/member/profile", name="profile")
		 */
		private function profileAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$user = $this->getUser();

			// Get Personal Details Form
			$personalForm = $this->createForm(PersonalType::class, $user);
			$personalForm->handleRequest($request);

			if($personalForm->isSubmitted() && $personalForm->isValid()) {

				// Check if avatar was uploaded
				if($user->getAvatarImage() != null) {

					$avatarFile = $user->getAvatarImage();

					$avatarFileName = md5(uniqid()).'.'.$avatarFile->guessExtension();

					// Move file to directory
					$avatarDirectory = $this->getParameter('avatars_real_directory').'/'.$user->getUsername();
					$avatarDbDirectory = $this->getParameter('avatars_directory').'/'.$user->getUsername();

					if($user->getRealAvatar() != null) {
						unlink($_SERVER['DOCUMENT_ROOT'].$user->getRealAvatar());
					}

					if(!is_dir($avatarDirectory) && is_readable($avatarDirectory)) {
						mkdir($avatarDirectory);
					}

					$avatarFile->move(
						$avatarDirectory,
						$avatarFileName
					);

					// Store file name
					$user->setAvatar($avatarDbDirectory.'/'.$avatarFileName);
				}

				$this->addFlash(
					'personalSuccess',
					'Your personal settings have been successfully updated'
				);

				// Save User
				$em = $this->getDoctrine()->getManager();
				$em->flush();
			}

			$user->setAvatarImage(null);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');

			return $this->render(
				'member/profile.html.twig',
				[
					'personalForm' => $personalForm->createView(),
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
				]
			);
		}

		/**
		 * @Route("/member/bank-details", name="bank_details")
		 */
		private function bankDetailsAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			$user = $this->getUser();

			// Get and handle Payment Form
			$paymentForm = $this->createForm(PaymentType::class, $user);
			$paymentForm->handleRequest($request);
            $bankForm = $this->createForm(BankChoiceType::class, $user);
            $paymentTwoForm = $this->createForm(BankDetailTwoType::class, $user);
			if($paymentForm->isSubmitted() && $paymentForm->isValid()) {
				
				$this->addFlash(
					'paymentSuccess',
					'Your payment details have been successfully updated'
				);

				// Save User
				$em = $this->getDoctrine()->getManager();
				$em->flush();
			}

			return $this->render(
				'member/profile-bank-details.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
                    'bankForm' => $bankForm->createView(),
                    'paymentTwoForm' => $paymentTwoForm->createView(),
					'paymentForm' => $paymentForm->createView()
				]
			);
		}



        /**
         * @Route("/member/bank-details-two", name="bank_details_two")
         */
        private function bankDetailsTwoAction(Request $request) {

            $user = $this->getUser();

            // Get and handle Payment Form

            $paymentForm = $this->createForm(PaymentType::class, $user);
            $paymentTwoForm = $this->createForm(BankDetailTwoType::class, $user);
            $bankForm = $this->createForm(BankChoiceType::class, $user);

            $paymentTwoForm->handleRequest($request);

            if($paymentTwoForm->isSubmitted() && $paymentTwoForm->isValid()) {

                $this->addFlash(
                    'paymentSuccess',
                    'Your payment details have been successfully updated'
                );

                // Save User
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            }

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

            // Call Formatter
            $formatter = $this->get('app.timeline_format');
            return $this->render(
                'member/profile-bank-details.html.twig',
                [
                    'adm' => $adminsettings,
                    'paymentForm' => $paymentForm->createView(),
                    'paymentTwoForm' => $paymentTwoForm->createView(),
                    'bankForm' => $bankForm->createView(),
                    'timeline' => $formatter->format($notifications)
                ]
            );
        }




		/**
		 * @Route("/member/change-password", name="change_password")
		 */
		private function changePasswordAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			$user = $this->getUser();

			// Get and Handle Settings Form
			$settingsForm = $this->createForm(SettingsType::class, $user);
			$settingsForm->handleRequest($request);

			if($settingsForm->isSubmitted() && $settingsForm->isValid()) {
				// Get encoder
				$encoder = $this->get("security.password_encoder");

				// Check Old Password
				if($encoder->isPasswordValid($user, $user->getOldPassword())) {
					$password = $encoder->encodePassword($user, $user->getPlainPassword());
					$user->setPassword($password);

					// Save User
					$em = $this->getDoctrine()->getManager();
					$em->flush();
		        	
		        	$this->addFlash(
						'settingsSuccess',
						'Password Changed'
					);
				} else {
		        	$this->addFlash(
						'settingsError',
						'Incorrect Password'
					);
				}
			}

			return $this->render(
				'member/profile-change-password.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'settingsForm' => $settingsForm->createView()
				]
			);
		}

		/**
		 * @Route("/member/packs", name="packs")
		 */
		private function packsAction() {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
			$formatter = $this->get('app.timeline_format');
			
			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT p FROM AppBundle:Pack p ORDER BY p.amount ASC";

			
			$packs = $em->createQuery($dql)->getResult();

			$referralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($this->getUser());

			foreach($packs as $pack) {
				if($referralsCount >= $this->getParameter('referral_loyalty')) {
					$ROI = $pack->getFeeders()+1;
				} else {
					$ROI = $pack->getFeeders();
				}
				$pack->setROI($ROI);

				$checker = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->checkUserCurrentPack($this->getUser(), $pack);

				if($checker > 0) {
					$active = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->checkUserActivePack($this->getUser(), $pack);
					if($active > 0) {
						// check feeder
						$pack->setStatus('feeders');
					} else {
						// Make Payment
						$pack->setUserFeeder($this->getDoctrine()->getRepository('AppBundle:Feeder')->getPackFeederEntry($this->getUser(), $pack));
						$pack->setStatus('payment');
					}
				} else {
					$pack->setStatus('granted');
				}
			}

			return $this->render(
				'member/packs-2.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'packs' => $packs
				]
			);
		}

        /**
         * @Route("/member/visit-notif/{id}", name="visit_notif")
         */
        private function visitNotificationAction(Request $request, $id) {

            $user = $this->getUser();

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findAll();
            $em = $this->getDoctrine()->getManager();

            if($user->getRole() == 'ROLE_SUPER_ADMIN'){
                return $this->redirectToRoute('list_admin_payments');
            }else{
                $notification  = $this->getDoctrine()->getRepository('AppBundle:Notification')->findOneBy(
                    array(
                        'id' => $id
                    )
                );
                if($notification->getStatus() == 'unread'){
                    $notification->setStatus('read');
                }
                if($notification->getType() == 2 ){
                    if($notification->getObject() != null){
                        return $this->redirectToRoute('make_payment', array('feeder' => $notification->getObject()));
                    }else{


                        $feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->findOneBy(
                            array(
                                'feeder' => $user->getId(),
                                'receiver' => $notification->getSubject()
                            )
                        );
                        $pack = $feeder->getPack();

                        return $this->redirectToRoute('await_merging', array('pack' => $pack));
                    }

                }elseif ($notification->getType() == 10001){

                    $feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->findOneBy(
                        array(
                            'feeder' => $notification->getSubject(),
                            'status' => 'fully_paid'
                        )
                    );
                    $notification->setStatus('read');

                    $em->persist($notification);
                    $em->flush();
                    $em->clear();



                    return $this->redirectToRoute('pack_subscription_feeders', array('pack' => $feeder->getPack()->getId(), 'feeder' => $feeder->getPack()->getId()));
                }elseif ($notification->getType() == 10002){



                    $notification->setStatus('read');

                    $em->persist($notification);
                    $em->flush();
                    $em->clear();



                    return $this->redirectToRoute('check_payment', array('feeder' => $notification->getSubject()));
                }elseif ($notification->getType() == 10006){

                    $feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->findOneBy(
                        array(
                            'feeder' => $notification->getSubject(),
                            'status' => 'fully_paid'
                        )
                    );
                    $notification->setStatus('read');

                    $em->persist($notification);
                    $em->flush();
                    $em->clear();



                    return $this->redirectToRoute('pack_subscription_feeders', array('pack' => $feeder->getPack()->getId(), 'feeder' => $feeder->getPack()->getId()));
                }elseif ($notification->getType() > 10000){
                    return $this->redirectToRoute('check_payment', array('feeder' => $user->getId()));
                }else{
                    return $this->redirectToRoute('');
                }

            }


        }

        /**
		 * @Route("/member/feeders", name="feeders")
		 */
		private function feedersAction() {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			// Subscriptions
			$subscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserActivePackSubscriptions($this->getUser());

			if($subscriptions == null) {

				$this->addFlash(
					'subscribeError',
					"You are not been activated under any pack"
				);

				return $this->redirectToRoute('packs');

			} else {

				foreach($subscriptions as $subscription) {
					$subscription->setFeeders($this->getDoctrine()->getRepository('AppBundle:Feeder')->getSubscriptionFeeders($subscription));
				}
			}

			return $this->render(
				'member/feeders.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'subscriptions' => $subscriptions
				]
			);
		}

		/**
		 * @Route("/member/pack-subscription/jury/{jury}/new/{pack}/{user}", name="pack_subscription_jury")
		 */
		private function packSubscriptionJuryAction(Request $request, $jury, $pack, $user) {
    //   switch
			// $session = $request->getSession();

			// $pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			
			$entityManager = $this->getDoctrine()->getEntityManager();

			$query = $entityManager->createQuery(
				'SELECT p
				FROM AppBundle:Pack p
				WHERE p.id = :id'
			)->setParameter('id', $pack)->setMaxResults(1);
	
			// returns an array of Product objects
			$pack =  $query->getOneOrNullResult();

			$limit = $this->getParameter('max_user_subscription_limit');

			$adminKeys = $this->getParameter('admin_users');

			$adminCollectionInterval = $this->getParameter('admin_collection_interval');

			$user = $this->getDoctrine()->getRepository('AppBundle:User')->find($user);

			// Check user active subscriptions limit
			if($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserCurrentSubscriptionsCount($user) < $limit) {
				// Check if user has subscribed for plan
				if($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->checkUserCurrentPack($user, $pack) > 0) {
					// Subscribed, deny access

					return $this->redirectToRoute('demi_god_case', array('jury' => $jury));
				} else {
					// Check first active subscriber for plan id
					$firstActiveSubscriber = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getFirstActiveSubscriber($pack);
					// Check last plan subscriber for plan id
					$lastSubscriber = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getLastSubscriber($pack);

					if(null === $firstActiveSubscriber) {

						$firstActiveSubscriber = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy(
							array(
								'pack' => $pack,
								'userType' => 'admin'
							)
						);

						// Start a new cycle
						$nextAdminTrack = null;
						$nextAdmin = $firstActiveSubscriber->getUser()->getId();
						$nextAdminTrackStatus = 'admin_cycling';
						$nextAdminTrackLimit = $adminCollectionInterval;

					} else {

						if($firstActiveSubscriber->getUserType() == 'admin' && $lastSubscriber->getUserType() == 'admin') {

							// set default params
							$nextAdminTrack = null;
							$nextAdmin = $adminKeys[0];
							$nextAdminTrackStatus = 'admin_cycling';
							$nextAdminTrackLimit = $adminCollectionInterval;

						} else {
							// calculate here
							switch ($lastSubscriber->getAdminTrackStatus()) {
								case 'user_cycling':
									$nextAdminTrack = $lastSubscriber->getAdminTrack() + 1;
									$nextAdmin = null;
									// check if to continue user_cycling or switch to 'user_cycled'
									if($nextAdminTrack == $lastSubscriber->getAdminTrackLimit()) {
										$nextAdminTrackStatus = 'user_cycled';
									} else {
										$nextAdminTrackStatus = 'user_cycling';
									}
									$nextAdminTrackLimit = $lastSubscriber->getAdminTrackLimit();
									break;

								case 'user_cycled':
									// switch to admin_cycling and add default start admin_cycling parameters
									$nextAdminTrack = null;
									$nextAdmin = $adminKeys[0];
									$nextAdminTrackStatus = 'admin_cycling';
									$nextAdminTrackLimit = $lastSubscriber->getAdminTrackLimit();
									break;

								case 'admin_cycling':
									// check if admin_cycling is to continue or switch to admin_cycled
									$nextAdminTrack = null;
									$lastAdminId = $lastSubscriber->getAdmin();

									$lastAdminUserKey = count($adminKeys) - 1;

									$currentAdminKey = null;

									for($i=0;$i<count($adminKeys);$i++) {
										if($lastAdminId == $adminKeys[$i]) {
											$currentAdminKey = $i;
										}
									}

									$nextAdminKey = $currentAdminKey;
                                    //$nextAdminKey = $currentAdminKey + 1;

									$nextAdmin = $adminKeys[$nextAdminKey];

									if($nextAdmin == $adminKeys[$lastAdminUserKey]) {
										$nextAdminTrackStatus = 'admin_cycled';
										$nextAdminTrackLimit = $adminCollectionInterval;
									} else {
										$nextAdminTrackStatus = 'admin_cycling';
										$nextAdminTrackLimit = $lastSubscriber->getAdminTrackLimit();
									}

									break;

								case 'admin_cycled':
									// switch to user_cycling and set default parameters
									$nextAdminTrack = 1;
									$nextAdmin = null;
									$nextAdminTrackStatus = 'user_cycling';
									$nextAdminTrackLimit = $adminCollectionInterval;
									break;
								
								default:
									// add flash for internal error
									break;
							}
						}
					}

					// Check if Matching Mode is set to manual
					$admin = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

					if($admin->getMatchMode() == 'manual') {
						
						$now = new \DateTime('now');
						$nowDay = date_format($now, "l");
						$endObject = new \DateTime('now');
						// Calculate Weekend intervals here
						if($nowDay == 'Sunday') {
							$end = $endObject->add(new \DateInterval('PT20H'));
						} else if($nowDay == 'Saturday') {
							$end = $endObject->add(new \DateInterval('PT20H'));
						} else {
							$end = $endObject->add(new \DateInterval('PT20H'));
						}

						// Get Total Feeders
						$referralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($user);
						if($referralsCount >= $this->getParameter('referral_loyalty')) {
							$totalFeeders = $pack->getFeeders()+1;
						} else {
							$totalFeeders = $pack->getFeeders();
						}

						// Create Subscription
						$packSubscription = new PackSubscription;
						$packSubscription->setPack($pack);
						$packSubscription->setUser($user);
						$packSubscription->setUserType('user');
						$packSubscription->setFeederCounter($totalFeeders);
						$packSubscription->setFeederTotal($totalFeeders);
						$packSubscription->setAdminTrack($nextAdminTrack);
						$packSubscription->setAdmin($nextAdmin);
						$packSubscription->setAdminTrackStatus($nextAdminTrackStatus);
						$packSubscription->setAdminTrackLimit($nextAdminTrackLimit);
						$packSubscription->setStatus('waiting');
						$packSubscription->setType('pack');
						$packSubscription->setCreated($now);

						try {

							// Save packSubscription
							$em = $this->getDoctrine()->getManager();
							$em->persist($packSubscription);
							$em->flush();

							// if nextAdminTrackStatus = user_cycled reset all admin counters
							if($nextAdminTrackStatus == 'user_cycled') {
								$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->resetAdminCounters($pack);
							}

							// Check if a user's wallet should be credited
							$shouldCreditReferrerWallet = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->shouldCreditReferrerWallet($user);

							if($shouldCreditReferrerWallet == 1) {
								// Get Referrer Details
								if($user->getReferrer() != null) {
									// Credit Wallet
									$referrer = $this->getDoctrine()->getRepository('AppBundle:User')->find($user->getReferrer());
									if($referrer != null) {
										// Get Amount
										$amount = $pack->getAmount();
										// Calculate Percentage
										$receiverShare = ($amount * $this->getParameter('referral_percentage')) / 100;
										// Add Transaction
										$transaction = new Transaction;
										$transaction->setPack($pack);
										$transaction->setFeederPackSubscription($packSubscription);
										$transaction->setFeeder($user);
										$transaction->setReceiver($referrer);
										$transaction->setAmount($amount);
										$transaction->setReceiverShare($receiverShare);
										$transaction->setStatus('not_confirmed');
										$transaction->setCreated($now);

										$em->persist($transaction);
										$em->flush();
									}
								}
							}

							// Add notification for Feeder
							$notification = new Notification;
							$notification->setType(2);
							$notification->setSubject($user->getId());
							$notificationDetails = "You subscribed for {$pack->getName()} Pack";
							$notification->setDetails($notificationDetails);
							$notification->setCreated($now);
                            $notification->setStatus("unread");

							$em->persist($notification);
							$em->flush();

							$em->clear();
						} catch (Exception $e) {

							return $this->redirectToRoute('demi_god_case', array('jury' => $jury));
						}

						return $this->redirectToRoute('demi_god_case', array('jury' => $jury));
					} else {
						if($nextAdmin != null) {
							// Pay Admin

							// Get Admin Subscription
							$receiver = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy(
								array(
									'pack' => $pack,
									'user' => $nextAdmin,
									'userType' => 'admin'
								)
							);

							$now = new \DateTime('now');
							$nowDay = date_format($now, "l");
							$endObject = new \DateTime('now');
							// Calculate Weekend intervals here
							if($nowDay == 'Sunday') {
								$end = $endObject->add(new \DateInterval('PT20H'));
							} else if($nowDay == 'Saturday') {
								$end = $endObject->add(new \DateInterval('PT20H'));
							} else {
								$end = $endObject->add(new \DateInterval('PT20H'));
							}

							// Get Total Feeders
							$referralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($user);
							if($referralsCount >= $this->getParameter('referral_loyalty')) {
								$totalFeeders = $pack->getFeeders()+1;
							} else {
								$totalFeeders = $pack->getFeeders();
							}

							// Create Subscription
							$packSubscription = new PackSubscription;
							$packSubscription->setPack($pack);
							$packSubscription->setUser($user);
							$packSubscription->setUserType('user');
							$packSubscription->setFeederCounter($totalFeeders);
							$packSubscription->setFeederTotal($totalFeeders);
							$packSubscription->setAdminTrack($nextAdminTrack);
							$packSubscription->setAdmin($nextAdmin);
							$packSubscription->setAdminTrackStatus($nextAdminTrackStatus);
							$packSubscription->setAdminTrackLimit($nextAdminTrackLimit);
							$packSubscription->setStatus('inactive');
							$packSubscription->setType('pack');
							$packSubscription->setCreated($now);

							try {

								// Save packSubscription
								$em = $this->getDoctrine()->getManager();
								$em->persist($packSubscription);
								$em->flush();

								// Create Feeder
								$feeder = new Feeder;
								$feeder->setFeeder($user);
								$feeder->setReceiver($receiver->getUser());
								$feeder->setPack($pack);
								$feeder->setFeederPackSubscription($packSubscription);
								$feeder->setReceiverPackSubscription($receiver);
								$feeder->setFeederTimeStart($now);
								$feeder->setFeederTimeEnd($end);
								$feeder->setStatus('not_paid');
								$feeder->setCreated($now);

								// Save feeder
								$em = $this->getDoctrine()->getManager();
								$em->persist($feeder);
								$em->flush();

								// Deduct counter
								$receiver->setFeederCounter(0);

								// if nextAdminTrackStatus = user_cycled reset all admin counters
								if($nextAdminTrackStatus == 'user_cycled') {
									$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->resetAdminCounters($pack);
								}

								// Check if a user's wallet should be credited
								$shouldCreditReferrerWallet = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->shouldCreditReferrerWallet($user);
								if($shouldCreditReferrerWallet == 1) {
									// Get Referrer Details
									if($user->getReferrer() != null) {
										// Credit Wallet
										$referrer = $this->getDoctrine()->getRepository('AppBundle:User')->find($user->getReferrer());
										if($referrer != null) {
											// Get Amount
											$amount = $pack->getAmount();
											// Calculate Percentage
											$receiverShare = ($amount * $this->getParameter('referral_percentage')) / 100;
											// Add Transaction
											$transaction = new Transaction;
											$transaction->setPack($pack);
											$transaction->setFeederPackSubscription($packSubscription);
											$transaction->setFeeder($user);
											$transaction->setReceiver($referrer);
											$transaction->setAmount($amount);
											$transaction->setReceiverShare($receiverShare);
											$transaction->setStatus('not_confirmed');
											$transaction->setCreated($now);

											$em->persist($transaction);
											$em->flush();
										}
									}
								}

								// Add notification for Feeder
								$notification = new Notification;
								$notification->setType(2);
								$notification->setSubject($user->getId());
								$notificationDetails = "You subscribed for {$pack->getName()} Pack";
								$notification->setDetails($notificationDetails);
								$notification->setCreated($now);
                                $notification->setStatus("unread");

								$em->persist($notification);
								$em->flush();

								$feederId = $feeder->getId();

								$em->clear();

							} catch(Exception $e) {

								return $this->redirectToRoute('demi_god_case', array('jury' => $jury));
							}

							return $this->redirectToRoute('demi_god_case', array('jury' => $jury));
						} else {
							// Pay User

							// Get Last User
							$receiver = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getNextReceiver($pack);

							$now = new \DateTime('now');
							$nowDay = date_format($now, "l");
							$endObject = new \DateTime('now');
							// Calculate Weekend intervals here
							if($nowDay == 'Sunday') {
								$end = $endObject->add(new \DateInterval('PT20H'));
							} else if($nowDay == 'Saturday') {
								$end = $endObject->add(new \DateInterval('PT20H'));
							} else {
								$end = $endObject->add(new \DateInterval('PT20H'));
							}

							// Get Total Feeders
							$referralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($user);
							if($referralsCount >= $this->getParameter('referral_loyalty')) {
								$totalFeeders = $pack->getFeeders()+1;
							} else {
								$totalFeeders = $pack->getFeeders();
							}

							// Create Subscription
							$packSubscription = new PackSubscription;
							$packSubscription->setPack($pack);
							$packSubscription->setUser($user);
							$packSubscription->setUserType('user');
							$packSubscription->setFeederCounter($totalFeeders);
							$packSubscription->setFeederTotal($totalFeeders);
							$packSubscription->setAdminTrack($nextAdminTrack);
							$packSubscription->setAdmin($nextAdmin);
							$packSubscription->setAdminTrackStatus($nextAdminTrackStatus);
							$packSubscription->setAdminTrackLimit($nextAdminTrackLimit);
							$packSubscription->setStatus('inactive');
							$packSubscription->setType('pack');
							$packSubscription->setCreated($now);

							try {

								// Save packSubscription
								$em = $this->getDoctrine()->getManager();
								$em->persist($packSubscription);
								$em->flush();

								// Create Feeder
								$feeder = new Feeder;
								$feeder->setFeeder($user);
								$feeder->setReceiver($receiver->getUser());
								$feeder->setPack($pack);
								$feeder->setFeederPackSubscription($packSubscription);
								$feeder->setReceiverPackSubscription($receiver);
								$feeder->setFeederTimeStart($now);
								$feeder->setFeederTimeEnd($end);
								$feeder->setStatus('not_paid');
								$feeder->setCreated($now);

								// Save feeder
								$em = $this->getDoctrine()->getManager();
								$em->persist($feeder);
								$em->flush();

								// Deduct counter
								$counter = $receiver->getFeederCounter() - 1;
								$receiver->setFeederCounter($counter);

								// if nextAdminTrackStatus = user_cycled reset all admin counters
								if($nextAdminTrackStatus == 'user_cycled') {
									$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->resetAdminCounters($pack);
								}

								// Check if a user's wallet should be credited
								$shouldCreditReferrerWallet = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->shouldCreditReferrerWallet($user);
								if($shouldCreditReferrerWallet == 1) {
									// Get Referrer Details
									if($user->getReferrer() != null) {
										// Credit Wallet
										$referrer = $this->getDoctrine()->getRepository('AppBundle:User')->find($user->getReferrer());
										if($referrer != null) {
											// Get Amount
											$amount = $pack->getAmount();
											// Calculate Percentage
											$receiverShare = ($amount * $this->getParameter('referral_percentage')) / 100;
											// Add Transaction
											$transaction = new Transaction;
											$transaction->setPack($pack);
											$transaction->setFeederPackSubscription($packSubscription);
											$transaction->setFeeder($user);
											$transaction->setReceiver($referrer);
											$transaction->setAmount($amount);
											$transaction->setReceiverShare($receiverShare);
											$transaction->setStatus('not_confirmed');
											$transaction->setCreated($now);

											$em->persist($transaction);
											$em->flush();
										}
									}
								}

								// Add notification for Feeder
								$notification = new Notification;
								$notification->setType(2);
								$notification->setSubject($user->getId());
								$notificationDetails = "You subscribed for {$pack->getName()} Pack";
								$notification->setDetails($notificationDetails);
								$notification->setCreated($now);
                                $notification->setStatus("unread");

								$em->persist($notification);
								$em->flush();

								$feederId = $feeder->getId();

								$em->clear();

							} catch (Exception $e) {

								return $this->redirectToRoute('demi_god_case', array('jury' => $jury));
							}

							return $this->redirectToRoute('demi_god_case', array('jury' => $jury));
						}
					}
				}
			} else {

				return $this->redirectToRoute('demi_god_case', array('jury' => $jury));

			}
		}

		/**
		 * @Route("/member/pack-subscription/new/{pack}", name="pack_subscription_new")
		 */
		private function packSubscriptionNewAction(Request $request, $pack) {

			$session = $request->getSession();

			// $pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			$entityManager = $this->getDoctrine()->getEntityManager();

			$query = $entityManager->createQuery(
				'SELECT p
				FROM AppBundle:Pack p
				WHERE p.id = :id'
			)->setParameter('id', $pack)->setMaxResults(1);
	
			// returns an array of Product objects
			$pack =  $query->getOneOrNullResult();

			$limit = $this->getParameter('max_user_subscription_limit');

			$adminKeys = $this->getParameter('admin_users');

			$adminCollectionInterval = $this->getParameter('admin_collection_interval');

			// Check user active subscriptions limit
			if($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserCurrentSubscriptionsCount($this->getUser()) < $limit) {
				// Check if user has subscribed for plan
				if($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->checkUserCurrentPack($this->getUser(), $pack) > 0) {
					// Subscribed, deny access

					$this->addFlash(
						'subscribeError',
						"You have subscribed for the {$pack->getName()} pack already"
					);

					return $this->redirectToRoute('packs');
				} else {

					// Check first active subscriber for plan id
					$firstActiveSubscriber = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getFirstActiveSubscriber($pack);
					// Check last plan subscriber for plan id
					$lastSubscriber = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getLastSubscriber($pack);

					if(null === $firstActiveSubscriber) {
                        try {
                                $em = $this->getDoctrine()->getManager();
                                $now = new \DateTime('now');
                                $nowDay = date_format($now, "l");
                                $endObject = new \DateTime('now');
                                // Calculate Weekend intervals here
                                if ($nowDay == 'Sunday') {
                                    $end = $endObject->add(new \DateInterval('PT20H'));
                                } else if ($nowDay == 'Saturday') {
                                    $end = $endObject->add(new \DateInterval('PT20H'));
                                } else {
                                    $end = $endObject->add(new \DateInterval('PT20H'));
                                }

                                $admin = $this->getDoctrine()->getRepository('AppBundle:User')->find(1);
                                $packSubscription = new PackSubscription;
                                $packSubscription->setPack($pack);
                                $packSubscription->setUser($admin);
                                $packSubscription->setUserType('admin');
                                $packSubscription->setFeederCounter(1);
                                $packSubscription->setFeederTotal(1);
                                $packSubscription->setAdminTrackLimit($this->getParameter('admin_collection_interval'));
                                $packSubscription->setType('pack');
                                $packSubscription->setStatus('active');
//		                        $packSubscription->setAutoRecirculate($pack);
		                        $packSubscription->setForcedWaiting(false);
		                        $packSubscription->setPaidCommission(false);
		                        $packSubscription->setRecommitRequired(true);
		                        $packSubscription->setCreated($now);
                                $em->persist($packSubscription);

                                $em->flush();
                        }catch (Exception $e) {
                            $this->addFlash(
                                'errorNotice',
                                'An error occured while trying to subscribe admin to  pack'
                            );
                        }

						$firstActiveSubscriber = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy(
							array(
								'pack' => $pack,
								'userType' => 'admin'
							)
						);

						// Start a new cycle
						$nextAdminTrack = null;
						$nextAdmin = $firstActiveSubscriber->getUser()->getId();
						$nextAdminTrackStatus = 'admin_cycling';
						$nextAdminTrackLimit = $adminCollectionInterval;

					} else {

						if($firstActiveSubscriber->getUserType() == 'admin' && $lastSubscriber->getUserType() == 'admin') {

							// set default params
							$nextAdminTrack = null;
							$nextAdmin = $adminKeys[0];
							$nextAdminTrackStatus = 'admin_cycling';
							$nextAdminTrackLimit = $adminCollectionInterval;

						} else {
							// calculate here
							switch ($lastSubscriber->getAdminTrackStatus()) {
								case 'user_cycling':
									$nextAdminTrack = $lastSubscriber->getAdminTrack() + 1;
									$nextAdmin = null;
									// check if to continue user_cycling or switch to 'user_cycled'
									if($nextAdminTrack == $lastSubscriber->getAdminTrackLimit()) {
										$nextAdminTrackStatus = 'user_cycled';
									} else {
										$nextAdminTrackStatus = 'user_cycling';
									}
									$nextAdminTrackLimit = $lastSubscriber->getAdminTrackLimit();
									break;

								case 'user_cycled':
									// switch to admin_cycling and add default start admin_cycling parameters
									$nextAdminTrack = null;
									$nextAdmin = $adminKeys[0];
									$nextAdminTrackStatus = 'admin_cycling';
									$nextAdminTrackLimit = $lastSubscriber->getAdminTrackLimit();
									break;

								case 'admin_cycling':
									// check if admin_cycling is to continue or switch to admin_cycled
									$nextAdminTrack = null;
									$lastAdminId = $lastSubscriber->getAdmin();

									$lastAdminUserKey = count($adminKeys) - 1;

									$currentAdminKey = null;

									for($i=0;$i<count($adminKeys);$i++) {
										if($lastAdminId == $adminKeys[$i]) {
											$currentAdminKey = $i;
										}
									}

									$nextAdminKey = $currentAdminKey;
                                    //$nextAdminKey = $currentAdminKey + 1;

									$nextAdmin = $adminKeys[$nextAdminKey];

									if($nextAdmin == $adminKeys[$lastAdminUserKey]) {
										$nextAdminTrackStatus = 'admin_cycled';
										$nextAdminTrackLimit = $adminCollectionInterval;
									} else {
										$nextAdminTrackStatus = 'admin_cycling';
										$nextAdminTrackLimit = $lastSubscriber->getAdminTrackLimit();
									}

									break;

								case 'admin_cycled':
									// switch to user_cycling and set default parameters
									$nextAdminTrack = 1;
									$nextAdmin = null;
									$nextAdminTrackStatus = 'user_cycling';
									$nextAdminTrackLimit = $adminCollectionInterval;
									break;

								default:
									// add flash for internal error
									break;
							}
						}
					}

					// Check if Matching Mode is set to manual
					$admin = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

					if($admin->getMatchMode() == 'manual') {
						
						$now = new \DateTime('now');
						$nowDay = date_format($now, "l");
						$endObject = new \DateTime('now');
						// Calculate Weekend intervals here
						if($nowDay == 'Sunday') {
							$end = $endObject->add(new \DateInterval('PT20H'));
						} else if($nowDay == 'Saturday') {
							$end = $endObject->add(new \DateInterval('PT20H'));
						} else {
							$end = $endObject->add(new \DateInterval('PT20H'));
						}

						// Get Total Feeders
						$referralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($this->getUser());
						if($referralsCount >= $this->getParameter('referral_loyalty')) {
							$totalFeeders = $pack->getFeeders()+1;
						} else {
							$totalFeeders = $pack->getFeeders();
						}

						// Create Subscription
						$packSubscription = new PackSubscription;
						$packSubscription->setPack($pack);
						$packSubscription->setUser($this->getUser());
						$packSubscription->setUserType('user');
						$packSubscription->setFeederCounter($totalFeeders);
						$packSubscription->setFeederTotal($totalFeeders);
						$packSubscription->setAdminTrack($nextAdminTrack);
						$packSubscription->setAdmin($nextAdmin);
						$packSubscription->setAdminTrackStatus($nextAdminTrackStatus);
						$packSubscription->setAdminTrackLimit($nextAdminTrackLimit);
						$packSubscription->setStatus('waiting');
						$packSubscription->setType('pack');
//						$packSubscription->setAutoRecirculate($pack);
						$packSubscription->setForcedWaiting(false);
						$packSubscription->setPaidCommission(false);
						$packSubscription->setRecommitRequired(true);
						$packSubscription->setCreated($now);

						try {

							// Save packSubscription
							$em = $this->getDoctrine()->getManager();
							$em->persist($packSubscription);
							$em->flush();

							// if nextAdminTrackStatus = user_cycled reset all admin counters
							if($nextAdminTrackStatus == 'user_cycled') {
								$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->resetAdminCounters($pack);
							}

							// Check if a user's wallet should be credited
							$shouldCreditReferrerWallet = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->shouldCreditReferrerWallet($this->getUser());
							if($shouldCreditReferrerWallet == 1) {
								// Get Referrer Details
								if($this->getUser()->getReferrer() != null) {
									// Credit Wallet
									$referrer = $this->getDoctrine()->getRepository('AppBundle:User')->find($this->getUser()->getReferrer());
									if($referrer != null) {
										// Get Amount
										$amount = $pack->getAmount();
										// Calculate Percentage
										$receiverShare = ($amount * $this->getParameter('referral_percentage')) / 100;
										// Add Transaction
										$transaction = new Transaction;
										$transaction->setPack($pack);
										$transaction->setFeederPackSubscription($packSubscription);
										$transaction->setFeeder($this->getUser());
										$transaction->setReceiver($referrer);
										$transaction->setAmount($amount);
										$transaction->setReceiverShare($receiverShare);
										$transaction->setStatus('not_confirmed');
										$transaction->setCreated($now);

										$em->persist($transaction);
										$em->flush();
									}
								}
							}

							// Add notification for Feeder
							$notification = new Notification;
							$notification->setType(2);
							$notification->setSubject($this->getUser()->getId());
							$notificationDetails = "You subscribed for {$pack->getName()} Pack";
							$notification->setDetails($notificationDetails);
							$notification->setCreated($now);
                            $notification->setStatus("unread");

							$em->persist($notification);
							$em->flush();

							$em->clear();

							$this->addFlash(
								'subscribeSuccess',
								'You have successfully subscribed for '.$pack->getName()
							);
						} catch(Exception $e) {

							$this->addFlash(
								'subscribeError',
								'An error occurred while subscribing for the '.$pack->getName().' pack. Please try again later'
							);

							return $this->redirectToRoute('packs');
						}

						return $this->redirectToRoute('await_merging', array('pack' => $pack->getId()));
					} else {
                    //    $nextAdmin = 1;
						if($nextAdmin != null) {
							// Pay Admin

							// Get Admin Subscription
							$receiver = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy(
								array(
									'pack' => $pack,
									'user' => $nextAdmin,
									'userType' => 'admin'
								)
							);

							$now = new \DateTime('now');
							$nowDay = date_format($now, "l");
							$endObject = new \DateTime('now');
							// Calculate Weekend intervals here
							if($nowDay == 'Sunday') {
								$end = $endObject->add(new \DateInterval('PT20H'));
							} else if($nowDay == 'Saturday') {
								$end = $endObject->add(new \DateInterval('PT20H'));
							} else {
								$end = $endObject->add(new \DateInterval('PT20H'));
							}

							// Get Total Feeders
							$referralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($this->getUser());
							if($referralsCount >= $this->getParameter('referral_loyalty')) {
								$totalFeeders = $pack->getFeeders()+1;
							} else {
								$totalFeeders = $pack->getFeeders();
							}

							// Create Subscription
							$packSubscription = new PackSubscription;
							$packSubscription->setPack($pack);
							$packSubscription->setUser($this->getUser());
							$packSubscription->setUserType('user');
							$packSubscription->setFeederCounter($totalFeeders);
							$packSubscription->setFeederTotal($totalFeeders);
							$packSubscription->setAdminTrack($nextAdminTrack);
							$packSubscription->setAdmin($nextAdmin);
							$packSubscription->setAdminTrackStatus($nextAdminTrackStatus);
							$packSubscription->setAdminTrackLimit($nextAdminTrackLimit);
							$packSubscription->setStatus('inactive');
							$packSubscription->setType('pack');
							$packSubscription->setCreated($now);

							try {

								// Save packSubscription
								$em = $this->getDoctrine()->getManager();
								$em->persist($packSubscription);
								$em->flush();

								// Create Feeder
								$feeder = new Feeder;
								$feeder->setFeeder($this->getUser());
								$feeder->setReceiver($receiver->getUser());
								$feeder->setPack($pack);
								$feeder->setFeederPackSubscription($packSubscription);
								$feeder->setReceiverPackSubscription($receiver);
								$feeder->setFeederTimeStart($now);
								$feeder->setFeederTimeEnd($end);
								$feeder->setStatus('not_paid');
								$feeder->setCreated($now);

								// Save feeder
								$em = $this->getDoctrine()->getManager();
								$em->persist($feeder);
								$em->flush();

								// Deduct counter
								$receiver->setFeederCounter(0);

								// if nextAdminTrackStatus = user_cycled reset all admin counters
								if($nextAdminTrackStatus == 'user_cycled') {
									$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->resetAdminCounters($pack);
								}

								// Check if a user's wallet should be credited
								$shouldCreditReferrerWallet = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->shouldCreditReferrerWallet($this->getUser());
								if($shouldCreditReferrerWallet == 1) {
									// Get Referrer Details
									if($this->getUser()->getReferrer() != null) {
										// Credit Wallet
										$referrer = $this->getDoctrine()->getRepository('AppBundle:User')->find($this->getUser()->getReferrer());
										if($referrer != null) {
											// Get Amount
											$amount = $pack->getAmount();
											// Calculate Percentage
											$receiverShare = ($amount * $this->getParameter('referral_percentage')) / 100;
											// Add Transaction
											$transaction = new Transaction;
											$transaction->setPack($pack);
											$transaction->setFeederPackSubscription($packSubscription);
											$transaction->setFeeder($this->getUser());
											$transaction->setReceiver($referrer);
											$transaction->setAmount($amount);
											$transaction->setReceiverShare($receiverShare);
											$transaction->setStatus('not_confirmed');
											$transaction->setCreated($now);

											$em->persist($transaction);
											$em->flush();
										}
									}
								}

								// Add notification for Feeder
								$notification = new Notification;
								$notification->setType(2);
								$notification->setSubject($this->getUser()->getId());
								$notificationDetails = "You subscribed for {$pack->getName()} Pack";
								$notification->setDetails($notificationDetails);
								$notification->setCreated($now);
                                $notification->setStatus("unread");

								$em->persist($notification);
								$em->flush();

								$feederId = $feeder->getId();

								$em->clear();

								$this->addFlash(
									'subscribeSuccess',
									'You have successfully subscribed for '.$pack->getName().'. You are to make payment to the person below'
								);
							} catch(Exception $e) {

								$this->addFlash(
									'subscribeError',
									'An error occurred while subscribing for the '.$pack->getName().' pack. Please try again later'
								);

								return $this->redirectToRoute('packs');
							}

							return $this->redirectToRoute('make_payment', array('feeder' => $feederId));
						} else {
							// Pay User

							// Get Last User
							$receiver = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getNextReceiver($pack);

							$now = new \DateTime('now');
							$nowDay = date_format($now, "l");
							$endObject = new \DateTime('now');
							// Calculate Weekend intervals here
							if($nowDay == 'Sunday') {
								$end = $endObject->add(new \DateInterval('PT20H'));
							} else if($nowDay == 'Saturday') {
								$end = $endObject->add(new \DateInterval('PT20H'));
							} else {
								$end = $endObject->add(new \DateInterval('PT20H'));
							}

							// Get Total Feeders
							$referralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($this->getUser());
							if($referralsCount >= $this->getParameter('referral_loyalty')) {
								$totalFeeders = $pack->getFeeders()+1;
							} else {
								$totalFeeders = $pack->getFeeders();
							}

							// Create Subscription
							$packSubscription = new PackSubscription;
							$packSubscription->setPack($pack);
							$packSubscription->setUser($this->getUser());
							$packSubscription->setUserType('user');
							$packSubscription->setFeederCounter($totalFeeders);
							$packSubscription->setFeederTotal($totalFeeders);
							$packSubscription->setAdminTrack($nextAdminTrack);
							$packSubscription->setAdmin($nextAdmin);
							$packSubscription->setAdminTrackStatus($nextAdminTrackStatus);
							$packSubscription->setAdminTrackLimit($nextAdminTrackLimit);
							$packSubscription->setStatus('inactive');
							$packSubscription->setType('pack');
							$packSubscription->setCreated($now);

							try {

								// Save packSubscription
								$em = $this->getDoctrine()->getManager();
								$em->persist($packSubscription);
								$em->flush();

								// Create Feeder
								$feeder = new Feeder;
								$feeder->setFeeder($this->getUser());
								$feeder->setReceiver($receiver->getUser());
								$feeder->setPack($pack);
								$feeder->setFeederPackSubscription($packSubscription);
								$feeder->setReceiverPackSubscription($receiver);
								$feeder->setFeederTimeStart($now);
								$feeder->setFeederTimeEnd($end);
								$feeder->setStatus('not_paid');
								$feeder->setCreated($now);

								// Save feeder
								$em = $this->getDoctrine()->getManager();
								$em->persist($feeder);
								$em->flush();

								// Deduct counter
								$counter = $receiver->getFeederCounter() - 1;
								$receiver->setFeederCounter($counter);

								// if nextAdminTrackStatus = user_cycled reset all admin counters
								if($nextAdminTrackStatus == 'user_cycled') {
									$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->resetAdminCounters($pack);
								}

								// Check if a user's wallet should be credited
								$shouldCreditReferrerWallet = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->shouldCreditReferrerWallet($this->getUser());
								if($shouldCreditReferrerWallet == 1) {
									// Get Referrer Details
									if($this->getUser()->getReferrer() != null) {
										// Credit Wallet
										$referrer = $this->getDoctrine()->getRepository('AppBundle:User')->find($this->getUser()->getReferrer());
										if($referrer != null) {
											// Get Amount
											$amount = $pack->getAmount();
											// Calculate Percentage
											$receiverShare = ($amount * $this->getParameter('referral_percentage')) / 100;
											// Add Transaction
											$transaction = new Transaction;
											$transaction->setPack($pack);
											$transaction->setFeederPackSubscription($packSubscription);
											$transaction->setFeeder($this->getUser());
											$transaction->setReceiver($referrer);
											$transaction->setAmount($amount);
											$transaction->setReceiverShare($receiverShare);
											$transaction->setStatus('not_confirmed');
											$transaction->setCreated($now);

											$em->persist($transaction);
											$em->flush();
										}
									}
								}

								// Add notification for Feeder
								$notification = new Notification;
								$notification->setType(2);
								$notification->setSubject($this->getUser()->getId());
								$notificationDetails = "You subscribed for {$pack->getName()} Pack";
								$notification->setDetails($notificationDetails);
								$notification->setCreated($now);
                                $notification->setStatus("unread");

								$em->persist($notification);
								$em->flush();

								$feederId = $feeder->getId();

								$em->clear();

								$this->addFlash(
									'subscribeSuccess',
									'You have successfully subscribed for '.$pack->getName().'. You are to make payment to the person below'
								);
							} catch (Exception $e) {

								$this->addFlash(
									'subscribeError',
									'An error occurred while subscribing for the '.$pack->getName().' pack. Please try again later'
								);

								return $this->redirectToRoute('packs');
							}

							return $this->redirectToRoute('make_payment', array('feeder' => $feederId));
						}
					}
				}
			} else {

				$this->addFlash(
					'subscribeError',
					'You have subscribed for '.$limit.' pack(s) already'
				);

				return $this->redirectToRoute('packs');
			}
		}

		/**
		 * @Route("/member/pack-subscription/await-merging/{pack}", name="await_merging")
		 */
		private function awaitMerging($pack) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
			);

			$entityManager = $this->getDoctrine()->getEntityManager();
			
			$formatter = $this->get('app.timeline_format');

			$timeline = $formatter->format($notifications);
			
			// $pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			$query = $entityManager->createQuery(
				'SELECT p
				FROM AppBundle:Pack p
				WHERE p.id = :id'
			)->setParameter('id', $pack)->setMaxResults(1);
	
			$pack =  $query->getOneOrNullResult();


			$subQuery = $entityManager->createQuery(
				'SELECT s
				FROM AppBundle:PackSubscription s
				WHERE s.user = :id'
			)->setParameter('id', $this->getUser()->getId())->setMaxResults(1);
	
			$packSub =  $subQuery->getOneOrNullResult();

			if($pack == null) {
				return $this->render('member/page-not-found.html.twig', []);
			} else {
				// return $this->render('member/awaiting-merging.html.twig', [
				if($this->getUser()->getPaymentMade() && $packSub->getStatus()== "waiting"){

					return $this->render('member/not-confirmed-page.html.twig', []);
				}else if($this->getUser()->getPaymentMade() && $packSub->getStatus() == "active"){
					return $this->redirectToRoute('dashboard');

				}else{
				return $this->render('member/payments.html.twig', [
                    'timeline' => $timeline,
                    'adm' => $adminsettings,
					'pack' => $pack,
					'user_id' => $this->getUser()->getId()
				]);
			  }
			}
		}

		/**
		 * @Route("/member/pack-subscription/feeder/{feeder}/make-payment", name="make_payment")
		 */
		private function makePaymentAction(Request $request, $feeder) {

            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			$userCheck = new UserCheck;

			$feeder = $userCheck->sanitize($this->getDoctrine()->getRepository('AppBundle:Feeder')->find($feeder));

			if(null !== $feeder) {

				// Verify User
				if($feeder->getFeeder()->getId() != $this->getUser()->getId()) {
					return $this->render(
						'member/unauthorized.html.twig',
						[
                            'timeline' => $formatter->format($notifications),
                            'adm' => $adminsettings
                        ]
					);
				}

				$now = new \DateTime('now');

				// if user hass't paid render form/else render payment details

				$packPaymentForm = $this->createForm(PackPaymentType::class, $feeder);
				$packPaymentForm->handleRequest($request);

				if($packPaymentForm->isSubmitted() && $packPaymentForm->isValid()) {
					try {

						if($feeder->getPaymentUploadedFile() != null) {
							// unlink file
							unlink($_SERVER['DOCUMENT_ROOT'].$feeder->getPaymentUploadedFile());
						}

						$paymentFile = $feeder->getPaymentImage();

						$md5_hash = md5(uniqid());

						$paymentFileName = $md5_hash.'.'.$paymentFile->guessExtension();

						// Move file to directory
						$paymentDirectory = $this->getParameter('payment_evidence_real_directory');
						$paymentDbDirectory = $this->getParameter('payment_evidence_directory');

						$paymentFile->move(
							$paymentDirectory,
							$paymentFileName
						);

//						$s3 = new S3Client([
//							'version' => 'latest',
//							'region'  => 'eu-west-1'
//						]);

//						try {
//							$s3->putObject([
//								'Bucket' => 'bucketeer-d4004df7-2b6d-41a7-82b2-8049089a3638',
//								'Key'    => $md5_hash,
//								'Body'   => fopen("$paymentDirectory/$paymentFileName", 'r'),
//								'ACL'    => 'public-read',
//							]);


//						} catch (S3Exception $e) {
//							echo "There was an error uploading the file.\n";
//						}

						// Store file name
						$feeder->setPaymentUploadedFile($paymentDbDirectory.'/'.$paymentFileName);

						if($feeder->getStatus() != 'scam') {
							// Set payment status
							$feeder->setStatus('payment_made');

							// Set receiver time start and time end
							$endObject = new \DateTime('now');
//							$end = $endObject->add(new \DateInterval('PT10H'));
							$end = $endObject->add(new \DateInterval('PT10H'));

							$feeder->setReceiverTimeStart($now);
							$feeder->setReceiverTimeEnd($end);
						} else {
							$feeder->setScamPaid('scam_paid');
							$feeder->setReceiverTimeEnd($feeder->getFeederTimeEnd());
						}

						// Save Payment
						$em = $this->getDoctrine()->getManager();
						$em->flush();

						// Add Notification
						$notification = new Notification;
						$notification->setType(3);
						$notification->setSubject($this->getUser()->getId());
						$notificationDetails = "You paid for {$feeder->getPack()->getName()} Pack";
						$notification->setDetails($notificationDetails);
						$notification->setCreated($now);
                        $notification->setStatus("unread");

						$em->persist($notification);
						$em->flush();
						$em->clear();

						$this->addFlash(
							'subscribeSuccess',
							'Your new payment details was saved. Contact your receiver to verify new payment details and confirm you'
						);
					} catch (Exception $e) {
						$this->addFlash(
							'paymentError',
							'Your payment could not be processed now, please try again'
						);
					}
				}

				if($feeder->getScamPaid() == 'scam_paid') {
					$renderForm = false;
				} elseif($feeder->getStatus() == 'not_paid' || $feeder->getStatus() == 'scam') {
					$renderForm = true;
				} else {
					$renderForm = false;
				}

                $amountToPay = $feeder->getPack()->getAmount();
//                if($adminsettings->getAppcurrency() != "USD"){
//                    $convertedDollarValue = $amountToPay / $adminsettings->getAppcurrencyexchangerate();
//                }else{
                    $convertedDollarValue = $amountToPay;
//                }

//                $curl = curl_init();
//                curl_setopt_array($curl, array(
//                    CURLOPT_RETURNTRANSFER => 1,
//                    CURLOPT_URL => "https://blockchain.info/tobtc?currency=USD&value=$convertedDollarValue"
//                ));
//                $btcamount = curl_exec($curl);


                $receiverId = $feeder->getReceiver();

                $receiver = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
                    array(
                        'id' => $receiverId
                    )
                );

//                    $bitcoinbox = new BtcGateway();

				////$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();
                $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                    array('subject' => $this->getUser()->getId()),
                    array('created' => 'DESC'),
                    10
                );
                $formatter = $this->get('app.timeline_format');
				return $this->render(
					'member/pack-payment.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
						'form' => $packPaymentForm->createView(),
                        'receiver' => $receiver,
						'feeder' => $feeder,
//                        'box' => $bitcoinbox,
						'renderForm' => $renderForm
					]
				);
			} else {
                $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                    array('subject' => $this->getUser()->getId()),
                    array('created' => 'DESC'),
                    10
                );
                $formatter = $this->get('app.timeline_format');
				// Render Page not found
				return $this->render(
					'member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);
			}
		}

		/**
		 * @Route("/member/pack-subscription/feeder/{feeder}/cancel-payment", name="cancel_payment")
		 */
		private function cancelPaymentAction($feeder) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->find($feeder);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($feeder == null) {

				// Render Page not found
				return $this->render(
					'member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);
			} else {

				$em = $this->getDoctrine()->getManager();

				// Cancel Feeder
				$feeder->setStatus('cancelled');

				// Cancel Feeder Pack Subscription
				$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->cancelSubscription($feeder->getFeederPackSubscription());

				// Credit Receiver Plan Subscriber Counter
				$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->incrementSubscriptionFeeders($feeder->getReceiverPackSubscription(), 1);

				// Check if feeder has active subscriptions
				$feederActiveSubscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserActivePackSubscriptionsCount($feeder->getFeeder());

				$em->flush();
				$em->clear();

				//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();
				
				if($feederActiveSubscriptions == 0) {
					// Block Feeder
					$this->getDoctrine()->getRepository('AppBundle:User')->blockUser($feeder->getFeeder());

					$this->get('security.token_storage')->setToken(null);

					return $this->redirectToRoute('login');

				} else {
					// Redirect to packs
					$this->addFlash(
						'subscribeError',
						"You have successfully cancelled the subscription on {$feeder->getPack()->getName()} Package. However, you were not blocked as the system detected you have an active package subscription running"
					);

					return $this->redirectToRoute('packs');
				}
			}
		}

		/**
		 * @Route("/member/packs/pack-subscription/check-payment/{feeder}", name="check_payment")
		 */
		private function checkPaymentAction($feeder) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->find($feeder);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if(null !== $feeder) {

				if($feeder->getReceiver()->getId() != $this->getUser()->getId()) {

					// Not authorized
					return $this->render(
						'member/unauthorized.html.twig',
						[
                            'timeline' => $formatter->format($notifications),
                            'adm' => $adminsettings]
					);
				} else {

					return $this->render(
						'member/check-payment.html.twig',
						[
                            'timeline' => $formatter->format($notifications),
                            'adm' => $adminsettings,
							'feeder' => $feeder
						]
					);
				}
			} else {

				// Render Page not found
				return $this->render(
					'member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);

			}
		}

		/**
		 * @Route("/member/packs/pack-subscription/confirm-payment/{feeder}", name="confirm_payment")
		 */
		private function feederConfirmAction($feeder) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->find($feeder);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

			$feederPackSubscription = $feeder->getFeederPackSubscription();

			if($feederPackSubscription->getForcedWaiting() == true ){

				$feederPackSubscription->setPaidCommission(true);
//						$feederPackSubscription->setStatus('active');
				$feederPackSubscription->setRecommitRequired(false);

				$feederPackSubscription->setForcedWaiting(false);

				$em = $this->getDoctrine()->getManager();
//						$em->persist( $feederPackSubscription );
				$em->flush();
			}

			if($this->getUser()->getRole() == 'ROLE_USER'){
				$receiver_name = $this->getUser()->getUsername();
				$feeder_details = $this->getDoctrine()->getRepository('AppBundle:Feeder')->find($feeder);
				$feeder_user_details = $this->getDoctrine()->getRepository('AppBundle:User')->find($feeder_details->getFeeder());
				$feeder_name = $feeder_user_details->getUsername();

				$telegram = new Telegram();
				$telegram->sendConfirmation($receiver_name,$feeder_name);
			}
			
            $formatter = $this->get('app.timeline_format');
			if(null !== $feeder) {

				if($feeder->getReceiver()->getId() != $this->getUser()->getId()) {

					// Not authorized
					return $this->render(
						'member/unauthorized.html.twig',
						[
                            'timeline' => $formatter->format($notifications),
                            'adm' => $adminsettings
                        ]
					);
				} else {

					$shouldAutoRecycle = $this->getParameter('auto_recycling');
					
					$feederPackSubscription = $feeder->getFeederPackSubscription();

					// Activate Pack Subscription
					$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->activateSubscription($feederPackSubscription);

					// Mark Feeder as Fully Paid
					$this->getDoctrine()->getRepository('AppBundle:Feeder')->markAsPaid($feeder);

//					$feeder->getFeederPackSubscription()->setPaidCommission(true);
//					$feeder->getFeederPackSubscription()->setForcedWaiting(true);


					if($feederPackSubscription->getForcedWaiting() == true ){

						$feederPackSubscription->setPaidCommission(true);
//						$feederPackSubscription->setStatus('active');
						$feederPackSubscription->setRecommitRequired(false);

						$feederPackSubscription->setForcedWaiting(false);

						$em = $this->getDoctrine()->getManager();
//						$em->persist( $feederPackSubscription );
						$em->flush();
					}

					// Check if transaction exists for feeder pack subscription
					$transaction = $this->getDoctrine()->getRepository('AppBundle:Transaction')->findOneBy(
						array(
							'feeder' => $feeder->getFeeder()->getId(),
							'feederPackSubscription' => $feederPackSubscription->getId(),
							'status' => 'not_confirmed'
						)
					);

//					$receiverSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserNextReceiver($pack, $receiver);


					if($transaction != null) {
						// confirm transaction
						$this->getDoctrine()->getRepository('AppBundle:Transaction')->confirmTransaction($transaction);

						// Get Feeder Referrer
						$feederReferrer = $feeder->getFeeder()->getReferrer();

						// Credit Referrer Wallet
						$this->getDoctrine()->getRepository('AppBundle:User')->creditPurse($feederReferrer, $transaction->getReceiverShare());
                        //$creditWallet = $this->getDoctrine()->getRepository('AppBundle:User')->creditPurse($feederReferrer, $transaction->getReceiverShare())
					}

					if($feeder->getReceiverPackSubscription()->getUserType() == 'user' && $feeder->getReceiverPackSubscription()->getType() == 'pack') {					
						
						// Get Count of Receiver Pack Subscription From Feeders table Where feeder is 'fully_paid'
						$paidFeedersCount = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getPaidFeedersCount($feeder->getReceiverPackSubscription());

						$realTotal = $feeder->getReceiverPackSubscription()->getFeederTotal();

						// If Count is == total of Receiver Plan Subscription, mark status as 'completed'
						if($paidFeedersCount >= $realTotal) {
							
							// Complete Subscription
							$this->getDoctrine()->getRepository('AppBundle:PackSubscription')->completeSubscription($feeder->getReceiverPackSubscription());
							
							if($shouldAutoRecycle == true) {
								// Auto recycle
								# Check if user has booked cancel
								$bookCancel = $feeder->getReceiverPackSubscription()->getBookCancel();

								if($bookCancel == null) {
									// Auto Recycle if plan subscription type is not wallet
									if($feeder->getReceiverPackSubscription()->getAutoRecirculate() != null) {
										$autoRecirculatePack = $feeder->getReceiverPackSubscription()->getAutoRecirculate();
									} else {
										$autoRecirculatePack = $feeder->getReceiverPackSubscription()->getPack();
									}
								} else {
									// Cancel Plan
									$cancelSubscription = $bookCancel;
								}
							} else {
								// Do Not Auto Recycle
								if($feeder->getReceiverPackSubscription()->getAutoRecirculate() != null) {
									$autoRecirculatePack = $feeder->getReceiverPackSubscription()->getAutoRecirculate();
								} else {
									// Complete Plan
									$completeSubscription = true;;

								}
							}
						}
					}

					//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

					if($shouldAutoRecycle == true) {
						// AutoRecycle here
						if(isset($cancelSubscription)) {
							$this->addFlash(
								'autoRecycleSuccess',
								"You have successfully canceled out of the {$feeder->getReceiverPackSubscription()->getPack()->getName()} subscription"
							);

							return $this->redirectToRoute('check_payment', array('feeder' => $feeder->getId()));
						} else if(isset($autoRecirculatePack)) {
//							if($feederPackSubscription->getForcedWaiting() == true){
//
//							}
//							$em = $this->getDoctrine()->getManager();
//							$feederPackSubscription->setPaidCommission(false);
////						            $feederPackSubscription->setStatus('active');
//							$feederPackSubscription->setRecommitRequired(false);
//
//							$em->flush();
							$this->addFlash(
								'autoRecycleSuccess',
								'You have successfully completed a pack and have been autorecirculated'
							);

							return $this->redirectToRoute('pack_subscription_new', array('pack' => $autoRecirculatePack->getId()));
						} else {
							return $this->redirectToRoute('check_payment', array('feeder' => $feeder->getId()));
						}
					} else {
						// Do not auto recycle
						if(isset($completeSubscription)) {
							$this->addFlash(
								'autoRecycleSuccess',
								"You have successfully completed the {$feeder->getReceiverPackSubscription()->getPack()->getName()} subscription"
							);

							return $this->redirectToRoute('check_payment', array('feeder' => $feeder->getId()));
						} else if(isset($autoRecirculatePack)) {

//							$em = $this->getDoctrine()->getManager();
//							$feederPackSubscription->setPaidCommission(false);
////						            $feederPackSubscription->setStatus('active');
//							$feederPackSubscription->setRecommitRequired(false);
//
//							$em->flush();

							$this->addFlash(
								'autoRecycleSuccess',
								'You have successfully completed a pack and have been autorecirculated'
							);

							return $this->redirectToRoute('pack_subscription_new', array('pack' => $autoRecirculatePack->getId()));
						} else {
							return $this->redirectToRoute('check_payment', array('feeder' => $feeder->getId()));
						}
					}
				}
			} else {

				// Render Page not found
				return $this->render(
					'member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);

			}
		}

		/**
		 * @Route("/member/pack/pack-subscription/feeder/{feeder}/extend-payment", name="extend_payment")
		 */
		private function feederExtendAction($feeder) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->find($feeder);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if(null !== $feeder) {

				if($feeder->getReceiver()->getId() != $this->getUser()->getId()) {

					// Not authorized
					return $this->render(
						'member/unauthorized.html.twig',
						[
                            'timeline' => $formatter->format($notifications),
                            'adm' => $adminsettings
                        ]
					);
				} else {

					if($feeder->getFeederTimeExtended() == null) {

						// Extend payment

						$feederTimeEnd = $feeder->getFeederTimeEnd();
						$newEnd = $feederTimeEnd->add(new \DateInterval('PT20H'));

						$this->getDoctrine()->getManager()
							->createQuery("UPDATE AppBundle:Feeder f SET f.feederTimeEnd = ?1, f.feederTimeExtended = ?2 WHERE f.id = ?3")
							->setParameter(1, $newEnd)
							->setParameter(2, 'extended')
							->setParameter(3, $feeder->getId())
							->execute();
					}

					//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

					return $this->redirectToRoute('check_payment', array('feeder' => $feeder->getId()));
				}
			} else {

				// Render Page not found
				return $this->render(
					'member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);

			}
		}

		/**
		 * @Route("/member/packs/pack-subscription/scam-payment/{feeder}", name="scam_payment")
		 */
		private function feederScamAction($feeder) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$feeder = $this->getDoctrine()->getRepository('AppBundle:Feeder')->find($feeder);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if(null !== $feeder) {
            if ($this->getUser()->getRole() != "ROLE_SUPER_ADMIN" ){
				if($feeder->getReceiver()->getId() != $this->getUser()->getId()) {

					// Not authorized
					return $this->render(
						'member/unauthorized.html.twig',
						[
                            'timeline' => $formatter->format($notifications),
                            'adm' => $adminsettings
                        ]
					);
				}
			} else {

					// Extend payment
					if($feeder->getStatus() == 'scam') {
						$newEnd = $feeder->getFeederTimeEnd();
					} else {
						$now = new \DateTime('now');
						$newEnd = $now->add(new \DateInterval('PT6H'));
					}

					$this->getDoctrine()->getManager()
                        ->createQuery("UPDATE AppBundle:Feeder f SET f.feederTimeEnd = ?1, f.status = ?2, f.scamPaid = ?3 WHERE f.id = ?4")
                        ->setParameter(1, $newEnd)
                        ->setParameter(2, 'scam')
                        ->setParameter(3, 'scam_paid')
                        ->setParameter(4, $feeder->getId())
                        ->execute();

					//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

					return $this->redirectToRoute('check_payment', array('feeder' => $feeder->getId()));
				}
			} else {

				// Render Page not found
				return $this->render(
					'member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);

			}
		}

		/**
		 * @Route("/member/packs/pack-subscription/cancel/{packSubscription}", name="cancel_pack_subscription")
		 */
		private function cancelPackSubscriptionAction($packSubscription) {

			$packSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($packSubscription);

			if($packSubscription === null) {

				$this->addFlash(
					'dashboardError',
					'Invalid Action'
				);

				return $this->redirectToRoute('dashboard');
			}

			if(count($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserFutureSubscriptions($this->getUser())) < 2) {

				$this->addFlash(
					'dashboardError',
					'You do not have more than one current subscriptions. Hence, you cant cancel this subscription as the system requires you to have a current subscription'
				);

				return $this->redirectToRoute('dashboard');
			}

			$packSubscription->setBookCancel('cancel');

			try {
				
				$em = $this->getDoctrine()->getManager();

				$em->flush();

				//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

				$this->addFlash(
					'dashboardSuccess',
					"You have successfully cancelled the {$packSubscription->getPack()->getName()} Pack"
				);

				$em->clear();

				return $this->redirectToRoute('dashboard');

			} catch(Exception $e) {

				$this->addFlash(
					'dashboardError',
					"An error occured while cancelling pack subscription. Please try again later"
				);

				return $this->redirectToRoute('dashboard');
			}
		}

		/**
		 * @Route("/member/packs/pack-subscription/{packSubscription}/book-autorecirculation", name="book_autorecirculation")
		 */
		private function bookAutoRecirculationAction($packSubscription) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			$packs = $this->getDoctrine()->getRepository('AppBundle:Pack')->findAll();

			$referralsCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveReferralsCount($this->getUser());

			foreach($packs as $pack) {
				if($referralsCount >= $this->getParameter('referral_loyalty')) {
					$ROI = $pack->getFeeders()+1;
				} else {
					$ROI = $pack->getFeeders();
				}
				$pack->setROI($ROI);

				$pack->setStatus('granted');
			}

			return $this->render(
				'member/book-auto-pack-subscription.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'packs' => $packs,
					'packSubscription' => $packSubscription
				]
			);
		}

		/**
		 * @Route("/member/packs/pack-subscription/{packSubscription}/do-auto-recirculation/{pack}", name="do_auto_recirculation")
		 */
		private function doAutoRecirculationAction($packSubscription, $pack) {

			$pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			$packSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($packSubscription);

			if($pack === null || $packSubscription === null) {

				$this->addFlash(
					'subscribeError',
					'Invalid Action'
				);

				return $this->redirectToRoute('packs');
			}

			if($packSubscription->getAutoRecirculate() != null) {

				if($packSubscription->getAutoRecirculate()->getId() == $pack->getId()) {

					$this->addFlash(
						'subscribeError',
						"You have booked an auto recirculate for {$pack->getName()} pack already"
					);

					return $this->redirectToRoute('book_autorecirculation', array('packSubscription' => $packSubscription->getId()));
				}
			}

			$packSubscription->setAutoRecirculate($pack);

			try {
				
				$em = $this->getDoctrine()->getManager();

				$em->flush();

				//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

				$this->addFlash(
					'subscribeSuccess',
					"You have successfully booked an auto recirculation for {$pack->getName()} pack"
				);

				$em->clear();

				return $this->redirectToRoute('book_autorecirculation', array('packSubscription' => $packSubscription->getId()));

			} catch(Exception $e) {

				$this->addFlash(
					'subscribeError',
					"An error occured while booking the {$pack->getName()} pack for you to autorecirculate on. Please try again later"
				);

				return $this->redirectToRoute('book_autorecirculation', array('packSubscription' => $packSubscription->getId()));
			}
		}

		/**
		 * @Route("/member/packs/pack-subscription/{packSubscription}/cancel-auto-recirculation", name="cancel_autorecirculation")
		 */
		private function cancelAutoRecirculationAction($packSubscription) {

			$packSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($packSubscription);

			if($packSubscription === null) {

				$this->addFlash(
					'subscribeError',
					'Invalid Action'
				);

				return $this->redirectToRoute('packs');
			}

			$packSubscription->setAutoRecirculate(NULL);

			try {
				
				$em = $this->getDoctrine()->getManager();

				$em->flush();

				//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

				$this->addFlash(
					'subscribeSuccess',
					"You have successfully reset autorecycle"
				);

				$em->clear();

				return $this->redirectToRoute('book_autorecirculation', array('packSubscription' => $packSubscription->getId()));

			} catch(Exception $e) {

				$this->addFlash(
					'subscribeError',
					"An error occured while resetting autorecirculation. Please try again later"
				);

				return $this->redirectToRoute('book_autorecirculation', array('packSubscription' => $packSubscription->getId()));
			}
		}

		/**
		 * @Route("/member/pack-subscription/{pack}/feeders", name="pack_subscription_feeders")
		 */
		private function packSubscriptionFeedersAction($pack) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			$pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			// Subscription
			$subscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserActivePackSubscription($this->getUser(), $pack);

			if($subscription == null) {

				$this->addFlash(
					'subscribeError',
					"You are not active under the {$pack->getName()} pack"
				);

				return $this->redirectToRoute('packs');

			} else {
				// check feeders
				$feeders = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getSubscriptionFeeders($subscription);
			}

			return $this->render(
				'member/pack-feeders.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'subscription' => $subscription,
					'feeders' => $feeders
				]
			);
		}

		/**
		 * @Route("/member/purse/{page}", name="purse", defaults={"page"=1})
		 */
		private function purseAction($page) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT t FROM AppBundle:Transaction t WHERE t.receiver = ?1 AND t.withdrawal = 0 ORDER BY t.created ASC";

			$query = $em->createQuery($dql)->setParameter(1, $this->getUser()->getId());

			$limit = 25;

			$transactions = $paginator->paginate($query, $page, $limit);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'member/purse.html.twig',
				[
				    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'transactions' => $transactions
				]
			);
		}

		/**
		 * @Route("/member/purse-withdrawal", name="withdraw_purse")
		 */
		private function withdrawPurseAction(Request $request) {
			// Check if Balance permits withdrawal
            $user = $this->getUser();
            $userPurse = $user->getPurse();
			$adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			

            $em = $this->getDoctrine()->getEntityManager();

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

            $formatter = $this->get('app.timeline_format');


		if($userPurse > 0) {

			$now = new \DateTime('now');

			
		} else {

			$this->addFlash(
				'walletError',
				'You do not have sufficient funds in your wallet to make a withdrawal'
			);

		}
			
			$form = $this->createFormBuilder()
			->add('amount', TextType::class, array(
				'label' => 'Amount',
				'attr' => array(
					'class' => 'form-control',
					'placeholder' => 'Amount'
				),
				'constraints' => array(
					new NotBlank(
						array(
							'message' => 'Please enter amount'
						)
					),
					new Length(
						array(
							'min' => 1,
							'max' => 40,
							'minMessage' => 'Amount must contain 3 or more characters',
							'maxMessage' => 'Amount cannot contain more than 40 characters'
						)
					),
					new Regex(
						array(
							'pattern' => '/\d/',
							'match' => true,
							'message' => 'Amount can only contain digits'
						)
					)
				),
				'required' => true
			))
			->getForm();

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()) {

			$data = $form->getData();

			if($data['amount'] > $userPurse) {

				$this->addFlash(
					'walletError',
					'Insufficient funds to initiate this withdrawal!'
				);
			} else {

				$user->setWithdrawalRequest(1);
				$user->setWithdrawalRequestApproved(0);
				$user->setAmountRequested($data['amount']);
//				$em->persist($referrer);

				$emt = $this->getDoctrine()->getManager();

				$transaction = new Transaction;
				$transaction->setReceiver($user);
				$transaction->setAmount($data['amount']);
				$transaction->setStatus('unconfirmed');
				$transaction->setWithdrawal(1);
				$transaction->setApproved(0);
				$transaction->setCreated($now);
				$emt->persist($transaction);
				$emt->flush();
				$emt->clear();

				$this->addFlash(
					'requestSuccess',
					"You have successfully requested to withdraw GHS ".number_format($data['amount'])	);

				
				return $this->redirect($request->getUri());

			}

		}

			return $this->render(
				'member/withdraw-purse.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
					'adm' => $adminsettings,
					'form' => $form->createView(),
					'user' => $user
                ]
			);
		}

		/**
		 * @Route("/member/purse-transfer", name="transfer_purse")
		 */
		private function purseTransferAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
			$formatter = $this->get('app.timeline_format');
			
			$form = $this->createFormBuilder()
				->add('username', TextType::class, array(
					'label' => "Receiver's Username",
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => "Receiver's Username"
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter username of receiver'
							)
						),
						new Length(
							array(
								'min' => 3,
								'max' => 40,
								'minMessage' => 'Receiver username must contain 3 or more characters',
								'maxMessage' => 'Receiver username cannot contain more than 40 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/^[A-Za-z0-9]+$/',
								'message' => 'Receiver username can only contain alphanumeric characters'
							)
						)
					),
					'required' => true
				))
				->add('amount', TextType::class, array(
					'label' => 'Amount',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Amount'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter amount'
							)
						),
						new Length(
							array(
								'min' => 1,
								'max' => 40,
								'minMessage' => 'Amount must contain 3 or more characters',
								'maxMessage' => 'Amount cannot contain more than 40 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => true,
								'message' => 'Amount can only contain digits'
							)
						)
					),
					'required' => true
				))
				->getForm();

			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				$data = $form->getData();

				// Check if username exists
				$receiver = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
					array(
						'username' => $data['username']
					)
				);

				if($receiver == null) {

					$this->addFlash(
						'transferError',
						"The user with the username: {$data['username']} was not found"
					);
				} else {

					$giver = $this->getUser();

					if($data['amount'] > $giver->getPurse()) {

						$this->addFlash(
							'transferError',
							'Insufficient funds to initiate this transfer'
						);
					} else {

						try {
							
							$giverPreviousBalance = $giver->getPurse();
							$giverNewBalance = $giverPreviousBalance - $data['amount'];

							// Debit Giver Amount
							$giver->setPurse($giverNewBalance);

							$receiverPreviousBalance = $receiver->getPurse();
							$receiverNewBalance = $receiverPreviousBalance + $data['amount'];

							// Credit Receiver Amount
							$receiver->setPurse($receiverNewBalance);

							$em = $this->getDoctrine()->getManager();

							$em->flush();

							$this->addFlash(
								'transferSuccess',
								"You have successfully transferred N".number_format($data['amount'])." to {$data['username']}"
							);

							return $this->redirect($request->getUri());

						} catch(Exception $e) {

							$this->addFlash(
								'transferError',
								'An error occured during transfer. Please try again later'
							);
						}
					}
				}
			}
			
			return $this->render(
				'member/transfer-purse.html.twig',
				[
				    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

		/**
		 * @Route("/member/referrals/create", name="create-referral")
		 */
		private function createReferralAction(Request $request) {

			$user = $this->getUser();

			$this->get('security.token_storage')->setToken(null);

			return $this->redirectToRoute('referral_link', array('username' => $user->getUsername()));
		}

		/**
		 * @Route("/member/referrals/{page}", name="referrals", defaults={"page" = 1})
		 */
		private function referralsAction($page) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT u FROM AppBundle:User u WHERE u.referrer = ?1 ORDER BY u.name ASC";

			$query = $em->createQuery($dql)->setParameter(1, $this->getUser()->getId());

			$limit = 10;

			$referrals = $paginator->paginate($query, $page, $limit);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'member/referrals.html.twig',
				[
				    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'referrals' => $referrals
				]
			);
		}

		/**
		 * @Route("/member/support/create", name="create_support")
		 */
		private function createSupportAction(Request $request) {
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$support = new Support;
			$supportForm = $this->createForm(SupportType::class, $support);
			$supportForm->handleRequest($request);

			$supportCount = $this->getDoctrine()->getRepository('AppBundle:Support')->getUserSupportsCount($this->getUser());

			if($supportForm->isSubmitted() && $supportForm->isValid()) {
				
				$support->setUserId($this->getUser()->getId());

				$support->setStatus('blank');

				$now = new \DateTime('now');
				$support->setCreatedDate($now);

				try {
					
					$em = $this->getDoctrine()->getManager();
					$em->persist($support);

					$em->flush();

					//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

					$this->addFlash(
						'ticketMessageSuccess',
						"You have successfully created a support #{$support->getId()}. Add a message to the support."
					);

					return $this->redirectToRoute('add_support_message', array('support' => $support->getId()));

				} catch(Exception $e) {
					
					$this->addFlash(
						'ticketError',
						'An error occured while creating the support. Please try again later'
					);
				}
			}

			return $this->render(
				'member/create-support.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'supportForm' => $supportForm->createView(),
					'supportCount' => $supportCount
				]
			);
		}

		/**
		 * @Route("/member/support/{support}/add-message", name="add_support_message")
		 */
		private function addSupportMessageAction(Request $request, $support) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$support = $this->getDoctrine()->getRepository('AppBundle:Support')->find($support);

			$supportCount = $this->getDoctrine()->getRepository('AppBundle:Support')->getUserSupportsCount($this->getUser());
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($support == null) {

				return $this->render(
					'member/unauthorized.html.twig',
					[
                        'timeline' => $formatter->format($notifications),
                        'adm' => $adminsettings
                    ]
				);
			}

			if($support->getUserId() != $this->getUser()->getId()) {

				return $this->render(
					'member/unauthorized.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);
			}

			$supportMessage = new SupportMessage;
			$supportMessageForm = $this->createForm(SupportMessageType::class, $supportMessage);
			$supportMessageForm->handleRequest($request);

			if($supportMessageForm->isSubmitted() && $supportMessageForm->isValid()) {
				
				// Check if attachment was uploaded
				if($supportMessage->getAttachmentFile() != null) {

					$attachmentFile = $supportMessage->getAttachmentFile();

					$attachmentFileName = md5(uniqid()).'.'.$attachmentFile->guessExtension();

					// Move file to directory
					$attachmentDirectory = $this->getParameter('tickets_attachment_real_directory').'/'.$this->getUser()->getUsername();
					$attachmentDbDirectory = $this->getParameter('tickets_attachment_directory').'/'.$this->getUser()->getUsername();

					if(!is_dir($attachmentDirectory) && is_readable($attachmentDirectory)) {
						mkdir($attachmentDirectory);
					}

					$attachmentFile->move(
						$attachmentDirectory,
						$attachmentFileName
					);

					// Store file name
					$supportMessage->setAttachment($attachmentDbDirectory.'/'.$attachmentFileName);
				}

				$supportMessage->setSupportId($support->getId());

				$supportMessage->setSender('user');

				$now = new \DateTime('now');
				$supportMessage->setCreatedDate($now);

				try {

					$em = $this->getDoctrine()->getManager();
					$em->persist($supportMessage);

					$em->flush();

					$support->setStatus('user_reply');

					$em->flush();

					//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

					return $this->redirectToRoute('view_support', array('support' => $support->getId()));

				} catch(Exception $e) {

					$this->addFlash(
						'ticketMessageError',
						"An error occured while adding a message to the support #{$support->getId()}"
					);

				}
			}

			return $this->render(
				'member/add-support-message.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'supportMessageForm' => $supportMessageForm->createView(),
					'support' => $support,
					'supportCount' => $supportCount
				]
			);
		}

		/**
		 * @Route("/member/support/{support}/view", name="view_support")
		 */
		private function viewTicketAction(Request $request, $support) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$support = $this->getDoctrine()->getRepository('AppBundle:Support')->find($support);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			$supportCount = $this->getDoctrine()->getRepository('AppBundle:Support')->getUserSupportsCount($this->getUser());

			if($support == null) {

				return $this->render(
					'member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);
			}

			if($support->getUserId() != $this->getUser()->getId()) {

				return $this->render(
					'member/unauthorized.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);
			}

			$supportMessage = new SupportMessage;
			$supportMessageForm = $this->createForm(SupportMessageType::class, $supportMessage);
			$supportMessageFormClone = clone $supportMessageForm;
			$supportMessageForm->handleRequest($request);

			if($supportMessageForm->isSubmitted() && $supportMessageForm->isValid()) {
				
				// Check if attachment was uploaded
				if($supportMessage->getAttachmentFile() != null) {

					$attachmentFile = $supportMessage->getAttachmentFile();

					$attachmentFileName = md5(uniqid()).'.'.$attachmentFile->guessExtension();

					// Move file to directory
					$attachmentDirectory = $this->getParameter('tickets_attachment_real_directory').'/'.$this->getUser()->getUsername();
					$attachmentDbDirectory = $this->getParameter('tickets_attachment_directory').'/'.$this->getUser()->getUsername();

					if(!is_dir($attachmentDirectory) && is_readable($attachmentDirectory)) {
						mkdir($attachmentDirectory);
					}

					$attachmentFile->move(
						$attachmentDirectory,
						$attachmentFileName
					);

					// Store file name
					$supportMessage->setAttachment($attachmentDbDirectory.'/'.$attachmentFileName);
				}

				$supportMessage->setSupportId($support->getId());

				$supportMessage->setSender('user');

				$now = new \DateTime('now');
				$supportMessage->setCreatedDate($now);

				try {

					$em = $this->getDoctrine()->getManager();
					$em->persist($supportMessage);

					$em->flush();

					$support->setStatus('user_reply');

					$em->flush();

					//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

					$this->addFlash(
						'ticketMessageSuccess',
						"You have successfully added a reply to the support #{$support->getId()}"
					);

					$supportMessageForm = $supportMessageFormClone;

				} catch(Exception $e) {

					$this->addFlash(
						'ticketMessageError',
						"An error occured while adding a reply to the support #{$support->getId()}"
					);

				}
			}

			$supportMessages = $this->getDoctrine()->getRepository('AppBundle:SupportMessage')->getSupportMessages($support);


			return $this->render(
				'member/view-support.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'support' => $support,
					'supportMessages' => $supportMessages,
					'supportMessageForm' => $supportMessageForm->createView(),
					'supportCount' => $supportCount
				]
			);
		}

		/**
		 * @Route("/member/support/{page}", name="support", defaults={"page" = 1})
		 */
		private function supportAction($page) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT s FROM AppBundle:Support s WHERE s.userId = ?1 ORDER BY s.createdDate DESC";

			$query = $em->createQuery($dql)->setParameter(1, $this->getUser()->getId());

			$limit = 25;
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			$supports = $paginator->paginate($query, $page, $limit);

			return $this->render(
				'member/supports.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'supports' => $supports
				]
			);
		}

		/**
		 * @Route("/member/jury/case/{case}", name="user_case")
		 */
		private function caseAction(Request $request, $case) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$case = $this->getDoctrine()->getRepository('AppBundle:Jury')->find($case);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($case == null) {

				return $this->render(
					'member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);
			}

			if($case->getFeederId() != $this->getUser()->getId() && $case->getReceiverId() != $this->getUser()->getId()) {

				return $this->render(
					'member/unauthorized.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);
			}

            $juryMessage = new JuryMessage;
			$juryMessageForm = $this->createForm(JuryMessageType::class, $juryMessage);
			$juryMessageFormClone = clone $juryMessageForm;
			$juryMessageForm->handleRequest($request);

			if($juryMessageForm->isSubmitted() && $juryMessageForm->isValid()) {
				
				// Check if attachment was uploaded
				if($juryMessage->getAttachmentFile() != null) {

					$attachmentFile = $juryMessage->getAttachmentFile();

					$attachmentFileName = md5(uniqid()).'.'.$attachmentFile->guessExtension();

					// Move file to directory
					$attachmentDirectory = $this->getParameter('court_attachment_real_directory').'/'.$this->getUser()->getUsername();
					$attachmentDbDirectory = $this->getParameter('court_attachment_directory').'/'.$this->getUser()->getUsername();

					if(!is_dir($attachmentDirectory) && is_readable($attachmentDirectory)) {
						mkdir($attachmentDirectory);
					}

					$attachmentFile->move(
						$attachmentDirectory,
						$attachmentFileName
					);

					// Store file name
					$juryMessage->setAttachment($attachmentDbDirectory.'/'.$attachmentFileName);
				}

				$juryMessage->setJuryId($case->getId());

				$juryMessage->setSenderId($this->getUser()->getId());

				$juryMessage->setSenderType('user');

				$now = new \DateTime('now');
				$juryMessage->setCreatedDate($now);

				if($case->getFeederId() == $this->getUser()->getId()) {
					$juryStatus = 'giver_reply';
				} elseif($case->getReceiverId() == $this->getUser()->getId()) {
					$juryStatus = 'receiver_reply';
				} else {
					$juryStatus = '';
				}

				try {

					$em = $this->getDoctrine()->getManager();
					$em->persist($juryMessage);

					$em->flush();

					$case->setStatus($juryStatus);

					$em->flush();

					//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();

					$this->addFlash(
						'courtMessageSuccess',
						"You have successfully added a statement to the case #{$case->getId()}"
					);

					$juryMessageForm = $juryMessageFormClone;

				} catch(Exception $e) {

					$this->addFlash(
						'courtMessageError',
						"An error occured while adding a statement to the case #{$case->getId()}"
					);

				}
			}

			$juryMessages = $this->getDoctrine()->getRepository('AppBundle:JuryMessage')->getJuryMessages($case);

			$feeder = $this->getDoctrine()->getRepository('AppBundle:User')->find($case->getFeederId());
            $userCheck = new UserCheck;
			$receiver = $userCheck->wash($this->getDoctrine()->getRepository('AppBundle:User')->find($case->getReceiverId()));

			$courtCount = $this->getDoctrine()->getRepository('AppBundle:Jury')->getUserJuryCount($this->getUser());
			
			return $this->render(
				'member/user-case.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'jury' => $case,
					'juryMessages' => $juryMessages,
					'juryMessageForm' => $juryMessageForm->createView(),
					'feeder' => $feeder,
					'receiver' => $receiver,
					'courtCount' => $courtCount
				]
			);
		}

		/**
		 * @Route("/member/jury/{page}", name="jury", defaults={"page"=1})
		 */
		private function juryAction($page) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT j FROM AppBundle:Jury j WHERE j.feederId = ?1 OR j.receiverId = ?1 ORDER BY j.createdDate DESC";

			$query = $em->createQuery($dql)->setParameter(1, $this->getUser()->getId());

			$limit = 25;

			$cases = $paginator->paginate($query, $page, $limit);

			$items = $cases->getItems();

			$casesItems = array();

			$userRepo = $this->getDoctrine()->getRepository('AppBundle:User');

			foreach($items as $item) {
				
				$item->setFeeder($userRepo->find($item->getFeederId()));
				
				$item->setReceiver($userRepo->find($item->getReceiverId()));

				$casesItems[] = $item;
			}

			$cases->setItems($casesItems);

			$courtCount = $this->getDoctrine()->getRepository('AppBundle:Jury')->getUserJuryCount($this->getUser());
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'member/list-jury.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'cases' => $cases,
					'courtCount' => $courtCount
				]
			);
		}

		/**
		 * @Route("/member/reviews/create", name="create_review")
		 */
		private function createReviewAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$review = new Review;
			$reviewForm = $this->createForm(ReviewType::class, $review);
			$reviewForm->handleRequest($request);
            $review->setUser($this->getUser());
            $review->setUserId($review->getUser()->getId());
            $now = new \DateTime('now');
            $review->setCreatedDate($now);

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($reviewForm->isSubmitted() && $reviewForm->isValid()) {
				

				try {
					
					$em = $this->getDoctrine()->getManager();
					$em->persist($review);

					$em->flush();

					$this->addFlash(
						'testimonySuccess',
						"You have successfully created a review"
					);

					return $this->redirectToRoute('reviews');

				} catch(Exception $e) {
					
					$this->addFlash(
						'testimonyError',
						'An error occured while creating the review. Please try again later'
					);
				}
			}

			$reviewCount = $this->getDoctrine()->getRepository('AppBundle:Review')->getUserReviewCount($this->getUser());

			return $this->render(
				'member/create-review.html.twig',
				[
				    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'reviewForm' => $reviewForm->createView(),
					'reviewCount' => $reviewCount
				]
			);
		}

		/**
		 * @Route("/member/reviews/{page}", name="reviews", defaults={"page"=1})
		 */
		private function reviewsAction($page) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$paginator = $this->get('knp_paginator');

			$dql = "SELECT r FROM AppBundle:Review r WHERE r.userId = ?1 ORDER BY r.createdDate DESC";

			$em = $this->getDoctrine()->getManager();

			$query = $em->createQuery($dql)->setParameter(1, $this->getUser()->getId());

			$limit = 10;

			$reviews = $paginator->paginate($query, $page, $limit);

			$items = $reviews->getItems();

			$reviewsItems = array();

			$userRepo = $this->getDoctrine()->getRepository('AppBundle:User');

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			foreach($items as $item) {
				
				$item->setUser($userRepo->find($item->getUserId()));

				$reviewsItems[] = $item;
			}

			$reviews->setItems($reviewsItems);

			return $this->render(
				'member/reviews.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'reviews' => $reviews
				]
			);
		}

		/**
		 * @Route("/member/mails/view/{mailRecipient}", name="member_view_mail")
		 */
		private function viewMail($mailRecipient) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$mailRecipient = $this->getDoctrine()->getRepository('AppBundle:MailRecipient')->find($mailRecipient);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($mailRecipient == null) {

				return $this->render('member/page-not-found.html.twig',
					[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
                    ]
				);
			}

			$mailRecipient->setStatus('read');

			$em = $this->getDoctrine()->getManager();

			$em->flush();

			//$this->getDoctrine()->getRepository('AppBundle:User')->clearGrass();
			$em->clear();

			return $this->render('member/view-mail.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'mailRecipient' => $mailRecipient
				]
			);
		}

		/**
		 * @Route("/member/mails/delete/{mailRecipient}", name="member_delete_mail")
		 */
		private function deleteMail($mailRecipient) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$mailRecipient = $this->getDoctrine()->getRepository('AppBundle:MailRecipient')->find($mailRecipient);

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($mailRecipient == null) {

				return $this->render('member/page-not-found.html.twig', [
                    'timeline' => $formatter->format($notifications),                     'adm' => $adminsettings
                ]);
			}

			$notification = $this->getDoctrine()->getRepository('AppBundle:Notification')->findOneBy(
				array(
					'object' => $mailRecipient->getId(),
					'subject' => $this->getUser()->getId(),
					'type' => 6
				)
			);

			if($notification == null) {

				return $this->render('member/page-not-found.html.twig', [
                    'timeline' => $formatter->format($notifications),                     'adm' => $adminsettings
                ]);
			}

			$em = $this->getDoctrine()->getManager();

			// Delete Notification
			$em->remove($notification);
			$em->flush();

			// Delete MailRecipient
			$em->remove($mailRecipient);
			$em->flush();
			$em->clear();

			return $this->redirectToRoute('member_mails');
		}

		/**
		 * @Route("/member/mails/{page}", name="member_mails", defaults={"page"=1})
		 */
		private function memberMails($page) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$paginator = $this->get('knp_paginator');

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT m FROM AppBundle:MailRecipient m WHERE m.user = ?1 ORDER BY m.created DESC";

			$query = $em->createQuery($dql)->setParameter(1, $this->getUser());

			$limit = 25;

			$mails = $paginator->paginate($query, $page, $limit);

			return $this->render(
				'member/list-mail.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'mails' => $mails
				]
			);
		}

		private function bounceAction() {

			return $this->redirectToRoute('logout');
		}

		private function godAction() {
			return $this->redirectToRoute('god_mode_dashboard');
		}

		/**
		 * Load stuff before calling any of the controller's methods
		 */
		public function __call($method, $args) {

			$securityToken = $this->get('security.token_storage')->getToken();

			$userCheck = new UserCheck;
			
			$user = $userCheck->verify($securityToken->getUser());

			$userConfirm = $this->getDoctrine()->getRepository('AppBundle:User')->find($user->getId());
			if($userConfirm->getStatus() != 'active') {
				$method = 'bounceAction';
				$args = array();
			}

			if($user->getDbGranted() == null && $user->getRole() == 'ROLE_SUPER_ADMIN' || $user->getRole() == "ROLE_ADMIN") {
				if(in_array($method, $userCheck->getAllowed())) {
					$method = 'godAction';
					$args = array();
				}
			}

			$user->setReferralsCount($this->getDoctrine()->getRepository('AppBundle:User')->getReferralsCount($user));

			$user->setPacks($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserCurrentSubscriptionsCount($user));

			$user->setSupportCount($this->getDoctrine()->getRepository('AppBundle:Support')->getActiveSupportCount());

			$user->setJuryCount($this->getDoctrine()->getRepository('AppBundle:Jury')->getActiveJuryCount());

			// Get Mail Messages
			$user->setMailCount($this->getDoctrine()->getRepository('AppBundle:MailRecipient')->getUnreadCount($this->getUser()));

			$user->setMenuMail($this->getDoctrine()->getRepository('AppBundle:MailRecipient')->getMenuMail($this->getUser()));

         	$securityToken->setUser($user);

        	return call_user_func_array(array($this, $method), $args);
		}


	}