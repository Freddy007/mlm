<?php
//TODO Add following Modules
/**
        http://developers.payulatam.com/es/index.html#
        https://dev.pagseguro.uol.com.br/
        https://www.cashenvoy.com/?cmd=developers
        https://voguepay.com/developers
        https://developers.paystack.co/docs

        Bitcoin

        https://www.myetherwallet.com/


 */
	namespace AppBundle\Controller;

	use AppBundle\Form\AdminsettingType;
	use Carbon\Carbon;
	use Cron\CronAction;
	use function GuzzleHttp\Psr7\str;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;

    use Symfony\Component\Config\Definition\Exception\Exception;
	use Symfony\Component\HttpFoundation\JsonResponse;
	use Symfony\Component\HttpFoundation\Request;

	use AppBundle\Entity\User;
	use AppBundle\Entity\Feeder;
	use AppBundle\Entity\Pack;
	use AppBundle\Entity\PackSubscription;
	use AppBundle\Entity\News;
	use AppBundle\Entity\Admin;
	use AppBundle\Entity\Notification;
	use AppBundle\Entity\Mail;
	use AppBundle\Entity\MailRecipient;
use AppBundle\Entity\Transaction;
use Symfony\Component\Form\Extension\Core\Type\TextType;

	use Symfony\Component\Security\Core\Authentication\Token\Telegram;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;

	use Symfony\Component\Security\Core\Authentication\Token\UserCheck;

	use AppBundle\Form\UserType;
	use AppBundle\Form\UserEditType;
	use AppBundle\Form\PackType;
    use AppBundle\Form\PersonalType;
    use AppBundle\Form\PaymentType;
	use AppBundle\Form\NewsType;
	use AppBundle\Form\NewsEditType;
    use AppBundle\Form\BankChoiceType;
    use AppBundle\Form\BankDetailTwoType;
    use AppBundle\Form\SettingsType;
	use AppBundle\Form\NoticeBoardType;
	use AppBundle\Form\MailType;

	class GodController extends Controller {

        /**
         * @Route("/god-mode/delete-transactions/", name="delete_transactions")
         */
        private function deleteTransactionsAction()
        {
            if($this->getUser()->getRole()=='ROLE_SUPER_ADMIN') {
                $em = $this->getDoctrine()->getManager();


                $mails = $this->getDoctrine()->getRepository('AppBundle:Mail')->findAll();
                $mailrecipients = $this->getDoctrine()->getRepository('AppBundle:MailRecipient')->findAll();
                $feeders = $this->getDoctrine()->getRepository('AppBundle:Feeder')->findAll();
                $transactions = $this->getDoctrine()->getRepository('AppBundle:Transaction')->findAll();
                $subscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findAll();
                $packs = $this->getDoctrine()->getRepository('AppBundle:Pack')->findAll();
                $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
                $news = $this->getDoctrine()->getRepository('AppBundle:News')->findAll();
                $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findAll();
                $reviews = $this->getDoctrine()->getRepository('AppBundle:Review')->findAll();
                $jurys = $this->getDoctrine()->getRepository('AppBundle:Jury')->findAll();
                $supports = $this->getDoctrine()->getRepository('AppBundle:Support')->findAll();
                $supportmessages = $this->getDoctrine()->getRepository('AppBundle:SupportMessage')->findAll();


                //Delete Mail Recipient
                foreach ($mailrecipients as $mailrecipient) {
                    $em->remove($mailrecipient);
                    $em->flush();
                }



                //Delete Mails
                foreach ($mails as $mail) {
                    $em->remove($mail);
                    $em->flush();
                }

                //Delete Transaction
                foreach ($transactions as $transaction) {
                    $em->remove($transaction);
                    $em->flush();
                }
                //Delete Feeders
                foreach ($feeders as $feeder) {
                    $em->remove($feeder);
                    $em->flush();
                }
                //Delete Subscriptions
                foreach ($subscriptions as $subscription) {
                    $em->remove($subscription);
                   $em->flush();
               }

                //Delete Packs
                foreach ($packs as $pack) {
                    if( $pack->getName() != "Standard Peerup Pack" ) {
                        $em->remove($pack);
                        $em->flush();
                    }
                }

                //Delete News
                foreach ($news as $newitem) {
                    $em->remove($newitem);
                    $em->flush();
                }


                //Delete Notifications
                foreach ($notifications as $notification) {
                    $em->remove($notification);
                    $em->flush();
                }

                //Delete Reviews
                foreach ($reviews as $review) {
                    $em->remove($review);
                    $em->flush();
                }


                //Delete Jury Cases
                foreach ($jurys as $jury) {
                    $em->remove($jury);
                    $em->flush();
                }


                //Delete Support Messages
                foreach ($supports as $support) {
                    $em->remove($support);
                    $em->flush();
                }

                //Delete Support Messages
                foreach ($supportmessages as $supportmessage) {
                    $em->remove($supportmessage);
                    $em->flush();
                }

                //Delete All users except admin 1
                foreach ($users as $user) {
                    if($user->getRole() != 'ROLE_SUPER_ADMIN'){
                        if( $user->getUsername() != "testuser" ) {
                            $em->remove($user);
                            $em->flush();
                        }
                    }
                }

                $this->addFlash(
                    'deleteSuccess',
                    'Transactions deleted successfully'
                );


                return $this->redirectToRoute('god_mode_dashboard');
            }else{
                $this->addFlash(
                    'deleteFailure',
                    'Transactions not deleted'
                );
                return $this->redirectToRoute('god_mode_dashboard');
            }
        }

        /**
         * @Route("/god-mode-settings/", name="god_mode_settings")
         */
        private function godSettingsAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $em = $this->getDoctrine()->getManager();

            $dql = "SELECT p FROM AppBundle:Pack p ORDER BY p.amount ASC";

            $packs = $em->createQuery($dql)->getResult();

            $admForm = $this->createForm(AdminsettingType::class, $adminsettings);
            $admForm->handleRequest($request);

            if($admForm->isSubmitted() && $admForm->isValid()) {

                try {

					return [""];
                    $em = $this->getDoctrine()->getManager();

                    $em->flush();

                    //$em->clear();

                    $this->addFlash(
                        'successNotice',
                        'Admin Settings Successfully Edited'
                    );
                    return $this->redirectToRoute('god_mode_settings');
                } catch (Exception $e) {

                    $this->addFlash(
                        'errorNotice',
                        'An error occured while updating the admin settings. Please try again later'
                    );
                }
            }
            $adminsettings->setSitelogoImage(null);
            $adminsettings->setSitelogolightImage(null);
//            $adminsettings->setSitesmalliconimage(null);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');


            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            // Call Formatter


            return $this->render(
                'god-mode/settings.html.twig',
                [
                    'packs' => $packs,
                    'adm' => $adminsettings,
                    'timeline' => $formatter->format($notifications),
                    'admForm' => $admForm->createView()
                ]
            );

        }

        /**
         * @Route("/god-mode/change-password", name="adminchange_password")
         */
        private function changePasswordAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

            if ($adminsettings->getEnvatoPurchasecode() == "manualpeerupxyzinstall"){
                $this->addFlash(
                    'errorNotice',
                    'You can not change passwords in demo mode'
                );
                return $this->redirectToRoute('god_mode_settings');
            }else{
                $user = $this->getUser();

                // Get and Handle Settings Form
                $settingsForm = $this->createForm(SettingsType::class, $user);
                $settingsForm->handleRequest($request);

                if($settingsForm->isSubmitted() && $settingsForm->isValid()) {
                    // Get encoder
                    $encoder = $this->get("security.password_encoder");

                    // Check Old Password
                    if($encoder->isPasswordValid($user, $user->getOldPassword())) {
                        $password = $encoder->encodePassword($user, $user->getPlainPassword());
                        $user->setPassword($password);

                        // Save User
                        $em = $this->getDoctrine()->getManager();
                        $em->flush();

                        $this->addFlash(
                            'settingsSuccess',
                            'Password Changed'
                        );
                    } else {
                        $this->addFlash(
                            'settingsError',
                            'Incorrect Password'
                        );
                    }
                }
                $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                    array('subject' => $this->getUser()->getId()),
                    array('created' => 'DESC'),
                    10
                );
                $formatter = $this->get('app.timeline_format');


                return $this->render(
                    'god-mode/adminprofile-change-password.html.twig',
                    [
                        'timeline' => $formatter->format($notifications),
                        'adm' => $adminsettings,
                        'settingsForm' => $settingsForm->createView()
                    ]
                );
            }

        }

        /**
         * @Route("/god-mode/profile", name="adminprofile")
         */
        private function adminProfileAction(Request $request) {

            $user = $this->getUser();

            // Get Personal Details Form
            $personalForm = $this->createForm(PersonalType::class, $user);
            $personalForm->handleRequest($request);

            if($personalForm->isSubmitted() && $personalForm->isValid()) {

                // Check if avatar was uploaded
                if($user->getAvatarImage() != null) {

                    $avatarFile = $user->getAvatarImage();

                    $avatarFileName = md5(uniqid()).'.'.$avatarFile->guessExtension();

                    // Move file to directory
                    $avatarDirectory = $this->getParameter('avatars_real_directory').'/'.$user->getUsername();
                    $avatarDbDirectory = $this->getParameter('avatars_directory').'/'.$user->getUsername();

                    if($user->getRealAvatar() != null) {
                        unlink($_SERVER['DOCUMENT_ROOT'].$user->getRealAvatar());
                    }

                    if(!is_dir($avatarDirectory) && is_readable($avatarDirectory)) {
                        mkdir($avatarDirectory);
                    }

                    $avatarFile->move(
                        $avatarDirectory,
                        $avatarFileName
                    );

                    // Store file name
                    $user->setAvatar($avatarDbDirectory.'/'.$avatarFileName);
                }

                $this->addFlash(
                    'personalSuccess',
                    'Your personal settings have been successfully updated'
                );

                // Save User
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            }

            $user->setAvatarImage(null);

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );


            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            // Call Formatter
            $formatter = $this->get('app.timeline_format');
            return $this->render(
                'god-mode/adminprofile.html.twig',
                [
                    'adm' => $adminsettings,
                    'personalForm' => $personalForm->createView(),
                    'timeline' => $formatter->format($notifications)
                ]
            );
        }

        /**
         * @Route("/god-mode/bank-details", name="adminbank_details")
         */
        private function bankDetailsAction(Request $request) {

            $user = $this->getUser();

            // Get and handle Payment Form
            $paymentForm = $this->createForm(PaymentType::class, $user);
            $paymentTwoForm = $this->createForm(BankDetailTwoType::class, $user);
            $bankForm = $this->createForm(BankChoiceType::class, $user);

            $paymentForm->handleRequest($request);

            if($paymentForm->isSubmitted() && $paymentForm->isValid()) {

                $this->addFlash(
                    'paymentSuccess',
                    'Your default payment details have been successfully updated'
                );

                // Save User
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            }


            $paymentTwoForm->handleRequest($request);
            if($paymentTwoForm->isSubmitted() && $paymentTwoForm->isValid()) {

                $this->addFlash(
                    'paymentSuccess',
                    'Your provisional payment details have been successfully updated'
                );

                // Save User
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            }

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

            // Call Formatter
            $formatter = $this->get('app.timeline_format');
            return $this->render(
                'god-mode/adminprofile-bank-details.html.twig',
                [
                    'adm' => $adminsettings,
                    'paymentForm' => $paymentForm->createView(),
                    'bankForm' => $bankForm->createView(),
                    'paymentTwoForm' => $paymentTwoForm->createView(),
                    'timeline' => $formatter->format($notifications)
                ]
            );
        }

        /**
         * @Route("/god-mode/bank-details-two", name="adminbank_details_two")
         */
        private function bankDetailsTwoAction(Request $request) {

            $user = $this->getUser();

            // Get and handle Payment Form

            $paymentForm = $this->createForm(PaymentType::class, $user);
            $paymentTwoForm = $this->createForm(BankDetailTwoType::class, $user);
            $bankForm = $this->createForm(BankChoiceType::class, $user);

            $paymentTwoForm->handleRequest($request);

            if($paymentTwoForm->isSubmitted() && $paymentTwoForm->isValid()) {

                $this->addFlash(
                    'paymentSuccess',
                    'Your payment details have been successfully updated'
                );

                // Save User
                $em = $this->getDoctrine()->getManager();
                $em->flush();
            }

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

            // Call Formatter
            $formatter = $this->get('app.timeline_format');
            return $this->render(
                'god-mode/adminprofile-bank-details.html.twig',
                [
                    'adm' => $adminsettings,
                    'paymentForm' => $paymentForm->createView(),
                    'paymentTwoForm' => $paymentTwoForm->createView(),
                    'bankForm' => $bankForm->createView(),
                    'timeline' => $formatter->format($notifications)
                ]
            );
        }

        /**
		 * @Route("/god-mode", name="god_mode_dashboard")
		 */
		private function godDashboardAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$usersCount = $this->getDoctrine()->getRepository('AppBundle:User')->getUsersCount();
			$totalUserCountsToday = $this->getDoctrine()->getRepository('AppBundle:User')->getTodayUsersCount();
			$newConfirmedPaymentsToday = $this->getDoctrine()->getRepository('AppBundle:User')->getConfirmedPaymentsToday();
			$referrersToday = $this->getDoctrine()->getRepository('AppBundle:User')->getReferrersToday();

			$totalWalletAmount = $this->getDoctrine()->getRepository('AppBundle:User')->getTotalWalletAmount();
			$withdrawalRequests = $this->getDoctrine()->getRepository('AppBundle:Transaction')->getTotalWithdrawalRequests();
			$todayWithdrawalRequests = $this->getDoctrine()->getRepository('AppBundle:Transaction')->getTodayWithdrawalRequests();
			$todayApprovedWithdrawalRequests = $this->getDoctrine()->getRepository('AppBundle:Transaction')->getApprovedTodayWithdrawalRequests();
			$activeUsersCount = $this->getDoctrine()->getRepository('AppBundle:User')->getActiveUsersCount();
			$blockedUsersCount = $this->getDoctrine()->getRepository('AppBundle:User')->getBlockedUsersCount();


			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT p FROM AppBundle:Pack p ORDER BY p.amount ASC";

            if($adminsettings->getAppMode() !== "verified") {

               // return $this->redirectToRoute('logout');
			}
			
			$packs = $em->createQuery($dql)->getResult();
			
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/dashboard.html.twig',
				[

                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'usersCount' => $usersCount,
					'totalUserCountsToday' => $totalUserCountsToday,
					'newConfirmedPaymentsToday' => $newConfirmedPaymentsToday,
					'activeUsersCount' => $activeUsersCount,
					'blockedUsersCount' => $blockedUsersCount,
                    // 'packsCount' => $packsCount,
					'packs' => $packs,
					'totalWalletAmount' => $totalWalletAmount,
					'withdrawalRequests' => $withdrawalRequests,
					'todayWithdrawalRequests' => $todayWithdrawalRequests,
					'todayApprovedWithdrawalRequests' => $todayApprovedWithdrawalRequests,
					'referrersToday' => $referrersToday
				]
			);
		}

        /**
         * @Route("/god-mode/list-super-admin/{page}", name="list_super_admin", defaults={"page" = 1})
         */
        private function listSuperAdminAction($page) {

            $paginator = $this->get('knp_paginator');

            $em = $this->getDoctrine()->getManager();

            $dql = "SELECT u FROM AppBundle:Admin u WHERE u.id = 1 ORDER BY u.id ASC";

            $query = $em->createQuery($dql);

            $limit = 10;

            $result = $paginator->paginate($query, $page, $limit);

            $result->setUsedRoute('list_super_admin');
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
            return $this->render(
                'god-mode/list-super-admin.html.twig',
                [
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
                    'admins' => $result
                ]
            );
        }

        /**
         * @Route("/god-mode/edit-super-admin/{admin}", name="edit_super_admin")
         */
        private function editSuperAdminAction(Request $request, $admin) {

            $admin = $this->getDoctrine()->getRepository('AppBundle:User')->find($admin);

            $form = $this->createForm(UserEditType::class, $admin);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                // Save User
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    $this->addFlash(
                        'successNotice',
                        'Admin edited successfully'
                    );

                    return $this->redirectToRoute('list_admin');
                } catch (Exception $e) {
                    $this->addFlash(
                        'errorNotice',
                        'No changes made to admin'
                    );
                }
            }
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
            return $this->render(
                'god-mode/edit-admin.html.twig',
                [
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
                    'form' => $form->createView(),
                    'admin' => $admin
                ]
            );
        }

		/**
		 * @Route("/god-mode/create-admin", name="create_admin")
		 */
		private function createAdminAction(Request $request) {

			$user = new User;
			$form = $this->createForm(UserType::class, $user);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {
				// Encode User Password
				$encoder = $this->get('security.password_encoder');
				$password = $encoder->encodePassword($user, $user->getPlainPassword());
				$user->setPassword($password);

				// Set User Role
				$user->setRole('ROLE_ADMIN');

				// Set Created Time
				$now = new \DateTime('now');
				$user->setCreated($now);

				// Set User Account Status
				$user->setStatus('active');
				$user->setActivationKey('activated');

				// Credit User Purse
				$user->setPurse("0");

				// Save User
				try {
					$em = $this->getDoctrine()->getManager();
					$em->persist($user);
					$em->flush();

					$this->addFlash(
						'successNotice',
						'Admin created successfully'
					);

					// Add to notifications
					$notification = new Notification;
					$notification->setType(1);
					$notification->setSubject($user->getId());
					$notification->setDetails("You signed up for {$this->getParameter('app_name')}");
					$notification->setCreated($now);
                    $notification->setStatus('unread');

					$em->persist($notification);
					$em->flush();

					$dql = "SELECT p FROM AppBundle:Pack p ORDER BY p.amount ASC";

					$packs = $em->createQuery($dql)->getResult();

//					$packs = $this->getDoctrine()->getRepository('AppBundle:Pack')->findAll();

//					foreach($packs as $pack) {
//						$packSubscription = new PackSubscription;
//						$packSubscription->setPack($pack);
//						$packSubscription->setUser($user);
//						$packSubscription->setUserType('admin');
//						$packSubscription->setFeederCounter(1);
//						$packSubscription->setFeederTotal(1);
//						$packSubscription->setAdminTrackLimit($this->getParameter('admin_collection_interval'));
//						$packSubscription->setType('pack');
//						$packSubscription->setStatus('active');
//						$packSubscription->setRecommitRequired(false);
//						$packSubscription->setPaidCommission(true);
//						$packSubscription->setForcedWaiting(false);
//						$packSubscription->setCreated($now);
//
//						$em->persist($packSubscription);
//					}
//					$em->flush();
//
//					foreach($packs as $pack) {
//						// Add to timeline
//						$notification = new Notification;
//						$notification->setType(2);
//						$notification->setSubject($user->getId());
//						$notificationDetails = "You subscribed for {$pack->getName()} Package";
//						$notification->setDetails($notificationDetails);
//						$notification->setCreated($now);
//                        $notification->setStatus('unread');
//
//						$em->persist($notification);
//					}
//					$em->flush();

					$em->clear();

					return $this->redirectToRoute('list_admin');
				} catch (Exception $e) {
					$this->addFlash(
						'errorNotice',
						'An error occured while creating admin'
					);
				}
			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/create-admin.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

		/**
		 * @Route("/god-mode/edit-admin/{admin}", name="edit_admin")
		 */
		private function editAdminAction(Request $request, $admin) {

			$admin = $this->getDoctrine()->getRepository('AppBundle:User')->find($admin);

			$form = $this->createForm(UserEditType::class, $admin);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {
				// Save User
				try {
					$em = $this->getDoctrine()->getManager();
					$em->flush();

					$this->addFlash(
						'successNotice',
						'Admin edited successfully'
					);

					return $this->redirectToRoute('list_admin');
				} catch (Exception $e) {
					$this->addFlash(
						'errorNotice',
						'No changes made to admin'
					);
				}
			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/edit-admin.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView(),
					'admin' => $admin
				]
			);
		}

        /**
         * @Route("/god-mode/manage-wallet/{user}", name="manage_wallet")
         */
        private function manageWallet(Request $request, $user) {

	        $paginator = $this->get('knp_paginator');

	        $from_request = $request->get("from");
	        $to_request = $request->get("to");

	        $from = ($from_request);
	        $to = ($to_request);


	        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($user);
            $user_wallet_balance = $user->getPurse();

	        $em = $this->getDoctrine()->getManager();

	        $dql = "SELECT u FROM AppBundle:User u WHERE u.referrer = ?1 ORDER BY u.name ASC";

	        $search_term = $request->get("q");

	        if ($search_term != null){
		        $dql = "SELECT u FROM AppBundle:User u WHERE u.referrer = ?1 AND u.username LIKE '%$search_term%' ";
	        }

	        if ($from != null && $to != null){
		        $dql = "SELECT p FROM AppBundle:User p WHERE p.referrer = ?1 AND p.created BETWEEN '$from' AND '$to' ";
	        }

	        $query = $em->createQuery($dql)->setParameter(1, $user->getId());

	        $limit = 15;

	        $referrals = $paginator->paginate($query, 1, $limit);

	        $form = $this->createFormBuilder()
	                     ->add('credit_wallet', TextType::class, array(
		                     'label' => "Credit Wallet",
		                     'attr' => array(
			                     'class' => 'form-control',
			                     'placeholder' => "Credit wallet"
		                     ),
		                     'constraints' => array(
			                     new NotBlank(
				                     array(
					                     'message' => 'Please enter an amount'
				                     )
			                     )
		                     ),
		                     'required' => true
	                     ))
	                     ->getForm();

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                // Save User
	            $data = $form->getData();
	            $amount_to_be_credited = $data["credit_wallet"];
	            $balancePlusAmount = $user_wallet_balance + $amount_to_be_credited;

	            try {

                    $user->setPurse($balancePlusAmount);
                    $em->persist($user);
                    $em->flush();

                    $this->addFlash(
                        'successNotice',
                        'Successfully credited user!'
                    );

                    return $this->redirectToRoute('list_users');
                } catch (Exception $e) {
                    $this->addFlash(
                        'errorNotice',
                        'No changes made to user'
                    );
                }
            }

            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );

            // Call Formatter
            $formatter = $this->get('app.timeline_format');

            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            return $this->render(
                'god-mode/manage-wallet.html.twig',
                [
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
                    'form' => $form->createView(),
                    'user' => $user,
                    'referrals' => $referrals
                ]
            );
        }

		/**
		 * @Route("/god-mode/edit-user/{user}", name="edit_user")
		 */
		private function editUserAction(Request $request, $user) {

			$user = $this->getDoctrine()->getRepository('AppBundle:User')->find($user);

			$form = $this->createForm(UserEditType::class, $user);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {
				// Save User
				try {
					$em = $this->getDoctrine()->getManager();
					$em->flush();

					$this->addFlash(
						'successNotice',
						'User edited successfully'
					);

					return $this->redirectToRoute('list_users');
				} catch (Exception $e) {
					$this->addFlash(
						'errorNotice',
						'No changes made to user'
					);
				}
			}

			$notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
				array('subject' => $this->getUser()->getId()),
				array('created' => 'DESC'),
				10
			);



			// Call Formatter
			$formatter = $this->get('app.timeline_format');

			$adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			return $this->render(
				'god-mode/edit-user.html.twig',
				[
					'timeline' => $formatter->format($notifications),
					'adm' => $adminsettings,
					'form' => $form->createView(),
					'user' => $user
				]
			);
		}



		/**
		 * @Route("/god-mode/list-admin/{page}", name="list_admin", defaults={"page" = 1})
		 */
		private function listAdminAction($page) {

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT u FROM AppBundle:User u WHERE (u.role = 'ROLE_ADMIN' OR u.role = 'ROLE_SUPER_ADMIN') AND u.id != 1 ORDER BY u.name ASC";

			$query = $em->createQuery($dql);

			$limit = 10;

			$result = $paginator->paginate($query, $page, $limit);

			$result->setUsedRoute('list_admin');
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/list-admin.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'admins' => $result
				]
			);
		}

		/**
		 * @Route("/god-mode/create-pack", name="create_pack")
		 */
		private function createPackAction(Request $request) {

			$pack = new Pack;
			$form = $this->createForm(PackType::class, $pack);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				// Set Created Time
				$now = new \DateTime('now');
				$pack->setCreated($now);
				$pack->setFeeders(0);

				// Save Plan
				try {
					$em = $this->getDoctrine()->getManager();
					$em->persist($pack);
					$em->flush();

					$this->addFlash(
						'successNotice',
						'Pack created successfully'
					);

					// Add Admin Subscriptions
					$admins = $this->getParameter('admin_users');

					$adminRepo = $this->getDoctrine()->getRepository('AppBundle:User');

					foreach($admins as $admin) {

						$adminObj = $adminRepo->find($admin);

						$packSubscription = new PackSubscription;
						$packSubscription->setPack($pack);
						$packSubscription->setUser($adminObj);
						$packSubscription->setUserType('admin');
						$packSubscription->setFeederCounter(1);
						$packSubscription->setFeederTotal(1);
						$packSubscription->setAdminTrackLimit($this->getParameter('admin_collection_interval'));
						$packSubscription->setType('pack');
						$packSubscription->setStatus('active');
						$packSubscription->setForcedWaiting(false);
						$packSubscription->setPaidCommission(false);
						$packSubscription->setRecommitRequired(false);

						$packSubscription->setCreated($now);

						$em->persist($packSubscription);
					}

					$em->flush();

					foreach($admins as $admin) {
						// Add to timeline
						$notification = new Notification;
						$notification->setType(2);
						$notification->setSubject($admin);
						$notificationDetails = "You subscribed for {$pack->getName()} Package";
						$notification->setDetails($notificationDetails);
						$notification->setCreated($now);
                        $notification->setStatus('unread');

						$em->persist($notification);
					}

					$em->flush();
					$em->clear();

					return $this->redirectToRoute('list_pack');

				} catch (Exception $e) {
					$this->addFlash(
						'errorNotice',
						'An error occured while creating pack'
					);
				}

			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/create-pack.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

		/**
		 * @Route("/god-mode/list-pack/{page}", name="list_pack", defaults={"page"=1})
		 */
		private function listPackAction($page) {

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT p FROM AppBundle:Pack p ORDER BY p.amount ASC";

			$query = $em->createQuery($dql);

			$limit = 10; // Set default

			$result = $paginator->paginate($query, $page, $limit);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/list-pack.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'packs' => $result
				]
			);
		}

		/**
		 * @Route("/god-mode/edit-pack/{pack}", name="edit_pack")
		 */
		private function editPackAction(Request $request, $pack) {
			
			$pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			$form = $this->createForm(PackType::class, $pack);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				// Save Pack
				try {
					$em = $this->getDoctrine()->getManager();
					$em->flush();
					$em->clear();

					$this->addFlash(
						'successNotice',
						'Pack edited successfully'
					);

					return $this->redirectToRoute('list_pack');

				} catch (Exception $e) {
					$this->addFlash(
						'errorNotice',
						'An error occured while editing pack'
					);
				}

			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/edit-pack.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView(),
					'pack' => $pack
				]
			);
		}

        /**
         * @Route("/god-mode/list_pack/{pack}", name="delete_pack")
         */
        private function deletePackAction($pack)
        {

            $pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);


            $title = $pack->getName();
            //Delete all feeders in subscriptions to that pack
            $feederSubs = $this->getDoctrine()->getRepository('AppBundle:Feeder')->findBy(
                array(
                    'pack' => $pack->getId()

                ));
            //Delete all subscriptions to that pack
            $packSubscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findBy(
                array(
                    'pack' => $pack->getId()

                ));



            try {

                $em = $this->getDoctrine()->getManager();

               /* foreach ($feederSubs as $feederSub) {
                    $em->remove($feederSub);
                    $em->persist($feederSub);
                    $em->flush();
                }

                foreach ($packSubscriptions as $packSubscription){
                    $em->remove($packSubscription);
                    $em->persist($packSubscription);
                    $em->flush();

                }*/

                $em->remove($pack);

                $em->persist($pack);

                $em->flush();

                $this->addFlash(
                    'PackSuccess',
                    "'$title' successfully deleted"
                );

                $em->clear();

                return $this->redirectToRoute('list_pack');

            } catch(Exception $e) {

                $this->addFlash(
                    'PackError',
                    "An error occured while deleting '$title'. Please try again later"
                );
                return $this->redirectToRoute('list_pack');
            }





        }

        /**
         * @Route("/god-mode/list-pack-subs/{page}", name="list_pack_subs", defaults={"page"=1})
         */
        private function listPackSubsAction($page) {

            $paginator = $this->get('knp_paginator');

            $em = $this->getDoctrine()->getManager();

            $dql = "SELECT p FROM AppBundle:PackSubscription p ORDER BY p.id ASC";

            $query = $em->createQuery($dql);

            $limit = 10; // Set default

            $result = $paginator->paginate($query, $page, $limit);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
            return $this->render(
                'god-mode/list-packsubscriptions.html.twig',
                [
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
                    'packsubs' => $result
                ]
            );
        }

		/**
		 * @Route("/god-mode/list-admin-payments/{page}", name="list_admin_payments", defaults={"page"=1})
		 * @param $page
		 * @param Request $request
		 *
		 * @return \Symfony\Component\HttpFoundation\Response
		 */
        private function listAdminPaymentsAction($page, Request $request) {

            $paginator = $this->get('knp_paginator');

            $em = $this->getDoctrine()->getManager();

	        $from_request = $request->get("from");
	        $to_request = $request->get("to");

	        $from = ($from_request);
	        $to = ($to_request);

	        $dql = "SELECT p FROM AppBundle:PackSubscription p ORDER BY p.id DESC";

	        if ($from != null && $to != null){
		        $dql = "SELECT p FROM AppBundle:PackSubscription p WHERE p.created BETWEEN '$from' AND '$to' ";
	        }

	        $search_term = $request->get("q");

	        if ($search_term != null){

//		        'SELECT p, c
//		        FROM App\Entity\Product p
//		        INNER JOIN p.category c
//		        WHERE p.id = :id'
//
		        $dql = "SELECT p,u FROM AppBundle:PackSubscription p INNER JOIN p.user u WHERE u.username LIKE '%$search_term%' ";
	        }

            $query = $em->createQuery($dql);

            $limit = 20; // Set default

            $result = $paginator->paginate($query, $page, $limit);

            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
            return $this->render(
                'god-mode/list-payments.html.twig',
                [
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
                    'adminpayments' => $result
                ]
            );
		}
		
		   /**
         * @Route("/god-mode/confirm-payments/{packSubscription}", name="confirm_pack_payments")
         */
        private function confirmPayment($packSubscription) {

			$packSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($packSubscription);

				$em = $this->getDoctrine()->getManager();

			if ($packSubscription->getStatus() == "active"){
				$subscription = $packSubscription->setStatus("waiting");

			}else{
				$subscription = $packSubscription->setStatus("active");
				$user = $packSubscription->getUser();
				$pack = $packSubscription->getPack();
				$now = new \DateTime('now');

				if($subscription->getUser()->getReferrer() != null ){
					// $wallet = $subscription->getUser()->getPurse();
					// $referrer_id = $subscription->getUser()->getReferrer();

					// $referrer = $this->getDoctrine()->getRepository('AppBundle:User')->find($referrer_id);

					// $wallet_amount  = $referrer->getPurse();


					$referrer = $this->getDoctrine()->getRepository('AppBundle:User')->find($user->getReferrer());
					if($referrer != null) {
						$referrer_previous_balance = $referrer->getPurse();
						// Get Amount
						$amount = $pack->getAmount();
						// Calculate Percentage
						$receiverShare = ($amount * $this->getParameter('referral_percentage')) / 100;

						$referrer->setPurse($referrer_previous_balance + $receiverShare);

						$em->persist($referrer);

						// Add Transaction
						$transaction = new Transaction;
						$transaction->setPack($pack);
						$transaction->setFeederPackSubscription($packSubscription);
						$transaction->setFeeder($user);
						$transaction->setReceiver($referrer);
						$transaction->setAmount($amount);
						$transaction->setReceiverShare($receiverShare);
						$transaction->setStatus('confirmed');
						$transaction->setCreated($now);

						$em->persist($transaction);
						$em->flush();
					}
					
				}

			}
		

			// $em = $this->getDoctrine()->getManager();
			$em->persist($subscription);
			$em->flush();

			return $this->redirectToRoute('list_admin_payments');
		}

        /**
         * @Route("/god-mode/do-activate-packsub/{pack}", name="do_activate_packsubscription")
         */
        private function doActivatePackSubscriptionrAction($pack) {

            $packsubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($pack);

            $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->activateSubscription($packsubscription);

            $this->addFlash(
                'successNotice',
                "{$packsubscription->getPack()->getName()} successfully activated"
            );

            return $this->redirectToRoute('list_pack_subs');
        }

        /**
         * @Route("/god-mode/do-block-packsub/{pack}", name="do_block_packsubscription")
         */
        private function doBlockPackSubscriptionrAction($pack) {

            $packsubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($pack);

            $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->blockSubscription($packsubscription);

            $this->addFlash(
                'successNotice',
                "{$packsubscription->getPack()->getName()} successfully activated"
            );

            return $this->redirectToRoute('list_pack_subs');
        }

		/**
		 * @Route("/god-mode/create-news", name="create_news")
		 */
		private function createNewsAction(Request $request) {

			$news = new News;
			$form = $this->createForm(NewsType::class, $news);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				$imageFile = $news->getImageFile();

				$imageFileName = md5(uniqid()).'.'.$imageFile->guessExtension();

				// Move file to directory
				$newsDirectory = $this->getParameter('news_image_real_directory');
				$newsDbDirectory = $this->getParameter('news_image_directory');

				if(!is_dir($newsDirectory) && is_readable($newsDirectory)) {
					mkdir($newsDirectory);
				}

				$imageFile->move(
					$newsDirectory,
					$imageFileName
				);

				// Store file name
				$news->setImage($newsDbDirectory.'/'.$imageFileName);

				// Set Created Date
				$now = new \DateTime('now');
				$news->setCreated($now);

				try {
					
					$em = $this->getDoctrine()->getManager();

					$em->persist($news);

					$em->flush();
					
					$this->addFlash(
						'newsSuccess',
						'News Article Successfully Created'
					);

					return $this->redirectToRoute('list_news');

				} catch (Exception $e) {
					
					$this->addFlash(
						'newsError',
						'An error occured while creating the news entry. Please try again later'
					);
				}
			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/create-news.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

		/**
		 * @Route("/god-mode/news/edit/{news}", name="edit_news")
		 */
		private function editNewsAction(Request $request, $news) {

			$news = $this->getDoctrine()->getRepository('AppBundle:News')->find($news);
			$form = $this->createForm(NewsEditType::class, $news);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				$imageFile = $news->getImageFile();

				if($imageFile != null) {
					// unlink file
					unlink($_SERVER['DOCUMENT_ROOT'].$news->getImage());

					$imageFileName = md5(uniqid()).'.'.$imageFile->guessExtension();

					// Move file to directory
					$newsDirectory = $this->getParameter('news_image_real_directory');
					$newsDbDirectory = $this->getParameter('news_image_directory');

					$imageFile->move(
						$newsDirectory,
						$imageFileName
					);

					// Store file name
					$news->setImage($newsDbDirectory.'/'.$imageFileName);
				}

				try {
					
					$em = $this->getDoctrine()->getManager();

					$em->flush();
					
					$this->addFlash(
						'newsSuccess',
						"'{$news->getTitle()}' Successfully Edited"
					);

					$em->clear();

					return $this->redirectToRoute('list_news');

				} catch (Exception $e) {
					
					$this->addFlash(
						'newsError',
						'An error occured while editing the news entry. Please try again later'
					);
				}
			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/edit-news.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView(),
					'news' => $news
				]
			);
		}

		/**
		 * @Route("/god-mode/news/delete/{news}", name="delete_news")
		 */
		private function deleteNewsAction($news) {

			$news = $this->getDoctrine()->getRepository('AppBundle:News')->find($news);

			$title = $news->getTitle();

			$img = $news->getImage();

			try {

				unlink($_SERVER['DOCUMENT_ROOT'].$img);

				$em = $this->getDoctrine()->getManager();

				$em->remove($news);

				$em->flush();

				$this->addFlash(
					'newsSuccess',
					"'$title' successfully deleted"
				);

				$em->clear();

				return $this->redirectToRoute('list_news');

			} catch(Exception $e) {

				$this->addFlash(
					'newsError',
					"An error occured while deleting '$title'. Please try again later"
				);

				return $this->redirectToRoute('list_news');
			}
		}

		/**
		 * @Route("/god-mode/news/list/{page}", name="list_news", defaults={"page"=1})
		 */
		private function listNewsAction($page) {

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT n FROM AppBundle:News n ORDER BY n.created DESC";

			$query = $em->createQuery($dql);

			$limit = 25;

			$news = $paginator->paginate($query, $page, $limit);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/list-news.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'news' => $news
				]
			);
		}
		/**
		 * @Route("/god-mode/set-notice-board", name="set_notice_board")
		 */
		private function setNoticeBoardAction(Request $request) {

			$noticeBoard = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1) != null ? $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1) : new Admin;
			$form = $this->createForm(NoticeBoardType::class, $noticeBoard);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {
				
				// Update Previous Entry
				if($noticeBoard->getTitle() == '' || $noticeBoard->getTitle() == null) {

					$noticeBoard->setTitle(NULL);
				}
				if($noticeBoard->getMessage() == '' || $noticeBoard->getMessage() == null) {

					$noticeBoard->setMessage(NULL);
				}

				if($noticeBoard->getId() == null) {
					// Alert Bar Blank, Insert New Entry
					try {
						$em = $this->getDoctrine()->getManager();

						$em->persist($noticeBoard);

						$em->flush();

						$em->clear();

						$this->addFlash(
							'successNotice',
							'Notice Board Successfully Created'
						);
					} catch(Exception $e) {

						$this->addFlash(
							'errorNotice',
							'An error occured while creating the notice board. Please try again later'
						);
					}
				} else {
					
					try {
						$em = $this->getDoctrine()->getManager();

						$em->flush();

						$em->clear();

						$this->addFlash(
							'successNotice',
							'Notice Board Successfully Edited'
						);
					} catch (Exception $e) {

						$this->addFlash(
							'errorNotice',
							'An error occured while updating the notice board. Please try again later'
						);
					}
				}
			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/notice-board.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

		/**
		 * @Route("/god-mode/unblock-user", name="unblock_user")
		 */
		private function unblockUserAction(Request $request) {

			$form = $this->createFormBuilder()
				->add('username', TextType::class, array(
					'label' => "Username",
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => "Username"
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter username'
							)
						),
						new Length(
							array(
								'min' => 3,
								'max' => 40,
								'minMessage' => 'Username must contain 3 or more characters',
								'maxMessage' => 'Username cannot contain more than 40 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/^[A-Za-z0-9]+$/',
								'message' => 'Username can only contain alphanumeric characters'
							)
						)
					),
					'required' => true
				))
				->getForm();

			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				$data = $form->getData();

				// Check if username exists
				$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
					array(
						'username' => $data['username']
					)
				);

				if($user == null) {

					$this->addFlash(
						'unblockError',
						"The user with the username: {$data['username']} was not found"
					);

				} else {

					$user->setStatus('active');

					try {

						$em = $this->getDoctrine()->getEntityManager();

						$em->flush();

						$em->clear();

						$this->addFlash(
							'unblockSuccess',
							"The user with the username: {$data['username']} has been successfully unblocked"
						);
					} catch(Exception $e) {

						$this->addFlash(
							'unblockError',
							"An error occured while unblocking the user with the username: {$data['username']}. Please try again later"
						);
					}
				}
			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/unblock-user.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

		/**
		 * @Route("/god-mode/reset-password", name="reset_password")
		 */
		private function resetPasswordAction(Request $request) {

			$form = $this->createFormBuilder()
				->add('username', TextType::class, array(
					'label' => "Username",
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => "Username"
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter username'
							)
						),
						new Length(
							array(
								'min' => 3,
								'max' => 40,
								'minMessage' => 'Username must contain 3 or more characters',
								'maxMessage' => 'Username cannot contain more than 40 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/^[A-Za-z0-9]+$/',
								'message' => 'Username can only contain alphanumeric characters'
							)
						)
					),
					'required' => true
				))
				->getForm();

			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				$data = $form->getData();

				// Check if username exists
				$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
					array(
						'username' => $data['username']
					)
				);

				if($user == null) {

					$this->addFlash(
						'resetError',
						"The user with the username: {$data['username']} was not found"
					);

				} else {

					// Reset Password
					// Get encoder
					$encoder = $this->get("security.password_encoder");
					$newPassword = 'password';
					$password = $encoder->encodePassword($user, $newPassword);
					$user->setPassword($password);

					try {

						$em = $this->getDoctrine()->getEntityManager();

						$em->flush();

						$em->clear();

						$this->addFlash(
							'resetSuccess',
							"The password of the user with the username: {$data['username']} has been successfully reset"
						);
					} catch(Exception $e) {

						$this->addFlash(
							'resetError',
							"An error occured while resetting password for the user with the username: {$data['username']}. Please try again later"
						);
					}
				}
			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/reset-password.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

		/**
		 * @Route("/god-mode/mails/compose", name="god_mode_compose_mail")
		 */
		private function composeMailAction(Request $request) {

			$mail = new Mail;
			$form = $this->createForm(MailType::class, $mail);
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				// Set Created Date
				$now = new \DateTime('now');
				$mail->setCreated($now);

				try {
					
					$em = $this->getDoctrine()->getManager();

					$em->persist($mail);

					$em->flush();

					// Send Mail to recipients
					
					// Grab Users
					$users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();

					$batchSize = 20;

					for($i = 0; $i < count($users); $i++) {

						if($users[$i]->getRole() == 'ROLE_USER') {

							$mailRecipient = new MailRecipient;
							$mailRecipient->setMail($mail);
							$mailRecipient->setUser($users[$i]);
							$mailRecipient->setStatus('unread');
							$mailRecipient->setCreated($now);
							$em->persist($mailRecipient);

							if(($i % $batchSize) === 0) {
								$em->flush();
								$em->clear();
							}
						}

					}

					$em->flush();
					$em->clear();

					// Send Notifications to recipients
					for($i = 0; $i < count($users); $i++) {

						if($users[$i]->getRole() == 'ROLE_USER') {

							$notification = new Notification;
							$mailRecipientObject = $this->getDoctrine()->getRepository('AppBundle:MailRecipient')->findOneBy(
								array(
									'mail' => $mail,
									'user' => $users[$i]
								)
							);
							$notification->setType(6);
							$notification->setSubject($users[$i]->getId());
							$notificationDetails = "Mail from Support Team";
							$notification->setDetails($notificationDetails);
							$notification->setObject($mailRecipientObject->getId());
							$notification->setCreated($now);
                            $notification->setStatus('unread');

							$em->persist($notification);

							if(($i % $batchSize) === 0) {
								$em->flush();
								$em->clear();
							}
						}
					}

					$em->flush();
					$em->clear();
					
					$this->addFlash(
						'mailSuccess',
						'Mail Sent'
					);

					return $this->redirectToRoute('god_mode_mails');

				} catch (Exception $e) {
					
					$this->addFlash(
						'mailError',
						'An error occured while sending the mail. Please try again later'
					);
				}
			}
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/compose-mail.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

		/**
		 * @Route("/god-mode/mails/view/{mail}", name="god_mode_view_mail")
		 */
		private function viewMailAction($mail) {

			$mail = $this->getDoctrine()->getRepository('AppBundle:Mail')->find($mail);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($mail == null) {
				
				return $this->render(
					'member/page-not-found.html.twig',
					[
                        'timeline' => $formatter->format($notifications),
                        'adm' => $adminsettings
                    ]
				);
			}

			return $this->render(
				'god-mode/view-mail.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'mail' => $mail
				]
			);
		}

		/**
		 * @Route("/god-mode/mails/{page}", name="god_mode_mails", defaults={"page"=1})
		 */
		private function listMailAction($page) {

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT m FROM AppBundle:Mail m ORDER BY m.created DESC";

			$query = $em->createQuery($dql);

			$limit = 25;

			$mails = $paginator->paginate($query, $page, $limit);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/list-mail.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'mail' => $mails
				]
			);
		}

		/**
		 * @Route("/god-mode/reviews/list/approve/{page}", name="list_approve_reviews", defaults={"page"=1})
		 */
		private function listApproveReviewsAction($page) {

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT r FROM AppBundle:Review r ORDER BY r.createdDate DESC";

			$query = $em->createQuery($dql);

			$limit = 25;

			$reviews = $paginator->paginate($query, $page, $limit);

			$items = $reviews->getItems();

			$reviewsItems = array();

			$userRepo = $this->getDoctrine()->getRepository('AppBundle:User');

			foreach($items as $item) {
				
				$item->setUser($userRepo->find($item->getUserId()));

				$reviewsItems[] = $item;
			}

            $formatter = $this->get('app.timeline_format');
			$reviews->setItems($reviewsItems);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
			return $this->render(
				'god-mode/list-approve-reviews.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'reviews' => $reviews
				]
			);
		}

		/**
		 * @Route("/god-mode/reviews/do/approve/{review}", name="do_approve_review")
		 */
		private function doApproveReviewAction($review) {
			
			$review = $this->getDoctrine()->getRepository('AppBundle:Review')->find($review);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($review === null) {
				return $this->render('member/page-not-found.html.twig', [
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings
				]);
			}

			$em = $this->getDoctrine()->getManager();

			$review->setStatus('approved');

			$em->flush();

			$em->clear();

			$this->addFlash(
				'reviewSuccess',
				'Review successfully Approved!!!'
			);

			return $this->redirectToRoute('list_approve_reviews');
		}

		/**
		 * @Route("/god-mode/reviews/do/disapprove/{review}", name="do_disapprove_review")
		 */
		private function doDisapproveReviewAction($review) {
			
			$review = $this->getDoctrine()->getRepository('AppBundle:Review')->find($review);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			if($review === null) {
				return $this->render('member/page-not-found.html.twig', [
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
				]);
			}

			$em = $this->getDoctrine()->getManager();

			$review->setStatus('disapproved');

			$em->flush();

			$em->clear();

			$this->addFlash(
				'reviewError',
				'Review successfully Disapproved!!!'
			);

			return $this->redirectToRoute('list_approve_reviews');
		}

		/**
		 * @Route("/god-mode/list-users/{page}/{sort}/{sponsor}", name="list_users", defaults={"page" = 1, "sort" = "date_DESC","sponsor" = "0"})
		 */
		private function listUsersAction($page, $sort, $sponsor, Request $request) {

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			switch ($sort) {
				case 'date_DESC':
					$sortKey = 'u.created DESC';
					break;

				case 'date_ASC':
					$sortKey = 'u.created ASC';
					break;

				case 'name_DESC':
					$sortKey = 'u.name DESC';
					break;

				case 'name_ASC':
					$sortKey = 'u.name ASC';
					break;

				case 'username_DESC':
					$sortKey = 'u.username DESC';
					break;

				case 'username_ASC':
					$sortKey = 'u.username ASC';
					break;

				case 'purse_ASC':
					$sortKey = 'u.purse ASC';
					break;

				case 'purse_DESC':
					$sortKey = 'u.purse DESC';
					break;
				
				default:
					$sortKey = 'u.created DESC';
					break;
			}

			if ($sponsor != 0){
				$dql = "SELECT u FROM AppBundle:User u WHERE u.role = 'ROLE_USER' AND referrer = $sponsor";

			}else{
				$dql = "SELECT u FROM AppBundle:User u WHERE u.role = 'ROLE_USER' ORDER BY $sortKey";
			}

			$from_request = $request->get("from");
			$to_request = $request->get("to");

			$from = ($from_request);
			$to = ($to_request);

			if ($from != null && $to != null){
				$dql = "SELECT u FROM AppBundle:User u WHERE u.role = 'ROLE_USER' AND u.created BETWEEN '$from' AND '$to' ORDER BY $sortKey";
			}

			$search_term = $request->get("q");

			if ($search_term != null){
				$dql = "SELECT u FROM AppBundle:User u WHERE u.role = 'ROLE_USER' AND u.username LIKE '%$search_term%' ORDER BY $sortKey";
			}

			$query = $em->createQuery($dql);

			$limit = 10;

			$result = $paginator->paginate($query, $page, $limit);
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/list-users.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'users' => $result,
					'page' => $page,
					'sort' => $sort
				]
			);
		}

		/**
		 * @Route("/god-mode/list-withdrawal-requests/{page}/{sort}", name="list_withdrawal_requests", defaults={"page" = 1, "sort" = "date_DESC"})
		 */
		private function listWithdrawalRequests($page, $sort, Request $request) {

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			switch ($sort) {
				case 'date_DESC':
					$sortKey = 'u.created DESC';
					break;

				case 'date_ASC':
					$sortKey = 'u.created ASC';
					break;

				case 'name_DESC':
					$sortKey = 'u.name DESC';
					break;

				case 'name_ASC':
					$sortKey = 'u.name ASC';
					break;

				case 'username_DESC':
					$sortKey = 'u.username DESC';
					break;

				case 'username_ASC':
					$sortKey = 'u.username ASC';
					break;

				default:
					$sortKey = 'u.created DESC';
					break;
			}

			$dql = "SELECT t FROM AppBundle:Transaction t WHERE t.withdrawal = 1";

			$sum_dql = "SELECT SUM(e.amount) AS balance FROM AppBundle:Transaction e WHERE e.withdrawal = 1";

			$sum_approved_dql = "SELECT SUM(e.amount) AS balance FROM AppBundle:Transaction e WHERE e.withdrawal = 1 AND e.approved = 1";
			$sum_unapproved_dql = "SELECT SUM(e.amount) AS balance FROM AppBundle:Transaction e WHERE e.withdrawal = 1 AND e.approved = 0 ";

			$from_request = $request->get("from");
			$to_request = $request->get("to");

			$from = ($from_request);
			$to = ($to_request);

			if ($from != null && $to != null){
				$dql = "SELECT p FROM AppBundle:Transaction p WHERE p.withdrawal = 1 AND p.created BETWEEN '$from' AND '$to' ";

				$sum_dql = "SELECT SUM(t.amount) AS balance FROM AppBundle:Transaction t WHERE t.withdrawal = 1 AND t.created BETWEEN '$from' AND '$to'";
				$sum_approved_dql = "SELECT SUM(e.amount) AS balance FROM AppBundle:Transaction e WHERE e.withdrawal = 1 AND e.approved = 1 AND e.created BETWEEN '$from' AND '$to'";
				$sum_unapproved_dql = "SELECT SUM(e.amount) AS balance FROM AppBundle:Transaction e WHERE e.withdrawal = 1 AND e.approved = 0 AND e.created BETWEEN '$from' AND '$to' ";
			}

			$search_term = $request->get("q");

			if ($search_term != null){
				$dql = "SELECT p,u FROM AppBundle:Transaction p INNER JOIN p.receiver u WHERE p.withdrawal = 1 AND u.username LIKE '%$search_term%' ";
			}

			$query = $em->createQuery($dql);

			$total = $em->createQuery($sum_dql)
//			              ->setParameter(1, $myAccountId)
			              ->getSingleScalarResult();

			$approved_total = $em->createQuery($sum_approved_dql)
//			              ->setParameter(1, $myAccountId)
			              ->getSingleScalarResult();

			$unapproved_total = $em->createQuery($sum_unapproved_dql)->getSingleScalarResult();

			$limit = 10;

			$result = $paginator->paginate($query, $page, $limit);
			$adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
				array('subject' => $this->getUser()->getId()),
				array('created' => 'DESC'),
				10
			);
			$formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/list-withdrawal-requests.html.twig',
				[
					'timeline' => $formatter->format($notifications),
					'adm' => $adminsettings,
					'users' => $result,
					'page' => $page,
					'sort' => $sort,
					'total' => $total,
					'approved' => $approved_total,
					'unapproved' => $unapproved_total
				]
			);
		}


		/**
		 * @Route("/god-mode/approve-withdrawal-request/{user}/{transaction}", name="approve_withdrawal_request")
		 */
		private function approveWithdrawalRequest($user, $transaction) {

			$user_obj = $this->getDoctrine()->getRepository('AppBundle:User')->find($user);
//			$transaction = $this->getDoctrine()->getRepository('AppBundle:User')->find($user);
			$wallet_amount = $user_obj->getPurse();
			$amount_requested = $user_obj->getAmountRequested();
			$new_balance = $wallet_amount - $amount_requested;

			$em = $this->getDoctrine()->getManager();

			// Add Transaction

			$emt = $this->getDoctrine()->getManager();

			$transaction_obj = $this->getDoctrine()->getRepository('AppBundle:Transaction')->find($transaction);
			$transaction_obj->setApproved(1);
			$emt->persist( $transaction_obj);
			$user_obj->setPurse($new_balance);
			$user_obj->setWithdrawalRequestApproved(1);
			$user_obj->setWithdrawalRequest(0);

			$emt->persist( $user_obj );
			$emt->flush();
			$emt->clear();


			$this->addFlash(
				'successNotice',
				"{$user_obj->getName()} successfully approved the withdrawalRequest"
			);

			return $this->redirectToRoute('list_withdrawal_requests');
		}



		/**
		 * @Route("/god-mode/do-block-user/{user}/{page}/{sort}", name="do_block_user")
		 */
		private function doBlockUserAction($user, $page, $sort) {

			$user = $this->getDoctrine()->getRepository('AppBundle:User')->find($user);
			
			$this->getDoctrine()->getRepository('AppBundle:User')->blockUser($user);

			$this->addFlash(
				'errorNotice',
				"{$user->getName()} successfully blocked from the system"
			);

			return $this->redirectToRoute('list_users', array('page' => $page, 'sort' => $sort));
		}

		/**
		 * @Route("/god-mode/do-activate-user/{user}/{page}/{sort}", name="do_activate_user")
		 */
		private function doActivateUserAction($user, $page, $sort) {

			$user = $this->getDoctrine()->getRepository('AppBundle:User')->find($user);
			
			$this->getDoctrine()->getRepository('AppBundle:User')->activateUser($user);

			$this->addFlash(
				'successNotice',
				"{$user->getName()} successfully activated"
			);

			return $this->redirectToRoute('list_users', array('page' => $page, 'sort' => $sort));
		}

		/**
		 * @Route("/god-mode/merge-users/{pack}/{page}", name="god_mode_merge_users", defaults={"page"=1})
		 */
		private function mergeUsersAction(Request $request,$pack, $page) {

			$pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			if($request->query->get('query')){

				$dql = "SELECT p FROM AppBundle:PackSubscription p WHERE p.status = 'waiting' AND p.feederCounter > 0 AND p.pack = ?1 AND p.userType != 'admin' AND p.user=".$request->query->get('query');

			}else{
				$dql = "SELECT p FROM AppBundle:PackSubscription p WHERE p.status = 'waiting' AND p.feederCounter > 0 AND p.pack = ?1 AND p.userType != 'admin'";
			}

			$activeUsers = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getActiveUsers($pack->getId());
			$activeUsersCount = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getActiveUsersCount($pack->getId());
			$activeRecommitUsersCount = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getRecommitUsersCount($pack->getId());
			$activeRecommitUsers = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getRecommitRequiredUsers($pack->getId());

			$query = $em->createQuery($dql)->setParameter(1, $pack->getId());

			$selectQuery = "SELECT p FROM AppBundle:PackSubscription p WHERE p.status = 'waiting' AND p.feederCounter > 0 AND p.pack = ?1 AND p.userType != 'admin'";

			$selectList = $em->createQuery($selectQuery)->setParameter(1, $pack->getId());

			$selectListResult=  $paginator->paginate($query, 1, 500);

			$limit = 30;

			$result = $paginator->paginate($query, $page, $limit);

			$admins = $this->getDoctrine()->getRepository('AppBundle:User')->getAllAdmins($this->getUser());
            $adminone = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
                array(
                    'id' => 1
                )
            );

			$mergeType = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1)->getMatchMode();

//			$selectQuery = "SELECT p FROM AppBundle:PackSubscription p WHERE p.status = 'active' AND p.feederCounter > 0 AND p.pack = ?1 AND p.userType != 'admin'";


			$adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
            $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                array('subject' => $this->getUser()->getId()),
                array('created' => 'DESC'),
                10
            );
            $formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/merge-users.html.twig',
				[
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings,
					'waitingSubscriptions' => $result,
					'activeUsers' => $activeUsers,
					'activeUsersCount' => $activeUsersCount,
					'recommitUsersCount' => $activeRecommitUsersCount,
					'activeRecommitUsers' => $activeRecommitUsers,
					'pack' => $pack,
					'admins' => $admins,
                    'adminone' => $adminone,
//                    'admintwo' => $admintwo,
//                    'adminthree' => $adminthree,
					'mergeType' => $mergeType,
					'selectList' => $selectListResult
				]
			);
		}

		/**
		 * @Route("/god-mode/recommitments/{pack}/{page}", name="recommitments", defaults={"page"=1})
		 */
		private function Recommitments(Request $request,$pack,$page) {

			$pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			$paginator = $this->get('knp_paginator');

			$em = $this->getDoctrine()->getManager();

			if($request->query->get('query')){

				$dql = "SELECT p FROM AppBundle:PackSubscription p WHERE p.status = 'waiting' AND p.feederCounter > 0 AND p.pack = ?1 AND p.userType != 'admin' AND p.user=".$request->query->get('query');

			}else{
				$dql = "SELECT p FROM AppBundle:PackSubscription p WHERE p.status = 'waiting' AND p.feederCounter > 0 AND p.pack = ?1 AND p.userType != 'admin'";
			}

			$activeUsers = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getActiveUsers($pack->getId());
			$activeUsersCount = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getActiveUsersCount($pack->getId());
			$activeRecommitUsersCount = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getRecommitUsersCount($pack->getId());

			if($request->query->get('receivers')){

				$activeRecommitUsers = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getActiveUsers($pack->getId());

			}else{

				$activeRecommitUsers = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getRecommitRequiredUsers($pack->getId());

			}

			$query = $em->createQuery($dql)->setParameter(1, $pack->getId());

			$selectQuery = "SELECT p FROM AppBundle:PackSubscription p WHERE p.status = 'waiting' AND p.feederCounter > 0 AND p.pack = ?1 AND p.userType != 'admin'";

			$selectList = $em->createQuery($selectQuery)->setParameter(1, $pack->getId());

			$selectListResult=  $paginator->paginate($query, 1, 500);

			$limit = 30;

			$result = $paginator->paginate($query, $page, $limit);

			$admins = $this->getDoctrine()->getRepository('AppBundle:User')->getAllAdmins($this->getUser());
			$adminone = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
				array(
					'id' => 1
				)
			);

			$mergeType = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1)->getMatchMode();

//			$selectQuery = "SELECT p FROM AppBundle:PackSubscription p WHERE p.status = 'active' AND p.feederCounter > 0 AND p.pack = ?1 AND p.userType != 'admin'";


			$adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
				array('subject' => $this->getUser()->getId()),
				array('created' => 'DESC'),
				10
			);
			$formatter = $this->get('app.timeline_format');
			return $this->render(
				'god-mode/recommitments.html.twig',
				[
					'timeline' => $formatter->format($notifications),
					'adm' => $adminsettings,
					'waitingSubscriptions' => $result,
					'activeUsers' => $activeUsers,
					'activeUsersCount' => $activeUsersCount,
					'recommitUsersCount' => $activeRecommitUsersCount,
					'activeRecommitUsers' => $activeRecommitUsers,
					'pack' => $pack,
					'admins' => $admins,
					'adminone' => $adminone,
//                    'admintwo' => $admintwo,
//                    'adminthree' => $adminthree,
					'mergeType' => $mergeType,
					'selectList' => $selectListResult
				]
			);
		}


		/**
		 * @Route("/god-mode/change-recommitment/{recommit_id}/{status}", name="change-recommitment")
		 */
		private function changeRecommitment($recommit_id,$status){

			$pack_subscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($recommit_id);
			$em = $this->getDoctrine()->getManager();
			$pack_subscription->setRecommitRequired($status);
			$em->flush();
			$em->clear();

			return new JsonResponse(['message'=>'successfully changed the recommitment field!']);
		}

		/**
		 * @Route("/god-mode/subscription-history/{user}", name="subscription-history")
		 */
		private function subscriptionHistory($user){
			$pack_subscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')
			                          ->getSubscriptionHistory($user);

			$results = array();
			$firstTimeSubscribers = array();

			foreach ($pack_subscriptions as $key=> $subscription){

				 $key_increment = $key+1;
				array_push($results,
					'<p>Subscription '.$key_increment. ' => Status: '.$subscription->getStatus().'| Date: '.
									date_format($subscription->getCreated(), 'Y-m-d H:i:s').'</p>'
				);
			}

			return new JsonResponse(['data' => ($results)]);
		}

		/**
		 * @Route("/god-mode/merge-user/{receiver}/{feederSubscription}/{pack}", name="god_mode_merge_user")
		 */
//		private function mergeUserAction($receiver, $feederSubscription, $pack, $flag = false) {
//
//			$receiver = $this->getDoctrine()->getRepository('AppBundle:User')->find($receiver);
//
//			$feederSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($feederSubscription);
//
//			$pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);
//
//			if($this->getDoctrine()->getRepository('AppBundle:User')->isAdmin($receiver) > 0) {
//
//				$isAdminReceiver = true;
//
//				// Admin Receiver
//				$receiverSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy(
//					array(
//						'pack' => $pack,
//						'user' => $receiver->getId(),
//						'userType' => 'admin'
//					)
//				);
//			} else {
//
//				$isUserReceiver = true;
//
//				// User Receiver
//				$receiverSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserNextReceiver($pack, $receiver);
//
//			}
//
////			$now = new \DateTime('now');
////			$endObject = new \DateTime('now');
////			$end = $endObject->add(new \DateInterval('PT24H'));
//
//			$em = $this->getDoctrine()->getManager();
//
//			if(isset($isUserReceiver) &&  $receiverSubscription->getRecommitRequired() == true) {
//
//				$receiverSubscription->setForcedWaiting( true );
//				$receiverSubscription->setStatus( 'waiting' );
//
////				$em = $this->getDoctrine()->getManager();
////				$em->persist( $receiverSubscription );
//				$em->flush();
//				$em->clear();
//
//
//				$this->addFlash(
//					'successNotice',
//					$receiver->getUsername() . " has been put back on waiting to pay recommitment fee."
//				);
//
////				return $this->redirectToRoute( 'god_mode_merge_users', array( 'pack' => $pack->getId() ) );
//			}
//
//			elseif(isset($isUserReceiver) &&  $receiverSubscription->getRecommitRequired() == false){
//
//                // Create Feeder
//				$this->setTimeOnMergeAndCreateFeeders($feederSubscription,$receiver,$pack,$receiverSubscription);
//
//				if ( $feederSubscription->getStatus() == 'waiting' ) {
//					$feederSubscription->setStatus( 'inactive' );
//				}
//
////				if(isset($isAdminReceiver)) {
////					// Deduct counter
////					$receiverSubscription->setFeederCounter(0);
////				}
//
//				elseif(isset($isUserReceiver)) {
//					// Deduct counter
//					$counter = $receiverSubscription->getFeederCounter() - 1;
//					$receiverSubscription->setFeederCounter($counter);
//
//					$name = strtoupper($feederSubscription->getUser()->getName());
//					$receiverName = $receiver->getName();
//
//					$texts = "";
//					$texts.= $name."(".$feederSubscription->getUser()->getUsername().")";
//					$texts.= ' HAS BEEN MERGED TO MAKE PAYMENT TO '.$receiverName."(".$receiver->getUsername().")";
//
////				$message = $feederSubscription->getUser()->getName()."(".$feederSubscription->getUser()->getUsername().")".' has been merged to make payment to '.$receiver->getName()."(".$receiver->getUsername().")";
//
//
////				$telegram = new Telegram(
////					($texts)
////				);
////				$telegram->send();
//				}
//
//
////				return $this->redirectToRoute( 'god_mode_merge_users', array( 'pack' => $pack->getId() ) );
//
//			}
//
//			elseif(isset($isAdminReceiver)) {
//
//				$this->setTimeOnMergeAndCreateFeeders($feederSubscription,$receiver,$pack,$receiverSubscription);
//
//
//				if ( $feederSubscription->getStatus() == 'waiting' ) {
//					$feederSubscription->setStatus( 'inactive' );
//				}
//
//				$receiverSubscription->setFeederCounter( 0 );
//
//
//				$em->flush();
////
//				if($flag == false) {
//
//					$this->addFlash(
//						'successNotice',
//						$feederSubscription->getUser()->getUsername().' has been merged to pay '.$receiver->getUsername()
//					);
//				}
//
//				$em->clear();
//
//				if($flag == false) {
//					return $this->redirectToRoute('god_mode_merge_users', array('pack' => $pack->getId()));
//				}
//
//			}
//
//
////			$em->flush();
////
////			if($flag == false) {
////
////				$this->addFlash(
////					'successNotice',
////					$feederSubscription->getUser()->getUsername().' has been merged to pay '.$receiver->getUsername()
////				);
////			}
////
////			$em->clear();
////
////			if($flag == false) {
////				return $this->redirectToRoute('god_mode_merge_users', array('pack' => $pack->getId()));
////			}
//			return $this->redirectToRoute( 'god_mode_merge_users', array( 'pack' => $pack->getId() ) );
//
//		}


		private function mergeUserAction($receiver, $feederSubscription, $pack, $flag = false) {


				$feeder_sub_id = $feederSubscription;
				$receiver = $this->getDoctrine()->getRepository('AppBundle:User')->find($receiver);

				$feederSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($feederSubscription);

				$pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

				if($this->getDoctrine()->getRepository('AppBundle:User')->isAdmin($receiver) > 0) {

					$isAdminReceiver = true;

					// Admin Receiver
					$receiverSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy(
						array(
							'pack' => $pack,
							'user' => $receiver->getId(),
							'userType' => 'admin'
						)
					);
				} else {

					$isUserReceiver = true;

					// User Receiver
					$receiverSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserNextReceiver($pack, $receiver);
				}

//				$feeder_subscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy(
//					array(
//						'pack' => $pack,
//						'user' => $receiver->getId(),
//						'userType' => 'admin'
//					);

				if(isset($isUserReceiver) && $receiverSubscription->getRecommitRequired() == true){
					$receiverSubscription->setForcedWaiting( true );
					$receiverSubscription->setStatus( 'waiting' );

					$em = $this->getDoctrine()->getManager();
//			    	$em->persist( $receiverSubscription );
					$em->flush();
					$em->clear();

					$this->addFlash(
						'successNotice',
						$receiver->getUsername() . " has been put back on waiting to pay recommitment fee."
					);
				}

				if(isset($isUserReceiver) &&  $receiverSubscription->getRecommitRequired() == false){

					$this->setTimeOnMergeAndCreateFeeders($feederSubscription,$receiver,$pack,$receiverSubscription);

					if ( $feederSubscription->getStatus() == 'waiting' ) {
						$feederSubscription->setStatus( 'inactive' );
					}

					$counter = $receiverSubscription->getFeederCounter() - 1;
					$receiverSubscription->setFeederCounter($counter);

					$name = strtoupper($feederSubscription->getUser()->getName());

					$receiverName = $receiver->getName();

					$texts = "";
					$texts.= $name."[".$feederSubscription->getUser()->getUsername()."]";
//					$texts.= ' HAS BEEN MERGED TO MAKE PAYMENT TO '.$receiverName."(".$receiver->getUsername().")";
					$texts.= ' has been merged to make payment to '.$receiverName."(".$receiver->getUsername().")";

					$message = $feederSubscription->getUser()->getName()."(".$feederSubscription->getUser()->getUsername().")".' has been merged to make payment to '.$receiver->getName()."(".$receiver->getUsername().")";

					$telegram = new Telegram();
					$telegram->send('text',$receiver->getUsername(),$feederSubscription->getUser()->getUsername(),'false');

					if($flag == false) {

						$this->addFlash(
							'successNotice',
							$feederSubscription->getUser()->getUsername().' has been merged to pay '.$receiver->getUsername()
						);
					}
				}

//				if($feederSubscription->getForcedWaiting() == true){
//					$telegram = new Telegram();
//					$telegram->send('text',$receiver->getUsername(),$feederSubscription->getUser()->getUsername(),'true');
//				}

//				if($feederSubscription->getStatus() == 'waiting'){
//
//	//					$feederDelete = $this->getDoctrine()->getRepository('AppBundle:Feeder')->deleteFeeder($feederSubscription,$receiver);
//
//					$this->setTimeOnMergeAndCreateFeeders($feederSubscription,$receiver,$pack,$receiverSubscription);
//
//					if ( $feederSubscription->getStatus() == 'waiting' ) {
//						$feederSubscription->setStatus( 'inactive' );
//					}
//
//					$counter = $receiverSubscription->getFeederCounter() - 1;
//					$receiverSubscription->setFeederCounter($counter);
//
//					if($flag == false) {
//
//						$this->addFlash(
//							'successNotice',
//							$feederSubscription->getUser()->getUsername().' has been merged to pay his recommitment to '.$receiver->getUsername()
//						);
//					}
//				}


				if(isset($isAdminReceiver)) {
	//				$this->getDoctrine()->getRepository('AppBundle:Feeder')->deleteFeeder($feederSubscription,$receiver);


					$this->setTimeOnMergeAndCreateFeeders($feederSubscription,$receiver,$pack,$receiverSubscription);


					if ( $feederSubscription->getStatus() == 'waiting' ) {
						$feederSubscription->setStatus( 'inactive' );
					}

					$receiverSubscription->setFeederCounter( 0 );

					if($flag == false) {

						$this->addFlash(
							'successNotice',
							$feederSubscription->getUser()->getUsername().' has been merged to pay '.$receiver->getUsername()
						);
					}
				}

				$em = $this->getDoctrine()->getManager();
				$em->flush();

				$em->clear();

				if($flag == false) {
					return $this->redirectToRoute('god_mode_merge_users', array('pack' => $pack->getId()));
				}
//			}

		}


		/**
		 * @Route("/god-mode/auto-merge-users/{pack}", name="god_mode_auto_merge_users")
		 */
		private function autoMergeUsersAction($pack) {

			// Get Waiting List
			$waitingList = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getWaitingList($pack);

			foreach($waitingList as $feederSubscription) {

				$receiverSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getNextActiveUser($pack);

                    if($receiverSubscription != null){
                        $this->mergeUserAction($receiverSubscription->getUser()->getId(), $feederSubscription->getId(), $pack, true);
                    }
			}

			$this->addFlash(
				'successNotice',
				'All Users Merged'
			);

			return $this->redirectToRoute('god_mode_merge_users', array('pack' => $pack));
		}

		/**
		 * @Route("/god-mode/change-merging-mode/{mode}/{pack}", name="god_mode_change_merging_mode")
		 */
		private function changeMergingModeAction($mode, $pack) {

			if($mode != 'automatic' && $mode != 'manual') {
                $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
                $notifications = $this->getDoctrine()->getRepository('AppBundle:Notification')->findBy(
                    array('subject' => $this->getUser()->getId()),
                    array('created' => 'DESC'),
                    10
                );
                $formatter = $this->get('app.timeline_format');
				return $this->render('member/page-not-found.html.twig', [
                    'timeline' => $formatter->format($notifications),
                    'adm' => $adminsettings]);

			} else {

				$admin = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

				$admin->setMatchMode($mode);

				$em = $this->getDoctrine()->getManager();

				$em->flush();

				$em->clear();

				$this->addFlash(
					'successNotice',
					'Merging Mode Changed to '.$mode
				);

				if($mode == 'automatic') {

					return $this->redirectToRoute('god_mode_auto_merge_users', array('pack' => $pack));

				} else {

					return $this->redirectToRoute('god_mode_merge_users', array('pack' => $pack));
				}

			}
		}

		public function expiredNotPaidUsers(){
//			$unpaidFeedersSql = "SELECT * FROM feeder WHERE status = 'not_paid' AND feeder_time_end < NOW()";

			$not_paid_users = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getNotPaidUsers();

			return $this->render(
				'god-mode/recommitments.html.twig',
				[
					'NotPaidUsers' => $not_paid_users,
				]
			);

		}

		/**
		 * @Route("/god-mode/run-cron", name="run_cron")
		 */
		public function runCron(){
			try{
				$cron = New UnpaidUsers();
				$cron->blockUnpaidUsers();
				$cron->moveCasesToJury();

				$this->addFlash(
					'successNotice',
					'Unpaid Users Successfully blocked!'
				);
			}catch (\Exception $e){
				$this->addFlash(
					'successNotice',
					'Error processing your request! Error:'.$e->getMessage()
				);
			}

			return $this->redirectToRoute('god_mode_dashboard');
		}

		/**
		 * Load stuff before calling any of the controller's methods
		 */
		public function __call($method, $args) {


			$securityToken = $this->get('security.token_storage')->getToken();

			$userCheck = new UserCheck;
			
			$user = $userCheck->verify($securityToken->getUser());

			$user->setReferralsCount($this->getDoctrine()->getRepository('AppBundle:User')->getReferralsCount($user));

			$user->setPacks($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserCurrentSubscriptionsCount($user));

			$user->setSupportCount($this->getDoctrine()->getRepository('AppBundle:Support')->getActiveSupportCount());

			$user->setJuryCount($this->getDoctrine()->getRepository('AppBundle:Jury')->getActiveJuryCount());

			// Get Mail Messages
			$user->setMailCount($this->getDoctrine()->getRepository('AppBundle:MailRecipient')->getUnreadCount($this->getUser()));
            $user->setNotificationCount($this->getDoctrine()->getRepository('AppBundle:Notification')->getAllUnreadNotificationCount($this->getUser()));

			$user->setMenuMail($this->getDoctrine()->getRepository('AppBundle:MailRecipient')->getMenuMail($this->getUser()));

         	$securityToken->setUser($user);

        	return call_user_func_array(array($this, $method), $args);
		}

		private function setTimeOnMergeAndCreateFeeders($feederSubscription, $receiver,$pack,$receiverSubscription){

			$now = new \DateTime('now');
			$endObject = new \DateTime('now');
			$end = $endObject->add(new \DateInterval('PT24H'));
			$em = $this->getDoctrine()->getManager();
			$feeder = new Feeder;
			$feeder->setFeeder( $feederSubscription->getUser() );
			$feeder->setReceiver( $receiver );
			$feeder->setPack( $pack );
			$feeder->setFeederPackSubscription( $feederSubscription );
			$feeder->setReceiverPackSubscription( $receiverSubscription );
			$feeder->setFeederTimeStart( $now );
			$feeder->setFeederTimeEnd( $end );
			$feeder->setStatus( 'not_paid' );
			$feeder->setCreated( $now );

			// Save feeder
			$em->persist( $feeder );
			$em->flush();
		}

		private function copy(){
			$receiver = $this->getDoctrine()->getRepository('AppBundle:User')->find($receiver);

			$feederSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->find($feederSubscription);

			$pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			if($this->getDoctrine()->getRepository('AppBundle:User')->isAdmin($receiver) > 0) {

				$isAdminReceiver = true;

				// Admin Receiver
				$receiverSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->findOneBy(
					array(
						'pack' => $pack,
						'user' => $receiver->getId(),
						'userType' => 'admin'
					)
				);
			} else {

				$isUserReceiver = true;

				// User Receiver
				$receiverSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserNextReceiver($pack, $receiver);

			}

//			$now = new \DateTime('now');
//			$endObject = new \DateTime('now');
//			$end = $endObject->add(new \DateInterval('PT24H'));

			$em = $this->getDoctrine()->getManager();

			if(isset($isUserReceiver) &&  $receiverSubscription->getRecommitRequired() == true) {

				$receiverSubscription->setForcedWaiting( true );
				$receiverSubscription->setStatus( 'waiting' );

//				$em = $this->getDoctrine()->getManager();
//				$em->persist( $receiverSubscription );
				$em->flush();
				$em->clear();


				$this->addFlash(
					'successNotice',
					$receiver->getUsername() . " has been put back on waiting to pay recommitment fee."
				);

//				return $this->redirectToRoute( 'god_mode_merge_users', array( 'pack' => $pack->getId() ) );
			}

			elseif(isset($isUserReceiver) &&  $receiverSubscription->getRecommitRequired() == false){

                // Create Feeder
				$this->setTimeOnMergeAndCreateFeeders($feederSubscription,$receiver,$pack,$receiverSubscription);

				if ( $feederSubscription->getStatus() == 'waiting' ) {
					$feederSubscription->setStatus( 'inactive' );
				}

//				if(isset($isAdminReceiver)) {
//					// Deduct counter
//					$receiverSubscription->setFeederCounter(0);
//				}

				elseif(isset($isUserReceiver)) {
					// Deduct counter
					$counter = $receiverSubscription->getFeederCounter() - 1;
					$receiverSubscription->setFeederCounter($counter);

					$name = strtoupper($feederSubscription->getUser()->getName());
					$receiverName = $receiver->getName();

					$texts = "";
					$texts.= $name."(".$feederSubscription->getUser()->getUsername().")";
					$texts.= ' HAS BEEN MERGED TO MAKE PAYMENT TO '.$receiverName."(".$receiver->getUsername().")";

//				$message = $feederSubscription->getUser()->getName()."(".$feederSubscription->getUser()->getUsername().")".' has been merged to make payment to '.$receiver->getName()."(".$receiver->getUsername().")";


//				$telegram = new Telegram(
//					($texts)
//				);
//				$telegram->send();
				}


//				return $this->redirectToRoute( 'god_mode_merge_users', array( 'pack' => $pack->getId() ) );

			}

			elseif(isset($isAdminReceiver)) {

				$this->setTimeOnMergeAndCreateFeeders($feederSubscription,$receiver,$pack,$receiverSubscription);


				if ( $feederSubscription->getStatus() == 'waiting' ) {
					$feederSubscription->setStatus( 'inactive' );
				}

				$receiverSubscription->setFeederCounter( 0 );


				$em->flush();
//
//				if($flag == false) {
//
//					$this->addFlash(
//						'successNotice',
//						$feederSubscription->getUser()->getUsername().' has been merged to pay '.$receiver->getUsername()
//					);
//				}

				$em->clear();

				if($flag == false) {
					return $this->redirectToRoute('god_mode_merge_users', array('pack' => $pack->getId()));
				}

			}


//			$em->flush();
//
//			if($flag == false) {
//
//				$this->addFlash(
//					'successNotice',
//					$feederSubscription->getUser()->getUsername().' has been merged to pay '.$receiver->getUsername()
//				);
//			}
//
//			$em->clear();
//
//			if($flag == false) {
//				return $this->redirectToRoute('god_mode_merge_users', array('pack' => $pack->getId()));
//			}
			return $this->redirectToRoute( 'god_mode_merge_users', array( 'pack' => $pack->getId() ) );
		}
	}


	class Db {
		// Neutralizes database resource
		public $conn = FALSE;
		// Connects to the database
		// Stores connection resource in a variable
		// Throws exception if error occurs
		public function __construct() {

			$now = new \DateTime();
			$mins = $now->getOffset() / 60;
			$sgn = ($mins < 0 ? -1 : 1);
			$mins = abs($mins);
			$hrs = floor($mins / 60);
			$mins -= $hrs * 60;
			$offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);
//			mysql://b7239037aadc34:aaf364cf@eu-cdbr-west-02.cleardb.net/heroku_59cb6f21957cd43?reconnect=true

//			$result = new \PDO("mysql:host=eu-cdbr-west-02.cleardb.net;dbname=heroku_59cb6f21957cd43", 'b7239037aadc34', 'aaf364cf');
			if(!$result) {
				throw new \Exception('Could not connect to the database server');
			} else {
				$this->conn = $result;
				$this->conn->exec("SET time_zone='$offset';");
			}
		}
		// Method For Sanitizing SQL
		public function sanitize($var) {
			return $this->conn->quote($var);
		}
	}

	class UnpaidUsers extends Db {


		public function blockUnpaidUsers() {

			if($this->conn) {

				// Select all Unpaid Feeders
				$unpaidFeedersSql = "SELECT * FROM feeder WHERE status = 'not_paid' AND feeder_time_end < NOW()";
				$unpaidFeedersStmt = $this->conn->prepare($unpaidFeedersSql);

				// Cancel Feeder Entry
				$cancelFeederEntrySql = "UPDATE feeder SET status = 'cancelled' WHERE id = ?";
				$cancelFeederEntryStmt = $this->conn->prepare($cancelFeederEntrySql);

				// Credit Receiver Pack Subscription's Feeder Counter
				$creditReceiverPackSubscriptionFeederCounterSql = "UPDATE pack_subscription SET feeder_counter = feeder_counter + 1 WHERE id = ?";
				$creditReceiverPackSubscriptionFeederCounterStmt = $this->conn->prepare($creditReceiverPackSubscriptionFeederCounterSql);

				// Get Receiver Pack Subscription
				$getReceiverPackSubscriptionSql = "SELECT * FROM pack_subscription WHERE id = ?";
				$getReceiverPackSubscriptionStmt = $this->conn->prepare($getReceiverPackSubscriptionSql);

				// Check Feeder Subscriptions
				$checkFeederSubscriptionsSql = "SELECT COUNT(*) AS count FROM pack_subscription WHERE user = ? AND status = 'active'";
				$checkFeederSubscriptionsStmt = $this->conn->prepare($checkFeederSubscriptionsSql);

				// Block Feeder
				$blockFeederSql = "UPDATE user SET status = 'blocked' WHERE id = ?";
				$blockFeederStmt = $this->conn->prepare($blockFeederSql);

				// Cancel Feeder PackSubscription
				$cancelFeederPackSubscriptionSql = "UPDATE pack_subscription SET status = 'cancelled' WHERE id = ?";
				$cancelFeederPackSubscriptionStmt = $this->conn->prepare($cancelFeederPackSubscriptionSql);

				// Execute Select all Unpaid Downlines
				$unpaidFeedersStmt->execute();

				// Start blocking Them
				while($row = $unpaidFeedersStmt->fetch(\PDO::FETCH_OBJ)) {

					// Cancel Feeder Entry
					$cancelFeederEntryStmt->execute(array($row->id));

					// Get Receiver Pack Subscription
					$getReceiverPackSubscriptionStmt->execute(array($row->receiver_pack_subscription));
					$receiverPackSubscription = $getReceiverPackSubscriptionStmt->fetch(\PDO::FETCH_OBJ);

					if($receiverPackSubscription->user_type == 'user') {
						// Credit Receiver's Pack Subscription's Feeder Counter
						$creditReceiverPackSubscriptionFeederCounterStmt->execute(array($row->receiver_pack_subscription));
					}

					// Cancel Feeder PackSubscription
					$cancelFeederPackSubscriptionStmt->execute(array($row->feeder_pack_subscription));

					// Check if feeder has other active subscriptions
					$checkFeederSubscriptionsStmt->execute(array($row->feeder));
					$feederActiveSubscriptions = $checkFeederSubscriptionsStmt->fetch(\PDO::FETCH_OBJ)->count;

					if($feederActiveSubscriptions == 0) {
						// Block Feeder
						$blockFeederStmt->execute(array($row->feeder));
					}
				}

			}

		}

		public function moveCasesToJury() {

			if($this->conn) {

				// Select all potential jury cases
				$juryCasesSql = "SELECT * FROM feeder WHERE (status = 'payment_made' AND receiver_time_end < NOW()) OR (status = 'scam' AND feeder_time_end < NOW() AND scam_paid = 'scam_paid') OR (status = 'scam' AND receiver_time_end < NOW() AND scam_paid = 'scam_paid')";
				$juryCasesStmt = $this->conn->prepare($juryCasesSql);

				// Add new jury case entry
				$newJurySql = "INSERT INTO jury VALUES('', ?, ?, ?, ?, ?, 'none', 'open', NOW())";
				$newJuryStmt = $this->conn->prepare($newJurySql);

				// Move to jury
				$moveToJurySql = "UPDATE feeder SET status = 'jury', jury = ? WHERE id = ?";
				$moveToJuryStmt = $this->conn->prepare($moveToJurySql);

				// Start moving to court
				$juryCasesStmt->execute();


				while($row = $juryCasesStmt->fetch(\PDO::FETCH_OBJ)) {

					// Add court Entry
					$newJuryStmt->execute(array($row->feeder, $row->receiver, $row->id, $row->receiver_pack_subscription, $row->pack));

					// Move case to jury
					$moveToJuryStmt->execute(array($this->conn->lastInsertId(),$row->id));
				}
			}

		}

	}


