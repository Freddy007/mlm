<?php
	
	namespace AppBundle\Controller;

    use AppBundle\Entity\Jury;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;

    use Symfony\Component\HttpFoundation\Request;

	use Symfony\Component\Security\Core\Authentication\Token\UserCheck;


	class CronController extends Controller {

        /**
         * @Route("/god-mode/move-jury-cases", name="move_jury_cases")
         */
        private function moveToJuryAction() {
            $cases = $this->getDoctrine()->getRepository('AppBundle:Jury')->getAllJuryCases();

            foreach ($cases as $case) {


                $newJuryCase = new Jury();
                $em = $this->getDoctrine()->getManager();
                $now = new \DateTime('now');

                $newJuryCase->setFeederId($case->getFeeder()->getId());
                $newJuryCase->setReceiverId($case->getReceiver()->getId());
                $newJuryCase->setFeeder($case->getFeeder());
                $newJuryCase->setReceiver($case->getReceiver());
                $newJuryCase->setCreatedDate($now);
                $newJuryCase->setStatus('open');
                $newJuryCase->setVerdict('none');
                $newJuryCase->setFeederEntryId($case->getId());
                $newJuryCase->setPackId($case->getPack()->getId());
                $newJuryCase->setPackSubscriptionId($case->getReceiverPackSubscription()->getId());

                $em->persist($newJuryCase);
                $em->flush();


                $this->getDoctrine()->getRepository('AppBundle:Feeder')->markAsJury($case);
                $this->addFlash(
                    'successNotice',
                    "Cases Successfully moved to Court"
                );

                $em->clear();



            }
            return $this->redirectToRoute('god_mode_dashboard');
        }


        /**
         * @Route("/god-mode/block-scam-users", name="block_scam_users")
         */
        private function blockScamUsersAction() {
            $unpaidfeeders = $this->getDoctrine()->getRepository('AppBundle:Feeder')->getUnpaidFeeders();

            foreach ($unpaidfeeders as $unpaidfeeder) {
                $this->getDoctrine()->getRepository('AppBundle:Feeder')->cancelFeederEntry($unpaidfeeder);

                $receiverPackSubscription = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getReceiverPackSubscription($unpaidfeeder);
                $feederPackSubScription = $unpaidfeeder->getFeederPackSubscription();

                if ($receiverPackSubscription->getUserType() == "user"){
                    $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->creditReceiverPackSubscriptionFeederCounter($unpaidfeeder);
                }

                $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->cancelSubscription($feederPackSubScription);
                $feederActiveSubscriptions = $this->getDoctrine()->getRepository('AppBundle:PackSubscription')->checkFeederSubscriptions($unpaidfeeder);

                if($feederActiveSubscriptions == 0) {
                    $this->getDoctrine()->getRepository('AppBundle:User')->blockUser($unpaidfeeder);
                }

                $this->addFlash(
                    'successNotice',
                    "Cases Successfully moved to Court"
                );

            }
            return $this->redirectToRoute('god_mode_dashboard');
        }

		/**
		 * Load stuff before calling any of the controller's methods
		 */
		public function __call($method, $args) {

			$securityToken = $this->get('security.token_storage')->getToken();

			$userCheck = new UserCheck;
			
			$user = $userCheck->verify($securityToken->getUser());

			$user->setReferralsCount($this->getDoctrine()->getRepository('AppBundle:User')->getReferralsCount($user));

			$user->setPacks($this->getDoctrine()->getRepository('AppBundle:PackSubscription')->getUserCurrentSubscriptionsCount($user));

			$user->setSupportCount($this->getDoctrine()->getRepository('AppBundle:Support')->getActiveSupportCount());

			$user->setJuryCount($this->getDoctrine()->getRepository('AppBundle:Jury')->getActiveJuryCount());

			// Get Mail Messages
			$user->setMailCount($this->getDoctrine()->getRepository('AppBundle:MailRecipient')->getUnreadCount($this->getUser()));

			$user->setMenuMail($this->getDoctrine()->getRepository('AppBundle:MailRecipient')->getMenuMail($this->getUser()));

         	$securityToken->setUser($user);

        	return call_user_func_array(array($this, $method), $args);
		}

	}