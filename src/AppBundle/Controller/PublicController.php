<?php
	
	namespace AppBundle\Controller;

	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use AppBundle\Controller\SecurityController;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\Translation\Translator;

	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\Loader\YamlFileLoader;
    use Symfony\Component\Security\Core\Authentication\Token\UserCheck;

    use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Email;
	use Symfony\Component\Validator\Constraints\Length;

	class PublicController extends Controller {

		/**
		 * @Route("/", name="home")
		 */
		public function homeAction(Request $request) {

			return $this->redirectToRoute('login');

            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

//            $securityCheck = new UserCheck();
//            $flag = $securityCheck->contain($adminsettings);
//
//            if($flag) {
//                $this->getDoctrine()->getRepository('AppBundle:Admin')->verified($adminsettings->getId());
//            }else{
//                $this->getDoctrine()->getRepository('AppBundle:Admin')->unverified($adminsettings->getId());
//            }

			// Community
			$news = $this->getDoctrine()->getRepository('AppBundle:News')->getHomeNews();

			// Reviews
			$reviews = $this->getDoctrine()->getRepository('AppBundle:Review')->getHomeReviews();

			// $packs = $this->getDoctrine()->getRepository('AppBundle:Pack')->findAll();
			
			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT p FROM AppBundle:Pack p ORDER BY p.amount ASC";

			
			$packs = $em->createQuery($dql)->getResult();

            $form = $this->createFormBuilder()
                ->add('name', TextType::class, array(
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Enter Your Name'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please Enter Your Name'
                            )
                        )
                    ),
                    'required' => false
                ))
                ->add('email', EmailType::class, array(
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Enter Your Email'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter an email address'
                            )
                        ),
                        new Email(
                            array(
                                'message' => 'Please enter a valid email address'
                            )
                        ),
                        new Length(
                            array(
                                'max' => 255,
                                'maxMessage' => 'Email cannot contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => false
                ))
                ->add('message', TextareaType::class, array(
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Enter Your Message',
                        'rows' => '5'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter a message'
                            )
                        )
                    ),
                    'required' => false
                ))
                ->getForm();


            $this->get('translator')->trans('Trusted and Genuine Returns for Everyone');
            $form->handleRequest($request);


            if($form->isSubmitted() && $form->isValid()) {

                $data = $form->getData();

                // Send email here
                $message = \Swift_Message::newInstance()
                    ->setSubject("{$adminsettings->getSitename()} User Support")
                    ->setFrom($this->getUser()->getEmail())
                    ->setTo($adminsettings->getSitemail())
                    ->setReplyTo($data['email'])
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/emails/support-mail.html.twig
                            'emails/support-mail.html.twig',
                            array('name' => $data['name'], 'email' => $data['email'], 'message' => $data['message'])
                        ),
                        'text/html'
                    );
                if($this->get('mailer')->send($message)) {
                    $this->addFlash(
                        'contactSuccess',
                        'Mail Sent! We will get back to you as soon as possible'
                    );
                } else {
                    $this->addFlash(
                        'contactError',
                        "Error sending email. Please try again"
                    );
                }
            }
			
			return $this->render('public/home.html.twig', [
				'reviews' => $reviews,
                'adm' => $adminsettings,
                'news' => $news,
                'form' => $form->createView(),
				'packs' => $packs
			]);
		}

		/**
		 * @Route("/ref/{username}", name="referral_link")
		 */
		public function referralLinkAction(Request $request, $username) {

			$session = $request->getSession();

			$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
				array(
					'username' => $username
				)
			);

			if($user == null) {

				// Check Session if Referrer Exists
				if($session->has('user_referrer')) {
					$session->remove('user_referrer');
				}

				$this->addFlash(
					'userReferrerError',
					"No user with the username '$username', was not found. If you proceed without correcting the username, the new user will be placed under nobody"
				);
			} else {

				$session->set('user_referrer', $user->getId());

				$this->addFlash(
					'userReferrer',
					"Referrer: {$user->getName()}"
				);
			}

			return $this->redirectToRoute('register_pack');
		}

		/**
		 * @Route("/register-pack", name="register_pack")
		 */
		public function registerPackAction(Request $request) {

			$adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			
			$em = $this->getDoctrine()->getManager();

			$dql = "SELECT p FROM AppBundle:Pack p ORDER BY p.amount ASC";

			$packs = $em->createQuery($dql)->getResult();

			// $packs = $this->getDoctrine()->getRepository('AppBundle:Pack')->findAll();

			$user = new User;
			$form = $this->createForm(UserType::class, $user);
			$form->handleRequest($request);
			// $session = $request->getSession();
			
			return $this->render('public/register-packs-2.html.twig', [
                'adm' => $adminsettings,
				'packs' => $packs,
				'i' => 0,
				'form' => $form->createView()
			]);
		}

		/**
		 * @Route("/contact", name="contact")
		 */
		public function contactAction(Request $request) {
            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

			$form = $this->createFormBuilder()
				->add('name', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Enter Your Name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please Enter Your Name'
							)
						)
					),
					'required' => false
				))
				->add('email', EmailType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Enter Your Email'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter an email address'
							)
						),
						new Email(
							array(
								'message' => 'Please enter a valid email address'
							)
						),
						new Length(
							array(
								'max' => 255,
								'maxMessage' => 'Email cannot contain more than 255 characters'
							)
						)
					),
					'required' => false
				))
				->add('message', TextareaType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Enter Your Message',
						'rows' => '5'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter a message'
							)
						)
					),
					'required' => false
				))
				->getForm();

			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				$data = $form->getData();
				
				// Send email here
					$message = \Swift_Message::newInstance()
    					->setSubject("{$adminsettings->getSitename()} User Support")
    					->setFrom($this->getParameter('app_sender_mail'))
    					->setTo($adminsettings->getSitemail())
    					->setReplyTo($data['email'])
				        ->setBody(
				            $this->renderView(
				                // app/Resources/views/emails/support-mail.html.twig
				                'emails/support-mail.html.twig',
				                array('name' => $data['name'], 'email' => $data['email'], 'message' => $data['message'])
				            ),
				            'text/html'
				        );
				    if($this->get('mailer')->send($message)) {
				    	$this->addFlash(
				    		'contactSuccess',
				    		'Mail Sent! We will get back to you as soon as possible'
				    	);
				    } else {
						$this->addFlash(
							'contactError',
							"Error sending email. Please try again"
						);
				    }
			}

			return $this->render('public/contact.html.twig', [
                'adm' => $adminsettings,
				'form' => $form->createView()
			]);
		}

		/**
		 * @Route("/forgot-password", name="forgot_password")
		 */
		public function forgotPasswordAction(Request $request) {

            $adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			$form = $this->createFormBuilder()
				->add('email', EmailType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Email'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter an email address'
							)
						),
						new Email(
							array(
								'message' => 'Please enter a valid email address'
							)
						),
						new Length(
							array(
								'max' => 255,
								'maxMessage' => 'Email cannot contain more than 255 characters'
							)
						)
					),
					'required' => false
				))
				->getForm();

			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()) {

				$data = $form->getData();

				// Check if email exists
				$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
					array(
						'email' => $data['email']
					)
				);

				if($user == null) {

					$this->addFlash(
						'forgotError',
						"The user with the email: {$data['email']} was not found"
					);

				} else {

					// Reset Password
						// Get encoder
						$encoder = $this->get("security.password_encoder");
				
						$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
						$newPassword = '';
		 				$max = strlen($characters) - 1;
		 				for ($i = 0; $i < 6; $i++) {
		      					$newPassword .= $characters[mt_rand(0, $max)];
						}

						$password = $encoder->encodePassword($user, $newPassword);
						$user->setPassword($password);

						// Save User
						$em = $this->getDoctrine()->getManager();
						$em->flush();

					// Send email here
    					$message = \Swift_Message::newInstance()
        					->setSubject("{$this->getParameter('app_name')} Password Reset")
        					->setFrom($this->getParameter('app_sender_mail'))
        					->setTo($user->getEmail())
					        ->setBody(
					            $this->renderView(
					                // app/Resources/views/emails/forgot-password-mail.html.twig
					                'emails/forgot-password-mail.html.twig',
					                array('password' => $newPassword)
					            ),
					            'text/html'
					        );
					    if($this->get('mailer')->send($message)) {
					    	$this->addFlash(
					    		'forgotSuccess',
					    		'Password successfully reset! Please check your email for your new password. Didnt see mail in your inbox? Check your spambox'
					    	);
					    } else {
							$this->addFlash(
								'forgotError',
								"Error sending reset email. Please try again"
							);
					    }
				}
			}

			return $this->render(
				'public/forgot-password.html.twig',
				[
                    'adm' => $adminsettings,
					'form' => $form->createView()
				]
			);
		}

        /**
         * @Route("/anti-spam", name="anti_spam")
         */
        public function AntiSpamAction() {


            return $this->render(
                'public/anti-spam.html.twig',
                [

                ]
            );
        }

        /**
         * @Route("/confidentiality-policy", name="confidentiality_policy")
         */
        public function ConfidentialityPolicyAction() {


            return $this->render(
                'public/confidentiality-policy.html.twig',
                [

                ]
            );
        }

        /**
         * @Route("/earn-daily", name="earn_daily")
         */
        public function EarnDailyAction() {


            return $this->render(
                'public/earn-daily.html.twig',
                [

                ]
            );
        }

        /**
         * @Route("/free-airtime", name="free_airtime")
         */
        public function FreeAirtimeAction() {


            return $this->render(
                'public/free-airtime.html.twig',
                [

                ]
            );
        }

        /**
         * @Route("/social-channels", name="social_channels")
         */
        public function SocialChannelsAction() {


            return $this->render(
                'public/social-channels.html.twig',
                [

                ]
            );
        }


	/**
		 * @Route("/send-payments", name="send-payments")
		 */
		public function sendPayment(Request $request) {
			$user_id = $request->get("user_id");
			$amount_paid = $request->get("amount_paid");
			$mobile_money_number = $request->get("mobile_money_number");
			$mobile_money_name = $request->get("mobile_money_name");

			$user = $this->getDoctrine()->getRepository('AppBundle:User')->find($user_id);

			$user->setPaymentMade(true);
			$user->setAmountPaid($amount_paid);
			$user->setMobileMoneyNumber($mobile_money_number);
			$user->setMobileMoneyName($mobile_money_name);
			$em = $this->getDoctrine()->getManager();
			$em->persist($user);
			$em->flush();

			return new JsonResponse(array('user_id' => $request->get('user_id')));
		}


		/**
		 * @Route("/send-wallet-payment", name="send-wallet-payment")
		 */
		public function sendWalletPayment(Request $request) {

			$entityManager = $this->getDoctrine()->getEntityManager();

			$wallet_holder_user_name = $request->get("statium_account_user_name");
			$package_cost = $request->get("pack_amount");

			$query = $entityManager->createQuery(
				'SELECT p
				FROM AppBundle:User p
				WHERE p.username = :username'
			)->setParameter('username', $wallet_holder_user_name)->setMaxResults(1);

			$wallet_user =  $query->getOneOrNullResult();

			if ($wallet_user == null ){

				$response = new Response(
					'Username does not exist!',
					Response::HTTP_FORBIDDEN,
					['content-type' => 'text/html']
				);

				return $response;

			}else if ($wallet_user->getPurse() < $package_cost ){
				$response = new Response(
					'Wallet amount insufficient!',
					Response::HTTP_UNAUTHORIZED,
					['content-type' => 'text/html']
				);

				return $response;
			}else {

				$user_id             = $request->get( "user_id" );
				$amount_paid         = $package_cost;
				$mobile_money_number = null;
				$mobile_money_name   = "N/A";

				$user = $this->getDoctrine()->getRepository( 'AppBundle:User' )->find( $user_id );

				$user->setPaymentMade( true );
				$user->setAmountPaid( $amount_paid );
				$user->setMobileMoneyNumber( $mobile_money_number );
				$user->setMobileMoneyName( $mobile_money_name );
				$em = $this->getDoctrine()->getManager();
				$em->persist( $user );
				$em->flush();

				return new JsonResponse( array( 'user_id' => $request->get( 'user_id' ) ) );
			}
		}

		/**
		 * @Route("/send-mails", name="send-mail")
		 */
		public function sendMail(Request $request){

			$message = \Swift_Message::newInstance()
			                         ->setSubject("User Support")
			                         ->setFrom("info@statiumhub.com")
			                         ->setTo("frederickankamah988@gmail.com")
			                         ->setReplyTo("info@statiumhub.com")
				                     ->setBody(
					$this->renderView(
					// app/Resources/views/emails/support-mail.html.twig
						'emails/support-mail.html.twig',
						array('name' => "Frederick", 'email' => 'frederickankamah988@gmail.com', 'message' => "Hello ")
					),
					'text/html'
				);
			if($this->get('mailer')->send($message)) {
				$this->addFlash(
					'contactSuccess',
					'Mail Sent! We will get back to you as soon as possible'
				);
				return new JsonResponse(array('success'));
			} else {
				$this->addFlash(
					'contactError',
					"Error sending email. Please try again"
				);
			}

			return new JsonResponse(array('user_id'));

		}

		/**
		 * @Route("/get-pack/{user}", name="get-pack")
		 */
	public function getPack($user){

			$entityManager = $this->getDoctrine()->getEntityManager();
		
			$query = $entityManager->createQuery(
				'SELECT p
				FROM AppBundle:PackSubscription p
				WHERE p.user = :id'
			)->setParameter('id', $user)
			->setMaxResults(1);
		
			// returns an array of Product objects
			$packSub =  $query->getOneOrNullResult();

		return new JsonResponse(array("name" => $packSub->getPack()->getName(), "amount" =>  $packSub->getPack()->getAmount(),
		"pv"     => $packSub->getPack()->getPV()
		));
		
		}

    }