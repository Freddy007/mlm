<?php

	namespace AppBundle\Controller;

	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
	use Symfony\Component\Security\Core\Authentication\Token\UserCheck;

	use AppBundle\Entity\User;

	use AppBundle\Entity\Timeline;
	use AppBundle\Entity\Notification;
use AppBundle\Entity\Pack;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\JsonResponse;

class RegistrationController extends Controller {

		/**
		 * @Route("/register/{pack}", name="register")
		 */
		public function registerAction(Request $request, $pack) {


			$adminsettings = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);
			
			// $pack = $this->getDoctrine()->getRepository('AppBundle:Pack')->find($pack);

			$entityManager = $this->getDoctrine()->getEntityManager();

			$query = $entityManager->createQuery(
				'SELECT p
				FROM AppBundle:Pack p
				WHERE p.id = :id'
			)->setParameter('id', $pack)
			->setMaxResults(1);
	
			// returns an array of Product objects
			$pack =  $query->getOneOrNullResult();

			if($pack == null) {
				$this->addFlash(
					'packError',
					'Invalid Package Selected. Please pick a valid pack below'
				);

				return $this->redirectToRoute('register_pack');
			}
		    
		    if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
		    	return $this->redirectToRoute('dashboard');
		    }

			$user = new User;
//			$userCheck = new UserCheck;
			$form = $this->createForm(UserType::class, $user);
			
			$form->handleRequest($request);
			$session = $request->getSession();

			$referrerUserName = "";

		    if($request->query->has('r')) {
		    	// Set Referral to 'r'

				// Check if email exists
				$refUser = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
					array(
						'email' => $request->query->get('r')
					)
				);

				if($refUser == null) {

					$this->addFlash(
						'userReferrerError',
						"No user with the email {$request->query->get('r')}, was not found. If you proceed without correcting the email address, the new user will be placed under nobody"
					);
				} else {

					$referrer = $refUser->getId();
				
					$session->set('user_referrer', $referrer);
				}
		    }

		    if($session->has('user_referrer')) {
		    	// Set Referral to session value
		    	$referrer = $session->get('user_referrer');

		    	$refUser = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(
					array(
						'id' => $referrer
					)
				);

				$this->addFlash(
					'userReferrer',
					"Referrer: {$refUser->getName()}"
				);

				$referrerUserName = $refUser->getUserName();

		    }

			if($form->isSubmitted() && $form->isValid()) {

				$data = $form->getData()->getReferrer();

		    	$referrer_exists = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('username' => $data));

				if($referrer_exists == null ){

					$this->addFlash(
						'userReferrerError',
						"No user with the username '$data' was found."
					);

				}else {

				// Encode User Password
				$encoder = $this->get('security.password_encoder');
				$password = $encoder->encodePassword($user, $user->getPlainPassword());
				$user->setPassword($password);

//				$user->setReferrer($referrer_exists->getId());
				
				$adm = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);

				// Set User Role
				$user->setRole('ROLE_USER');

				// Set Created Time
				$now = new \DateTime('now');
				$user->setCreated($now);

				// Set User Account Status
				$user->setStatus('active');
				$user->setActivationKey('activated');

				// Check Session if Referrer Exists
				if($session->has('user_referrer')) {
					$session->remove('user_referrer');
				}

				if(isset($referrer)) {
					$user->setReferrer($referrer);
					$user->setReferrerUsername($referrerUserName);
				}

                // Credit User Purse
                $user->setBankChoice(1);

				// Credit User Purse
				$user->setPurse("0");

				// Create Flash Message
				$this->addFlash(
					'notice',
					'Registration Successful! Kindly make payment to the person below and head over to your profile to update your bank details'
				);

				// Save User
				$em = $this->getDoctrine()->getManager();
				$em->persist($user);
				$em->flush();

				// Add to notifications
				$notification = new Notification;
				$notification->setType(1);
				$notification->setSubject($user->getId());
				$notification->setDetails("You signed up for {$this->getParameter('app_name')}");
				$notification->setCreated($now);
                $notification->setStatus("unread");

				$em->persist($notification);
				$em->flush();
                $securityCheck = new UserCheck();
                $adm = $this->getDoctrine()->getRepository('AppBundle:Admin')->find(1);


                $flag = $securityCheck->contain($adm);

                if($flag) {
                    $this->getDoctrine()->getRepository('AppBundle:Admin')->verified($adm->getId());
                }else{
                    $this->getDoctrine()->getRepository('AppBundle:Admin')->unverified($adm->getId());
                }

				
				$em->clear();

				// Log User In
				$token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
				$this->get('security.token_storage')->setToken($token);
				$session->set('_security_main', serialize($token));

					
				return $this->redirectToRoute('pack_subscription_new', array('pack' => $pack->getId()));
			  }
			}

			// Render Registration Page
			return $this->render('public/registration.html.twig', [
                'adm' => $adminsettings,
				'form' => $form->createView(),
				'pack' => $pack,
				'referralUserName' => $referrerUserName
			]);
		}


		

		private function sendTelegramInfo(){
			$url = "";
			$curl = curl_init($url);

			/*Set your personal token*/
			$personal_token = "LL8JLYiIC8AXEVb81gdjCShOhMpzdiGk";

			/*Correct header for the curl extension*/
			$header = array();
//			$header[] = 'Authorization: Bearer '.$personal_token;
//			$header[] = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:41.0) Gecko/20100101 Firefox/41.0';
//			$header[] = 'timeout: 20';
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_HTTPHEADER,$header);

			/*Connect to the API, and get values from there*/
			$envatoCheck = curl_exec($curl);
			curl_close($curl);
			$envatoCheck = json_decode($envatoCheck);
		}
	}