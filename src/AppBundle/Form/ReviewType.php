<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;

	class ReviewType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {

			$builder
				->add('testimony', TextareaType::class, array(
					'label' => 'Review',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Enter Review Here'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter review'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 150,
								'minMessage' => 'Review must contain 5 or more characters',
								'maxMessage' => 'Review cannot contain more than 150 characters'
							)
						)
					),
					'required' => false
				));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\Review'
			]);
		}
	}