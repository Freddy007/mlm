<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;

	class PackType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			
			$builder
				->add('name', TextType::class, array(
					'label' => 'Name',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter pack name'
							)
						),
						new Length(
							array(
								'min' => 3,
								'max' => 40,
								'minMessage' => 'Pack name must contain 3 or more characters',
								'maxMessage' => 'Pack name cannot contain more than 40 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => false,
								'message' => 'Pack name can only contain alphabets'
							)
						)
					),
					'required' => true
				))
				->add('amount', TextType::class, array(
					'label' => 'Amount',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Amount'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter amount'
							)
						),
						new Length(
							array(
								'min' => 3,
								'max' => 40,
								'minMessage' => 'Amount must contain 3 or more characters',
								'maxMessage' => 'Amount cannot contain more than 40 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => true,
								'message' => 'Amount can only contain digits'
							)
						)
					),
					'required' => true
				))
				->add('feeders', TextType::class, array(
					'label' => 'Feeders',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Feeders'
					),
					'constraints' => array(
						// new NotBlank(
						// 	array(
						// 		'message' => 'Please enter feeders'
						// 	)
						// ),
						new Length(
							array(
								'min' => 1,
								'max' => 2,
								'minMessage' => 'Feeders must contain 1 or more characters',
								'maxMessage' => 'Feeders cannot contain more than 2 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => true,
								'message' => 'Feeders can only contain digits'
							)
						)
					),
					'required' => false
				));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\Pack'
			]);
		}
	}