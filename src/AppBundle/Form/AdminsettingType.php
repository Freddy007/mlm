<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\Extension\Core\Type\UrlType;
    use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
	use Symfony\Component\Form\Extension\Core\Type\FileType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;
	use Symfony\Component\Validator\Constraints\Image;

	class AdminsettingType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
                ->add('envatoPurchasecode', TextType::class, array(
                    'label' => 'Envato Purchase Code',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter your purchase code'
                            )
                        ),
                        new Length(
                            array(
                                'min' => 2,
                                'max' => 255,
                                'minMessage' => 'Envato Purchase Code must contain 2 or more characters',
                                'maxMessage' => 'Envato Purchase Code must contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => true
                ))
                ->add('siteurl', UrlType::class, array(
                    'label' => 'Site URL',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter a Site URL'
                            )
                        ),
                        new Length(
                            array(
                                'min' => 2,
                                'max' => 255,
                                'minMessage' => 'Site URL must contain 2 or more characters',
                                'maxMessage' => 'Site URL contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => true
                ))
                ->add('sitemail', EmailType::class, array(
                    'label' => 'Site Email',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter the Support/Contact Site Email'
                            )
                        ),
                        new Length(
                            array(
                                'min' => 2,
                                'max' => 255,
                                'minMessage' => 'Site Email must contain 2 or more characters',
                                'maxMessage' => 'Site Email contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => true
                ))
                ->add('sitename', TextType::class, array(
                    'label' => 'Site Name',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter a Site Name'
                            )
                        ),
                        new Length(
                            array(
                                'min' => 2,
                                'max' => 255,
                                'minMessage' => 'Site Name must contain 2 or more characters',
                                'maxMessage' => 'Site Name contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => true
                ))
                ->add('appcurrency', TextType::class, array(
                    'label' => 'Site Currency Code',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter Currency Code'
                            )
                        ),
                        new Length(
                            array(
                                'min' => 3,
                                'max' => 3,
                                'minMessage' => 'Site Currency Code must contain 2 or more characters',
                                'maxMessage' => 'Site Currency Code contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => true
                ))
                ->add('appcurrencysymbol', TextType::class, array(
                    'label' => 'Site Currency Symbol',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter Currency Symbol'
                            )
                        ),
                        new Length(
                            array(
                                'min' => 1,
                                'max' => 255,
                                'minMessage' => 'Site Currency Symbol must contain 2 or more characters',
                                'maxMessage' => 'Site Currency Symbol contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => true
                ))
                ->add('appcurrencyexchangerate', TextType::class, array(
                    'label' => 'Exchange Rate',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter Site Currency-to-$ Exchange Rate'
                            )
                        ),
                        new Length(
                            array(
                                'min' => 1,
                                'max' => 255,
                                'minMessage' => 'Site Currency-to-$ Exchange Rate must contain 2 or more characters',
                                'maxMessage' => 'Site Currency-to-$ Exchange Rate contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => true
                ))
                ->add('matchMode', ChoiceType::class, array(
                    'label' => 'Site Matching Mode',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'choices' => array(
                        'Select Mode' => '',
                        'Automatic' => 'automatic',
                        'Manual' => 'manual'
                    ),
                    'constraints' => array(
                        new Length(
                            array(
                                'min' => 5,
                                'max' => 255,
                                'minMessage' => 'Bitcoin address must contain 5 or more characters',
                                'maxMessage' => 'Bitcoin address cannot contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => false
                ));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\Admin'
			]);
		}
	}