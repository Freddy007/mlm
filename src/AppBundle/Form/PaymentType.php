<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;

	class PaymentType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('bank_name_one', TextType::class, array(
					'label' => 'Bank Name',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Name of Bank'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please select enter name of bank'
							)
						)
					),
					'required' => true
				))
				->add('agency_one', TextType::class, array(
					'label' => 'Agency',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Agency'
					),
					'required' => false
				))
				->add('account_name_one', TextType::class, array(
					'label' => 'Account Name',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Account Name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter account name'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 255,
								'minMessage' => 'Account name must contain 5 or more characters',
								'maxMessage' => 'Account name cannot contain more than 255 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => false,
								'message' => 'Account name can only contain alphabets'
							)
						)
					),
					'required' => true
				))
				->add('account_number_one', TextType::class, array(
					'label' => 'Account Number',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Account Number'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter account number'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 255,
								'minMessage' => 'Account number must contain 5 or more characters',
								'maxMessage' => 'Account number cannot contain more than 255 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => true,
								'message' => 'Account number can only contain digits'
							)
						)
					),
					'required' => true
				))
				->add('operation_one', TextType::class, array(
					'label' => 'Operation',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Operation'
					),
					'required' => false
				))
				->add('cpf_one', TextType::class, array(
					'label' => 'CPF',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'CPF'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter CPF'
							)
						)
					),
					'required' => true
				))
				->add('account_type_one', ChoiceType::class, array(
					'label' => 'Account Type',
					'attr' => array(
						'class' => 'form-control'
					),
					'choices' => array(
						'Select Account Type' => '',
						'Savings' => 'Savings',
						'Current' => 'Current'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please select an account type'
							)
						)
					),
					'required' => true
				))/*
				->add('bank_name_two', TextType::class, array(
					'label' => 'Alt Bank Name - (optional)',
					'attr' => array(
						'class' => 'form-control',
                        'placeholder' => 'Provisional Bank Name'
					),
					'constraints' => array(
					),
					'required' => false
				))
				->add('agency_two', TextType::class, array(
					'label' => 'Alt Agency Name - (optional)',
					'attr' => array(
						'class' => 'form-control',
                        'placeholder' => 'Provisional Agency'
					),
					'constraints' => array(
					),
					'required' => false
				))
				->add('account_name_two', TextType::class, array(
					'label' => 'Alt Account Name - (optional)',
					'attr' => array(
						'class' => 'form-control',
                        'placeholder' => 'Provisional Account Name'
					),
					'constraints' => array(
					),
					'required' => false
				))
				->add('account_number_two', TextType::class, array(
					'label' => 'Alt Account Number - (optional)',
					'attr' => array(
						'class' => 'form-control',
                        'placeholder' => 'Provisional Account Number'
					),
					'constraints' => array(
					),
					'required' => false
				))
				->add('operation_two', TextType::class, array(
					'label' => 'Alt Operation - (optional)',
					'attr' => array(
						'class' => 'form-control',
                        'placeholder' => 'Provisional Operation'
					),
					'constraints' => array(
					),
					'required' => false
				))
				->add('cpf_two', TextType::class, array(
					'label' => 'Alt CPF - (optional)',
					'attr' => array(
						'class' => 'form-control',
                        'placeholder' => 'Provisional CPF'
					),
					'required' => false
				))
				->add('account_type_two', ChoiceType::class, array(
					'label' => 'Alt Account Type - (optional)',
					'attr' => array(
						'class' => 'form-control'
					),
                    'choices' => array(
                        'Select Account Type' => '',
                        'Savings' => 'Savings',
                        'Current' => 'Current'
                    ),
					'required' => false
				))*/;





		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\User'
			]);
		}
	}