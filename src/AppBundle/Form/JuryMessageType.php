<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\Form\Extension\Core\Type\FileType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Image;

	class JuryMessageType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {

			$builder
				->add('message', TextareaType::class, array(
					'label' => 'Statement',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Enter Statement Here'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter statement'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 1000,
								'minMessage' => 'Statement must contain 5 or more characters',
								'maxMessage' => 'Statement cannot contain more than 1000 characters'
							)
						)
					),
					'required' => false
				))
				->add('attachmentFile', FileType::class, array(
					'label' => 'Evidence (Optional)',
					'constraints' => array(
						new Image(
							array(
								'maxSize' => '2M',
								'maxSizeMessage' => 'The image is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}'
							)
						)
					),
					'required' => false
				));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\JuryMessage'
			]);
		}
	}