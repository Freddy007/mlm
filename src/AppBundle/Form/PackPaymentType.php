<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
	use Symfony\Component\Form\Extension\Core\Type\FileType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;
	use Symfony\Component\Validator\Constraints\Image;

	class PackPaymentType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('payment_method', ChoiceType::class, array(
					'label' => 'Payment Method',
					'attr' => array(
						'class' => 'form-control'
					),
					'choices' => array(
						'Select Payment Method' => '',
                        'I Paid the Standard Account' => 'I Paid the Standard Account',
						'I Paid the Provisional Account' => 'I Paid the Provisional Account'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please select a payment method'
							)
						),
						new Length(
							array(
								'min' => 2,
								'max' => 255,
								'minMessage' => 'Payment Method must contain 2 or more characters',
								'maxMessage' => 'Payment Method cannot contain more than 255 characters'
							)
						)
					),
					'required' => true
				))
				->add('paymentImage', FileType::class,array(
					'label' => 'Add Payment Image (Screenshot/Scanned Teller)',
                    'attr' => array(
                        'class' => 'hide_file'
                    ),
					'constraints' => array(
						new Image(
							array(
								'maxSize' => '2M',
								'maxSizeMessage' => 'The image is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}'
							)
						),
						new NotBlank(
							array(
								'message' => 'Please upload payment evidence'
							)
						)
					),
					'required' => true
				));
		}


		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\Feeder'
			]);
		}
	}