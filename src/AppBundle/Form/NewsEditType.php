<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\Form\Extension\Core\Type\FileType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Image;

	class NewsEditType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {

			$builder
				->add('title', TextType::class, array(
					'label' => 'Title',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Title'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter title'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 255,
								'minMessage' => 'Title must contain 5 or more characters',
								'maxMessage' => 'Title cannot contain more than 255 characters'
							)
						)
					),
					'required' => true
				))
				->add('article', TextareaType::class, array(
					'label' => 'Article',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Article'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter article'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 3000,
								'minMessage' => 'Article must contain 5 or more characters',
								'maxMessage' => 'Article cannot contain more than 3000 characters'
							)
						)
					),
					'required' => true
				))
				->add('imageFile', FileType::class,array(
					'label' => 'Add Article Image',
					'constraints' => array(
						new Image(
							array(
								'maxSize' => '2M',
								'maxSizeMessage' => 'The image is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}'
							)
						)
					),
					'required' => false
				));
		}


		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\News'
			]);
		}
	}