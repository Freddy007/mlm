<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\Form\Extension\Core\Type\FileType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Image;

	class SupportMessageType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {

			$builder
				->add('message', TextareaType::class, array(
					'label' => 'Support Message',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Enter Support Message Here'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter support message'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 1000,
								'minMessage' => 'Support Message must contain 5 or more characters',
								'maxMessage' => 'Support Message cannot contain more than 1000 characters'
							)
						)
					),
					'required' => false
				))
				->add('attachmentFile', FileType::class, array(
					'label' => 'Add Image',
					'constraints' => array(
						new Image(
							array(
								'maxSize' => '2M',
								'maxSizeMessage' => 'The image is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}'
							)
						)
					),
					'required' => false
				));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\SupportMessage'
			]);
		}
	}