<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;

	class BankChoiceType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('bank_choice', ChoiceType::class, array(
					'label' => 'Choose a Bank Account',
					'attr' => array(
						'class' => 'form-control'
					),
					'choices' => array(
						'Choose a Bank Account' => '',
						'Use the Default Account' => 'Use the Default Account',
						'Use the Provisional Account' => 'Use the Provisional Account'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please select an account type'
							)
						)
					),
					'required' => true
				));





		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\User'
			]);
		}
	}