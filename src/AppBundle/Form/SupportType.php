<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;

	class SupportType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {

			$builder
				->add('subject', TextType::class, array(
					'label' => 'Support Subject',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Support Subject'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter support subject'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 255,
								'minMessage' => 'Support subject must contain 5 or more characters',
								'maxMessage' => 'Support subject cannot contain more than 255 characters'
							)
						)
					),
					'required' => true
				));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\Support'
			]);
		}
	}