<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
	use Symfony\Component\Form\Extension\Core\Type\FileType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;
	use Symfony\Component\Validator\Constraints\Image;

	class PersonalType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('name', TextType::class, array(
					'label' => 'Full name',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Full name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your full name'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 255,
								'minMessage' => 'Full name must contain 5 or more characters',
								'maxMessage' => 'Full name cannot contain more than 255 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => false,
								'message' => 'Full name can only contain alphabets'
							)
						)
					),
					'required' => true
				))
				->add('avatarImage', FileType::class,array(
					'label' => 'Change Avatar',
					'constraints' => array(
						new Image(
							array(
								'maxSize' => '2M',
								'maxSizeMessage' => 'The image is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}'
							)
						)
					),
					'required' => false
				))
				->add('phone_one', TextType::class, array(
					'label' => 'Mobile Money No.',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Phone No'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your phone no'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 30,
								'minMessage' => 'Phone number must contain at least 5 numbers',
								'maxMessage' => 'Phone number must contain at most 30 numbers'
							)
						),
						new regex(
							array(
								'pattern' => '/\d+/',
								'message' => 'Phone number can only contain numbers!'
							)
						)
					),
					'required' => true
				))
				->add('phone_two', TextType::class, array(
					'label' => 'Phone No 2',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Phone No'
					),
					'constraints' => array(
						new Length(
							array(
                                'min' => 5,
                                'max' => 30,
                                'minMessage' => 'Phone number must contain at least 5 numbers',
                                'maxMessage' => 'Phone number must contain at most 30 numbers'
                            )
						),
						new regex(
							array(
								'pattern' => '/\d+/',
								'message' => 'Phone number can only contain numbers!'
							)
						)
					),
					'required' => false
				));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\User'
			]);
		}
	}