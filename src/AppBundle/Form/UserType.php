<?php

	namespace AppBundle\Form;

use Doctrine\ORM\Query\Expr\Select;
use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\PasswordType;
	use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
	use Symfony\Component\Form\Extension\Core\Type\EmailType;
	use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\CountryType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Email;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;

	class UserType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder

			->add('referrer', TextType::class, array(
				'attr' => array(
					'class' => 'form-control',
					'placeholder' => 'Referrer username'
				),
				'constraints' => array(
					new NotBlank(
						array(
							'message' => 'Please enter a referrer username'
						)
					),
					new Length(
						array(
							'min' => 3,
							'max' => 40,
							'minMessage' => 'Referrer username must contain 3 or more characters',
							'maxMessage' => 'Referrer username cannot contain more than 40 characters'
						)
					),
					new Regex(
						array(
							'pattern' => '/^[A-Za-z0-9]+$/',
							'message' => 'Referrer username can only contain alphanumeric characters'
						)
					)
				),
				'required' => false
			))

				->add('username', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Username'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter a username'
							)
						),
						new Length(
							array(
								'min' => 3,
								'max' => 40,
								'minMessage' => 'Username must contain 3 or more characters',
								'maxMessage' => 'Username cannot contain more than 40 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/^[A-Za-z0-9]+$/',
								'message' => 'Username can only contain alphanumeric characters'
							)
						)
					),
					'required' => false
				))
				->add('name', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Full name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your full name'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 255,
								'minMessage' => 'Full name must contain 5 or more characters',
								'maxMessage' => 'Full name cannot contain more than 255 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => false,
								'message' => 'Full name can only contain alphabets'
							)
						)
					),
					'required' => false
				))
                ->add('email', EmailType::class, array(
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Email'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter an email address'
                            )
                        ),
                        new Email(
                            array(
                                'message' => 'Please enter a valid email address'
                            )
                        ),
                        new Length(
                            array(
                                'max' => 255,
                                'maxMessage' => 'Email cannot contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => false
				))
				
				->add('sex', ChoiceType::class, array(
					'choices' => [
							'Male' => 'male',
							'Female' => 'female',
					
					],
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Sex'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please choose your sex'
                            )
                        )
                    ),
                    'required' => false
                ))
				->add('phone_one', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Mobile Money Number'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your Mobile Money no'
							)
						),
						new Length(
							array(
								'min' => 4,
								'max' => 20,
								'minMessage' => 'Phone number must be at least 4 numbers',
								'maxMessage' => 'Phone number must contain exactly 20 numbers'
							)
						),
						new regex(
							array(
								'pattern' => '/\d+/',
								'message' => 'Phone number can only contain numbers!'
							)
						)
					),
					'required' => false
				))

				->add('bank_name', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Bank Name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your Bank Name'
							)
						),
						new Length(
							array(
								'min' => 4,
								'max' => 20,
								'minMessage' => 'Bank name must be at least 4 numbers'
							)
						)
					),
					'required' => false
				))

				->add('bank_account_number', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Bank Account Number'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your Bank Acount Number'
							)
						)
					),
					'required' => false
				))

				->add('bank_account_name', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Bank Account Name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your Bank Acount Name'
							)
					
						)
					),
					'required' => false
				))

				->add('plainPassword', RepeatedType::class, [
	                'type' => PasswordType::class,
	                'first_options' => [
	                	'attr' => array(
	                		'class' => 'form-control',
	                		'placeholder' => 'Password'
	                	),
	                	'constraints' => array(
	                		new NotBlank(
	                			array(
	                				'message' => 'Please enter a password'
	                			)
	                		),
							new Length(
								array(
									'min' => 5,
									'max' => 16,
									'minMessage' => 'Password must contain 5 or more characters',
									'maxMessage' => 'Password cannot contain more than 16 characters'
								)
							)
	                	),
	                	'required' => false
	                ],
	                'second_options' => [
	                	'attr' => array(
	                		'class' => 'form-control',
	                		'placeholder' => 'Confirm Password'
	                	),
	                	'constraints' => array(
	                		new NotBlank(
	                			array(
	                				'message' => 'Please enter password again'
	                			)
	                		),
							new Length(
								array(
									'min' => 5,
									'max' => 16,
									'minMessage' => 'Confirm Password must contain 5 or more characters',
									'maxMessage' => 'Confirm Password cannot contain more than 16 characters'
								)
							)
	                	),
	                	'required' => false
	                ],
	                'required' => false,
	                'invalid_message' => 'Passwords do not match'
            	]);
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\User'
			]);
		}

	}