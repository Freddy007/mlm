<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\TextareaType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;

	class MailType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			
			$builder
				->add('subject', TextType::class, array(
					'label' => 'Subject',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Subject'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter subject'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 255,
								'minMessage' => 'Subject must contain 5 or more characters',
								'maxMessage' => 'Subject cannot contain more than 255 characters'
							)
						)
					),
					'required' => true
				))
				->add('mail', TextareaType::class, array(
					'label' => 'Mail',
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Mail'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter mail'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 3000,
								'minMessage' => 'Mail must contain 5 or more characters',
								'maxMessage' => 'Mail cannot contain more than 3000 characters'
							)
						)
					),
					'required' => true
				));
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\Mail'
			]);
		}

	}