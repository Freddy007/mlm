<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\PasswordType;
	use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Length;

	class SettingsType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('oldPassword', PasswordType::class, [
                	'label' => 'Old Password',
                	'attr' => array(
                		'class' => 'form-control',
                		'placeholder' => 'Old Password'
                	),
                	'constraints' => array(
                		new NotBlank(
                			array(
                				'message' => 'Please enter old password'
                			)
                		),
						new Length(
							array(
								'min' => 5,
								'max' => 16,
								'minMessage' => 'Old Password must contain 5 or more characters',
								'maxMessage' => 'Old Password cannot contain more than 16 characters'
							)
						)
                	),
                	'required' => true
				])
				->add('plainPassword', RepeatedType::class, [
	                'type' => PasswordType::class,
	                'first_options' => [
	                	'label' => 'New Password',
	                	'attr' => array(
	                		'class' => 'form-control',
	                		'placeholder' => 'New Password'
	                	),
	                	'constraints' => array(
	                		new NotBlank(
	                			array(
	                				'message' => 'Please enter new password'
	                			)
	                		),
							new Length(
								array(
									'min' => 5,
									'max' => 16,
									'minMessage' => 'New Password must contain 5 or more characters',
									'maxMessage' => 'New Password cannot contain more than 16 characters'
								)
							)
	                	),
	                	'required' => true
	                ],
	                'second_options' => [
	                	'label' => 'Confirm Password',
	                	'attr' => array(
	                		'class' => 'form-control',
	                		'placeholder' => 'Confirm Password'
	                	),
	                	'constraints' => array(
	                		new NotBlank(
	                			array(
	                				'message' => 'Please enter password again'
	                			)
	                		),
							new Length(
								array(
									'min' => 5,
									'max' => 16,
									'minMessage' => 'Confirm Password must contain 5 or more characters',
									'maxMessage' => 'Confirm Password cannot contain more than 16 characters'
								)
							)
	                	),
	                	'required' => true
	                ],
	                'required' => true,
	                'invalid_message' => 'Passwords do not match'
            	]);
		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\User'
			]);
		}
	}