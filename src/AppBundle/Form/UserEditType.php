<?php

	namespace AppBundle\Form;

	use Symfony\Component\Form\AbstractType;
	use Symfony\Component\Form\FormBuilderInterface;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\PasswordType;
	use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
	use Symfony\Component\Form\Extension\Core\Type\EmailType;
	use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\CountryType;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	use Symfony\Component\Validator\Constraints\NotBlank;
	use Symfony\Component\Validator\Constraints\Email;
	use Symfony\Component\Validator\Constraints\Length;
	use Symfony\Component\Validator\Constraints\Regex;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;
    use Symfony\Component\Form\FormEvents;

	class UserEditType extends AbstractType {

		public function buildForm(FormBuilderInterface $builder, array $options) {
			$builder
				->add('username', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Username'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter a username'
							)
						),
						new Length(
							array(
								'min' => 3,
								'max' => 40,
								'minMessage' => 'Username must contain 3 or more characters',
								'maxMessage' => 'Username cannot contain more than 40 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/^[A-Za-z0-9]+$/',
								'message' => 'Username can only contain alphanumeric characters'
							)
						)
					),
					'required' => false
				))
				->add('name', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Full name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your full name'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 255,
								'minMessage' => 'Full name must contain 5 or more characters',
								'maxMessage' => 'Full name cannot contain more than 255 characters'
							)
						),
						new Regex(
							array(
								'pattern' => '/\d/',
								'match' => false,
								'message' => 'Full name can only contain alphabets'
							)
						)
					),
					'required' => false
				))
                ->add('email', EmailType::class, array(
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Email'
                    ),
                    'constraints' => array(
                        new NotBlank(
                            array(
                                'message' => 'Please enter an email address'
                            )
                        ),
                        new Email(
                            array(
                                'message' => 'Please enter a valid email address'
                            )
                        ),
                        new Length(
                            array(
                                'max' => 255,
                                'maxMessage' => 'Email cannot contain more than 255 characters'
                            )
                        )
                    ),
                    'required' => false
                ))
                ->add('purse', TextType::class, array(
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Email'
                    ),
                    'constraints' => array(
                        new Length(
                            array(
                                'max' => 255,
                                'maxMessage' => 'Purse cannot contain more than 255 characters'
                            )
                        ),
						new     regex(
                            array(
                                'pattern' => '/\d+/',
                                'message' => 'Purse can only contain numbers!'
                            )
                        )
                    ),
                    'required' => false
                ))
				->add('phone_one', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Mobile Money Number'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your Mobile Money Number'
							)
						),
						new Length(
							array(
								'min' => 5,
								'max' => 22,
								'minMessage' => 'Phone number must contain at least 5 characters',
								'maxMessage' => 'Phone number must contain at most 22 characters'
							)
						),
						new     regex(
							array(
								'pattern' => '/\d+/',
								'message' => 'Phone number can only contain numbers!'
							)
						)
					),
					'required' => false
				))

				->add('bank_name', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Bank Name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your Bank Name'
							)
						),
						new Length(
							array(
								'min' => 4,
								'max' => 20,
								'minMessage' => 'Bank name must be at least 4 numbers'
							)
						)
					),
					'required' => false
				))

				->add('bank_account_number', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Bank Account Number'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your Bank Acount Number'
							)
						)
					),
					'required' => false
				))

				->add('bank_account_name', TextType::class, array(
					'attr' => array(
						'class' => 'form-control',
						'placeholder' => 'Bank Account Name'
					),
					'constraints' => array(
						new NotBlank(
							array(
								'message' => 'Please enter your Bank Acount Name'
							)

						)
					),
					'required' => false
				));



		}

		public function configureOptions(OptionsResolver $resolver) {
			$resolver->setDefaults([
				'data_class' => 'AppBundle\Entity\User'
			]);
		}

	}