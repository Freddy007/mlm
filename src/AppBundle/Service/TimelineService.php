<?php

	namespace AppBundle\Service;

	use Doctrine\ORM\EntityManager;

	class TimelineService {

		protected $em;

		public function __construct(EntityManager $em) {
			$this->em = $em;
		}

		private function extractText($text, $length) {
			$extract = substr($text,0,$length);
			// find position of last space in extract
			$lastSpace = strrpos($extract,' ');
			// use $lastSpace to set length of new extract and add ...
			if(strlen($text) <= $length) {
				return $text;
			} else {
				return substr($extract, 0, $lastSpace).'... ';
			}
		}

        public function format($timeline) {
            $formatted = array();

            foreach($timeline as $entry) {

                $day = date_format($entry->getCreated(), "d M. Y");

                switch ($entry->getType()) {
                    case 1:
                        $entry->setIcon('fa-sign-in');
                        $entry->setBackground('bg-red');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    case 2:
                        $entry->setIcon('fa-pie-chart');
                        $entry->setBackground('bg-orange');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    case 3:
                        $entry->setIcon('fa-money');
                        $entry->setBackground('bg-maroon');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    case 4:
                        $entry->setIcon('fa-ticket');
                        $entry->setBackground('bg-purple');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    case 5:
                        $entry->setIcon('fa-comment');
                        $entry->setBackground('bg-olive');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    case 6:
                        $entry->setIcon('fa-envelope');
                        $entry->setBackground('bg-blue');
                        $mail = $this->em->createQuery("SELECT m FROM AppBundle:MailRecipient m WHERE m.id = ?1")->setParameter(1, $entry->getObject())->getOneOrNullResult();
                        $mail->getMail()->setMail($this->extractText(strip_tags($mail->getMail()->getMail()), 200));
                        $entry->setMisc($mail);
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    case 10001:
                        $entry->setIcon('fa-compress');
                        $entry->setBackground('bg-green');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    case 10002:
                        $entry->setIcon('fa-upload');
                        $entry->setBackground('bg-green');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');

                        break;

                    case 10003:
                        $entry->setIcon('fa-upload');
                        $entry->setBackground('bg-green');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');

                        break;

                    case 10004:
                        $entry->setIcon('fa-refresh');
                        $entry->setBackground('bg-green');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');

                        break;

                    case 10005:
                        $entry->setIcon('fa-close');
                        $entry->setBackground('bg-green');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    case 10006:
                        $entry->setIcon('fa-check');
                        $entry->setBackground('bg-green');
                        $entry->setColor('#edf2fa');
                        $entry->setTextColor('white');
                        break;

                    default:
                        # code...
                        break;
                }

                $formatted[$day][] = $entry;
            }

            return $formatted;
        }

	}