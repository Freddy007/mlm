<?php

namespace AppBundle\Service;
/**
 * Created by IntelliJ IDEA.
 * User: Freddy
 * Date: 3/4/2018
 * Time: 11:00 AM
 */

function getPack($user){

	$entityManager = $this->getDoctrine()->getEntityManager();

	$query = $entityManager->createQuery(
		'SELECT p
		FROM AppBundle:Pack p
		WHERE p.id = :id'
	)->setParameter('id', $pack)
	->setMaxResults(1);

	// returns an array of Product objects
	$pack =  $query->getOneOrNullResult();

}