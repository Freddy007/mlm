<?php

namespace AppBundle\Service\Cryptos\Classes;


class BtcGateway {
	private $gateway;
	
	function randomHash($lenght = 7) {
		$random = substr(md5(rand()),0,$lenght);
		return $random;
	}
	
	function create_payment_box($bitcoinaddress, $amount) {
		global $gateway;
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		$boxID = $this->randomHash(15);
		$_SESSION['btc_boxID'] = $boxID;
		$_SESSION['btc_address'] = $bitcoinaddress;
		$_SESSION['btc_amount'] = $amount;
		$address = $bitcoinaddress;
		$box = '<div class="panel panel-default" id="PaymentBox_'.$boxID.'">
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h3><span class="text text-warning"><img src="/assets/imgs/Bitcoin.png" width="32px" height="32px"></span> Bitcoin Payment Box</h3>
					</div>
					<div class="col-sm-4 col-md-4 col-lg-4">
						'.$this->QRCode($bitcoinaddress,$amount).'
					</div>
					<div class="col-sm-8 col-md-8 col-lg-8" style="padding:10px;">
						Send <b>'.$amount.' BTC</b><br/><input type="text" id="address_'.$boxID.'" class="form-control" value="'.$address.'">
						or scan QR Code with your mobile device<br/><br/>
						<small>Do not refresh page, payment status will be updated automatically.</small>
					</div>
					<div class="col-sm-12 col-md-12 col-lg-12">
						<center><span id="PaymentStatus_'.$boxID.'"></span></center>
						<input type="hidden" id="payment_boxID" value="'.$boxID.'">
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		$("#address_'.$boxID.'").focus(function() {
		   $(this).select();
		});
		function updatePaymentStatus() {
			btc_gateway_update_status("'.$boxID.'");
		}
		setInterval(updatePaymentStatus,3000);
		</script>';
		return $box;
	}
	
	function QRCode($bitcoinaddress, $bitamount) {
		global $gateway;
		$address = $bitcoinaddress;
		$amount = $bitamount;
		return '<img src="https://chart.googleapis.com/chart?chs=500x500&cht=qr&chl=bitcoin:'.$address.'?amount='.$amount.'&choe=UTF-8" class="img-responsive">';
	}
	
	function StdClass2array($class) {
		$array = array();

		foreach ($class as $key => $item)
		{
				if ($item instanceof StdClass) {
						$array[$key] = StdClass2array($item);
				} else {
						$array[$key] = $item;
				}
		}

		return $array;
	}
}
?>