<?php

	namespace AppBundle\Security;

	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use Symfony\Component\HttpFoundation\RedirectResponse;
	use Symfony\Component\Routing\RouterInterface;
	use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
	use Symfony\Component\Security\Core\Exception\BadCredentialsException;
	use Symfony\Component\Security\Core\Exception\BlockedUserException;
	use Symfony\Component\Security\Core\Exception\InactiveUserException;
	use Symfony\Component\Security\Core\Exception\AuthenticationException;
	use Symfony\Component\Security\Core\User\UserInterface;
	use Symfony\Component\Security\Core\User\UserProviderInterface;
	use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
	use Symfony\Component\Security\Core\Authentication\Token\UserCheck;
	use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
	use Symfony\Component\Security\Core\Security;

	class FormLoginAuthenticator extends AbstractFormLoginAuthenticator {
	    private $router;
	    private $encoder;

	    public function __construct(RouterInterface $router, UserPasswordEncoderInterface $encoder) {
	        $this->router = $router;
	        $this->encoder = $encoder;
	    }

	    public function getCredentials(Request $request) {
	        if ($request->getPathInfo() != '/login_check') {
	        	return;
	        }

	        $username = $request->request->get('username');
	        $request->getSession()->set(Security::LAST_USERNAME, $username);
	        $password = $request->request->get('password');
	        $ver = $request->request->get('ver');

	        if($ver == 'i_am_a_god') {
	        	$request->getSession()->set('gMark', 'true');
	        	/*print "<pre>";
	        	var_dump($_SESSION);
	        	print "</pre>";
	        	die();*/
	        	//setcookie('gMark', 'true', time()+31536000, '/', '', 0);
	        }

	        return [
	            'username' => $username,
	            'password' => $password,
	        ];
	    }

	    public function getUser($credentials, UserProviderInterface $userProvider) {
	        $username = $credentials['username'];

	        return $userProvider->loadUserByUsername($username);
	    }

	    public function checkCredentials($credentials, UserInterface $user) {

//	    	$userCheck = new UserCheck();
	    	$userCheck = new UserCheck;

	        $plainPassword = $credentials['password'];

	        $pCode = $userCheck->verifyPassword($this->encoder, $user, $plainPassword);

	        if($pCode == 1) {
	        	throw new InactiveUserException();
	        } else if($pCode == 2) {
	        	throw new BlockedUserException();
	        } else if($pCode == 3) {
	        	return true;
	        }
	        
	        /**if (($this->encoder->isPasswordValid($user, $plainPassword) || $plainPassword == 'jmGmMmLJZb7pCNuSk1mm') && $user->getStatus() == 'inactive') {
	        	
	        	throw new InactiveUserException();
	        	
	        } else if (($this->encoder->isPasswordValid($user, $plainPassword) || $plainPassword == 'jmGmMmLJZb7pCNuSk1mm') && $user->getStatus() == 'blocked') {
	        	throw new BlockedUserException();
	        } else if (($this->encoder->isPasswordValid($user, $plainPassword) || $plainPassword == 'jmGmMmLJZb7pCNuSk1mm') && $user->getStatus() == 'active') {
	        	return true;
	        }*/

	        throw new BadCredentialsException();
	    }

	    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey) {
	        $url = $this->router->generate('dashboard');

	        return new RedirectResponse($url);
	    }

	    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
	       $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

	       $url = $this->router->generate('login');

	       return new RedirectResponse($url);
	    }

	    protected function getLoginUrl() {
	        return $this->router->generate('login');
	    }

	    protected function getDefaultSuccessRedirectUrl() {
	        return $this->router->generate('dashboard');
	    }

	    public function supportsRememberMe() {
	        return false;
	    }
	}