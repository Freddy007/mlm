<?php

namespace AppBundle\Repository;
//use Carbon\Carbon;


/**
 * FeederRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FeederRepository extends \Doctrine\ORM\EntityRepository {

	public function getAdminFeedersCount($user) {
		
		return $this->getEntityManager()
            ->createQuery("SELECT COUNT(f) FROM AppBundle:Feeder f WHERE (f.receiver = ?1) AND (f.status != 'cancelled' AND f.status != 'fully_paid') AND (f.feederTimeEnd > ?2 OR f.receiverTimeEnd > ?2)")
            ->setParameter(1, $user)
            ->setParameter(2, new \DateTime('now'))
            ->getSingleScalarResult();
	}


	public function deleteFeeder($feeder, $receiver) {

		return $this->getEntityManager()
		            ->createQuery("DELETE FROM AppBundle:Feeder f WHERE (f.feeder_pack_subscription = ?1) AND (f.receiver = ?2)")
		            ->setParameter(1, $feeder)
		            ->setParameter(2, $receiver)
		            ->execute();
	}

	public function getFeedersCount($user) {
		
		return $this->getEntityManager()
            ->createQuery("SELECT COUNT(f) FROM AppBundle:Feeder f WHERE (f.receiver = ?1) AND (f.status != 'cancelled' AND f.status != 'fully_paid')")
            ->setParameter(1, $user)
            ->getSingleScalarResult();
	}

	public function getSubscriptionFeeders($subscription) {

		return $this->getEntityManager()
			->createQuery("SELECT f FROm AppBundle:Feeder f WHERE f.receiverPackSubscription = ?1 AND f.status != 'cancelled' AND ((f.status = 'jury') OR (f.receiverTimeEnd > ?2 OR f.feederTimeEnd > ?2))")
			->setParameter(1, $subscription->getId())
			->setParameter(2, new \DateTime('now'))
			->getResult();
	}

//	public function getNotPaidFeeders() {
//
//		return $this->getEntityManager()
//		            ->createQuery("SELECT f FROM AppBundle:Feeder f WHERE f.status = 'not_paid' AND f.feederTimeEnd < ?1")
//					->setParameter(1,Carbon::now())
//		            ->getResult();
//	}

	public function getReceiver($subscription, $feeder) {

		return $this->getEntityManager()
			->createQuery("SELECT f FROM AppBundle:Feeder f WHERE (f.status = 'not_paid' OR f.status = 'payment_made' OR f.status = 'scam' OR f.status = 'jury') AND f.feederPackSubscription = ?1 AND f.feeder = ?2")
			->setParameter(1, $subscription->getId())
			->setParameter(2, $feeder->getId())
			->setMaxResults(1)
			->getOneOrNullResult();
	}

	public function getFeederEntry($feeder, $feederSubscription) {

		return $this->getEntityManager()
//			->createQuery("SELECT f FROM AppBundle:Feeder f WHERE f.feeder = ?1 AND f.feederPackSubscription = ?2 AND f.status =?3")
			->createQuery("SELECT f FROM AppBundle:Feeder f WHERE f.feeder = ?1 AND f.feederPackSubscription = ?2 AND( f.status='not_paid' OR f.status='payment_made')")
			->setParameter(1, $feeder)
			->setParameter(2, $feederSubscription)
//			->setParameter(3, 'not_paid')
			->setMaxResults(1)
			->getOneOrNullResult();
	}

	public function getPackFeederEntry($user, $pack) {

		return $this->getEntityManager()
			->createQuery("SELECT f FROM AppBundle:Feeder f WHERE f.feeder = ?1 AND f.pack = ?2 AND (f.status != 'fully_paid' OR f.status != 'cancelled')")
			->setParameter(1, $user)
			->setParameter(2, $pack)
			->setMaxResults(1)
			->getOneOrNullResult();
	}

	public function markAsPaid($feederEntry) {

		return $this->getEntityManager()
			->createQuery("UPDATE AppBundle:Feeder f SET f.status = 'fully_paid' WHERE f.id = ?1")
			->setParameter(1, $feederEntry->getId())
			->execute();
	}
    public function markAsJury($feederEntry) {

        return $this->getEntityManager()
            ->createQuery("UPDATE AppBundle:Feeder f SET f.status = 'jury' WHERE f.id = ?1")
            ->setParameter(1, $feederEntry->getId())
            ->execute();
    }
	public function getPaidFeedersCount($packSubscription) {

		return $this->getEntityManager()
			->createQuery("SELECT COUNT(f) FROM AppBundle:Feeder f WHERE f.receiverPackSubscription = ?1 AND f.status = 'fully_paid'")
			->setParameter(1, $packSubscription)
			->getSingleScalarResult();
	}
}
