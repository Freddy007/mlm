<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PackSubscription
 *
 * @ORM\Table(name="pack_subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PackSubscriptionRepository")
 */
class PackSubscription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Pack", inversedBy="packSubscriptions")
     *
     * @ORM\JoinColumn(name="pack", referencedColumnName="id")
     */
    private $pack;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id" )
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=10)
     */
    private $userType;

    /**
     * @var int
     *
     * @ORM\Column(name="feeder_counter", type="integer")
     */
    private $feederCounter;

    /**
     * @var int
     *
     * @ORM\Column(name="feeder_total", type="integer")
     */
    private $feederTotal;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Pack")
     *
     * @ORM\JoinColumn(name="auto_recirculate", referencedColumnName="id", nullable=true , onDelete="CASCADE")
     */
    private $autoRecirculate;

    /**
     * @var string
     *
     * @ORM\Column(name="book_cancel", type="string", nullable=true)
     */
    private $bookCancel;

    /**
     * @var int
     *
     * @ORM\Column(name="admin_track", type="integer", nullable=true)
     */
    private $adminTrack;

    /**
     * @var int
     *
     * @ORM\Column(name="admin", type="integer", nullable=true)
     */
    private $admin;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_track_status", type="string", length=15, nullable=true)
     */
    private $adminTrackStatus;

    /**
     * @var int
     *
     * @ORM\Column(name="admin_track_limit", type="integer")
     */
    private $adminTrackLimit;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=10)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10)
     */
    private $type;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="paid_commission", type="boolean")
	 */
	private $paidCommission;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="forced_waiting", type="boolean")
	 */
	private $forcedWaiting;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="recommit_required", type="boolean")
	 */
    private $recommitRequired;
    
   

		/**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    // Additional Data to work with
    private $feeders;


    private $userFeeder;

    /**
     * Set userFeeder
     *
     * @param string $userFeeder
     *
     * @return PackSubscription
     */
    public function setUserFeeder($userFeeder)
    {
        $this->userFeeder = $userFeeder;

        return $this;
    }

    /**
     * Get userFeeder
     *
     * @return string
     */
    public function getUserFeeder()
    {
        return $this->userFeeder;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pack
     *
     * @param integer $pack
     *
     * @return PackSubscription
     */
    public function setPack($pack)
    {
        $this->pack = $pack;

        return $this;
    }

    /**
     * Get pack
     *
     * @return int
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return PackSubscription
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userType
     *
     * @param string $userType
     *
     * @return PackSubscription
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType
     *
     * @return string
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set feederCounter
     *
     * @param integer $feederCounter
     *
     * @return PackSubscription
     */
    public function setFeederCounter($feederCounter)
    {
        $this->feederCounter = $feederCounter;

        return $this;
    }

    /**
     * Get feederCounter
     *
     * @return int
     */
    public function getFeederCounter()
    {
        return $this->feederCounter;
    }

    /**
     * Set feederTotal
     *
     * @param integer $feederTotal
     *
     * @return PackSubscription
     */
    public function setFeederTotal($feederTotal)
    {
        $this->feederTotal = $feederTotal;

        return $this;
    }

    /**
     * Get feederTotal
     *
     * @return int
     */
    public function getFeederTotal()
    {
        return $this->feederTotal;
    }

    /**
     * Set autoRecirculate
     *
     * @param integer $autoRecirculate
     *
     * @return PackSubscription
     */
    public function setAutoRecirculate($autoRecirculate)
    {
        $this->autoRecirculate = $autoRecirculate;

        return $this;
    }

    /**
     * Get autoRecirculate
     *
     * @return int
     */
    public function getAutoRecirculate()
    {
        return $this->autoRecirculate;
    }

    /**
     * Set bookCancel
     *
     * @param string $bookCancel
     *
     * @return PackSubscription
     */
    public function setBookCancel($bookCancel)
    {
        $this->bookCancel = $bookCancel;

        return $this;
    }

    /**
     * Get bookCancel
     *
     * @return string
     */
    public function getBookCancel()
    {
        return $this->bookCancel;
    }

    /**
     * Set adminTrack
     *
     * @param integer $adminTrack
     *
     * @return PackSubscription
     */
    public function setAdminTrack($adminTrack)
    {
        $this->adminTrack = $adminTrack;

        return $this;
    }

    /**
     * Get adminTrack
     *
     * @return int
     */
    public function getAdminTrack()
    {
        return $this->adminTrack;
    }

    /**
     * Set admin
     *
     * @param integer $admin
     *
     * @return PackSubscription
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return int
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set adminTrackStatus
     *
     * @param string $adminTrackStatus
     *
     * @return PackSubscription
     */
    public function setAdminTrackStatus($adminTrackStatus)
    {
        $this->adminTrackStatus = $adminTrackStatus;

        return $this;
    }

    /**
     * Get adminTrackStatus
     *
     * @return string
     */
    public function getAdminTrackStatus()
    {
        return $this->adminTrackStatus;
    }

    /**
     * Set adminTrackLimit
     *
     * @param integer $adminTrackLimit
     *
     * @return PackSubscription
     */
    public function setAdminTrackLimit($adminTrackLimit)
    {
        $this->adminTrackLimit = $adminTrackLimit;

        return $this;
    }

    /**
     * Get adminTrackLimit
     *
     * @return int
     */
    public function getAdminTrackLimit()
    {
        return $this->adminTrackLimit;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return PackSubscription
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PackSubscription
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return PackSubscription
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set feeders
     */
    public function setFeeders($feeders)
    {
        $this->feeders = $feeders;
    }

    /**
     * Get feeders
     */
    public function getFeeders()
    {
        return $this->feeders;
    }

	/**
	 * Set paidCommission
	 */
	public function setPaidCommission($status)
	{
		$this->paidCommission = $status;
	}

	/**
	 * Get paidCommission
	 */
	public function getPaidCommission()
	{
		return $this->paidCommission;
	}

	public function setForcedWaiting($status)
	{
		 $this->forcedWaiting = $status;
	}

	public function getForcedWaiting()
	{
		return $this->forcedWaiting;
	}

	public function setRecommitRequired($status)
	{
		$this->recommitRequired = $status;
	}

	public function getRecommitRequired()
	{
		return $this->recommitRequired;
    }

    
    // public function setActive($active)
	// {
	// 	$this->active = $active;
	// }

	// public function getActive()
	// {
	// 	return $this->active;
    // }

}

