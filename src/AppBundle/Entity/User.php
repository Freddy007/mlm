<?php

	namespace AppBundle\Entity;

	use Doctrine\ORM\Mapping as ORM;
	use Symfony\Component\Security\Core\User\UserInterface;
	use Symfony\Component\Validator\Constraints as Assert;
	use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

	/**
	 * @ORM\Entity
 	 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
	 * @UniqueEntity(fields="username", message="Username Already in Use")
	 * @UniqueEntity(fields="email", message="Email Address Already in Use")
	 */

	class User implements UserInterface {
    
    	/**
     	 * @ORM\Id;
     	 * @ORM\Column(type="integer")
     	 * @ORM\GeneratedValue(strategy="AUTO")
     	 */
    	protected $id;

	    /**
	     * @ORM\Column(type="string", length=40, unique=true)
	     */
	    protected $username;
	    /**
	     * @ORM\Column(type="string", length=255)
	     */
	    protected $name;

        /**
         * @ORM\Column(type="string", length=255, unique=true)
         */
		protected $email;
		
		 /**
         * @ORM\Column(type="string", length=100)
         */
		protected $sex;

	    /**
	     * @Assert\Length(max=4096)
	     */
	    protected $plainPassword;

	    /**
	     * @Assert\Length(max=4096)
	     */
	    protected $oldPassword;

	    /**
	     * @ORM\Column(type="string", length=40)
	     */
	    protected $password;


	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $avatar;

	    /**
	     * Image
	     */
	    protected $avatarImage;

	    /**
	     * @ORM\Column(type="string", length=11)
	     */
	    protected $phoneOne;

        /**
         * @ORM\Column(type="string", length=11, nullable=true)
         */
        protected $phoneTwo;

        /**
         * @ORM\Column(type="integer", length=11, nullable=true)
         */
        protected $bankChoice;






	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $bankName;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $agency;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $bankAccountName;

	    /**
	     * @ORM\Column(type="string", length=40, nullable=true)
	     */
	    protected $bankAccountNumber;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $operation;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $cpf;

	    /**
	     * @ORM\Column(type="string", length=40, nullable=true)
	     */
	    protected $bankAccountType;


	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $bankNameTwo;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $agencyTwo;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $accountNameTwo;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $accountNumberTwo;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $operationTwo;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $cpfTwo;

	    /**
	     * @ORM\Column(type="string", length=255, nullable=true)
	     */
	    protected $accountTypeTwo;


        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        protected $bankNameOne;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        protected $agencyOne;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        protected $accountNameOne;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        protected $accountNumberOne;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        protected $operationOne;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        protected $cpfOne;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        protected $accountTypeOne;

	    /**
	     * @ORM\Column(type="integer", length=11)
	     */
	    protected $purse;

	    /**
	     * @ORM\Column(type="string", length=40)
	     */
	    protected $status;

	    /**
	     * @ORM\Column(type="string", length=40)
	     */
	    protected $role;

	    /**
	     * @ORM\Column(type="integer", length=11, nullable=true)
	     */
	    protected $referrer;

	    /**
	     * @ORM\Column(type="string", length=11, nullable=true)
	     */
	    protected $referrerUsername;

	    /**
	     * @ORM\Column(type="datetime", length=255)
	     */
	    protected $created;

	    /**
	     * @ORM\Column(type="string", length=40)
	     */
	    protected $activationKey;

	    // Relational Objects
	    protected $notifications;

	    /**
	     * User Plans
	     */
	    protected $packs;

	    /**
	     * User Formatted Purse
	     */
	    protected $formattedPurse;

	    /**
	     * User Referrals Count
	     */
	    protected $referralsCount;

	    /**
	     * Admin Ticket Count
	     */
	    protected $ticketCount;

	    /**
	     * Admin Court Count
	     */
	    protected $courtCount;

	    /**
	     * User Menu Mail
	     */
	    protected $menuMail;

	    /**
	     * User Mail Count
	     */
	    protected $mailCount;



        /**
         * User Notification Count
         */
        protected $notificationCount;


	    /**
	     * User DB Granted
	     */
		protected $dbGranted;
		
		 /**
	     * @ORM\Column(name="withdrawal_request", type="boolean")
	     */
		protected $withdrawalRequest;

        /**
	     * @ORM\Column(name="withdrawal_request_approved", type="boolean")
	     */
		protected $withdrawalRequestApproved;

		 /**
	     * @ORM\Column(name="payment_made", type="boolean")
	     */
		protected $paymentMade;


		 /**
	     * @ORM\Column(name="amount_paid", type="integer")
	     */
		protected $amountPaid;

		 /**
	     * @ORM\Column(name="mobile_money_name", type="string")
	     */
		protected $mobileMoneyName;

		 /**
	     * @ORM\Column(name="mobile_money_number", type="string")
	     */
		protected $mobileMoneyNumber;

		/**
	     * @ORM\Column(name="amount_requested", type="integer")
	     */
		protected $amountRequested;

	    public function eraseCredentials() {
	        return null;
	    }

	    public function getId() {
	        return $this->id;
	    }

	    public function setUsername($username) {
	        $this->username = $username;
	    }

	    public function getUsername() {
	        return $this->username;
	    }



	    public function setName($name) {
	        $this->name = $name;
	    }

	    public function getName() {
	        return $this->name;
	    }

	    public function getEmail() {
	        return $this->email;
	    }

	    public function setEmail($email) {
	        $this->email = $email;
	    }

		public function getSex() {
	        return $this->sex;
	    }

	    public function setSex($sex) {
	        $this->sex = $sex;
	    }


	    public function getPassword() {
	        return $this->password;
	    }

	    public function setPassword($password) {
	        $this->password = $password;
	    }

	    public function getOldPassword() {
	        return $this->oldPassword;
	    }

	    public function setOldPassword($oldPassword) {
	        $this->oldPassword = $oldPassword;
	    }

	    public function getPlainPassword() {
	        return $this->plainPassword;
	    }

	    public function setPlainPassword($plainPassword) {
	        $this->plainPassword = $plainPassword;
	    }

	    public function getAvatar() {
	    	if($this->avatar == null) {
                return 'img/avatar_m.png';
	    	} else {
	        	return $this->avatar;
	    	}
	    }

	    public function getRealAvatar() {
	    	return $this->avatar;
	    }

	    public function setAvatar($avatar) {
	        $this->avatar = $avatar;
	    }

	    public function getAvatarImage() {
	    	return $this->avatarImage;
	    }

	    public function setAvatarImage($avatarImage) {
	        $this->avatarImage = $avatarImage;
	    }

	    public function getPhoneOne() {
	        return $this->phoneOne;
	    }

	    public function setPhoneOne($phoneOne) {
	        $this->phoneOne = $phoneOne;
	    }

	    public function getPhoneTwo() {
	        return $this->phoneTwo;
	    }

        public function setPhoneTwo($phoneTwo) {
            $this->phoneTwo = $phoneTwo;
        }

        public function getBankChoice()
        {
            return $this->bankChoice;
        }

        public function setBankChoice($bankChoice)
        {
            $this->bankChoice = $bankChoice;
        }













        public function getBankName() {
	    	
	    	return $this->bankName;
	    }

	    public function setBankName($bankName) {
	    	$this->bankName = $bankName;
	    }

        public function getAgency() {
	    	
	    	return $this->agency;
	    }

	    public function setAgency($agency) {
	    	$this->agency = $agency;
	    }

        public function getBankAccountName() {
            return $this->bankAccountName;
        }

        public function setBankAccountName($bankAccountName) {
            $this->bankAccountName = $bankAccountName;
        }

        public function getBankAccountNumber() {
            return $this->bankAccountNumber;
        }

        public function setBankAccountNumber($bankAccountNumber) {
            $this->bankAccountNumber = $bankAccountNumber;
        }


        public function getOperation() {
	    	
	    	return $this->operation;
	    }

	    public function setOperation($operation) {
	    	$this->operation = $operation;
	    }

        public function getCpf() {
	    	
	    	return $this->cpf;
	    }

	    public function setCpf($cpf) {
	    	$this->cpf = $cpf;
	    }
	    public function getBankAccountType() {
	        return $this->bankAccountType;
	    }

	    public function setBankAccountType($bankAccountType) {
	        $this->bankAccountType = $bankAccountType;
	    }

        public function getBankNameTwo()
        {
            return $this->bankNameTwo;
        }


        public function setBankNameTwo($bankNameTwo)
        {
            $this->bankNameTwo = $bankNameTwo;
        }

        public function getAgencyTwo()
        {
            return $this->agencyTwo;
        }

        public function setAgencyTwo($agencyTwo)
        {
            $this->agencyTwo = $agencyTwo;
        }

        public function getAccountNameTwo()
        {
            return $this->accountNameTwo;
        }

        public function setAccountNameTwo($accountNameTwo)
        {
            $this->accountNameTwo = $accountNameTwo;
        }

        public function getAccountNumberTwo()
        {
            return $this->accountNumberTwo;
        }

        public function setAccountNumberTwo($accountNumberTwo)
        {
            $this->accountNumberTwo = $accountNumberTwo;
        }

        public function getOperationTwo()
        {
            return $this->operationTwo;
        }
        public function setOperationTwo($operationTwo)
        {
            $this->operationTwo = $operationTwo;
        }

        public function getCpfTwo()
        {
            return $this->cpfTwo;
        }

        public function setCpfTwo($cpfTwo)
        {
            $this->cpfTwo = $cpfTwo;
        }

        public function getAccountTypeTwo()
        {
            return $this->accountTypeTwo;
        }

        public function setAccountTypeTwo($accountTypeTwo)
        {
            $this->accountTypeTwo = $accountTypeTwo;
        }

        public function getBankNameOne()
        {
            return $this->bankNameOne;
        }

        public function setBankNameOne($bankNameOne)
        {
            $this->bankNameOne = $bankNameOne;
        }

        public function getAgencyOne()
        {
            return $this->agencyOne;
        }

        /**
         * @param mixed $agencyOne
         */
        public function setAgencyOne($agencyOne)
        {
            $this->agencyOne = $agencyOne;
        }

        /**
         * @return mixed
         */
        public function getAccountNameOne()
        {
            return $this->accountNameOne;
        }

        /**
         * @param mixed $accountNameOne
         */
        public function setAccountNameOne($accountNameOne)
        {
            $this->accountNameOne = $accountNameOne;
        }

        /**
         * @return mixed
         */
        public function getAccountNumberOne()
        {
            return $this->accountNumberOne;
        }

        /**
         * @param mixed $accountNumberOne
         */
        public function setAccountNumberOne($accountNumberOne)
        {
            $this->accountNumberOne = $accountNumberOne;
        }

        /**
         * @return mixed
         */
        public function getOperationOne()
        {
            return $this->operationOne;
        }

        /**
         * @param mixed $operationOne
         */
        public function setOperationOne($operationOne)
        {
            $this->operationOne = $operationOne;
        }

        /**
         * @return mixed
         */
        public function getCpfOne()
        {
            return $this->cpfOne;
        }

        /**
         * @param mixed $cpfOne
         */
        public function setCpfOne($cpfOne)
        {
            $this->cpfOne = $cpfOne;
        }

        /**
         * @return mixed
         */
        public function getAccountTypeOne()
        {
            return $this->accountTypeOne;
        }

        /**
         * @param mixed $accountTypeOne
         */
        public function setAccountTypeOne($accountTypeOne)
        {
            $this->accountTypeOne = $accountTypeOne;
        }



        public function getTicketCount()
        {
            return $this->ticketCount;
        }


        public function setTicketCount($ticketCount)
        {
            $this->ticketCount = $ticketCount;
        }


        public function getCourtCount()
        {
            return $this->courtCount;
        }

        public function setCourtCount($courtCount)
        {
            $this->courtCount = $courtCount;
        }















	    
	    



	    public function getPurse() {
	        return $this->purse;
	    }

	    public function setPurse($purse) {
	        $this->purse = $purse;
	    }

	    public function getStatus() {
	        return $this->status;
	    }

	    public function setStatus($status) {
	        $this->status = $status;
	    }

	    public function getRole() {
	        return $this->role;
	    }

	    public function setRole($role = null) {
	        $this->role = $role;
	    }

	    public function getRoles() {
	        return [$this->getRole()];
	    }

	    public function getReferrer() {
	        return $this->referrer;
	    }

	    public function setReferrer($referrer) {
	        $this->referrer = $referrer;
	    }

		public function getReferrerUsername() {
			return $this->referrerUsername;
		}

		public function setReferrerUsername($referrerUsername) {
			$this->referrerUsername = $referrerUsername;
		}

	    public function getCreated() {
	        return $this->created;
	    }

	    public function setCreated($created) {
	        $this->created = $created;
	    }

	    public function getActivationKey() {
	        return $this->activationKey;
	    }

	    public function setActivationKey($activationKey) {
	        $this->activationKey = $activationKey;
	    }

	    public function getNotifications() {
	        return $this->notifications;
	    }

	    public function setNotifications($notifications) {
	        $this->notifications = $notifications;
	    }
        public function getNotificationCount()
        {
            return $this->notificationCount;
        }


        public function setNotificationCount($notificationCount)
        {
            $this->notificationCount = $notificationCount;
        }
	    public function getSalt() {
	        return null;
	    }

	    public function getFormattedPurse() {
	    	if($this->purse == 0) {
	    		return number_format($this->purse,2);
	    	} else {
	    		return number_format($this->purse);
	    	}
	    }

	    public function getPacks() {
	    	return $this->packs;
	    }

	    public function setPacks($packs) {
	    	$this->packs = $packs;
	    }

	    public function getReferralsCount() {
	    	return $this->referralsCount;
	    }

	    public function setReferralsCount($referralsCount) {
	    	$this->referralsCount = $referralsCount;
	    }

	    public function getSupportCount() {
	    	return $this->supportCount;
	    }

	    public function setSupportCount($supportCount) {
	    	$this->supportCount = $supportCount;
	    }

	    public function getJuryCount() {
	    	return $this->juryCount;
	    }

	    public function setJuryCount($juryCount) {
	    	$this->juryCount = $juryCount;
	    }

	    public function getMenuMail() {
	    	return $this->menuMail;
	    }

	    public function setMenuMail($menuMail) {
	    	$this->menuMail = $menuMail;
	    }

	    public function getMailCount() {
	    	return $this->mailCount;
	    }

	    public function setMailCount($mailCount) {
	    	$this->mailCount = $mailCount;
	    }

	    public function getDbGranted() {
	    	return $this->dbGranted;
	    }

	    public function setDbGranted($dbGranted) {
	    	$this->dbGranted = $dbGranted;
		}
		

		public function getWithdrawalRequest() {
	    	return $this->withdrawalRequest;
	    }

	    public function setWithdrawalRequest($withdrawalRequest) {
	    	$this->withdrawalRequest = $withdrawalRequest;
		}

		public function getWithdrawalRequestApproved() {
			return $this->withdrawalRequestApproved;
		}

		public function setWithdrawalRequestApproved($withdrawalRequestApproved) {
	    	$this->withdrawalRequestApproved = $withdrawalRequestApproved;
		}

		public function getPaymentMade() {
	    	return $this->paymentMade;
	    }

	    public function setPaymentMade($paymentMade) {
	    	$this->paymentMade = $paymentMade;
		}

		public function getAmountPaid() {
	    	return $this->amountPaid;
	    }

	    public function setAmountPaid($amountPaid) {
	    	$this->amountPaid = $amountPaid;
		}

		public function getMobileMoneyName() {
	    	return $this->mobileMoneyName;
	    }

	    public function setMobileMoneyName($mobileMoneyName) {
	    	$this->mobileMoneyName = $mobileMoneyName;
		}

		public function getMobileMoneyNumber() {
	    	return $this->mobileMoneyNumber;
	    }

	    public function setMobileMoneyNumber($mobileMoneyNumber) {
	    	$this->mobileMoneyNumber = $mobileMoneyNumber;
		}

		public function getAmountRequested() {
	    	return $this->amountRequested;
	    }

	    public function setAmountRequested($amountRequested) {
	    	$this->amountRequested = $amountRequested;
	    }
	}