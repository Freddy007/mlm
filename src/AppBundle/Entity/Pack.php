<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pack
 *
 * @ORM\Table(name="pack")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PackRepository")
 *
 */


class Pack
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

     /**
     * @var string
     *
     * @ORM\Column(name="work_station", type="string")
     */
    private $workStation;

    
     /**
     * @var string
     *
     * @ORM\Column(name="work_session", type="string")
     */
    private $workSession;

     /**
     * @var string
     *
     * @ORM\Column(name="meeting_room", type="string")
     */
    private $meetingRoom;
    /**
     * @var int
     *
     * @ORM\Column(name="feeders", type="integer")
     */
    private $feeders;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    // Doctrine Relational Data
    /**
     *
     * @ORM\OneToMany(targetEntity="PackSubscription", mappedBy="pack", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $packSubscriptions;

    private $status;

    private $userFeeder;

    private $ROI;

     /**
	 * @var boolean
	 *
	 * @ORM\Column(name="pv", type="integer")
	 */
	private $pv;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="workstation_package", type="boolean")
	 */
	private $workstationPackage;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Pack
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Pack
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    
      /**
     * Get workstation
     *
     * @return string
     */
    public function getWorkStation()
    {
        return $this->workStation;
    }

    /**
     * Set workstation
     *
     *
     * 
     */
    public function setWorkStation($workStation)
    {
         $this->workStation = $workStation;
    }

      
      /**
     * Get WorkSession
     *
     * @return string
     */
    public function getWorkSession()
    {
        return $this->workSession;
    }

    /**
     * Set workSession
     *
     *
     * 
     */
    public function setWorkSession($workSession)
    {
         $this->workSession = $workSession;
    }


     /**
     * Get meetingRoom
     *
     * @return string
     */
    public function getMeetingRoom()
    {
        return $this->meetingRoom;
    }

    /**
     * Set meetingRoom
     *
     *
     * 
     */
    public function setMeetingRoom($meetingRoom)
    {
         $this->meetingRoom = $meetingRoom;
    }
    

    public function getFormattedAmount()
    {
        return number_format($this->amount, 0, '.', ',');
    }

    /**
     * Set feeders
     *
     * @param integer $feeders
     *
     * @return Pack
     */
    public function setFeeders($feeders)
    {
        $this->feeders = $feeders;

        return $this;
    }

    /**
     * Get feeders
     *
     * @return int
     */
    public function getFeeders()
    {
        return $this->feeders;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Pack
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getPackSubscriptions()
    {
        return $this->packSubscriptions;
    }

    /**
     * @param mixed $packSubscriptions
     */
    public function setPackSubscriptions($packSubscriptions)
    {
        $this->packSubscriptions = $packSubscriptions;
    }




    /**
     * Set status
     *
     * @param string $status
     *
     * @return Plan
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set userFeeder
     *
     * @param string $userFeeder
     *
     * @return Plan
     */
    public function setUserFeeder($userFeeder)
    {
        $this->userFeeder = $userFeeder;

        return $this;
    }

    /**
     * Get userFeeder
     *
     * @return string
     */
    public function getUserFeeder()
    {
        return $this->userFeeder;
    }

    /**
     * Set ROI
     *
     * @param integer $ROI
     *
     * @return Plan
     */
    public function setROI($ROI)
    {
        $this->ROI = $ROI;

        return $this;
    }

    /**
     * Get ROI
     *
     * @return string
     */
    public function getROI()
    {
        if($this->ROI != null) {
            return number_format($this->amount * $this->ROI);
        } else {
            return number_format($this->amount * $this->feeders);
        }
    }

    public function setPV($pv)
	{
		$this->recommitRequired = $pv;
	}

	public function getPV()
	{
		return $this->pv;
    }

    public function getWorkstationPackage()
	{
		return $this->workstationPackage;
    }
}

