<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JuryMessage
 *
 * @ORM\Table(name="jury_message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JuryMessageRepository")
 */
class JuryMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="jury_id", type="integer")
     */
    private $juryId;

    /**
     * @var int
     *
     * @ORM\Column(name="sender_id", type="integer")
     */
    private $senderId;

    private $sender;

    /**
     * @var string
     *
     * @ORM\Column(name="sender_type", type="string", length=40)
     */
    private $senderType;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="attachment", type="string", length=255, nullable=true)
     */
    private $attachment;

    private $attachmentFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set juryId
     *
     * @param integer $juryId
     *
     * @return JuryMessage
     */
    public function setJuryId($juryId)
    {
        $this->juryId = $juryId;

        return $this;
    }

    /**
     * Get juryId
     *
     * @return int
     */
    public function getJuryId()
    {
        return $this->juryId;
    }

    /**
     * Set senderId
     *
     * @param integer $senderId
     *
     * @return JuryMessage
     */
    public function setSenderId($senderId)
    {
        $this->senderId = $senderId;

        return $this;
    }

    /**
     * Get senderId
     *
     * @return int
     */
    public function getSenderId()
    {
        return $this->senderId;
    }

    /**
     * Set sender
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
    }

    /**
     * Get sender
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set senderType
     *
     * @param string $senderType
     *
     * @return JuryMessage
     */
    public function setSenderType($senderType)
    {
        $this->senderType = $senderType;

        return $this;
    }

    /**
     * Get senderType
     *
     * @return string
     */
    public function getSenderType()
    {
        return $this->senderType;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return JuryMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set attachment
     *
     * @param string $attachment
     *
     * @return JuryMessage
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * Get attachment
     *
     * @return string
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Set attachmentFile
     */
    public function setAttachmentFile($attachmentFile)
    {
        $this->attachmentFile = $attachmentFile;
    }

    /**
     * Get attachmentFile
     */
    public function getAttachmentFile()
    {
        return $this->attachmentFile;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return JuryMessage
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }
}

