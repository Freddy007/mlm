<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Pack")
     * @ORM\JoinColumn(name="pack", referencedColumnName="id")
     */
    private $pack;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="PackSubscription")
     * @ORM\JoinColumn(name="feeder_pack_subscription", referencedColumnName="id")
     */
    private $feederPackSubscription;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="feeder", referencedColumnName="id")
     */
    private $feeder;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="receiver", referencedColumnName="id")
     */
    private $receiver;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="withdrawal", type="boolean")
	 */
	private $withdrawal;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="approved", type="boolean")
	 */
	private $approved;

    /**
     * @var int
     *
     * @ORM\Column(name="receiver_share", type="integer")
     */
    private $receiverShare;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=40)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pack
     *
     * @param integer $pack
     *
     * @return Transaction
     */
    public function setPack($pack)
    {
        $this->pack = $pack;

        return $this;
    }

    /**
     * Get pack
     *
     * @return int
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * Set feederPackSubscription
     *
     * @param integer $feederPackSubscription
     *
     * @return Transaction
     */
    public function setFeederPackSubscription($feederPackSubscription)
    {
        $this->feederPackSubscription = $feederPackSubscription;

        return $this;
    }

    /**
     * Get feederPackSubscription
     *
     * @return int
     */
    public function getFeederPackSubscription()
    {
        return $this->feederPackSubscription;
    }

    /**
     * Set feeder
     *
     * @param integer $feeder
     *
     * @return Transaction
     */
    public function setFeeder($feeder)
    {
        $this->feeder = $feeder;

        return $this;
    }

    /**
     * Get feeder
     *
     * @return int
     */
    public function getFeeder()
    {
        return $this->feeder;
    }

    /**
     * Set receiver
     *
     * @param integer $receiver
     *
     * @return Transaction
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return int
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set receiverShare
     *
     * @param integer $receiverShare
     *
     * @return Transaction
     */
    public function setReceiverShare($receiverShare)
    {
        $this->receiverShare = $receiverShare;

        return $this;
    }

    /**
     * Get receiverShare
     *
     * @return int
     */
    public function getReceiverShare()
    {
        return $this->receiverShare;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Transaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Transaction
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get formattedAmount
     *
     * @return string
     */
    public function getFormattedAmount()
    {
        return number_format($this->amount);
    }

    /**
     * Get formattedShare
     *
     * @return string
     */
    public function getFormattedShare()
    {
        return number_format($this->receiverShare);
    }

	/**
	 * @return bool
	 */
	public function getWithdrawal() {
		return $this->withdrawal;
	}

	/**
	 * @param bool $withdrawal
	 */
	public function setWithdrawal( $withdrawal ) {
		$this->withdrawal = $withdrawal;
	}

	/**
	 * @return bool
	 */
	public function getApproved() {
		return $this->approved;
	}

	/**
	 * @param bool $approved
	 */
	public function setApproved( $approved ) {
		$this->approved = $approved;
	}


}

