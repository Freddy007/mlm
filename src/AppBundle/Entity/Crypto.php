<?php
//
//namespace AppBundle\Entity;
//
//use Doctrine\ORM\Mapping as ORM;
//use Symfony\Component\Security\Core\User\UserInterface;
//use Symfony\Component\Validator\Constraints as Assert;
//use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
//
///**
// * Admin
// * @ORM\Entity(repositoryClass="AppBundle\Repository\CryptoRepository")
// */
//class Admin
//{
//    /**
//     *
//     * @ORM\Column(type="integer")
//     * @ORM\Id
//     * @ORM\GeneratedValue(strategy="AUTO")
//     */
//    protected $id;
//
//    /**
//     * @ORM\Column(type="string", length=255)
//     */
//    protected $bitcoinApiKey;
//
//    /**
//     * @ORM\Column(type="string", length=255)
//     */
//    protected $litecoinApiKey;
//
//    /**
//     * @ORM\Column(type="string", length=255)
//     */
//    protected $dogecoinApiKey;
//
//    /**
//     * @ORM\Column(type="string", length=255)
//     */
//    protected $blockioSecretPin;
//
//    /**
//     * @ORM\Column(type="string", length=255)
//     */
//    protected $bitcoinConfirmCount;
//
//    /**
//     * @ORM\Column(type="string", length=255)
//     */
//    protected $litecoinConfirmCount;
//
//    /**
//     * @ORM\Column(type="string", length=255)
//     */
//    protected $dogecoinConfirmCount;
//
//}
//
