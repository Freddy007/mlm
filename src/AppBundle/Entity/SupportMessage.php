<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SupportMessage
 *
 * @ORM\Table(name="support_message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SupportMessageRepository")
 */
class SupportMessage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="support_id", type="integer")
     */
    private $supportId;

    /**
     * @var string
     *
     * @ORM\Column(name="sender", type="string", length=40)
     */
    private $sender;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="attachment", type="string", length=255, nullable=true)
     */
    private $attachment;

    private $attachmentFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set supportId
     *
     * @param integer $supportId
     *
     * @return SupportMessage
     */
    public function setSupportId($supportId)
    {
        $this->supportId = $supportId;

        return $this;
    }

    /**
     * Get supportId
     *
     * @return int
     */
    public function getSupportId()
    {
        return $this->supportId;
    }

    /**
     * Set sender
     *
     * @param string $sender
     *
     * @return SupportMessage
     */
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return SupportMessage
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set attachment
     *
     * @param string $attachment
     *
     * @return SupportMessage
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * Get attachment
     *
     * @return string
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Set attachmentFile
     */
    public function setAttachmentFile($attachmentFile)
    {
        $this->attachmentFile = $attachmentFile;
    }

    /**
     * Get attachmentFile
     */
    public function getAttachmentFile()
    {
        return $this->attachmentFile;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return SupportMessage
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }
}

