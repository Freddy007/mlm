<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Feeder
 *
 * @ORM\Table(name="feeder")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FeederRepository")
 */
class Feeder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="feeder", referencedColumnName="id")
     */
    private $feeder;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="receiver", referencedColumnName="id")
     */
    private $receiver;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Pack")
     * @ORM\JoinColumn(name="pack", referencedColumnName="id" , onDelete="CASCADE")
     */
    private $pack;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="PackSubscription")
     * @ORM\JoinColumn(name="feeder_pack_subscription", referencedColumnName="id")
     */
    private $feederPackSubscription;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="PackSubscription")
     * @ORM\JoinColumn(name="receiver_pack_subscription", referencedColumnName="id")
     */
    private $receiverPackSubscription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="feeder_time_start", type="datetime")
     */
    private $feederTimeStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="feeder_time_end", type="datetime")
     */
    private $feederTimeEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="feeder_time_extended", type="string", length=10, nullable=true)
     */
    private $feederTimeExtended;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_method", type="string", length=255, nullable=true)
     */
    private $paymentMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_bank_name", type="string", length=255, nullable=true)
     */
    private $paymentBankName;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_agency", type="string", length=255, nullable=true)
     */
    private $paymentAgency;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_operation", type="string", length=255, nullable=true)
     */
    private $paymentOperation;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_cpf", type="string", length=255, nullable=true)
     */
    private $paymentCpf;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_account_number", type="string", length=255, nullable=true)
     */
    private $paymentAccountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_account_name", type="string", length=255, nullable=true)
     */
    private $paymentAccountName;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_depositor_name", type="string", length=255, nullable=true)
     */
    private $paymentDepositorName;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_location", type="string", length=255, nullable=true)
     */
    private $paymentLocation;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_uploaded_file", type="string", length=255, nullable=true)
     */
    private $paymentUploadedFile;

    /**
     * Image
     */
    private $paymentImage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="receiver_time_start", type="datetime", nullable=true)
     */
    private $receiverTimeStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="receiver_time_end", type="datetime", nullable=true)
     */
    private $receiverTimeEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="scam_paid", type="string", nullable=true)
     */
    private $scamPaid;

    /**
     * @var int
     *
     * @ORM\Column(name="jury", type="integer", nullable=true)
     */
    private $jury;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set feeder
     *
     * @param integer $feeder
     *
     * @return Feeder
     */
    public function setFeeder($feeder)
    {
        $this->feeder = $feeder;

        return $this;
    }

    /**
     * Get feeder
     *
     * @return int
     */
    public function getFeeder()
    {
        return $this->feeder;
    }

    /**
     * Set receiver
     *
     * @param integer $receiver
     *
     * @return Feeder
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return int
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set pack
     *
     * @param integer $pack
     *
     * @return Feeder
     */
    public function setPack($pack)
    {
        $this->pack = $pack;

        return $this;
    }

    /**
     * Get pack
     *
     * @return int
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * Set feederPackSubscription
     *
     * @param integer $feederPackSubscription
     *
     * @return Feeder
     */
    public function setFeederPackSubscription($feederPackSubscription)
    {
        $this->feederPackSubscription = $feederPackSubscription;

        return $this;
    }

    /**
     * Get feederPackSubscription
     *
     * @return int
     */
    public function getFeederPackSubscription()
    {
        return $this->feederPackSubscription;
    }

    /**
     * Set receiverPackSubscription
     *
     * @param integer $receiverPackSubscription
     *
     * @return Feeder
     */
    public function setReceiverPackSubscription($receiverPackSubscription)
    {
        $this->receiverPackSubscription = $receiverPackSubscription;

        return $this;
    }

    /**
     * Get receiverPackSubscription
     *
     * @return int
     */
    public function getReceiverPackSubscription()
    {
        return $this->receiverPackSubscription;
    }

    /**
     * Set feederTimeStart
     *
     * @param \DateTime $feederTimeStart
     *
     * @return Feeder
     */
    public function setFeederTimeStart($feederTimeStart)
    {
        $this->feederTimeStart = $feederTimeStart;

        return $this;
    }

    /**
     * Get feederTimeStart
     *
     * @return \DateTime
     */
    public function getFeederTimeStart()
    {
        return $this->feederTimeStart;
    }

    /**
     * Set feederTimeEnd
     *
     * @param \DateTime $feederTimeEnd
     *
     * @return Feeder
     */
    public function setFeederTimeEnd($feederTimeEnd)
    {
        $this->feederTimeEnd = $feederTimeEnd;

        return $this;
    }

    /**
     * Get feederTimeEnd
     *
     * @return \DateTime
     */
    public function getFeederTimeEnd()
    {
        return $this->feederTimeEnd;
    }

    /**
     * Set feederTimeExtended
     *
     * @param string $feederTimeExtended
     *
     * @return Feeder
     */
    public function setFeederTimeExtended($feederTimeExtended)
    {
        $this->feederTimeExtended = $feederTimeExtended;

        return $this;
    }

    /**
     * Get feederTimeExtended
     *
     * @return string
     */
    public function getFeederTimeExtended()
    {
        return $this->feederTimeExtended;
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     *
     * @return Feeder
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set paymentBankName
     *
     * @param string $paymentBankName
     *
     * @return Feeder
     */
    public function setPaymentBankName($paymentBankName)
    {
        $this->paymentBankName = $paymentBankName;

        return $this;
    }

    /**
     * Get paymentBankName
     *
     * @return string
     */
    public function getPaymentBankName()
    {
        return $this->paymentBankName;
    }

    /**
     * Set paymentAccountNumber
     *
     * @param string $paymentAccountNumber
     *
     * @return Feeder
     */
    public function setPaymentAccountNumber($paymentAccountNumber)
    {
        $this->paymentAccountNumber = $paymentAccountNumber;

        return $this;
    }

    /**
     * Get paymentAccountNumber
     *
     * @return string
     */
    public function getPaymentAccountNumber()
    {
        return $this->paymentAccountNumber;
    }

    /**
     * Set paymentAccountName
     *
     * @param string $paymentAccountName
     *
     * @return Feeder
     */
    public function setPaymentAccountName($paymentAccountName)
    {
        $this->paymentAccountName = $paymentAccountName;

        return $this;
    }

    /**
     * Get paymentAccountName
     *
     * @return string
     */
    public function getPaymentAccountName()
    {
        return $this->paymentAccountName;
    }

    /**
     * Set paymentDepositorName
     *
     * @param string $paymentDepositorName
     *
     * @return Feeder
     */
    public function setPaymentDepositorName($paymentDepositorName)
    {
        $this->paymentDepositorName = $paymentDepositorName;

        return $this;
    }

    /**
     * Get paymentDepositorName
     *
     * @return string
     */
    public function getPaymentDepositorName()
    {
        return $this->paymentDepositorName;
    }

    /**
     * Set paymentLocation
     *
     * @param string $paymentLocation
     *
     * @return Feeder
     */
    public function setPaymentLocation($paymentLocation)
    {
        $this->paymentLocation = $paymentLocation;

        return $this;
    }

    /**
     * Get paymentLocation
     *
     * @return string
     */
    public function getPaymentLocation()
    {
        return $this->paymentLocation;
    }

    /**
     * @return string
     */
    public function getPaymentAgency()
    {
        return $this->paymentAgency;
    }

    /**
     * @param string $paymentAgency
     *
     * @return Feeder
     */
    public function setPaymentAgency($paymentAgency)
    {
        $this->paymentAgency = $paymentAgency;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentOperation()
    {
        return $this->paymentOperation;
    }

    /**
     * @param string $paymentOperation
     *
     * @return Feeder
     */
    public function setPaymentOperation($paymentOperation)
    {
        $this->paymentOperation = $paymentOperation;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentCpf()
    {
        return $this->paymentCpf;
    }

    /**
     * @param string $paymentCpf
     *
     * @return Feeder
     */
    public function setPaymentCpf($paymentCpf)
    {
        $this->paymentCpf = $paymentCpf;

        return $this;
    }






    /**
     * Set paymentUploadedFile
     *
     * @param string $paymentUploadedFile
     *
     * @return Feeder
     */
    public function setPaymentUploadedFile($paymentUploadedFile)
    {
        $this->paymentUploadedFile = $paymentUploadedFile;

        return $this;
    }

    /**
     * Get paymentUploadedFile
     *
     * @return string
     */
    public function getPaymentUploadedFile()
    {
        return $this->paymentUploadedFile;
    }

    public function setPaymentImage($paymentImage) {
        $this->paymentImage = $paymentImage;
    }

    public function getPaymentImage() {
        return $this->paymentImage;
    }

    /**
     * Set receiverTimeStart
     *
     * @param \DateTime $receiverTimeStart
     *
     * @return Feeder
     */
    public function setReceiverTimeStart($receiverTimeStart)
    {
        $this->receiverTimeStart = $receiverTimeStart;

        return $this;
    }

    /**
     * Get receiverTimeStart
     *
     * @return \DateTime
     */
    public function getReceiverTimeStart()
    {
        return $this->receiverTimeStart;
    }

    /**
     * Set receiverTimeEnd
     *
     * @param \DateTime $receiverTimeEnd
     *
     * @return Feeder
     */
    public function setReceiverTimeEnd($receiverTimeEnd)
    {
        $this->receiverTimeEnd = $receiverTimeEnd;

        return $this;
    }

    /**
     * Get receiverTimeEnd
     *
     * @return \DateTime
     */
    public function getReceiverTimeEnd()
    {
        return $this->receiverTimeEnd;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Feeder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set scamPaid
     *
     * @param integer $scamPaid
     *
     * @return Feeder
     */
    public function setScamPaid($scamPaid)
    {
        $this->scamPaid = $scamPaid;

        return $this;
    }

    /**
     * Get scamPaid
     *
     * @return int
     */
    public function getScamPaid()
    {
        return $this->scamPaid;
    }

    /**
     * Set jury
     *
     * @param integer $jury
     *
     * @return Feeder
     */
    public function setJury($jury)
    {
        $this->jury = $jury;

        return $this;
    }

    /**
     * Get jury
     *
     * @return int
     */
    public function getJury()
    {
        return $this->jury;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Feeder
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}

