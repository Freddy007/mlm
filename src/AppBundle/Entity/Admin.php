<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Admin
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdminRepository")
 */
class Admin
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=6)

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $sitelogo;

    /**
     * Image
     */
    protected $sitelogoimage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $sitelogolight;


    /**
     * Image
     */
    protected $sitelogolightimage;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $defaultmaleavatar;


    /**
     * Image
     */
    protected $defaultmaleavatarimage;

    /**
     *
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $envatoPurchasecode;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $sitename;



    /**
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $appcurrency;

    /**
     *
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $appcurrencysymbol;



    /**
     *
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $appcurrencyexchangerate;


    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $sitemail;


    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $siteurl;


    /**
     *
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $message;

    /**
     *
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $matchMode;

    /**
     *
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $appMode;


    public function getId()
    {
        return $this->id;
    }

    public function getSitelogo()
    {
        return $this->sitelogo;
    }

    public function setSitelogo($sitelogo)
    {
        $this->sitelogo = $sitelogo;
    }

    public function getSitelogoImage()
    {
        return $this->sitelogoimage;
    }

    public function setSitelogoImage($sitelogoimage)
    {
        $this->sitelogoimage = $sitelogoimage;
    }

    public function getSitelogolight()
    {
        return $this->sitelogolight;
    }

    public function setSitelogolight($sitelogolight)
    {
        $this->sitelogolight = $sitelogolight;
    }

    public function getSitelogolightImage()
    {
        return $this->sitelogolightimage;
    }

    public function setSitelogolightImage($sitelogolightimage)
    {
        $this->sitelogolightimage = $sitelogolightimage;
    }

    public function getDefaultmaleavatar()
    {
        return $this->defaultmaleavatar;
    }

    public function setDefaultmaleavatar($defaultmaleavatar)
    {
        $this->defaultmaleavatar = $defaultmaleavatar;
    }

    public function getDefaultmaleavatarimage()
    {
        return $this->defaultmaleavatarimage;
    }

    public function setDefaultmaleavatarimage($defaultmaleavatarimage)
    {
        $this->defaultmaleavatarimage = $defaultmaleavatarimage;
    }

    public function getEnvatoPurchasecode()
    {
        return $this->envatoPurchasecode;
    }

    public function setEnvatoPurchasecode($envatoPurchasecode)
    {
        $this->envatoPurchasecode = $envatoPurchasecode;
    }

    public function getSitename()
    {
        return $this->sitename;
    }

    public function setSitename($sitename)
    {
        $this->sitename = $sitename;

        return $this;
    }

    public function getAppcurrency()
    {
        return $this->appcurrency;
    }
    public function setAppcurrency($appcurrency)
    {
        $this->appcurrency = $appcurrency;
    }
    public function getAppcurrencysymbol()
    {
        return $this->appcurrencysymbol;
    }
    public function setAppcurrencysymbol($appcurrencysymbol)
    {
        $this->appcurrencysymbol = $appcurrencysymbol;
    }
    public function getAppcurrencyexchangerate()
    {
        return $this->appcurrencyexchangerate;
    }
    public function setAppcurrencyexchangerate($appcurrencyexchangerate)
    {
        $this->appcurrencyexchangerate = $appcurrencyexchangerate;
    }






    public function getSitemail()
    {
        return $this->sitemail;
    }

    public function setSitemail($sitemail)
    {
        $this->sitemail = $sitemail;
    }


    public function getSiteurl()
    {
        return $this->siteurl;
    }

    public function setSiteurl($siteurl)
    {
        $this->siteurl = $siteurl;
    }





    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }
    public function getMessage()
    {
        return $this->message;
    }
    public function setMatchMode($matchMode)
    {
        $this->matchMode = $matchMode;

        return $this;
    }
    public function getMatchMode()
    {
        return $this->matchMode;
    }
    public function setAppMode($appMode)
    {
        $this->appMode = $appMode;

        return $this;
    }
    public function getAppMode()
    {
        return $this->appMode;
    }

}

