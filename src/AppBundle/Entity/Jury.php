<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Jury
 *
 * @ORM\Table(name="jury")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JuryRepository")
 */
class Jury
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="feeder_id", type="integer")
     */
    private $feederId;

    private $feeder;

    /**
     * @var int
     *
     * @ORM\Column(name="receiver_id", type="integer")
     */
    private $receiverId;

    private $receiver;

    /**
     * @var int
     *
     * @ORM\Column(name="feeder_entry_id", type="integer")
     */
    private $feederEntryId;

    /**
     * @var int
     *
     * @ORM\Column(name="pack_subscription_id", type="integer")
     */
    private $packSubscriptionId;

    /**
     * @var int
     *
     * @ORM\Column(name="pack_id", type="integer")
     */
    private $packId;

    /**
     * @var string
     *
     * @ORM\Column(name="verdict", type="text")
     */
    private $verdict;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=40)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set feederId
     *
     * @param integer $feederId
     *
     * @return Jury
     */
    public function setFeederId($feederId)
    {
        $this->feederId = $feederId;

        return $this;
    }

    /**
     * Get feederId
     *
     * @return int
     */
    public function getFeederId()
    {
        return $this->feederId;
    }

    /**
     * Set feeder
     */
    public function setFeeder($feeder)
    {
        $this->feeder = $feeder;
    }

    /**
     * Get feeder
     */
    public function getFeeder()
    {
        return $this->feeder;
    }

    /**
     * Set receiverId
     *
     * @param integer $receiverId
     *
     * @return Jury
     */
    public function setReceiverId($receiverId)
    {
        $this->receiverId = $receiverId;

        return $this;
    }

    /**
     * Get receiverId
     *
     * @return int
     */
    public function getReceiverId()
    {
        return $this->receiverId;
    }

    /**
     * Set receiver
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * Get receiver
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set feederEntryId
     *
     * @param integer $feederEntryId
     *
     * @return Jury
     */
    public function setFeederEntryId($feederEntryId)
    {
        $this->feederEntryId = $feederEntryId;

        return $this;
    }

    /**
     * Get feederEntryId
     *
     * @return int
     */
    public function getFeederEntryId()
    {
        return $this->feederEntryId;
    }

    /**
     * Set packSubscriptionId
     *
     * @param integer $packSubscriptionId
     *
     * @return Jury
     */
    public function setPackSubscriptionId($packSubscriptionId)
    {
        $this->packSubscriptionId = $packSubscriptionId;

        return $this;
    }

    /**
     * Get packSubscriptionId
     *
     * @return int
     */
    public function getPackSubscriptionId()
    {
        return $this->packSubscriptionId;
    }

    /**
     * Set packId
     *
     * @param integer $packId
     *
     * @return Jury
     */
    public function setPackId($packId)
    {
        $this->packId = $packId;

        return $this;
    }

    /**
     * Get packId
     *
     * @return int
     */
    public function getPackId()
    {
        return $this->packId;
    }

    /**
     * Set verdict
     *
     * @param string $verdict
     *
     * @return Jury
     */
    public function setVerdict($verdict)
    {
        $this->verdict = $verdict;

        return $this;
    }

    /**
     * Get verdict
     *
     * @return string
     */
    public function getVerdict()
    {
        return $this->verdict;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Jury
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Jury
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }
}

