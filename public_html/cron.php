<?php
	namespace Cron;


	define('TIMEZONE', 'Africa/Accra');
	date_default_timezone_set(TIMEZONE);

	class Db {
		// Neutralizes database resource
		public $conn = FALSE;
		// Connects to the database
		// Stores connection resource in a variable
		// Throws exception if error occurs
		public function __construct() {
		
			$now = new \DateTime();
			$mins = $now->getOffset() / 60;
			$sgn = ($mins < 0 ? -1 : 1);
			$mins = abs($mins);
			$hrs = floor($mins / 60);
			$mins -= $hrs * 60;
			$offset = sprintf('%+d:%02d', $hrs*$sgn, $mins);
			
			$result = new \PDO("mysql:host=localhost;dbname=cashkito_god_storage", 'cashkito_god', 'Cash.Kito.Money.123');
			if(!$result) {
				throw new \Exception('Could not connect to the database server');
			} else {
				$this->conn = $result;
				$this->conn->exec("SET time_zone='$offset';");
			}
		}
		// Method For Sanitizing SQL
		public function sanitize($var) {
			return $this->conn->quote($var);
		}
	}

	class CronAction extends Db {

		public function blockUnpaidUsers() {

			if($this->conn) {

				// Select all Unpaid Feeders
				$unpaidFeedersSql = "SELECT * FROM feeder WHERE status = 'not_paid' AND feeder_time_end < NOW()";
				$unpaidFeedersStmt = $this->conn->prepare($unpaidFeedersSql);

				// Cancel Feeder Entry
				$cancelFeederEntrySql = "UPDATE feeder SET status = 'cancelled' WHERE id = ?";
				$cancelFeederEntryStmt = $this->conn->prepare($cancelFeederEntrySql);

				// Credit Receiver Pack Subscription's Feeder Counter
				$creditReceiverPackSubscriptionFeederCounterSql = "UPDATE pack_subscription SET feeder_counter = feeder_counter + 1 WHERE id = ?";
				$creditReceiverPackSubscriptionFeederCounterStmt = $this->conn->prepare($creditReceiverPackSubscriptionFeederCounterSql);
				
				// Get Receiver Pack Subscription
				$getReceiverPackSubscriptionSql = "SELECT * FROM pack_subscription WHERE id = ?";
				$getReceiverPackSubscriptionStmt = $this->conn->prepare($getReceiverPackSubscriptionSql);

				// Check Feeder Subscriptions
				$checkFeederSubscriptionsSql = "SELECT COUNT(*) AS count FROM pack_subscription WHERE user = ? AND status = 'active'";
				$checkFeederSubscriptionsStmt = $this->conn->prepare($checkFeederSubscriptionsSql);

				// Block Feeder
				$blockFeederSql = "UPDATE user SET status = 'blocked' WHERE id = ?";
				$blockFeederStmt = $this->conn->prepare($blockFeederSql);

				// Cancel Feeder PackSubscription
				$cancelFeederPackSubscriptionSql = "UPDATE pack_subscription SET status = 'cancelled' WHERE id = ?";
				$cancelFeederPackSubscriptionStmt = $this->conn->prepare($cancelFeederPackSubscriptionSql);

				// Execute Select all Unpaid Downlines
				$unpaidFeedersStmt->execute();

				// Start blocking Them
				while($row = $unpaidFeedersStmt->fetch(\PDO::FETCH_OBJ)) {

					// Cancel Feeder Entry
					$cancelFeederEntryStmt->execute(array($row->id));
					
					// Get Receiver Pack Subscription
					$getReceiverPackSubscriptionStmt->execute(array($row->receiver_pack_subscription));
					$receiverPackSubscription = $getReceiverPackSubscriptionStmt->fetch(\PDO::FETCH_OBJ);

					if($receiverPackSubscription->user_type == 'user') {
						// Credit Receiver's Pack Subscription's Feeder Counter
						$creditReceiverPackSubscriptionFeederCounterStmt->execute(array($row->receiver_pack_subscription));
					}

					// Cancel Feeder PackSubscription
					$cancelFeederPackSubscriptionStmt->execute(array($row->feeder_pack_subscription));

					// Check if feeder has other active subscriptions
					$checkFeederSubscriptionsStmt->execute(array($row->feeder));
					$feederActiveSubscriptions = $checkFeederSubscriptionsStmt->fetch(\PDO::FETCH_OBJ)->count;

					if($feederActiveSubscriptions == 0) {
						// Block Feeder
						$blockFeederStmt->execute(array($row->feeder));
					}
				}

			}

		}

		public function moveCasesToJury() {

			if($this->conn) {

				// Select all potential jury cases
				$juryCasesSql = "SELECT * FROM feeder WHERE (status = 'payment_made' AND receiver_time_end < NOW()) OR (status = 'scam' AND feeder_time_end < NOW() AND scam_paid = 'scam_paid') OR (status = 'scam' AND receiver_time_end < NOW() AND scam_paid = 'scam_paid')";
				$juryCasesStmt = $this->conn->prepare($juryCasesSql);

				// Add new jury case entry
				$newJurySql = "INSERT INTO jury VALUES('', ?, ?, ?, ?, ?, 'none', 'open', NOW())";
				$newJuryStmt = $this->conn->prepare($newJurySql);

				// Move to jury
				$moveToJurySql = "UPDATE feeder SET status = 'jury', jury = ? WHERE id = ?";
				$moveToJuryStmt = $this->conn->prepare($moveToJurySql);
				
				// Start moving to court
				$juryCasesStmt->execute();
				
				
				while($row = $juryCasesStmt->fetch(\PDO::FETCH_OBJ)) {

					// Add court Entry
					$newJuryStmt->execute(array($row->feeder, $row->receiver, $row->id, $row->receiver_pack_subscription, $row->pack));

					// Move case to jury
					$moveToJuryStmt->execute(array($this->conn->lastInsertId(),$row->id));
				}
			}

		}

	}

	$cronAction = new CronAction;
	$cronAction->blockUnpaidUsers();
	$cronAction->moveCasesToJury();